## # ## GETTING STARTED ##
---------------------
The ROS packages associated with Cool1000 are:

cool1000_controller  : The dynamixel package for interfacing with the Cool1000.

cool1000_description : Contains URDF and stl of Cool1000.

cool1000_gazebo      : Contains the Gazebo config files.

## INSTALLATION ##
------------
Install the ROS dynamixel drivers

    sudo apt-get install ros-hydro-dynamixel-*

Clone the Repo into your workspace and make using catkin_make

## CONTROLLING THE HARDWARE ##
------------------------

To control hardware launch

    roslaunch cool1000_controller cool1000_hardware.launch

## GAZEBO SIMULATION ##

Ubuntu  12.04 
Ros Hydro

### INSTALLATION ###
Follow instructions in below link for installing standalone version of gazebo.
 
Link: http://gazebosim.org/tutorials?tut=install&ver=1.9&cat=get_started
 
Here we installed Pre-compiled binaries of Gazebo 1.9.6. Install ROS dependecies as well.


### INSTALLING GAZEBOROSPKGS ###

Install pre-built binaries of rosgazebo packages for connecting ros with gazebo. Follow the below link:
http://gazebosim.org/tutorials?tut=ros_installing&cat=connect_ros

Follow instructions in the tutorial for checking each of the the installation stages.

If whole installation procedures are right and working fine below command will load an empty world in gazebo


```
#!python

roslaunch gazebo_ros empty_world.launch
```


### LOADING COOL ARM URDF MODEL IN GAZEBO ###

For loading URDF model of cool1000, open the terminal and follow the below steps
1.Run 
```
#!python

roscore
```

2.Open another tab and enter below command
  

```
#!python

rosrun gazebo_ros spawn_model -file `rospack find cool1000_description`/urdf/cool1000.urdf -urdf -x 0 -y 0 -z 0 -model cool1000
```


3.Open another tab and enter


```
#!python

roslaunch gazebo_ros empty_world.launch
```


Note: We need to go to cool1000_ros_experimental package before following the above steps and also need to source each terminals using the below command:


```
#!python

source devel/setup.bash
```
