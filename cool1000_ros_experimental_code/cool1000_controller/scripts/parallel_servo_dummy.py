#!/usr/bin/env python

__version__ = "0.1"

'''
This is a python script to start parallel servos 

'''


import rospy
import roslib
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64

parallel_servo_1 = rospy.Publisher('/dummy_joint8_controller/command',Float64)
parallel_servo_2 = rospy.Publisher('/dummy_joint2_controller/command',Float64)

def callback(data):
    #print "callback1"
    #print data.current_pos
    curr_pos = data.data
    parallel_servo_1.publish(curr_pos)
    parallel_servo_2.publish(curr_pos)

def listener():
    rospy.init_node('parallel_servo_starter', anonymous=True)
    print "parallel_servo_starter node started"
    rospy.Subscriber("/dual_servo_controller/command",Float64, callback)
    rospy.spin()


if __name__ == '__main__':
    listener()
