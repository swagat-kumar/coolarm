#!/usr/bin/env python

import roslib
import time
import rospy
from std_msgs.msg import Float64

joint_names = ( 'joint1_controller',
		'dual_servo_controller',
		'joint3_controller',
		'joint4_controller',
		'joint5_controller',
		'joint6_controller',
                'joint7_controller',
		'gripper_open_controller_1')
               
joint_commands = (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.95)


if __name__ == '__main__':

    pubs = [rospy.Publisher(name + '/command', Float64) for name in joint_names]
    rospy.init_node('make_zero_pose', anonymous=True)
    rospy.loginfo("ROS pose zero dummy Python Node")    
    
    for i in range(len(pubs)):
        time.sleep(.2)
        print "Sending command"
        pubs[i].publish(joint_commands[i])
        
    rospy.sleep(5)
