#!/usr/bin/env python

__version__ = "0.1"
__author__ = "unais@asimovrobotics.com"

'''
This is a python script to start parallel servos of each arm

'''


import rospy
import roslib
from dynamixel_msgs.msg import JointState
from std_msgs.msg import Float64

parallel_servo = rospy.Publisher('/joint8_controller/command',Float64)

def arm1_callback(data):
    #print "callback1"
    #print data.current_pos
    curr_pos = data.current_pos
    parallel_servo.publish(curr_pos)

def listener():
    rospy.init_node('parallel_servo_starter', anonymous=True)
    print "parallel_servo_starter node started"
    rospy.Subscriber("/joint2_controller/state",JointState, arm1_callback)
    rospy.spin()


if __name__ == '__main__':
    listener()
