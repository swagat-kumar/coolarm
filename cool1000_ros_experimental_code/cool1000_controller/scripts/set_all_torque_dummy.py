#!/usr/bin/env python
import roslib
import rospy
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState
from dynamixel_controllers.srv import TorqueEnable
import time
import sys

joint_names = ( 'joint1_controller',
		'dummy_joint2_controller',
                'dummy_joint8_controller',				
		'joint3_controller',
		'joint4_controller',
		'joint5_controller',
		'joint6_controller',
                'joint7_controller',
		'gripper_open_controller_1')



if __name__ == '__main__':

    rospy.init_node("set_all_torque_dummy")
    rospy.loginfo("ROS Set all torque dummy Python Node")
	
    ivalue = int(rospy.get_param('~set_torque','0'))

    #if len(sys.argv) != 2:
    #    print 'Usage: set_all_torque.py <0,1>\n'
    #    print 'Controls if the motors are activated and have torque\n'
    #    print '1 is True, 0 is False\n'
    #    sys.exit(1)

#    print 'Setting torque to '+sys.argv[1]

    if len(sys.argv) == 2:
        print 'Setting from command line argument\n'
        ivalue  = int(sys.argv[1])
        
    print 'Setting torque to '+str(ivalue)
    
    for joint_name in joint_names:
        print 'Looking for service ', joint_name
        rospy.wait_for_service('/'+joint_name+'/torque_enable')

        try:
            torquer = rospy.ServiceProxy('/'+joint_name+'/torque_enable', TorqueEnable)
            response = torquer(ivalue)
            print 'Response from '+joint_name+':', response
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
           
    rospy.sleep(1)

