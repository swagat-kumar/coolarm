<?xml version="1.0"?>

<!-- Robot Description format of COOL1000 arm -->
<!-- 
Documentation:

URDF
http://wiki.ros.org/urdf

Gazebo installation
http://gazebosim.org/tutorials?tut=ros_installing&cat=connect_ros
-->
<robot name="cool1000">

	<!-- Used for fixing robot to Gazebo 'base_link' -->
	<link name="world"/>
	<joint name="fixed" type="fixed">
		<parent link="world"/>
		<child link="base_link"/>
	</joint>

	<!--Base-Link -->   
	<link name="base_link">
		<visual>
			<!-- origin xyz="-0.865 -1.62 0" rpy="0 0 0" /-->
			<origin xyz="-1.62 0.865 0" rpy="0 0 -1.57" />
			<geometry>
				<mesh filename="package://cool1000_description/meshes/Cool1000.dae" scale=".001 .001 .001"/>  
			</geometry>
			<material name="grey">
				<color rgba="0.819 0.8433 0.9294 1"/>
        		</material>
		</visual>
		<collision>
			<!-- origin xyz="-0.865 -1.62 0" rpy="0 0 0" /-->
			<origin xyz="-1.62 0.865 0" rpy="0 0 -1.57" />
			<geometry>
				<mesh filename="package://cool1000_description/meshes/Cool1000.dae" scale=".001 .001 .001"/> 
			</geometry>
		</collision>
		<inertial>
			<origin rpy="0 0 0" xyz="0 0 0"/>
			<mass value="1.0"/>
			<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
		</inertial>
	</link>
	
		<!-- Link1 -->
	<link name="link1">
		<visual>
			<!-- origin xyz="-0.322 -0.805 -0.058" rpy="0 0 0" /-->
			<origin xyz="-0.805 0.322 -0.058" rpy="0 0 -1.57" />
			<geometry>
				<mesh filename="package://cool1000_description/meshes/link1.dae"/>  
			</geometry>	
			<material name="grey">
				<color rgba="0.819 0.8433 0.9294 1"/>
			</material>
		</visual>
		<collision>
			<!-- origin xyz="-0.322 -0.805 -0.058" rpy="0 0 0" /-->
			<origin xyz="-0.805 0.322 -0.058" rpy="0 0 -1.57" />
			<geometry>
     	  			<mesh filename="package://cool1000_description/meshes/link1.dae"/>  
			</geometry>
     		</collision>
     		<inertial>
        		<origin rpy="0 0 0" xyz="0 0 0"/>
        		<mass value="1.0"/>
        		<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
      		</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 1 -->
   	<joint name="joint1" type="revolute">
		<axis xyz="0 0 1" /> 
		<limit effort="3.0" velocity="0.01" lower="-2.617" upper="2.617"/>
		<origin xyz="0 0 0.06" rpy="0 0 0" />
	   	<parent link="base_link" />
	  	<child link="link1" />
	        <dynamics damping="1000.0" friction="1.0"/>
	</joint>

		<!-- Link2 -->
  	<link name="link2">
   		<visual>
    			<!--origin xyz="-0.315 -0.805 -0.13" rpy="0 0 0" /-->
    			<origin xyz="-0.805 0.315 -0.13" rpy="0 0 -1.57" />
    			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link2.dae"/>  
    			</geometry>
    			<material name="grey">
      				<color rgba="0.819 0.8433 0.9294 1"/>
    			</material>
   		</visual>
   		<collision>
    			<!--origin xyz="-0.315 -0.805 -0.13" rpy="0 0 0" /-->
    			<origin xyz="-0.805 0.315 -0.13" rpy="0 0 -1.57" />
    			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link2.dae"/>  
    			</geometry>
   		</collision>
   		<inertial>
        		<origin rpy="0 0 0" xyz="0 0 0"/>
        		<mass value="1.0"/>
        		<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
   		</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 2 -->
	<joint name="joint2" type="revolute">
		<axis xyz="0 1 0" /> 
		<limit effort="3.0" velocity="0.01" lower="-1.91" upper="1.91"/>
		<origin xyz="0 0 0.069" rpy="0 0 0" />
   		<parent link="link1" />
  	 	<child link="link2" />
 		<dynamics damping="100.0" friction="1.0"/>
	</joint>

	<!-- Link3 -->
	<link name="link3">
 		<visual>
  			<!--origin xyz="-0.315 -0.805 -0.205" rpy="0 0 0" /-->
  			<origin xyz="-0.805 0.315 -0.205" rpy="0 0 -1.57" />
  			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link3.dae"/>  
  			</geometry>
  			<material name="grey">
     				<color rgba="0.819 0.8433 0.9294 1"/>
  			</material>
 		</visual>
 		<collision>
  			<!--origin xyz="-0.315 -0.805 -0.205" rpy="0 0 0" /-->
  			<origin xyz="-0.805 0.315 -0.205" rpy="0 0 -1.57" />
			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link3.dae"/>  
			</geometry>
	
		</collision>
		<inertial>
        		<origin rpy="0 0 0" xyz="0 0 0"/>
        		<mass value="1.0"/>
        		<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
   		</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 3 -->
	<joint name="joint3" type="revolute">
		<axis xyz="0 0 1" /> 
		<limit effort="3.0" velocity="0.01" lower="-2.617" upper="2.617"/>
		<origin xyz="0 0 0.073" rpy="0 0 0" />
   		<parent link="link2" />
  	 	<child link="link3" />
 		<dynamics damping="100.0" friction="1.0"/>
	</joint>
	
		<!-- Link4 -->
	<link name="link4">
 		<visual>
  			<!--origin xyz="-0.315 -0.805 -0.275" rpy="0 0 0" /-->
  			<origin xyz="-0.805 0.315 -0.275" rpy="0 0 -1.57" />
  			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link4.dae"/>  
  			</geometry>
  			<material name="grey">
     				<color rgba="0.819 0.8433 0.9294 1"/>
  			</material>
 		</visual>
 		<collision>
  			<!--origin xyz="-0.315 -0.805 -0.275" rpy="0 0 0" /-->
  			<origin xyz="-0.805 0.315 -0.275" rpy="0 0 -1.57" />
			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link4.dae"/>  
			</geometry>
	
		</collision>
		<inertial>
        		<origin rpy="0 0 0" xyz="0 0 0"/>
        		<mass value="1.0"/>
        		<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
   		</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 4 -->
	<joint name="joint4" type="revolute">
		<axis xyz="0 1 0" /> 
		<limit effort="3.0" velocity="0.01" lower="-1.91" upper="1.91"/>
		<origin xyz="0 0 0.071" rpy="0 0 0" />
   		<parent link="link3" />
  	 	<child link="link4" />
 		<dynamics damping="100.0" friction="1.0"/>
	</joint>

	<!-- Link5 -->
	<link name="link5">
 		<visual>
  			<!--origin xyz="-0.315 -0.805 -0.345" rpy="0 0 0" /-->
  			<origin xyz="-0.315 -0.805 -0.345" rpy="0 0 0" />
  			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link5.dae"/>  
  			</geometry>
  			<material name="grey">
     			<color rgba="0.819 0.8433 0.9294 1"/>
  		</material>
 	</visual>
 	<collision>
  		<!--origin xyz="-0.315 -0.805 -0.345" rpy="0 0 0" /-->
  			<origin xyz="-0.315 -0.805 -0.345" rpy="0 0 0" />
	<geometry>
     		<mesh filename="package://cool1000_description/meshes/link5.dae"/>  
	</geometry>	
	</collision>
	<inertial>
        	<origin rpy="0 0 0" xyz="0 0 0"/>
       		<mass value="1.0"/>
        	<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
   	</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 5 -->
	<joint name="joint5" type="revolute">
		<axis xyz="0 1 0" /> 
		<limit effort="3.0" velocity="0.01" lower="-1.91" upper="1.91"/>
		<!--origin xyz="0 0 0.071" rpy="0 0 0" /-->
		<origin xyz="0 0 0.071" rpy="0 0 1.57" />
   		<parent link="link4" />
  		<child link="link5" />
 		<dynamics damping="100.0" friction="1.0"/>
	</joint>

		<!-- Link6 -->
	<link name="link6">
 		<visual>
  			<!--origin xyz="-0.315 -0.805 -0.425" rpy="0 0 0" /-->
  			<origin xyz="-0.315 -0.805 -0.425" rpy="0 0 0" />
  			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link6.dae"/>  
  			</geometry>
  			<material name="grey">
     				<color rgba="0.819 0.8433 0.9294 1"/>
  			</material>
 		</visual>
 		<collision>
  			<!--origin xyz="-0.315 -0.805 -0.425" rpy="0 0 0" /-->
  			<origin xyz="-0.315 -0.805 -0.425" rpy="0 0 0" />
			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link6.dae"/>  
			</geometry>	
		</collision>
		<inertial>
        		<origin rpy="0 0 0" xyz="0 0 0"/>
        		<mass value="1.0"/>
        		<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
   		</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 6 -->
	<joint name="joint6" type="revolute">
		<axis xyz="1 0 0" /> 
		<limit effort="3.0" velocity="0.01" lower="-1.91" upper="1.91"/>
		<origin xyz="0 0 0.083" rpy="0 0 -3.14" />
   		<parent link="link5" />
  	 	<child link="link6" />
 		<dynamics damping="100.0" friction="1.0"/>
	</joint>

	<!-- Link7 -->
	<link name="link7">
 		<visual>
  			<!--origin xyz="-0.315 -0.805 -0.460" rpy="0 0 0" /-->
  			<origin xyz="0.315 0.805 -0.460" rpy="0 0 3.14" />
  			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link7.dae"/>  
  			</geometry>
  			<material name="grey">
     				<color rgba="0.819 0.8433 0.9294 1"/>
  			</material>
 		</visual>
 		<collision>
  			<!--origin xyz="-0.315 -0.805 -0.460" rpy="0 0 0" /-->
  			<origin xyz="0.315 0.805 -0.460" rpy="0 0 3.14" />
			<geometry>
     				<mesh filename="package://cool1000_description/meshes/link7.dae"/>  
			</geometry>
		</collision>
		<inertial>
        		<origin rpy="0 0 0" xyz="0 0 0"/>
        		<mass value="1.0"/>
        		<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
   		</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 7 -->
	<joint name="joint7" type="revolute">
		<axis xyz="0 0 1" /> 
		<limit effort="3.0" velocity="0.01" lower="-2.617" upper="2.617"/>
		<origin xyz="0 0 0.035" rpy="0 0 1.57" />
   		<parent link="link6" />
  	 	<child link="link7" />
 		<dynamics damping="100.0" friction="1.0"/>
	</joint>

	<!-- Dummy gripper base link and joint -->
	<!-- Added for locating EEF in MoveIt! -->
	<link name="gripper_base"/>
	<joint name="gripper_base_joint" type="fixed">
		<origin xyz="0.0 0.00 .11" rpy="0 0 0" />
		<parent link="link7"/>
		<child link="gripper_base"/>
	</joint>
		
		<!-- gripper_left -->
	<link name="gripper_left">
 		<visual>
  			<origin xyz=".33 0.85 -0.55" rpy="0 0 3.14" />
  			<geometry>
     				<mesh filename="package://cool1000_description/meshes/gripper_left.dae"/>  
  			</geometry>
  			<material name="grey">
     				<color rgba="0.819 0.8433 0.9294 1"/>
  			</material>
 		</visual>
 		<collision>
  			<origin xyz=".33 0.85 -0.55" rpy="0 0 3.14" />
			<geometry>
     				<mesh filename="package://cool1000_description/meshes/gripper_left.dae"/>  
			</geometry>
	
		</collision>
		<inertial>
        		<origin rpy="0 0 0" xyz="0 0 0"/>
        		<mass value="1.0"/>
        		<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
   		</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 8 -->
	<joint name="joint8" type="prismatic">
		<axis xyz="0 1 0" /> 
		<limit effort="3.0" velocity="0.01" lower="-0.17" upper="-0.15"/>
		<origin xyz="-0.0 0.13 .095" rpy="0 0 0" />
   		<parent link="link7" />
  		<child link="gripper_left" />
 		<dynamics damping="100.0" friction="1.0"/>
	</joint>

	<!-- gripper_right -->
	<link name="gripper_right">
 		<visual>
  			<origin xyz=".33 0.785 -0.55" rpy="0 0 3.14" />
  			<geometry>
     				<mesh filename="package://cool1000_description/meshes/gripper_right.dae"/>  
  			</geometry>
  			<material name="grey">
     				<color rgba="0.819 0.8433 0.9294 1"/>
  			</material>
 		</visual>
 		<collision>
  			<origin xyz=".33 0.785 -0.55" rpy="0 0 3.14" />
			<geometry>
     				<mesh filename="package://cool1000_description/meshes/gripper_right.dae"/>  
			</geometry>	
		</collision>
		<inertial>
        		<origin rpy="0 0 0" xyz="0 0 0"/>
        		<mass value="1.0"/>
        		<inertia ixx="1.0" ixy="0.0" ixz="0.0" iyy="100.0" iyz="0.0" izz="1.0"/>
   		</inertial>
	</link>

	<!--Joint Configuration -->
	<!-- joint 9 -->
	<joint name="joint9" type="prismatic">
		<axis xyz="0 1 0" /> 
		<limit effort="3.0" velocity="0.01" lower="-0.16" upper="-0.14"/>
		<origin xyz="-0.0 0.16 .095" rpy="0 0 0" />
   		<parent link="link7" />
  	 	<child link="gripper_right" />
 		<dynamics damping="100.0" friction="1.0"/>
	</joint>

</robot>
