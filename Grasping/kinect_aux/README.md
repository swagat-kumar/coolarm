kinect_aux
==========

[![Build Status](https://travis-ci.org/muhrix/kinect_aux.png?branch=hydro)](https://travis-ci.org/muhrix/kinect_aux)

This is a standalone ROS Hydro package (using catkin) which provides access to auxiliary features of the Microsoft kinect, allowing control of the tilt angle and LED status, as well as reading accelerometer data.

This package may be used in conjunction with openni_camera.

For more information: http://www.ros.org/wiki/kinect_aux
