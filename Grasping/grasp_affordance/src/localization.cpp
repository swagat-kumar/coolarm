#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <pthread.h>
#include "localize_handle/affordances.h"
#include "localize_handle/cylindrical_shell.h"
#include "localize_handle/callbacks.h"
#include "localize_handle/affordances.h"
#include "localize_handle/cylindrical_shell.h"
#include <grasp_affordance/HandlePose.h>

#define EIGEN_DONT_PARALLELIZE

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloudRGBA;

//#define locate_handles
#define run_robot_demo
//#define record_demo_video

std::vector<int> handle_indices;

std::vector< std::vector<cv::Point> > handle_points;
std::vector<int> selected_hdl_index;
std::vector<int> handle_num;
std::vector<int> handles;
std::vector<geometry_msgs::Point> principal_vector_points;
int flag_show_hdls = 0;
int flag_rr = 0;
int cur_hdl;
int pick_placed;

void *demo(void *aff_ptr)
{
  ros::NodeHandle node("robot_demo");

  Affordances *g_affordances = (Affordances *) aff_ptr;

  std::vector<std::vector<CylindricalShell> > g_handles;

  tf::StampedTransform g_transform;

  ros::Subscriber sub_kinect;
  ros::Subscriber sub_fr;
  ros::Subscriber sub_frr;
  ros::Subscriber sub_r_cnt;

  ros::Publisher pub_handle_data;
  ros::Publisher pub_fk;
  ros::Publisher pub_k_cnt;


  std_msgs::Int32 r_cnt;
  std_msgs::Int32 k_cnt;
  std_msgs::Float64MultiArray eigenArray;
  std_msgs::Float64MultiArray loc_eef;
  grasp_affordance::HandlePose handle_pose;

  r_cnt.data = 1;
  k_cnt.data = 0;

  std_msgs::Bool fk;
  std_msgs::Bool fr;
  std_msgs::Bool frr;

  fk.data = false;
  fr.data = true;

  //#####  Transformations  #####//
  tf::TransformListener transform_listener;


  transform_listener.waitForTransform("/robot_base" ,"/camera_depth_optical_frame", ros::Time(0),
                                      ros::Duration(3));


  transform_listener.lookupTransform("/robot_base", "/camera_depth_optical_frame", ros::Time(0), g_transform);


  //#####  Subsciptions  #####//

//  sub_kinect = node.subscribe<sensor_msgs::PointCloud2>("/camera/depth_registered/points", 1,
//    boost::bind(chatterCallback, _1, boost::ref(fk), boost::ref(g_transform), boost::ref(*g_affordances),
//                boost::ref(g_handles), boost::ref(eigenArray), boost::ref(selected_hdl_index),
//                boost::ref(flag_show_hdls), boost::ref(handle_points), boost::ref(principal_vector_points)));
  sub_kinect = node.subscribe<sensor_msgs::PointCloud2>("/camera/depth_registered/points", 1,
    boost::bind(chatterCallback, _1, boost::ref(fk), boost::ref(g_transform), boost::ref(*g_affordances),
                boost::ref(g_handles), boost::ref(selected_hdl_index),
                boost::ref(flag_show_hdls), boost::ref(handle_points), boost::ref(principal_vector_points)));

//  sub_fr = node.subscribe<std_msgs::Bool>("/flag_robot_reached", 1, boost::bind(robotFlagCallback, _1,
//                                                                                boost::ref(fr)));
  sub_fr = node.subscribe<std_msgs::Bool>("/flag/robot_arm/request_data", 1, boost::bind(robotFlagCallback, _1,
                                                                                         boost::ref(fr)));
  sub_frr = node.subscribe<std_msgs::Bool>("/flag/robot_arm/received_data", 1,
                                           boost::bind(robotReceivedFlagCallback, _1, boost::ref(frr)));
  sub_r_cnt = node.subscribe<std_msgs::Int32>("/data/count/robot_arm", 1,
                                              boost::bind(robotDataCountCallback, _1, boost::ref(r_cnt)));

  //#####  Publishers  #####//
//  pub_handle_data = node.advertise<std_msgs::Float64MultiArray>("/handle/data", 1);
  pub_fk = node.advertise<std_msgs::Bool>("/flag/kinect/transmitting_data", 1);
  pub_k_cnt = node.advertise<std_msgs::Int32>("/data/count/kinect", 1);
  pub_handle_data = node.advertise<grasp_affordance::HandlePose>("/handle/data", 1);

  // how often things are published
  ros::Rate rate(10);

  //##### Main loop for finding the handles and sending them as messages to robot #####//
  int count_hdls = 0;
  bool locate_new_handles;
//  std::cout << "Press \n1: To Continue\n0: To Exit" << std::endl;
  int c;
//  std::cin >> c;
  c = 1;
  if(c == 1)
      locate_new_handles = true;
  else
  {
      locate_new_handles = false;
      std::cout << "Exiting ...!!" << std::endl;
      ros::shutdown();
  }

  int hdl = 0;
  while(ros::ok())
  {

//    if(count_hdls == 0)
    if(locate_new_handles)
    {
      //##### Point Cloud is processed for handles #####//
      while(fr.data && !fk.data)
      {
        //After point cloud is obtained from kinect and processed, the flag 'fk' is set,
        ros::spinOnce();
      }

      count_hdls = g_handles.size();
      cur_hdl = 0;
      locate_new_handles = false;
      handle_num.resize(g_handles.size());
      for(int i = 0; i < g_handles.size(); i++)
        handle_num[i] = i+1;
    }

    std::cout << "No. of principal vectors: " << principal_vector_points.size()/4 << std::endl;
    std::cout << "\nENTER HANDLE NO. TO PICK\tOR PRESS 0 TO LOCATE NEW HANDLES: " << std::endl;
    do
    {
      std::cin >> hdl;
      if(hdl == 0)
      {
        std::cout << "Locating new handles\n" << std::endl;
        count_hdls = 0;
        handle_num.resize(0);
        locate_new_handles = true;
        principal_vector_points.resize(0);
        std::cout << locate_new_handles << std::endl;
        fr.data = true;
        fk.data = false;
      }
      else if(hdl > g_handles.size())
        std::cout << "Please enter a number between 1 to " << g_handles.size() << "\n: " << std::endl;
    }while(hdl > g_handles.size());

    if(hdl != 0)
    {
//      loc_eef.data.resize(3);
////      for(int i = 0; i < 3; i++)
////        loc_eef.data[i] = eigenArray.data[3*(hdl-1) + i];
//        loc_eef.data[0] = principal_vector_points[4*(hdl-1)].x;
//        loc_eef.data[1] = principal_vector_points[4*(hdl-1)].y;
//        loc_eef.data[2] = principal_vector_points[4*(hdl-1)].z;

        handle_pose.centroid.x = principal_vector_points[4*(hdl-1)].x;
        handle_pose.centroid.y = principal_vector_points[4*(hdl-1)].y;
        handle_pose.centroid.z = principal_vector_points[4*(hdl-1)].z;
        handle_pose.axis.x = principal_vector_points[4*(hdl-1)+1].x;
        handle_pose.axis.y = principal_vector_points[4*(hdl-1)+1].y;
        handle_pose.axis.z = principal_vector_points[4*(hdl-1)+1].z;
        handle_pose.normal.x = principal_vector_points[4*(hdl-1)+3].x;
        handle_pose.normal.y = principal_vector_points[4*(hdl-1)+3].y;
        handle_pose.normal.z = principal_vector_points[4*(hdl-1)+3].z;

      fk.data = true;

      ros::Rate rate(1);
      for(int i = 0; i < 3; i++)
        rate.sleep();
    }

    if(count_hdls!=0)
    {
      //Since robot would have received previous handle point the robot received flag 'frr' is reset
      frr.data = false;

      //##### Wait for the robot to receive #####//

      std::cout << "kinect sends count: " << k_cnt.data << std::endl;

      while(!frr.data)
      {
        pub_fk.publish(fk);
//        pub_handle_data.publish(loc_eef);
        pub_handle_data.publish(handle_pose);
        pub_k_cnt.publish(k_cnt);

//        Spin for 'frr' flag to be set
        ros::spinOnce();
      }

//      ##### Wait for robot to reach the target #####//
      std::cout << "kinect waits for robot to reach: " << k_cnt.data << std::endl;

      k_cnt.data = k_cnt.data + 1;

      while(!fr.data || (r_cnt.data != k_cnt.data))
      {
        ros::spinOnce();
      }
      cur_hdl++;
      fk.data = false;
      count_hdls--;
      flag_rr = 20;

      if(count_hdls == 0)
      {
        locate_new_handles = true;
        principal_vector_points.resize(0);
      }

      handle_num[hdl-1] = 0;
      for(int i = 0; i < handle_num.size(); i++)
          std::cout << "Handle " << i+1 << ": " << handle_num[i]<< std::endl;
//      std::cout << "remove " << hdl << ": " << handle_num[hdl-1]<< std::endl;
      pick_placed = hdl;
      rate.sleep();
    }
  }
}

void showImage(cv::Mat &image, visualization_msgs::MarkerArray &marker_array)
{
  // Black patch to display "Robot has reached" at the bottom of the screen
  cv::Mat blackPatch(50, image.cols, CV_8UC3, cv::Scalar::all(0));
  blackPatch.copyTo(image.rowRange(image.rows - 50, image.rows));

//  visualization_msgs::MarkerArray marker_array;
  for(int i = 0; i < handle_points.size(); i++)
  {
    bool found = false;
    if(handle_num[i] != 0)
    {
//        std::cout << "hdl " << i+1 << ": " << handle_num[i] << std::endl;
      found = true;
    }

    // Displaying the handle locations
    // Displaying the selected handle location
    if(found)
    {
      char handleIndex_txt[10];
      sprintf(handleIndex_txt, "%d", i+1);

      cv::rectangle(image, handle_points[i][0], handle_points[i][1], cv::Scalar(0, 255, 0), 2, 8, 0);
//      cv::putText(image, handleIndex_txt, handle_points[i][1], 1, 2.0,
//                           cv::Scalar(0, 0, 255), 2, 8, 0);
      cv::putText(image, handleIndex_txt, cv::Point(selected_hdl_index[i] % 640, selected_hdl_index[i] / 640 + 0), 1, 2.0,
                  cv::Scalar(0, 0, 255), 2, 8, 0);
      cv::circle(image, cv::Point(selected_hdl_index[i] % 640, selected_hdl_index[i] / 640 + 0), 12,
                 cv::Scalar(0, 0, 255), 2, 8, 0);

      visualization_msgs::Marker marker_axis;
      visualization_msgs::Marker marker_normal;

      // Set the frame ID and timestamp.  See the TF tutorials for information on these.
      marker_normal.header.frame_id = marker_axis.header.frame_id = "/robot_base";
      marker_normal.header.stamp = marker_axis.header.stamp = ros::Time::now();

      // Set the namespace and id for this marker.  This serves to create a unique ID
      // Any marker sent with the same namespace and id will overwrite the old one
      marker_normal.ns = marker_axis.ns = "Principal_axes_vectors";
      marker_axis.id = 2*i;
      marker_normal.id = 2*i+1;
      marker_normal.type = marker_axis.type = visualization_msgs::Marker::ARROW;
      marker_normal.action = marker_axis.action = visualization_msgs::Marker::ADD;
      marker_normal.pose.orientation.w = marker_axis.pose.orientation.w = 1.0;

      // scale.x: diameter of shaft and scale.y:diameter of head, in case of giving the start and end points
      //http://wiki.ros.org/rviz/DisplayTypes/Marker
      marker_normal.scale.x = marker_axis.scale.x = 0.008;
      marker_normal.scale.y = marker_axis.scale.y = 0.01;

      marker_axis.color.r = 1.0f;
      marker_normal.color.b = 1.0f;
      marker_normal.color.a = marker_axis.color.a = 1.0;

      marker_normal.lifetime = marker_axis.lifetime = ros::Duration();

      if(principal_vector_points.size() > 4*i+3)
      {
          marker_axis.points.push_back(principal_vector_points[4*i]);
          marker_axis.points.push_back(principal_vector_points[4*i+1]);
          marker_normal.points.push_back(principal_vector_points[4*i+2]);
          marker_normal.points.push_back(principal_vector_points[4*i+3]);
      }

      marker_array.markers.push_back(marker_axis);
      marker_array.markers.push_back(marker_normal);
    }
  }

  // Displaying that the robot has reached
  if(flag_rr)
  {
    char handleIndex_txt[40];
//    sprintf(handleIndex_txt, "ROBOT HAS PICK AND PLACED HANDLE: %d", cur_hdl);
    sprintf(handleIndex_txt, "ROBOT HAS PICK AND PLACED HANDLE: %d", pick_placed);

    cv::putText(image, handleIndex_txt, cv::Point(60, 460), 1, 1.5,
                 cv::Scalar(0, 0, 255), 2, 8, 0);
    flag_rr--;
  }

  // display the image
  cv::imshow("RobotArmDemo", image);
  cv::waitKey(1);

  return;
}

void *demo_video(void *node_arg)
{
  ros::NodeHandle node("demo_video");
  ros::Subscriber imageSub;
  ros::Publisher marker_pub = node.advertise<visualization_msgs::MarkerArray>("principal_axes_vectors", 10);
  visualization_msgs::MarkerArray marker_principal_axes;
  cv::Mat drawImage;

  int flag_image = 0;
  //##### Video Writer #####//
  cv::VideoWriter writer("/home/meenu/Desktop/handleVideo.avi", CV_FOURCC('M','J','P','G'), 10,
                         cv::Size(640, 480), true);

  ros::CallbackQueue q1;
  node.setCallbackQueue(&q1);

  imageSub = node.subscribe<sensor_msgs::Image>("/camera/rgb/image_color", 1, boost::bind(colorImagecallBack,
                                                            _1, boost::ref(drawImage), boost::ref(flag_image)));
  ros::Rate rate(10);



  while(ros::ok())
  {
    q1.callOne();
    if(flag_image)
    {
//      if(drawImage.data!=NULL)
      {
        showImage(drawImage, marker_principal_axes);
        marker_pub.publish(marker_principal_axes);
        marker_principal_axes.markers.resize(0);
        writer.write(drawImage);
      }
    }

    flag_image = 0;
    rate.sleep();
  }

  writer.release();
}

int main(int argc, char** argv)
{
  // initialize ROS
  ros::init(argc, argv, "localization");
  ros::NodeHandle node("~");


  // constants
  const int PCD_FILE = 0;
  const int SENSOR = 1;

  double g_update_interval;
  int point_cloud_source;

  // set point cloud source from launch file
  node.param("point_cloud_source", point_cloud_source, SENSOR);

  // set point cloud update interval from launch file
  node.param("update_interval", g_update_interval, 10.0);

  std::string range_sensor_frame;

  // point cloud read from file
  if (point_cloud_source == PCD_FILE)
  {
    Affordances g_affordances;
    g_affordances.initParams(node);

    PointCloudRGBA::Ptr cloud_rgba(new PointCloudRGBA);
    std::vector<std::vector<CylindricalShell> > g_handles;

    bool g_has_read = false;
    range_sensor_frame = "/map";
    std::string file = g_affordances.getPCDFile();
    PointCloudRGBA::Ptr cloud(new PointCloudRGBA);
    // load point cloud from PCD file
    if (pcl::io::loadPCDFile(file, *cloud) == -1)
    {
      std::cerr << "Couldn't read pcd file" << std::endl;
      return (-1);
    }
    printf("Loaded *.pcd-file: %s\n", file.c_str());

    PointCloud::Ptr g_cloud(new PointCloud);
    std::vector<CylindricalShell> g_cylindrical_shells;

    copyPointCloud(cloud, g_cloud, cloud_rgba);

    // search grasp affordances using samples
    double start_time = omp_get_wtime();
    std::vector<int> indices = g_affordances.createRandomIndices(g_cloud, g_affordances.getNumSamples());

    g_cylindrical_shells = g_affordances.searchAffordances(g_cloud, indices);

    // search handles
    g_handles = g_affordances.searchHandles(g_cloud, g_cylindrical_shells);

    // set boolean variable so that visualization topics get updated
    g_has_read = true;
    // measure runtime
    printf("Affordance and handle search done in %.3f sec.\n", omp_get_wtime() - start_time);
  }


  // point cloud read from sensor
  else if (point_cloud_source == SENSOR)
  {
    Affordances g_affordances;

    // read parameters
    g_affordances.initParams(node);

    pthread_t threads[2];

//#ifdef run_robot_demo
    int r_demo = pthread_create(&threads[0], NULL, demo, (void *) &g_affordances);
    if (r_demo)
    {
       cout << "Error:unable to create thread for demo, " << r_demo << endl;
       exit(-1);
    }
//#endif

//#ifdef record_demo_video
    int r_demo_video = pthread_create(&threads[1], NULL, demo_video, (void *) &node);
    if (r_demo_video)
    {
       cout << "Error:unable to create thread for demo display, " << r_demo_video << endl;
       exit(-1);
    }
//#endif

    for(int i = 0; i <= 1; i++)
      pthread_join(threads[i],NULL);
  }

  return 0;
}
