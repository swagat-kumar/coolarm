#include "localize_handle/callbacks.h"

//#define supervoxelization

void copyPointCloud(const PointCloudRGBA::ConstPtr &in_cloud, PointCloud::Ptr &cloud_xyz,
                    PointCloudRGBA::Ptr &cloud_xyzrgba)
{
  cloud_xyz->width = in_cloud->width;
  cloud_xyz->height = in_cloud->height;
  cloud_xyz->is_dense = in_cloud->is_dense;
  cloud_xyz->points.resize(in_cloud->points.size());

  for(size_t i = 0; i < in_cloud->size(); i++)
  {
    cloud_xyz->points[i].x = in_cloud->points[i].x;
    cloud_xyz->points[i].y = in_cloud->points[i].y;
    cloud_xyz->points[i].z = in_cloud->points[i].z;
  }

  cloud_xyzrgba->width = in_cloud->width;
  cloud_xyzrgba->height = in_cloud->height;
  cloud_xyzrgba->is_dense = in_cloud->is_dense;
  cloud_xyzrgba->points.resize(in_cloud->points.size());

  for(size_t i = 0; i < in_cloud->size(); i++)
  {
    cloud_xyzrgba->points[i].x = in_cloud->points[i].x;
    cloud_xyzrgba->points[i].y = in_cloud->points[i].y;
    cloud_xyzrgba->points[i].z = in_cloud->points[i].z;
    cloud_xyzrgba->points[i].rgba = in_cloud->points[i].rgba;
  }
}

void changeColor(PointCloudRGBA::Ptr &cloud, std::vector<int> &indices, int r, int g, int b)
{
  int idx;

  for(int i = 0; i < indices.size(); i++)
  {
    idx = indices[i];
    cloud->points[idx].r = r;
    cloud->points[idx].g = g;
    cloud->points[idx].b = b;
  }
}

void robotFlagCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag)
{
  flag.data = ob->data;
}

void robotReceivedFlagCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag)
{
  flag.data = ob->data;
}

void robotDataCountCallback(const std_msgs::Int32::ConstPtr &ob, std_msgs::Int32 &data_count)
{
  data_count.data = ob->data;
}

void colorImagecallBack(const sensor_msgs::ImageConstPtr &image, cv::Mat &drawImage, int &flag)
{

  cv_bridge::CvImagePtr cv_ptr_frames; // kinect frames

  try
  {
    cv_ptr_frames = cv_bridge::toCvCopy(image,"bgr8");
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  drawImage = cv::Mat(cv_ptr_frames->image);
  flag = 1;

  return;
}

//void chatterCallback(const sensor_msgs::PointCloud2::ConstPtr &input, std_msgs::Bool &fk,
//                     tf::StampedTransform &g_transform, Affordances &g_affordances,
//                     std::vector<std::vector<CylindricalShell> > &g_handles,
//                     std_msgs::Float64MultiArray &eigenArray, std::vector<int> &slctd_hdl_idx, int &flag_show_hdls,
//                     std::vector< std::vector<cv::Point> > &rect_pts,
//                     std::vector<geometry_msgs::Point> &principal_vect_pts)
void chatterCallback(const sensor_msgs::PointCloud2::ConstPtr &input, std_msgs::Bool &fk,
                     tf::StampedTransform &g_transform, Affordances &g_affordances,
                     std::vector<std::vector<CylindricalShell> > &g_handles,
                     std::vector<int> &slctd_hdl_idx, int &flag_show_hdls, std::vector< std::vector<cv::Point> > &rect_pts,
                     std::vector<geometry_msgs::Point> &principal_vect_pts)
{

  if(fk.data)
    return;

  flag_show_hdls = 0;

  std::cout << "Processing for new handles " << std::endl;
  double time = omp_get_wtime();

  pcl::PointCloud<pcl::PointXYZ>::Ptr g_voxelized_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  PointCloud::Ptr g_cloud(new PointCloud);
  std::vector<CylindricalShell> g_cylindrical_shells;

  //  convert ROS sensor message to PCL point cloud
#ifdef supervoxelization
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr g_cloud_rgba(new pcl::PointCloud<pcl::PointXYZRGBA>);
  pcl::fromROSMsg(*input, *g_cloud_rgba);

  g_cylindrical_shells = g_affordances.searchAffordancesSuperVoxelization(g_cloud_rgba, &g_transform, g_voxelized_cloud);

#else  
  pcl::fromROSMsg(*input, *g_cloud);
  g_cylindrical_shells = g_affordances.searchAffordances(g_cloud, &g_transform);
#endif

  std::cout << "Found: " << g_cylindrical_shells.size() << std::endl;
//  ros::shutdown();

  if (g_cylindrical_shells.size() == 0)
  {
    printf("No handles found!\n");
    return;
  }
  else
  {
#ifdef supervoxelization
      g_handles = g_affordances.searchHandles(g_voxelized_cloud, g_cylindrical_shells);
#else
      g_handles = g_affordances.searchHandles(g_cloud, g_cylindrical_shells);
#endif
      printf("Processed in: %.3f sec\n",omp_get_wtime() - time);
      if(g_handles.size() > 0)
      {
        std_msgs::Float64MultiArray eigenArray;
        std::cout << "No handles found: " << g_handles.size() << std::endl;
        fk.data = selectHandle(g_handles, g_transform, eigenArray, slctd_hdl_idx, flag_show_hdls, rect_pts,
                               principal_vect_pts);
      }
      else
      {
        std::cout << "No handles are found in the scene..." << std::endl;
        std::cout << "Place object within working space\n" << std::endl;
      }
  }


}

static bool compareSecondElementSmaller(const std::vector<int>& p1, const std::vector<int>& p2)
{
  return p1[1] < p2[1];
}

bool selectHandle(std::vector<std::vector<CylindricalShell> > &g_handles, tf::StampedTransform &g_transform,
                  std_msgs::Float64MultiArray &eigenArray, std::vector<int> &slctd_hdl_idx,
                  int &flag_show_hdls, std::vector< std::vector<cv::Point> > &handle_rect_pts,
                  std::vector<geometry_msgs::Point> &principal_axes)
{

  int hdl_num;
  int selected_hdl = 0;

  std::vector<int> handle_indices(g_handles.size());
  std::vector<int> handle_nbr_idx(g_handles.size());

  handle_rect_pts.resize(g_handles.size());

  for(int hdl = 0; hdl < g_handles.size(); hdl++)
  {
    std::vector<CylindricalShell> handle = g_handles[hdl];

    int min_index = 0;
    int max_index = 0;

    double min_radius = handle[0].getRadius();
    double max_radius = handle[0].getRadius();

    std::vector<cv::Point> rect_pts(2);

    std::vector < std::vector<int> > list_centroid_idx(handle.size(), std::vector<int>(2));

    rect_pts[0].x = 640;
    rect_pts[0].y = 480;
    rect_pts[1].x = 0;
    rect_pts[1].y = 0;

    // Finding points to form rectangle around handle
    for(int j = 0; j < handle.size(); j++)
    {
      std::vector<int> nn_indices = handle[j].getNNIndices();

      for(int k = 0; k < nn_indices.size(); k++)
      {
        int rem = nn_indices[k] % 640;
        int quo = nn_indices[k] / 640;

        if(rem < rect_pts[0].x)
          rect_pts[0].x = rem - 3;
        else if(rem > rect_pts[1].x)
          rect_pts[1].x = rem + 3;

        if(quo < rect_pts[0].y)
          rect_pts[0].y = quo;
        else if(quo > rect_pts[1].y)
          rect_pts[1].y = quo;
      }

      int idx = handle[j].getNeighborhoodCentroidIndex();

      list_centroid_idx[j][0] = j;
      list_centroid_idx[j][1] = idx;

    }

    std::sort(list_centroid_idx.begin(), list_centroid_idx.end(), compareSecondElementSmaller);

    handle_rect_pts[hdl].resize(2);

    handle_rect_pts[hdl] = rect_pts;

    handle_indices[hdl] = list_centroid_idx[list_centroid_idx.size() / 2][1];
    handle_nbr_idx[hdl] = list_centroid_idx[list_centroid_idx.size() / 2][0];

//    std::cout << "Handle: " << hdl << " Nbrs: " << g_handles[hdl].size() << " radius: " << radius << std::endl;
  }
  flag_show_hdls = 1;

  hdl_num = 0;

  if(hdl_num < g_handles.size())
  {
    selected_hdl = 1;

    slctd_hdl_idx.resize(g_handles.size());
    for(int i = 0; i < g_handles.size(); i++)
    {
      slctd_hdl_idx[i] = handle_indices[i];
    }

    flag_show_hdls = 2;
  }
  else
  {
    std::cout << "\nNo handle is selected\n";
    selected_hdl = 0;
    flag_show_hdls = 0;

    return false;
  }

  if(selected_hdl == 1)
  {
    std::vector<double> vect;
//    std::vector<geometry_msgs::Point> principal_axes;

    for(int i = 0; i < g_handles.size(); i++)
    {
      std::vector<CylindricalShell> handle = g_handles[i];
      geometry_msgs::Point pt;

      std::vector<Eigen::Vector3d> centroid;
//      std::vector<Eigen::Vector3d> t_centroid;
      centroid.push_back(handle[handle_nbr_idx[i]].getCentroid());

      tf::Vector3 ptv3(centroid[0](0), centroid[0](1), centroid[0](2));
      tf::Vector3 tf_ptv3 = g_transform * ptv3;// tf_ptv3 = Rotation_matrix*ptv3 + translation_point

      Eigen::Vector3d tf_egnpt;
      tf_egnpt(0) = tf_ptv3.getX();
      tf_egnpt(1) = tf_ptv3.getY();
      tf_egnpt(2) = tf_ptv3.getZ();
//      t_centroid.push_back(tf_egnpt);

      for(int j = 0; j < 3; j++)
//        vect.push_back(t_centroid[0](i));
          vect.push_back(tf_egnpt(j));

      //Axis vector
      std::vector<Eigen::Vector3d> axis;
//      std::vector<Eigen::Vector3d> t_axis;
      axis.push_back(handle[handle_nbr_idx[i]].getCurvatureAxis());
      axis[0] = axis[0]*0.05;
      //For start point of axis vector
      pt.x = tf_egnpt(0);
      pt.y = tf_egnpt(1);
      pt.z = tf_egnpt(2);
      principal_axes.push_back(pt);

      //For end point of axis vector
      ptv3.setX(axis[0](0) + centroid[0](0));
      ptv3.setY(axis[0](1) + centroid[0](1));
      ptv3.setZ(axis[0](2) + centroid[0](2));

      tf_ptv3 = g_transform * ptv3;
      pt.x = tf_ptv3.getX();
      pt.y = tf_ptv3.getY();
      pt.z = tf_ptv3.getZ();
      principal_axes.push_back(pt);

      //Normal vector
      std::vector<Eigen::Vector3d> normal;
//      std::vector<Eigen::Vector3d> t_normal;
      normal.push_back(handle[handle_nbr_idx[i]].getNormal());
      normal[0] = normal[0]*0.05;
      //For start point of normal vector
      pt.x = tf_egnpt(0);
      pt.y = tf_egnpt(1);
      pt.z = tf_egnpt(2);
      principal_axes.push_back(pt);

      //For end point of normal vector
      ptv3.setX(normal[0](0) + centroid[0](0));
      ptv3.setY(normal[0](1) + centroid[0](1));
      ptv3.setZ(normal[0](2) + centroid[0](2));

      tf_ptv3 = g_transform * ptv3;
      pt.x = tf_ptv3.getX();
      pt.y = tf_ptv3.getY();
      pt.z = tf_ptv3.getZ();
      principal_axes.push_back(pt);

    }
    eigenArray.data = vect;
  }
  return true;
}


// Creates, initializes and returns a new viewer
boost::shared_ptr<pcl::visualization::CloudViewer> createViewer()
{
  boost::shared_ptr<pcl::visualization::CloudViewer> v(new pcl::visualization::CloudViewer("Localize Handles"));
  return (v);
}

