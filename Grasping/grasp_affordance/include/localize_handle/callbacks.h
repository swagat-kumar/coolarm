#ifndef CALLBACKS_H
#define CALLBACKS_H

#include "localize_handle/affordances.h"
#include "localize_handle/cylindrical_shell.h"
#include <ctype.h>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <iostream>
#include <stdio.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <vector>
#include <fstream>
#include <pcl/visualization/cloud_viewer.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int32.h>
#include <ros/publisher.h>
#include <ros/subscriber.h>
#include <std_msgs/String.h>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#define EIGEN_DONT_PARALLELIZE

typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloudRGBA;

void copyPointCloud(const PointCloudRGBA::ConstPtr &in_cloud, PointCloud::Ptr &cloud_xyz,
                    PointCloudRGBA::Ptr &cloud_xyzrgba);

void changeColor(PointCloudRGBA::Ptr &cloud, std::vector<int> &indices, int r, int g, int b);

void robotFlagCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag);

void robotReceivedFlagCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag);

void robotDataCountCallback(const std_msgs::Int32::ConstPtr &ob, std_msgs::Int32 &data_count);

void colorImagecallBack(const sensor_msgs::ImageConstPtr &image, cv::Mat &drawImage, int &flag);

//void chatterCallback(const sensor_msgs::PointCloud2::ConstPtr &input, std_msgs::Bool &fk,
//                     tf::StampedTransform &g_transform, Affordances &g_affordances,
//                     std::vector<std::vector<CylindricalShell> > &g_handles,
//                     std_msgs::Float64MultiArray &eigenArray, std::vector<int> &hdl_indices, int &slctd_hdl_idx,
//                     int &flag_show_hdls);

void chatterCallback(const sensor_msgs::PointCloud2::ConstPtr &input, std_msgs::Bool &fk,
                     tf::StampedTransform &g_transform, Affordances &g_affordances,
                     std::vector<std::vector<CylindricalShell> > &g_handles,
                     std::vector<int> &slctd_hdl_idx, int &flag_show_hdls, std::vector< std::vector<cv::Point> > &rect_pts,
                     std::vector<geometry_msgs::Point> &principal_vect_pts);

void showImage(cv::Mat &image, visualization_msgs::MarkerArray &marker_array);

//bool selectHandle(std::vector<std::vector<CylindricalShell> > &g_handles, tf::StampedTransform &g_transform,
//                  std_msgs::Float64MultiArray &eigenArray, std::vector<int> &hdl_indices, int &slctd_hdl_idx,
//                  int &flag_show_hdls);

bool selectHandle(std::vector<std::vector<CylindricalShell> > &g_handles, tf::StampedTransform &g_transform,
                  std_msgs::Float64MultiArray &eigenArray, std::vector<int> &slctd_hdl_idx,
                  int &flag_show_hdls, std::vector< std::vector<cv::Point> > &rect_pts,
                  std::vector<geometry_msgs::Point> &principal_axes);

boost::shared_ptr<pcl::visualization::CloudViewer> createViewer();

//cv::VideoWriter writer;
#endif // CALLBACKS_H
