cmake_minimum_required(VERSION 2.8.3)
project(grasp_affordance)
#set(CMAKE_BUILD_TYPE Debug)
find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  geometry_msgs
  eigen_conversions
  pcl_ros
  pcl_conversions
  tf
  tf_conversions
  cv_bridge
  message_generation
  #cmake_modules #Uncomment cmake_modules in case of ROS Indigo
)

## find Lapack
find_package(LAPACK REQUIRED)

## find Eigen package
find_package(Eigen REQUIRED)

add_message_files(
  FILES
  HandlePose.msg
)

generate_messages(DEPENDENCIES geometry_msgs)


catkin_package(
  INCLUDE_DIRS
    include
  CATKIN_DEPENDS
    roscpp
    std_msgs
    geometry_msgs
    eigen_conversions
    pcl_ros
    pcl_conversions
    tf
    tf_conversions
    message_runtime
  DEPENDS
    Eigen
    LAPACK
)

include_directories(
  ${catkin_INCLUDE_DIRS} ${Eigen_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} ${PCL_INCLUDE_DIRS} include
)

link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

## compiler optimization flags
set(CMAKE_CXX_FLAGS "-DNDEBUG -O3 -fopenmp -Wno-deprecated")
#set(CMAKE_CXX_FLAGS "-O3 -fopenmp -Wno-deprecated")
## create executables
add_executable(get_handles src/localization.cpp src/callbacks.cpp src/affordances.cpp src/cylindrical_shell.cpp)

## add dependencies
add_dependencies(get_handles get_handles_generate_messages_cpp)


## create libraries
#add_library(${PROJECT_NAME}_affordances src/affordances.cpp)
#add_library(${PROJECT_NAME}_cylindrical_shell src/cylindrical_shell.cpp)

## link libraries to localization executable
target_link_libraries(get_handles ${catkin_LIBRARIES} ${PCL_LIBRARIES} libvtkCommon.so libvtkFiltering.so )
#target_link_libraries(get_handles ${PROJECT_NAME}_affordances)

target_link_libraries(get_handles lapack)


## link libraries to affordances library
#target_link_libraries(${PROJECT_NAME}_affordances ${catkin_LIBRARIES})
#target_link_libraries(${PROJECT_NAME}_affordances ${PROJECT_NAME}_cylindrical_shell)
#target_link_libraries(${PROJECT_NAME}_affordances lapack)

