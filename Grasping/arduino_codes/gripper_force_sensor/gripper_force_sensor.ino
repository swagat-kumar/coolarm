//*****************************************************************************
// Example Sketch for the 0.5" Force Sensitive Resistor
// 3/17/11
// 
// Set up the FSR as R2 of voltage divider circuit with R1 a value of 27k
// Vout connected to Analog input 0
// Prints resistance value of FSR
// 
// WARNING: Resistance value only valid for 27k resistor if using a different
// value, use the equation
// 
//    x = (Rfsr/(R1+Rfsr))*1023
//
// where x is the value from A0, and R1 is the non-FSR resistor, to find the 
// FSR resistance in Ohms. 
//
// To find the actual weight applied to the sensor, refer to the graph on page
// 5 of the datasheet (theoretical values only!) or calibrate the circuit using
// of a known weight. 


//*****************************************************************************

#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64.h>

ros::NodeHandle  nh;

//std_msgs::String str_msg;

//ros::Publisher chatter("chatter", &str_msg);
std_msgs::Float64 res_msg_1;
std_msgs::Float64 res_msg_2;

ros::Publisher res_topic_1("force_sensor_1", &res_msg_1);
ros::Publisher res_topic_2("force_sensor_2", &res_msg_2);

#define FORCE_1 0
#define FORCE_2 1

float value_1 = 0;
float resistance_1 = 0;
float value_2 = 0;
float resistance_2 = 0;

//char str[50];
void setup()
{
  nh.initNode();
//  nh.advertise(chatter);
    nh.advertise(res_topic_1);
    nh.advertise(res_topic_2);
}

void loop()
{
  value_1 = analogRead(FORCE_1);
  value_2 = analogRead(FORCE_2);
  
//  resistance = ((26.4 * value)/(1-(value/1023.0)));
  resistance_1 = ((27 * value_1)/(1023-value_2));
  resistance_2 = ((27 * value_2)/(1023-value_2));

//  sprintf(str,"Resistance: %f",resistance);
//  str_msg.data = str;
//  chatter.publish( &str_msg );

  res_msg_1.data = 1023-value_1;
  res_msg_2.data = 1023-value_2;
  res_topic_1.publish(&res_msg_1);
  res_topic_2.publish(&res_msg_2);
  nh.spinOnce();
// Serial.println(resistance,DEC);  
  
  delay(100);
}

