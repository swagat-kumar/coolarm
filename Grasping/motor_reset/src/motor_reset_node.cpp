#include <ros/ros.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>

void motorStateCallback(const dynamixel_msgs::JointState::ConstPtr &jointMsg, bool &is_moving, double velocity,
                        double current_pos, double error)
{
  is_moving = jointMsg->is_moving;
  velocity = jointMsg->velocity;
  current_pos = jointMsg->current_pos;
  error = jointMsg->error;
  return;
}
void checkMotorCallback(const std_msgs::Bool::ConstPtr &msg, bool &check_status)
{
  check_status = msg->data;
  return;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "gripper_motor_reset");
  ros::NodeHandle n;
  ros::Rate rate(30);


  bool check_status = false;

//  gripper status variables
  bool is_motor_moving = false;
  double velocity;
  double current_pos;
  double error;

  ros::Subscriber motor_state_sub = n.subscribe<dynamixel_msgs::JointState>("/gripper_open_controller_1/state", 100,
                                                    boost::bind(motorStateCallback, _1, boost::ref(is_motor_moving),
                                                                boost::ref(velocity), boost::ref(current_pos), boost::ref(error)));
  ros::Subscriber check_status_sub = n.subscribe<std_msgs::Bool>("/check_gripper_motor_status", 100,
                                                     boost::bind(checkMotorCallback, _1, boost::ref(check_status)));

  ros::Publisher command_pub;
  command_pub = n.advertise<std_msgs::Float64>("/gripper_open_controller_1/command", 100);

  ros::Publisher reset_ok_pub;
  reset_ok_pub = n.advertise<std_msgs::Bool>("/gripper_is_reset", 100);

  std_msgs::Bool reset_ok;

  std_msgs::Float64 theta_grip;
//  theta_grip.data = -0.98; //open
//  command_pub.publish(theta_grip);

  while(ros::ok())
  {
    ros::spinOnce();

    if(check_status)
    {
      if(is_motor_moving)
      {
        while(is_motor_moving)
        {
          if(fabs(velocity) < 0.0015)
          {
            std::cout << "velocity: " << velocity << ", current posn: " << current_pos << std::endl;
//            char str[100];
//            sprintf(str,"rostopic pub /gripper_open_controller_1/command std_msgs/Float64 \"data: %lf\" ",current_pos);
//            std::cout << str << std::endl;
//            system(str);
            theta_grip.data = current_pos;
            command_pub.publish(theta_grip);
            system("rosservice call /gripper_open_controller_1/set_torque_limit 2.0");
          }
          ros::spinOnce();
//          is_motor_moving = false;
        }
      }
      else
      {
        if(error > 0.02)
        {
          theta_grip.data = current_pos;
          command_pub.publish(theta_grip);
          system("rosservice call /gripper_open_controller_1/set_torque_limit 2.0");
        }
      }
      for(int i = 0; i<100; i++)
      {
        reset_ok.data = true;
        reset_ok_pub.publish(reset_ok);
      }
      check_status = false;
    }

    rate.sleep();
  }
}
