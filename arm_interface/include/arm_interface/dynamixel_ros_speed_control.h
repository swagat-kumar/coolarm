#ifndef DYNAMIXEL_ROS_SPEED_CONTROL_H
#define DYNAMIXEL_ROS_SPEED_CONTROL_H

#include <ros/ros.h>
#include <dynamixel_controllers/SetSpeed.h>

//void setSpeedJoint1(ros::NodeHandle &node, double velocity);
//void setSpeedJoint2(ros::NodeHandle &node, double velocity);
//void setSpeedJoint3(ros::NodeHandle &node, double velocity);
//void setSpeedJoint4(ros::NodeHandle &node, double velocity);
//void setSpeedJoint5(ros::NodeHandle &node, double velocity);
//void setSpeedJoint6(ros::NodeHandle &node, double velocity);
//void setSpeedJoint7(ros::NodeHandle &node, double velocity);
//void setSpeedGripper(ros::NodeHandle &node, double velocity);

void setSpeedJoint1(std::vector<ros::ServiceClient> &jt_client, double velocity);
void setSpeedJoint2(std::vector<ros::ServiceClient> &jt_client, double velocity);
void setSpeedJoint3(std::vector<ros::ServiceClient> &jt_client, double velocity);
void setSpeedJoint4(std::vector<ros::ServiceClient> &jt_client, double velocity);
void setSpeedJoint5(std::vector<ros::ServiceClient> &jt_client, double velocity);
void setSpeedJoint6(std::vector<ros::ServiceClient> &jt_client, double velocity);
void setSpeedJoint7(std::vector<ros::ServiceClient> &jt_client, double velocity);
void setSpeedGripper(std::vector<ros::ServiceClient> &jt_client, double velocity);

#endif // DYNAMIXEL_ROS_SPEED_CONTROL_H
