#ifndef INTERFACECONTROL_H
#define INTERFACECONTROL_H

#include "arm_interface/callbacks.h"
#include <opencv2/opencv.hpp>
#include <coolarm_fk.h>
#include "arm_interface/dynamixel_ros_speed_control.h"
#include <dynamixel_controllers/SetSpeed.h>
#include <gnuplot_ci.h>

#define NC 3

//#define ORIENTATION

//#define JLA

#define JLA_NSO
#define CONST_VELOCITY

using namespace cv;

class InterfaceControl
{
public:
    // Motor status data
    std::vector<double> jtangle;// For storing the current joint motor position(angle in degrees or radians)

    // Flags for communicating the handle pose data
    std_msgs::Bool f_kinect;// Flag for whether the handle pose data is being transmitted or not
    std_msgs::Bool f_robot;// Flag for whether the robot has reached the current handle pose data
    std_msgs::Bool f_robot_rcvd;// Flag for whether the robot has receive the handle pose data that is being transmitted
    std_msgs::Int32 r_cnt;// Flag for the count of handle pose that the interface node is expecting
    std_msgs::Int32 k_cnt;// Flag for the count of handle pose data that is being transmitted

    // variable for storing gripper force sensor values
    std_msgs::Float64 force_sensor[2];

    // Transform from the Robot arm frame to kinect depth optical frame
    tf::StampedTransform g_transform;

    // For storing handle pose data-
    // 0- centroid
    // 1- point on the axis vector at a distance from the centroid
    // 2- point on the normal vector at a distance from the centroid
    std::vector<geometry_msgs::Point> handle_pose;

    // Flag for request for gripper reset
//    std_msgs::Bool gripper_reset;

    //  gripper status variables
//    bool is_motor_moving = false;// whether gripper motor is moving
//    double velocity;// the velocity of the gripper motor
//    double current_pos;// current position of the gripper motor
//    double error;// error between current and goal position of the gripper motor

    InterfaceControl(void);

    // Functions for creating publishers and subscribers
    void createDynamixelCntrlPubSub(std::vector<ros::Publisher> &command_pub, std::vector<ros::Subscriber> &state_sub,
                                    ros::NodeHandle &node);

    void subscribeServiceCntrlSpeed(ros::NodeHandle &node, std::vector<ros::ServiceClient> &client);

    void createPoppyCntrlPubSub(std::vector<ros::Publisher> &poppy_pos, std::vector<ros::Publisher> &poppy_speed,
                                ros::NodeHandle &node);

    void createCommnProtocolPubSub(std::vector<ros::Subscriber> &commn_sub, std::vector<ros::Publisher> &commn_pub,
                                   ros::NodeHandle &node);

    // Function to get the data from transmitted by the sender
    void getDatafromSender(std::vector<ros::Publisher> &commn_pub);

    tf::Vector3 transformPoint(geometry_msgs::Point &pt, tf::StampedTransform transform);

    std::vector<geometry_msgs::Point> getPathPointalongNormalVector(int num_pts);

    // Functions for obtaining the path planning and inverse kinematics
    void obtainPathPlanning(double initial_theta[], int num_steps, std::vector<int> iterations,
                            std::vector<geometry_msgs::Point> point, double theta_0[], cv::Mat &theta_step,
                            cv::Mat &theta_dot_step, ofstream &f1, ofstream &f2, ofstream &f3, int &d);// std::vector<ofstream*> &files_ptr);

    void computeInvKinematics(geometry_msgs::Point centroid, double desired_pose[], double initial_posn[],
                              int num_iterations, cv::Mat &theta_all, cv::Mat &theta_dot_all, ofstream &f1, ofstream &f2,
                              ofstream &f4);// ofstream* &f1, ofstream* &f2, ofstream* &f4);

    void jla_orient(double pose_t[], double initial_theta[], double theta_final[],
                    ofstream &f1, ofstream &f2, ofstream &f3, ofstream &f4, ofstream &f5);

//    void JacobianInvKinOrientation(double target_pose[], double initial_theta[], cv::Mat &theta_step, cv::Mat &theta_dot_step,
//                                   cv::Mat &orient);
    void JacobianInvKinOrientation(double target_pose[], double initial_theta[], double theta_final[], cv::Mat &orient, ofstream &f1,
                                   ofstream &f2, ofstream &f3, ofstream &f4);

    // Dynamixel control of the arm
    void getTime(cv::Mat &theta_dot, cv::Mat &theta_dot_step, double initial_theta[], float time[]);

    void openGripperDynamixel(ros::Publisher &command_gripper, float wait_time_secs);

    void setJointSpeed(cv::Mat &step_speed, std::vector<ros::ServiceClient> &jt_client);

    void reachPointDynamixel(std::vector<ros::Publisher> &command_pub, std::vector<ros::ServiceClient> &client, cv::Mat &theta_step,
                             cv::Mat &theta_dot_step, float time[], bool give_jtspeed)
;

    void closeGripperDynamixel(ros::Publisher &command_gripper);

    // Poppy control of the arm
    void openGripperPoppy(ros::Publisher &poppy_pub, ros::Publisher &poppy_speed, float wait_time_secs);

    void reachPointPoppy(std::vector<ros::Publisher> &poppy_pos, std::vector<ros::Publisher> &poppy_speed,
                         cv::Mat &theta_step, cv::Mat &theta_dot_step, float time[]);

    void closeGripperPoppy(ros::Publisher &poppy_pub, ros::Publisher &poppy_speed);

    void getRotationAngles(cv::Mat &R, double angles[]);

    void getOrientationMatrix(cv::Mat &matrix);
};

#endif // INTERFACECONTROL_H
