#ifndef INCLUDEDEFS_H
#define INCLUDEDEFS_H

#include <ros/ros.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <nnet.h>
#include <ksom_vmc.h>
#include <mymatrix.h>
#include <gnuplot_ci.h>
#include <dynamixel_controllers/SetTorqueLimit.h>
#include <dynamixel_controllers/TorqueEnable.h>
#include <csignal>
#include <cstdio>
//#include <motor_reset.h>
#include <coolarm_fk.h>
#include "arm_interface/interfacecontrol.h"
#include "arm_interface/dynamixel_ros_speed_control.h"
#include <Eigen/Dense>

using namespace std;
using namespace cv;
using namespace nnet;
using namespace vmc;
using namespace gnuplot_ci;

//#define NC 3   // Dimension of Cartesian Space

#define PLOT

// Select any one, dynamixel_control or poppy_control
#define DYNAMIXEL_CONTROL
//#define POPPY_CONTROL

#endif // INCLUDEDEFS_H
