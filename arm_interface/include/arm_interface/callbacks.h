#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Int32.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include "grasp_affordance/HandlePose.h"

void jtStateCallback1(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle);

void jtStateCallback2(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle);

void jtStateCallback3(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle);

void jtStateCallback4(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle);

void jtStateCallback5(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle);

void jtStateCallback6(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle);

void jtStateCallback7(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle);

void jtStateCallback8(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle);

//void handleDataCallback(const std_msgs::Float64MultiArray::ConstPtr &array, std::vector<double> &point,
//                        std_msgs::Bool &flag_frr);

void handleDataCallback(const grasp_affordance::HandlePose::ConstPtr &pose, std::vector<geometry_msgs::Point> &point,
                        std_msgs::Bool &flag_frr, tf::StampedTransform transform);

void handleGenFlagCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag);

void kinectDataCountCallback(const std_msgs::Int32::ConstPtr &ob, std_msgs::Int32 &data_count);

//void gripperResetCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag);

void forceSensorCallback(const std_msgs::Float64::ConstPtr &ob, std_msgs::Float64 &force);

//void motorStateCallback(const dynamixel_msgs::JointState::ConstPtr &jointMsg, bool &is_moving, double velocity,
//                        double current_pos, double error);

//========================================
// Signal Handling
// does not work as the controller passes on to the subscribers
void signal_callback_handler(int signum);

#endif // CALLBACKS_H
