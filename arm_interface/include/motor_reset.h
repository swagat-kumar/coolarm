#include <ros/ros.h>
#include <std_msgs/Float64.h>

void resetGripperMotor(bool &is_motor_moving, double &velocity, double &current_pos, ros::Publisher &command_pub, double &error,
                       bool &pub_current_pos, bool &get_out)
{
  std_msgs::Float64 theta_grip;
  bool reset_status;
  get_out = false;

//  if(!is_motor_moving && fabs(velocity) < 0.0015)
//  {
//    if(fabs(error) < 0.02)
//    {
//      get_out = true;
//      pub_current_pos = false;
//      system("rosservice call /gripper_open_controller_1/set_torque_limit 2.0");
//    }
//    else
//    {
////      get_out = false;
//      pub_current_pos = true;
//    }
//  }
//  else if(!is_motor_moving && fabs(velocity) > 0.0015)
//  {
////    get_out = false;
//    pub_current_pos = false;
//  }
//  else if(is_motor_moving && fabs(velocity) < 0.0015)
//  {
//    if(fabs(error) < 0.02)
//    {
////      get_out = false;
////      pub_current_pos = false;
//      pub_current_pos = true;
//      system("rosservice call /gripper_open_controller_1/set_torque_limit 2.0");
//    }
//    else
//    {
////      get_out = false;
//      pub_current_pos = true;
//    }
//  }
//  else if(is_motor_moving && fabs(velocity) > 0.0015)
//  {
////    get_out = false;
//    pub_current_pos = false;
//  }

  if(is_motor_moving && fabs(velocity) < 0.0015)
  {
    if(fabs(error) > 0.02)
    {
        theta_grip.data = current_pos;
        command_pub.publish(theta_grip);
        system("rosservice call /gripper_open_controller_1/set_torque_limit 1");
        get_out = true;
    }
//    else
//    {
////      get_out = false;
//      pub_current_pos = true;
//    }
  }

  return;


//  if(!is_motor_moving)
//  {
//    if(fabs(velocity) < 0.0015)
//    {
//      if(fabs(error) > 0.02)
//      {
//        std::cout << "velocity: " << velocity << ", current posn: " << current_pos << std::endl;
////        char str[100];
////        sprintf(str,"rostopic pub /gripper_open_controller_1/command std_msgs/Float64 \"data: %lf\" ",current_pos);
////        std::cout << str << std::endl;
////        system(str);
////        theta_grip.data = current_pos;
////        command_pub.publish(theta_grip);
////        pub_current_pos = true;
//        system("rosservice call /gripper_open_controller_1/set_torque_limit 2.0");

//        reset_status = true;
//        return reset_status;
//      }
//      else
//      {
//        system("rosservice call /gripper_open_controller_1/set_torque_limit 2.0");
//        reset_status = true;
//        return reset_status;
//      }
//    }
//    else
//    {
//      reset_status = false;
//      return reset_status;
//    }
//  }
//  else
//  {
//    if(fabs(velocity) < 0.0015)
//    {
//      if(fabs(error) > 0.02)
//      {
////        theta_grip.data = current_pos;
////        command_pub.publish(theta_grip);
//        system("rosservice call /gripper_open_controller_1/set_torque_limit 2.0");
//        reset_status = true;
//        return reset_status;
//      }
//      else
//      {
//        system("rosservice call /gripper_open_controller_1/set_torque_limit 2.0");
//        reset_status = true;
//        return reset_status;
//      }
//    }
//    else
//    {
//      reset_status = false;
//      return reset_status;
//    }
//  }
}
