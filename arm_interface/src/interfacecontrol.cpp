#include "arm_interface/interfacecontrol.h"

InterfaceControl::InterfaceControl(void)
{
    jtangle.resize(8);
    for(int i = 0; i < jtangle.size(); i++)
        jtangle[i] = 0;

    f_kinect.data = false;
    f_robot.data = true;
    f_robot_rcvd.data = false;
//    r_cnt.data = 0;
    r_cnt.data = -1;
    k_cnt.data = -1;

    handle_pose.resize(3);
}

void InterfaceControl::
createDynamixelCntrlPubSub(std::vector<ros::Publisher> &command_pub, std::vector<ros::Subscriber> &state_sub,
                           ros::NodeHandle &node)
{
    command_pub[0] = node.advertise<std_msgs::Float64>("/joint1_controller/command", 100);
    command_pub[1] = node.advertise<std_msgs::Float64>("/dual_servo_controller/command", 100);
    command_pub[2] = node.advertise<std_msgs::Float64>("/joint3_controller/command", 100);
    command_pub[3] = node.advertise<std_msgs::Float64>("/joint4_controller/command", 100);
    command_pub[4] = node.advertise<std_msgs::Float64>("/joint5_controller/command", 100);
    command_pub[5] = node.advertise<std_msgs::Float64>("/joint6_controller/command", 100);
    command_pub[6] = node.advertise<std_msgs::Float64>("/joint7_controller/command", 100);
    command_pub[7] = node.advertise<std_msgs::Float64>("/gripper_open_controller_1/command", 100);

    state_sub[0] = node.subscribe<dynamixel_msgs::JointState>("/joint1_controller/state",
                                                  1, boost::bind(jtStateCallback1, _1, boost::ref(jtangle[0])));
    state_sub[1] = node.subscribe<dynamixel_msgs::JointState>("/dummy_joint2_controller/state",
                                                  1, boost::bind(jtStateCallback2, _1, boost::ref(jtangle[1])));
    state_sub[2] = node.subscribe<dynamixel_msgs::JointState>("/joint3_controller/state",
                                                  1, boost::bind(jtStateCallback3, _1, boost::ref(jtangle[2])));
    state_sub[3] = node.subscribe<dynamixel_msgs::JointState>("/joint4_controller/state",
                                                  1, boost::bind(jtStateCallback4, _1, boost::ref(jtangle[3])));
    state_sub[4] = node.subscribe<dynamixel_msgs::JointState>("/joint5_controller/state",
                                                  1, boost::bind(jtStateCallback5, _1, boost::ref(jtangle[4])));
    state_sub[5] = node.subscribe<dynamixel_msgs::JointState>("/joint6_controller/state",
                                                  1, boost::bind(jtStateCallback6, _1, boost::ref(jtangle[5])));
    state_sub[6] = node.subscribe<dynamixel_msgs::JointState>("/joint7_controller/state",
                                                  1, boost::bind(jtStateCallback7, _1, boost::ref(jtangle[6])));
    state_sub[7] = node.subscribe<dynamixel_msgs::JointState>("/gripper_open_controller_1/state",
                                                  1, boost::bind(jtStateCallback8, _1, boost::ref(jtangle[7])));

}

void InterfaceControl::
subscribeServiceCntrlSpeed(ros::NodeHandle &node, std::vector<ros::ServiceClient> &client)
{
    client[0] = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint1_controller/set_speed");
    client[1] = node.serviceClient<dynamixel_controllers::SetSpeed>("/dummy_joint2_controller/set_speed");
    client[2] = node.serviceClient<dynamixel_controllers::SetSpeed>("/dummy_joint8_controller/set_speed");
    client[3] = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint3_controller/set_speed");
    client[4] = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint4_controller/set_speed");
    client[5] = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint5_controller/set_speed");
    client[6] = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint6_controller/set_speed");
    client[7] = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint7_controller/set_speed");
//    client[8] = node.serviceClient<dynamixel_controllers::SetSpeed>("/gripper_open_controller_1/set_speed");
}

void InterfaceControl::
createPoppyCntrlPubSub(std::vector<ros::Publisher> &poppy_pos, std::vector<ros::Publisher> &poppy_speed,
                       ros::NodeHandle &node)
//void InterfaceControl::
//createPoppyCntrlPubSub(ros::Publisher &poppy_pos, ros::Publisher &poppy_speed,
//                       ros::NodeHandle &node)
{
//    poppy_pos = node.advertise<std_msgs::Float64MultiArray>("jt_pos", 100);
    poppy_pos[0] = node.advertise<std_msgs::Float64>("jt1_pos",100);
    poppy_pos[1] = node.advertise<std_msgs::Float64>("jt2_pos",100);
    poppy_pos[2] = node.advertise<std_msgs::Float64>("jt3_pos",100);
    poppy_pos[3] = node.advertise<std_msgs::Float64>("jt4_pos",100);
    poppy_pos[4] = node.advertise<std_msgs::Float64>("jt5_pos",100);
    poppy_pos[5] = node.advertise<std_msgs::Float64>("jt6_pos",100);
    poppy_pos[6] = node.advertise<std_msgs::Float64>("jt7_pos",100);
    poppy_pos[7] = node.advertise<std_msgs::Float64>("grp_pos",100);

//    poppy_speed = node.advertise<std_msgs::Float64MultiArray>("jt_speed", 100);
    poppy_speed[0] = node.advertise<std_msgs::Float64>("jt1_speed",100);
    poppy_speed[1] = node.advertise<std_msgs::Float64>("jt2_speed",100);
    poppy_speed[2] = node.advertise<std_msgs::Float64>("jt3_speed",100);
    poppy_speed[3] = node.advertise<std_msgs::Float64>("jt4_speed",100);
    poppy_speed[4] = node.advertise<std_msgs::Float64>("jt5_speed",100);
    poppy_speed[5] = node.advertise<std_msgs::Float64>("jt6_speed",100);
    poppy_speed[6] = node.advertise<std_msgs::Float64>("jt7_speed",100);
    poppy_speed[7] = node.advertise<std_msgs::Float64>("grp_speed",100);
}

void InterfaceControl::
createCommnProtocolPubSub(std::vector<ros::Subscriber> &commn_sub, std::vector<ros::Publisher> &commn_pub,
                          ros::NodeHandle &node)
{
    // commn_sub[0]: subscriber for flag that the data is being transmitted by sender, sub_fk
    // commn_sub[1]: subscriber for the count number of the data that is being transmitted, sub_k_cnt
    // commn_sub[2]: subscriber for the data, sub_handle_data

    // commn_pub[0]: publisher for flag requesting the new data, pub_fr
    // commn_pub[1]: publisher for the count number of the data requested by the receiver, pub_r_cnt
    // commn_pub[2]: publisher for flag that the data is received by the receiver, pub_frr

    commn_sub[0] = node.subscribe<std_msgs::Bool>("/flag/kinect/transmitting_data", 1,
                                                  boost::bind(handleGenFlagCallback, _1, boost::ref(f_kinect)));

    commn_sub[1] = node.subscribe<std_msgs::Int32>("/data/count/kinect", 1,
                                                   boost::bind(kinectDataCountCallback, _1, boost::ref(k_cnt)));

    commn_sub[2] = node.subscribe<grasp_affordance::HandlePose>
            ("/handle/data", 1, boost::bind(handleDataCallback, _1, boost::ref(handle_pose), boost::ref(f_robot_rcvd),
                                            boost::ref(g_transform)));

//    commn_sub[2] = node.subscribe<std_msgs::Float64MultiArray>("/handle/data", 1,
//                                                               boost::bind(handleDataCallback, _1, boost::ref(centroid),
//                                                               boost::ref(f_robot_rcvd)));


    commn_pub[0] = node.advertise<std_msgs::Bool>("/flag/robot_arm/request_data", 1);

    commn_pub[1] = node.advertise<std_msgs::Int32>("/data/count/robot_arm", 1);

    commn_pub[2] = node.advertise<std_msgs::Bool>("/flag/robot_arm/received_data", 1);

//    ros::Publisher pub_grip_motor_status;// Publisher to check the gripper motor status

//    pub_grip_motor_status = node.advertise<std_msgs::Bool>("/check_gripper_motor_status", 1);

//    ros::Subscriber gripper_reset_is_ok = node.subscribe<std_msgs::Bool>
//            ("/gripper_is_reset", 100, boost::bind(gripperResetCallback, _1, boost::ref(gripper_reset)));

}

void InterfaceControl::
getDatafromSender(std::vector<ros::Publisher> &commn_pub)
{
    // update the flags for communicating that the arm has pick and placed the object
    f_robot.data = true;
    f_kinect.data = false;
    f_robot_rcvd.data = false;
    r_cnt.data = r_cnt.data + 1;

    // commn_pub[0]: publisher for flag requesting the new data, pub_fr
    // commn_pub[1]: publisher for the count number of the data requested by the receiver, pub_r_cnt
    // commn_pub[2]: publisher for flag that the data is received by the receiver, pub_frr
    std::cout << "Robot wants count: " << r_cnt.data << std::endl;

    // Wait for the new handle pose that is requested by the arm to be transmitted
//        while((!f_kinect.data && f_robot.data) || k_cnt.data != r_cnt.data)
    while((!f_kinect.data) || k_cnt.data != r_cnt.data)
    {
        commn_pub[0].publish(f_robot);
        commn_pub[1].publish(r_cnt);
//      pub_fr.publish(f_robot);
//      pub_r_cnt.publish(r_cnt);

      ros::spinOnce();
    }
    f_robot.data = false;
    f_robot_rcvd.data = false;

    while(!f_robot_rcvd.data)
    {
      ros::spinOnce();

      commn_pub[0].publish(f_robot);
      commn_pub[2].publish(f_robot_rcvd);
//      pub_fr.publish(f_robot);
//      pub_frr.publish(f_robot_rcvd);
    }
    std::cout << "Robot received count: " << r_cnt.data << std::endl;
}

tf::Vector3 InterfaceControl::
transformPoint(geometry_msgs::Point &pt, tf::StampedTransform transform)
{
    tf::Vector3 p(pt.x, pt.y, pt.z);
    tf::Vector3 tf_p = transform * p;

//    pt.x = tf_p.getX();
//    pt.y = tf_p.getY();
//    pt.z = tf_p.getZ();
    return tf_p;
}

std::vector<geometry_msgs::Point> InterfaceControl::
getPathPointalongNormalVector(int num_pts)
{
    tf::Vector3 tf_centroid = transformPoint(handle_pose[0], g_transform);
    tf::Vector3 tf_normal = transformPoint(handle_pose[2], g_transform);

    tf::Vector3 tf_normal_vect = tf_normal - tf_centroid;
    tf::Vector3 z_axis(0,0,1);
    tf_normal_vect.normalize();
    float cos_theta = tf_normal_vect.dot(z_axis);
    // reverse the normal vector in case it is along z-axis of the depth_optical_frame
    std::vector<geometry_msgs::Point> points_on_path;

#ifdef ORIENTATION
    double vx = handle_pose[2].x - handle_pose[0].x;
    double vy = handle_pose[2].y - handle_pose[0].y;
    double vz = handle_pose[2].z - handle_pose[0].z;

    tf::Vector3 normal;
    normal.setX(vx);
    normal.setY(vy);
    normal.setZ(vz);
    normal.normalize();

    cout << "Normal Vector: [" << vx << " " << vy << " " << vz << "]" << endl;
    normal *= -1;

    for(int i = 0; i < num_pts-1; i++)
    {
        float dist_from_centroid = 0.1*(num_pts-1-i)/(num_pts-1);// 0.1 is for 10cm distance from centroid
        tf::Vector3 vect(normal);
        vect *= dist_from_centroid;
        geometry_msgs::Point pt;
        pt.x = vect.getX() + handle_pose[0].x;
        pt.y = vect.getY() + handle_pose[0].y;
        pt.z = vect.getZ() + handle_pose[0].z;
        points_on_path.push_back(pt);
    }
    points_on_path.push_back(handle_pose[0]);
#else
    if(cos_theta > -0.3)
    {
        double vx = handle_pose[2].x - handle_pose[0].x;
        double vy = handle_pose[2].y - handle_pose[0].y;
        double vz = handle_pose[2].z - handle_pose[0].z;

        tf::Vector3 normal;
        normal.setX(vx);
        normal.setY(vy);
        normal.setZ(vz);
        normal.normalize();
        normal *= -1;

        for(int i = 0; i < num_pts-1; i++)
        {
            float dist_from_centroid = 0.1*(num_pts-1-i)/(num_pts-1);// 0.1 is for 10cm distance from centroid
            tf::Vector3 vect(normal);
            vect *= dist_from_centroid;
            geometry_msgs::Point pt;
            pt.x = vect.getX() + handle_pose[0].x;
            pt.y = vect.getY() + handle_pose[0].y;
            pt.z = vect.getZ() + handle_pose[0].z;
            points_on_path.push_back(pt);
        }
        points_on_path.push_back(handle_pose[0]);
    }
    else
    {
        double vx = handle_pose[2].x - handle_pose[0].x;
        double vy = handle_pose[2].y - handle_pose[0].y;
        double vz = handle_pose[2].z - handle_pose[0].z;

        tf::Vector3 normal;
        normal.setX(vx);
        normal.setY(vy);
        normal.setZ(vz);
        normal.normalize();

        for(int i = 0; i < num_pts-1; i++)
        {
            float dist_from_centroid = 0.1*(num_pts-1-i)/(num_pts-1);// 0.1 is for 10cm distance from centroid
            tf::Vector3 vect(normal);
            vect *= dist_from_centroid;
            geometry_msgs::Point pt;
            pt.x = vect.getX() + handle_pose[0].x;
            pt.y = vect.getY() + handle_pose[0].y;
            pt.z = vect.getZ() + handle_pose[0].z;
            points_on_path.push_back(pt);
        }
        points_on_path.push_back(handle_pose[0]);
    }
#endif

    return points_on_path;
}

void InterfaceControl::
obtainPathPlanning(double initial_theta[], int num_steps, std::vector<int> iterations,
                   std::vector<geometry_msgs::Point> point, double theta_0[], cv::Mat &theta_step,
                   cv::Mat &theta_dot_step, ofstream &f1, ofstream &f2, ofstream &f3, int &d) //std::vector<ofstream*> &files_ptr)
{
    double theta[NL];
    for(int i = 0; i < NL; i++)
    {
        theta[i] = initial_theta[i];
    }

    int row = -1;
    cv::Mat theta_all(iterations[0],NL,CV_64F,0.0);
    cv::Mat theta_dot_all(iterations[0],NL,CV_64F,0.0);

    for(int i = 0; i < num_steps; i++)
    {
        computeInvKinematics(point[i], theta_0, theta, iterations[i], theta_all,
                             theta_dot_all, f1, f2, f3);//files_ptr[0], files_ptr[1], files_ptr[3]);

        if(i==0)
        {
            row++;
            for(int j = 0; j < NL; j++)
            {
                theta_step.at<double>(row,j) = theta_all.at<double>(iterations[i]-1,j);
//                theta_dot_step.at<double>(row,j) = DEG2RAD(30);// for pypot control
                theta_dot_step.at<double>(row,j) = 0.2;// for ros dynamixel control
            }
        }
        else
        {
#ifdef CONST_VELOCITY
            row++;
            for(int j = 0; j < NL; j++)
            {
                theta_step.at<double>(row,j) = theta_all.at<double>(iterations[i]-1,j);
//                    theta_dot_step.at<double>(i,j) = DEG2RAD(5);// for pypot
                theta_dot_step.at<double>(i,j) = 0.025;// for ros dynamixel control
            }
#else
            for(int k = 0; k < iterations[i]/d; k++)
            {
                row++;
                for(int j = 0; j < NL; j++)
                {
                    theta_step.at<double>(row,j) = theta_all.at<double>(d*(k+1)-1,j);
                    theta_dot_step.at<double>(row,j) = theta_dot_all.at<double>(d*(k+1)-1,j);
                }
            }
#endif
        }

        for(int j = 0; j < NL; j++)
            theta[j] = theta_step.at<double>(row,j);
    }
}

void InterfaceControl::
computeInvKinematics(geometry_msgs::Point centroid, double desired_pose[], double initial_posn[],
                     int num_iterations, cv::Mat &theta_all, cv::Mat &theta_dot_all, ofstream &f1, ofstream &f2,
                     ofstream &f4)// ofstream* &f1, ofstream* &f2, ofstream* &f4)
{
    double dt = 0.01;// time step for iterations
    // Initial robot position, also represents current computed joint angles as iteration proceeds
    double theta[NL];
    for(int i = 0; i < NL; i++)
        theta[i] = initial_posn[i];

    double theta_0[NL];// joint angles which gives desired pose of the arm at the centroid
    for(int i = 0; i < NL; i++)
        theta_0[i] = desired_pose[i];

    double posn[NC];
//    int N = num_iterations;
    cv::Mat Posn_T(NC,1, CV_64F, 0.0);
    cv::Mat Posn_T_dot(NC,1, CV_64F, 0.0); //time-derivative
    cv::Mat Posn_C(NC,1, CV_64F, posn);
    cv::Mat Jp(3,NL,CV_64F, 0.0);
    cv::Mat Theta_dot(NL,1,CV_64F, 0.0);
    cv::Mat Error(NC,1,CV_64F, 0.0);
    cv::Mat Jpinv(NL,NC, CV_64F, 0.0);
    cv::Mat Jpos(6,3,CV_64F,0.0);
    cv::Mat singval(3,1,CV_64F,0.0); // singular values
    cv::Mat q0_dot(NL,1,CV_64F,0.0); // Self-motion velocity

    // Gains
    cv::Mat Kp(NC,NC,CV_64F,0.0);
    Kp = 5 * cv::Mat::eye(Kp.rows, Kp.cols, CV_64F);

    // Control Loop
//    ofstream f1("actual.txt");
//    ofstream f2("rconfig.txt");
//    ofstream f3("rank.txt");
//    ofstream f4("target.txt");
//    ofstream f5("error.txt");
//    ofstream f6("thnorm2.txt");
//    ofstream f7("thdot.txt");

    float Tmax = num_iterations*dt;
    double w = 1.0; //2*M_PI/Tmax;
    int idx = 0;
    float max_speed = 0;
    float thd_max = DEG2RAD(30);
    float thd_min = DEG2RAD(-30);

    for(double t = 0; t < Tmax; t = t + dt)
    {
//        //Target Pose (trajectory)
//#ifdef TARGET_TRAJ
//        Posn_T.at<double>(0,0) = 0.15;
//        Posn_T.at<double>(1,0) = 0.2*sin(w*t);
//        Posn_T.at<double>(2,0) = 0.3+0.2*cos(w*t);

//        // Derivative of Target pose
//        Posn_T_dot.at<double>(0,0) = 0.0;
//        Posn_T_dot.at<double>(1,0) = 0.2*w*cos(w*t);
//        Posn_T_dot.at<double>(2,0) = -0.2*w*sin(w*t);
//#endif

//#ifdef TARGET_POINT
        Posn_T.at<double>(0,0) = centroid.x+0.01;
        Posn_T.at<double>(1,0) = centroid.y-0.08;
        Posn_T.at<double>(2,0) = centroid.z;

        // Derivative of Target pose
        Posn_T_dot.at<double>(0,0) = 0.0;
        Posn_T_dot.at<double>(1,0) = 0.0;
        Posn_T_dot.at<double>(2,0) = 0.0;
//#endif

        for(int i = 0; i < NC; i++)
        {
            f4 << Posn_T.at<double>(i,0) << "\t";
        }
        f4 << endl;

        Error = Posn_T - Posn_C;

        position_jacobian(theta, Jp);

        cv::SVD::compute(Jp, singval, cv::SVD::NO_UV);

        double rank = 0;
        for(int i = 0; i < NC; ++i)
        {
            if(singval.at<double>(i,1) > 0.0001)
                rank = rank + 1;
        }
//        f3 << rank << endl;

        // null space optimization for obtained a desired joint configuration
        for(int i = 0; i < NL; ++i)
        {
            q0_dot.at<double>(i,0) = 2*(theta[i] -theta_0[i]) / NL * pow((theta_max[i] - theta_min[i]),2.0);
        }

        cv::invert(Jp, Jpinv, cv::DECOMP_SVD);

        // Joint angle velocity using Self-motion component
        Theta_dot = Jpinv * (Posn_T_dot + Kp*Error) - 20*(cv::Mat::eye(NL,NL,CV_64F)-Jpinv*Jp) * q0_dot;

        double th_norm = 0.0;
        for(int i = 0; i < NL; ++i)
        {
//            if(Theta_dot.at<double>(i) > thd_max)
//                Theta_dot.at<double>(i) = thd_max;
//            else if(Theta_dot.at<double>(i) < thd_min)
//                Theta_dot.at<double>(i) = thd_min;

            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);
            th_norm += theta[i]*theta[i];

//            f7 << Theta_dot.at<double>(i) << "\t";
        }

//        f7 << endl;

        th_norm = sqrt(th_norm);

//        f6 << th_norm << endl;

        coolarm_7dof_FK(theta, posn);
        joint_position(theta, Jpos);

        for(int i = 0; i < NL; ++i)
        {
            theta_all.at<double>(idx,i) = theta[i];
            theta_dot_all.at<double>(idx,i) = Theta_dot.at<double>(i);
                if(fabs(Theta_dot.at<double>(i)) > max_speed)
                    max_speed = fabs(Theta_dot.at<double>(i));
        }
        idx++;

        for(int i = 0; i < 6; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        for(int i = 0; i < NC; ++i)
        {
            Posn_C.at<double>(i) = posn[i];
            f1 << Posn_C.at<double>(i) << "\t";
        }
        f1 << endl;

//        f5 << t << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/3)<< endl;

    }
//    f1.close();
//    f2.close();
//    f3.close();
//    f4.close();
//    f5.close();
//    f6.close();
//    f7.close();
}

void InterfaceControl::
jla_orient(double pose_t[], double initial_theta[], double theta_final[],
           ofstream &f1, ofstream &f2, ofstream &f3, ofstream &f4, ofstream &f5)
{
    // Initial values
    double pose[6] = {0,0,0,0,0,0};
    double theta[NL] = {0,0,0,0,0,0,0};

    for(int i = 0; i < NL; i++)
        theta[i] = initial_theta[i];

    double dt = 0.01;
    bool VALID_FLAG;

    cv::Mat Pose_T(6,1, CV_64F, 0.0);
    cv::Mat Pose_T_dot(6,1, CV_64F, 0.0); //time-derivative

    cv::Mat Pose_C(6,1, CV_64F, pose);
    cv::Mat Jp(3,7,CV_64F, 0.0);
    cv::Mat Jw(3,7,CV_64F, 0.0);
    cv::Mat J(6,7,CV_64F, 0.0);
    cv::Mat Theta_dot(7,1,CV_64F, 0.0);
    cv::Mat Error(6,1,CV_64F, 0.0);
    cv::Mat Jpinv(7,6, CV_64F, 0.0);
    cv::Mat Jpos(6,3,CV_64F,0.0);
    cv::Mat singval(6,1,CV_64F,0.0); // singular values
    cv::Mat q0_dot(NL,1,CV_64F,0.0); // Self-motion velocity

    // Gains
    cv::Mat Kp(6,6,CV_64F,0.0);
    Kp = 5.0 * cv::Mat::eye(Kp.rows, Kp.cols, CV_64F);

    // Control Loop

//    ofstream f1("actual.txt");
//    ofstream f2("rconfig.txt");
//    ofstream f3("rank.txt");
//    ofstream f4("target.txt");
//    ofstream f5("error.txt");
    int Tmax = 100;
    double w = 1.0; //2*M_PI/Tmax;
    for(double t = 0; t < Tmax; t = t + dt)
    {
        //Target Pose (trajectory)
        Pose_T.at<double>(0,0) = pose_t[0];//0.15;
        Pose_T.at<double>(1,0) = pose_t[1];//0.2*sin(w*t);
        Pose_T.at<double>(2,0) = pose_t[2];//0.3+0.2*cos(w*t);
        Pose_T.at<double>(3,0) = pose_t[4];
        Pose_T.at<double>(4,0) = pose_t[5];
        Pose_T.at<double>(5,0) = pose_t[3];

        // Derivative of Target pose

        Pose_T_dot.at<double>(0,0) = 0.0;
        Pose_T_dot.at<double>(1,0) = 0.0;//0.2*w*cos(w*t);
        Pose_T_dot.at<double>(2,0) = 0.0;//-0.2*w*sin(w*t);
        Pose_T_dot.at<double>(3,0) = 0.0;
        Pose_T_dot.at<double>(4,0) = 0.0;
        Pose_T_dot.at<double>(5,0) = 0.0;


        for(int i = 0; i < 6; i++)
        {
            f4 << Pose_T.at<double>(i,0) << "\t";
        }
        f4 << endl;


        Error = Pose_T - Pose_C;

        position_jacobian(theta, Jp);
        orientation_jacobian(theta, Jw);
        //angular_jacobian(theta, Jw);  // Does not work ....

        cv::vconcat(Jp, Jw, J);

        cv::SVD::compute(J, singval, cv::SVD::NO_UV);

        double rank = 0;
        for(int i = 0; i < 7; ++i)
        {
            if(singval.at<double>(i,1) > 0.0001)
                rank = rank + 1;
        }
        f3 << rank << endl;

        cv::invert(J, Jpinv, cv::DECOMP_SVD);

//        //Control Input: Joint Angle velocities
//        Theta_dot = Jpinv * (Pose_T_dot + Kp*Error);

        // null space optimization for obtained a desired joint configuration
        for(int i = 0; i < NL; ++i)
        {
            q0_dot.at<double>(i,0) = 2*(theta[i]) / NL * pow((theta_max[i] - theta_min[i]),2.0);
        }

        // Joint angle velocity using Self-motion component
        Theta_dot = Jpinv * (Pose_T_dot + Kp*Error) - 50*(cv::Mat::eye(NL,NL,CV_64F)-Jpinv*J) * q0_dot;

        double theta_itmd[NL];
        for(int i = 0; i < NL; ++i)
            theta_itmd[i] = theta[i] + dt * Theta_dot.at<double>(i);

        bool flag = true;

        for(int i = 0; i < NL; i++)
        {
            if(theta_itmd[i] > theta_max[i])
            {
                flag = false;
                std::cout << "Joint " << i << ": " << theta_itmd[i]
                          << " > " << theta_max[i] << std::endl;
                theta_itmd[i] = theta_max[i];
            }
            else if(theta_itmd[i] < theta_min[i])
            {
                flag = false;
                std::cout << "Joint " << i << ": " << theta_itmd[i]
                          << " < " << theta_min[i] << std::endl;
                theta_itmd[i] = theta_min[i];
            }
        }

        if(!flag)
        {
            std::cout << "Theta: [";
            for(int i = 0; i < NL; i++)
            {
                std::cout << " " << theta_itmd[i];
                theta[i] = theta_itmd[i];
            }
            std::cout << "]" << std::endl;
            std::cout << "Iteration: " << t/dt << ", t: " << t << std::endl;
            std::cout << "EXCEED LIMITS...!!!" << std::endl;
        }

        for(int i = 0; i < NL; i++)
            theta[i] = theta_itmd[i];

        coolarm_pose_fk(theta, pose, VALID_FLAG);
        joint_position(theta, Jpos);

        if(VALID_FLAG)
        {
            for(int i = 0; i < 6; ++i)
            {
                Pose_C.at<double>(i) = pose[i];
                f1 << Pose_C.at<double>(i) << "\t";
            }
            f1 << endl;
        }

        double cur_error = sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0);
        f5 << t << "\t" << cur_error << endl;

        if(cur_error < 0.01)
            break;
    }

    for(int i = 0; i < NL; i++)
        theta_final[i] = theta[i];

    cout << "POSE_T: [";
    for(int i = 0; i < 6; ++i)
        cout << pose_t[i] << " ";
    cout << "]" << endl;

    cout << "POSE_A: [";
    for(int i = 0; i < 3; ++i)
        cout << pose[i] << " ";
    cout << pose[5] << " ";
    cout << pose[3] << " ";
    cout << pose[4] << " ";
    cout << "]" << endl;

    coolarm_pose_fk(theta, pose, VALID_FLAG);
    joint_position(theta, Jpos);

    for(int i = 0; i < 6; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f2 << Jpos.at<double>(i,j) << "\t";
        f2 << endl;
    }
    f1 << endl << endl;
    f2 << endl << endl;
    f3 << endl << endl;
    f4 << endl << endl;
    f5 << endl << endl;

}

void InterfaceControl::
JacobianInvKinOrientation(double target_pose[], double initial_theta[], double theta_final[], cv::Mat &orient, ofstream &f1,
                          ofstream &f2, ofstream &f3, ofstream &f4)
{
    double pose_t[6];
    for(int i = 0; i < 3; i++)
        pose_t[i] = target_pose[i];
    pose_t[3] = target_pose[4];
    pose_t[4] = target_pose[5];
    pose_t[5] = target_pose[3];

    // Initial values
    double pose[6] = {0,0,0,0,0,0};// pose_t[3]: beta:y-axis, pose_t[4]: gamma:z-axis, pose_t[5]: alpha:x-axis
    double theta[NL];
    for(int i = 0; i < NL; i++)
        theta[i] = initial_theta[i];

    double dt = 0.01;
    bool VALID_FLAG;

    coolarm_pose_fk(theta, pose, VALID_FLAG);

    cv::Mat Pose_T(6,1, CV_64F, pose_t);
    cv::Mat Pose_C(6,1, CV_64F, pose);
    cv::Mat Jp(3,7,CV_64F, 0.0);
    cv::Mat Jw(3,7,CV_64F, 0.0);
    cv::Mat J(6,7,CV_64F, 0.0);
    cv::Mat Theta_dot(7,1,CV_64F, 0.0);
    cv::Mat Error(6,1,CV_64F, 0.0);
    cv::Mat Jpos(6,3,CV_64F, 0.0);

    Error = Pose_T - Pose_C;
    double prev_error = sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0);
    int idx = 0;

    // Control Loop
    for(double t = 0; t < 1000; t = t + dt)
    {
        position_jacobian(theta, Jp);
        orientation_jacobian(theta, Jw);

        cv::vconcat(Jp, Jw, J);
        double alpha = cv::norm(J.t()*Error) / cv::norm(J*J.t()*Error);

        Theta_dot = alpha * J.t() * Error;

        double theta_itmd[NL];
        for(int i = 0; i < NL; ++i)
        {
            theta_itmd[i] = theta[i] + dt * Theta_dot.at<double>(i);
        }

        bool flag = true;

        for(int i = 0; i < NL; i++)
        {
            if(theta_itmd[i] > theta_max[i])
            {
                flag = false;
                std::cout << "Joint " << i << ": " << theta_itmd[i]
                          << " > " << theta_max[i] << std::endl;
                theta_itmd[i] = theta_max[i];
            }
            else if(theta_itmd[i] < theta_min[i])
            {
                flag = false;
                std::cout << "Joint " << i << ": " << theta_itmd[i]
                          << " < " << theta_min[i] << std::endl;
                theta_itmd[i] = theta_min[i];
            }
        }

        coolarm_pose_fk(theta_itmd, pose, VALID_FLAG);

        if(VALID_FLAG)  // IF THE VALID_FLAG THEN WHAT TO DO?????
        {
            for(int i = 0; i < 6; ++i)
                Pose_C.at<double>(i) = pose[i];
        }

        Error = Pose_T - Pose_C;
        double cur_error = sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0);

        if(cur_error == 0 || (cur_error > prev_error && idx!=0))
        {
            break;
        }
        else
        {
            for(int i = 0; i < NL; i++)
                theta[i] = theta_itmd[i];
            idx++;
            prev_error = cur_error;
        }

        f1 << t << "\t" << cur_error << endl;

        joint_position(theta, Jpos);
        for(int i = 0; i < 6; i++)
        {
            for(int j = 0; j < 3; j++)
                f3 << Jpos.at<double>(i,j) << "\t";
            f3 << endl;
        }
        f3 << endl << endl;

        for(int i = 0; i < 3; i++ )
            f4 << Pose_C.at<double>(i) << "\t";
        f4 << endl;
    }

    for(int i = 0; i < NL; i++)
        theta_final[i] = theta[i];
    cout << "POSE_T: [";
    for(int i = 0; i < 6; ++i)
        cout << pose_t[i] << " ";
    cout << "]" << endl;

    cout << "POSE_A: [";
    for(int i = 0; i < 6; ++i)
        cout << pose[i] << " ";
    cout << "]" << endl;

    // store the final joints position
    for(int i = 0; i < 6; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f2 << Jpos.at<double>(i,j) << "\t";
        f2 << endl;
    }

    f1 << endl << endl;
    f4 << endl << endl;
    f2 << endl << endl;
}

void InterfaceControl::
getTime(cv::Mat &theta, cv::Mat &theta_dot_step, double initial_theta[], float time[])
{
    double prev_pos[theta.cols];
    for(int i=0; i<theta.cols; i++)
        prev_pos[i] = initial_theta[i];

    cout << "From getTime:" << endl;
    for(int i=0; i < theta.rows; i++)
    {
        cout << "step " << i+1 << ":[";
        float max_time = 0;
        for(int j=0; j < theta.cols; j++)
        {
            float t = (theta.at<double>(i,j) - prev_pos[j])/theta_dot_step.at<double>(i,j);
            cout << t << "\t";
            if(fabs(t) > max_time)
                max_time = fabs(t);

            prev_pos[j] = theta.at<double>(i,j);
        }
        time[i] = max_time;
        cout << "]" << endl;
    }
    cout << endl;
}

void InterfaceControl::
openGripperDynamixel(ros::Publisher &command_gripper, float wait_time_secs)
{
    std_msgs::Float64 theta_grip;

    theta_grip.data = -0.95; //open
    command_gripper.publish(theta_grip);

    ros::Duration(wait_time_secs).sleep();

}

void InterfaceControl::
setJointSpeed(cv::Mat &step_speed, std::vector<ros::ServiceClient> &jt_client)
{
//    ros::NodeHandle node;
    int step = 0;

    setSpeedJoint1(jt_client, step_speed.at<double>(step,0));
    setSpeedJoint2(jt_client, step_speed.at<double>(step,1));
    setSpeedJoint3(jt_client, step_speed.at<double>(step,2));
    setSpeedJoint4(jt_client, step_speed.at<double>(step,3));
    setSpeedJoint5(jt_client, step_speed.at<double>(step,4));
    setSpeedJoint6(jt_client, step_speed.at<double>(step,5));
    setSpeedJoint7(jt_client, step_speed.at<double>(step,6));
}

void InterfaceControl::
reachPointDynamixel(std::vector<ros::Publisher> &command_pub, std::vector<ros::ServiceClient> &client, cv::Mat &theta_step,
                    cv::Mat &theta_dot_step, float time[], bool give_jtspeed)
{
    std::cout << "ARM reaches goal position in " << theta_step.rows << " steps" << std::endl;

    for(int k = 0; k < theta_step.rows; k++)
    {
        // send the calculated angles to the robot
        std_msgs::Float64 theta[theta_step.cols];

//        double secs = ros::Time::now().toSec();
        // Set the joint motor speed
        cv::Mat joint_speed(theta_dot_step.row(k));
        if(give_jtspeed)
        {
//            std::cout << "setting speed for motors" << std::endl;
            setJointSpeed(joint_speed, client);
        }

        bool flag = true;
//        std::cout << "Theta to reach:[";
//        for(int i = 0; i < NL; i++)
//        {
////            std::cout << " " << theta_step.at<double>(k,i);
//            if(theta_step.at<double>(k,i) > theta_max[i])
//            {
//                flag = false;
//                std::cout << "Theta target is greater than limit for joint " << i << ": theta_t=" << theta_step.at<double>(k,i)
//                          << " theta_max=" << theta_max[i];
//            }
//            else if(theta_step.at<double>(k,i) < theta_min[i])
//            {
//                flag = false;
//                std::cout << "Theta target is lesser than limit for joint " << i << ": theta_t=" << theta_step.at<double>(k,i)
//                          << " theta_min=" << theta_min[i];
//            }
//        }
//        std::cout << std::endl;


        if(flag)
        {
            for(int i = 0; i < theta_step.cols; ++i)
            {
                theta[i].data = theta_step.at<double>(k,i);
                command_pub[i].publish(theta[i]);
            }
            float t;
            if(k==0)
            {

//                if(theta_step.rows > 1)
                    t = time[k] - 0.55*time[k];//0.55
//                else
//                {
//#ifdef CONST_VELOCITY
//                t = time[k];
//#endif
//                }

            }
            else
            {
    #ifdef CONST_VELOCITY
                t = time[k];//-0.4*time[k];//0.575
    #else
                t = time[k]*4;
    #endif
            }
//            if(theta_step.rows != 1)
                ros::Duration(t).sleep();
        }
    }
}

void InterfaceControl::
closeGripperDynamixel(ros::Publisher &command_gripper)
{
    std_msgs::Float64 theta_grip;
    theta_grip.data = -1; //start with open position

    float d[2];
    while(force_sensor[0].data < 620 && force_sensor[1].data < 670)
    {
        theta_grip.data = theta_grip.data+0.02;
        if(theta_grip.data > 0.95)
        {
            break;
//            exit(0);
        }

        command_gripper.publish(theta_grip);

        d[0] = force_sensor[0].data;
        d[1] = force_sensor[1].data;
        std::cout << "gripper: " << theta_grip.data << " force: " << d[0] << ", " << d[1] << std::endl;
        ros::spinOnce();
        while(d[0] == force_sensor[0].data && d[1] == force_sensor[1].data)
        {
          ros::spinOnce();
        }
    }
    std::cout << "Break at gripper: " << theta_grip.data << " force: " << force_sensor[0].data << ", " << force_sensor[1].data
              << std::endl;
}

void InterfaceControl::
openGripperPoppy(ros::Publisher &poppy_pub, ros::Publisher &poppy_speed, float wait_time_secs)
{
    std_msgs::Float64 theta_grip;
    std_msgs::Float64 speed;

    theta_grip.data = RAD2DEG(-0.95); //open
    speed.data = 20;// 20 degrees per second speed
//    std::cout << "Publishing\n";
    poppy_pub.publish(theta_grip);
    poppy_speed.publish(speed);

//    std::cout << "wait time\n";
    ros::Duration(wait_time_secs).sleep();
}

void InterfaceControl::
reachPointPoppy(std::vector<ros::Publisher> &poppy_pos, std::vector<ros::Publisher> &poppy_speed, cv::Mat &theta_step,
                cv::Mat &theta_dot_step, float time[])
//void InterfaceControl::
//reachPointPoppy(ros::Publisher &poppy_pos, ros::Publisher &poppy_speed, cv::Mat &theta_step,
//                cv::Mat &theta_dot_step)
{
    std::cout << "ARM reaches goal position in " << theta_step.rows << " steps" << std::endl;

    for(int k = 0; k < theta_step.rows; k++)
    {
        std_msgs::Float64 theta[theta_step.cols];
        std_msgs::Float64 speed[theta_step.cols];

        for(int i = 0; i < theta_step.cols; ++i)
        {
            theta[i].data = RAD2DEG(theta_step.at<double>(k,i));
            speed[i].data = RAD2DEG(theta_dot_step.at<double>(k,i));

            poppy_pos[i].publish(theta[i]);
            poppy_speed[i].publish(speed[i]);

        }

        float t;
        if(k==0)
            t = time[k] + 0.18*time[k];
        else
            t = time[k] + 0.225*time[k];
        ros::Duration(t).sleep();
    }
}

void InterfaceControl::closeGripperPoppy(ros::Publisher &poppy_pub,  ros::Publisher &poppy_speed)
{
    std_msgs::Float64 theta_grip;
    std_msgs::Float64 speed;

    theta_grip.data = RAD2DEG(-0.95); //start with open position
    speed.data = 20;// 20 degrees per second speed
//    std::cout << "Closing Gripper\n";
    while(force_sensor[0].data < 200 && force_sensor[1].data < 250)
    {
        theta_grip.data = theta_grip.data+RAD2DEG(0.05);
        if(theta_grip.data > RAD2DEG(0.9))
        {
            break;
//            exit(0);
        }

        poppy_pub.publish(theta_grip);
        poppy_speed.publish(speed);

        float d1 = force_sensor[0].data;
        float d2 = force_sensor[1].data;
        std::cout << "gripper: " << theta_grip.data << " force: " << d1 << ", " << d2 << std::endl;
        ros::spinOnce();
        while(d1 == force_sensor[0].data && d2 == force_sensor[1].data)
        {
          ros::spinOnce();
//          std::cout << "d: " << d1  << ", " << d2 << " force: " << force_sensor[0].data
//                    << force_sensor[1].data << std::endl;
        }
    }
    std::cout << "Gripper Closed\n";
}

void InterfaceControl::
getRotationAngles(cv::Mat &R, double angles[])
{
//    double beta = asin(R.at<double>(0,2));
    bool VALID;
    // Beta - Pitch - Rotation about Y-axis. consider the positive value of the sqrt
//    angles[1] = atan2(R.at<double>(0,2), fabs(sqrt(pow(R.at<double>(1,2), 2) + pow(R.at<double>(2,2), 2.0) )) );

//    if(angles[1] > -M_PI/2.0 && angles[1] < M_PI/2.0)
//    {
//        // Gamma - Yaw- Rotation about Z-axis
//        angles[2] = atan2(-R.at<double>(0,1)/cos(angles[1]), R.at<double>(0,0)/cos(angles[1]));
//        // Alpha - Roll - Rotation about X-axis
//        angles[0] = atan2(-R.at<double>(1,2)/cos(angles[1]), R.at<double>(2,2)/cos(angles[1]));

//        VALID = true;
//    }
//    else if (angles[1] == M_PI/2.0)
//    {
//        angles[2] = 0.0;
//        angles[0] = atan2(R.at<double>(1,0), R.at<double>(1,1));
//        VALID = true;
//    }
//    else if(angles[1] == - M_PI/2.0)
//    {
//        angles[2] = 0.0;
//        angles[0] = atan2(-R.at<double>(1,0), R.at<double>(1,1));
//        VALID = true;
//    }
//    else
//    {
//        VALID = false;
//    }

    // Beta - Pitch - Rotation about Y-axis. consider the positive value of the sqrt
    angles[1] = atan2(-R.at<double>(2,0), fabs(sqrt(pow(R.at<double>(0,0), 2) + pow(R.at<double>(1,0), 2.0) )) );

    if(angles[1] > -M_PI/2.0 && angles[1] < M_PI/2.0)
    {
        // Alpha - Roll- Rotation about z-axis
        angles[2] = atan2(R.at<double>(1,0)/cos(angles[1]), R.at<double>(0,0)/cos(angles[1]));

        // Gamma - Yaw - Rotation about X-axis
        angles[0] = atan2(R.at<double>(2,1)/cos(angles[1]), R.at<double>(2,2)/cos(angles[1]));

        VALID = true;
    }
    else if (angles[1] == M_PI/2.0)
    {
        angles[2] = 0.0;
        angles[0] = atan2(R.at<double>(0,1), R.at<double>(1,1));

        VALID = true;
    }
    else if(angles[1] == -M_PI/2.0)
    {
        angles[2] = 0.0;
        angles[0] = -atan2(R.at<double>(0,1), R.at<double>(1,1));

        VALID = true;
    }
    else
    {
        VALID = false;
    }
}

void InterfaceControl::
getOrientationMatrix(cv::Mat &matrix)
{
    double val_x, val_y, val_z;

    // Get the normalized axis vector in to first column of matrix
    val_x = handle_pose[1].x - handle_pose[0].x;
    val_y = handle_pose[1].y - handle_pose[0].y;
    val_z = handle_pose[1].z - handle_pose[0].z;

    tf::Vector3 axis;
    axis.setX(val_x);
    axis.setY(val_y);
    axis.setZ(val_z);
    axis.normalize();

    matrix.at<double>(0,0) = axis.getX();
    matrix.at<double>(1,0) = axis.getY();
    matrix.at<double>(2,0) = axis.getZ();

    // Get the normalized normal vector in to third column of matrix
    val_x = handle_pose[2].x - handle_pose[0].x;
    val_y = handle_pose[2].y - handle_pose[0].y;
    val_z = handle_pose[2].z - handle_pose[0].z;

    tf::Vector3 normal;
    normal.setX(val_x);
    normal.setY(val_y);
    normal.setZ(val_z);

    normal.normalize();

    matrix.at<double>(0,2) = normal.getX();
    matrix.at<double>(1,2) = normal.getY();
    matrix.at<double>(2,2) = normal.getZ();

    // Get the normalized 3rd vector in to second column of matrix
    tf::Vector3 vector = normal.cross(axis);
    vector.normalize();

    matrix.at<double>(0,1) = vector.getX();
    matrix.at<double>(1,1) = vector.getY();
    matrix.at<double>(2,1) = vector.getZ();

    cout << "Axis: [" << matrix.at<double>(0,0) << " " << matrix.at<double>(1,0) << " " << matrix.at<double>(2,0) << "]" << endl;
    cout << "3Vector: [" << matrix.at<double>(0,1) << " " << matrix.at<double>(1,1) << " " << matrix.at<double>(2,1) << "]" << endl;
    cout << "Normal: [" << matrix.at<double>(0,2) << " " << matrix.at<double>(1,2) << " " << matrix.at<double>(2,2) << "]" << endl;

    return;
}
