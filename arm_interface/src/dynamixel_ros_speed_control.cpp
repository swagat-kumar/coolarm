#include "arm_interface/dynamixel_ros_speed_control.h"

//void setSpeedJoint1(ros::NodeHandle &node, double velocity)
void setSpeedJoint1(std::vector<ros::ServiceClient> &jt_client, double velocity)
{
//    ros::ServiceClient jt_client;// Service to set speed motor
//    jt_client = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint1_controller/set_speed");

    dynamixel_controllers::SetSpeed jt_set_speed;

    jt_set_speed.request.speed = velocity;
//    std::cout << "Jt 1: " << jt_set_speed.request.speed << std::endl;
    jt_client[0].call(jt_set_speed);

    return;
}

//void setSpeedJoint2(ros::NodeHandle &node, double velocity)
void setSpeedJoint2(std::vector<ros::ServiceClient> &jt_client, double velocity)
{
//    ros::ServiceClient jt_client_1;// Service to set speed motor
//    jt_client_1 = node.serviceClient<dynamixel_controllers::SetSpeed>("/dummy_joint2_controller/set_speed");
//    ros::ServiceClient jt_client_2;// Service to set speed motor
//    jt_client_2 = node.serviceClient<dynamixel_controllers::SetSpeed>("/dummy_joint8_controller/set_speed");

    dynamixel_controllers::SetSpeed jt_set_speed_1;
    dynamixel_controllers::SetSpeed jt_set_speed_2;

    jt_set_speed_1.request.speed = velocity;
    jt_set_speed_2.request.speed = velocity;
//    std::cout << "Jt 2: " << jt_set_speed_1.request.speed << std::endl;
    jt_client[1].call(jt_set_speed_1);
    jt_client[2].call(jt_set_speed_2);

    return;
}

//void setSpeedJoint3(ros::NodeHandle &node, double velocity)
void setSpeedJoint3(std::vector<ros::ServiceClient> &jt_client, double velocity)
{
//    ros::ServiceClient jt_client;// Service to set speed motor
//    jt_client = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint3_controller/set_speed");

    dynamixel_controllers::SetSpeed jt_set_speed;

    jt_set_speed.request.speed = velocity;
//    std::cout << "Jt 3: " << jt_set_speed.request.speed << std::endl;
    jt_client[3].call(jt_set_speed);

    return;
}

//void setSpeedJoint4(ros::NodeHandle &node, double velocity)
void setSpeedJoint4(std::vector<ros::ServiceClient> &jt_client, double velocity)
{
//    ros::ServiceClient jt_client;// Service to set speed motor
//    jt_client = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint4_controller/set_speed");

    dynamixel_controllers::SetSpeed jt_set_speed;

    jt_set_speed.request.speed = velocity;
//    std::cout << "Jt 4: " << jt_set_speed.request.speed << std::endl;
    jt_client[4].call(jt_set_speed);

    return;
}

//void setSpeedJoint5(ros::NodeHandle &node, double velocity)
void setSpeedJoint5(std::vector<ros::ServiceClient> &jt_client, double velocity)
{
//    ros::ServiceClient jt_client;// Service to set speed motor
//    jt_client = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint5_controller/set_speed");

    dynamixel_controllers::SetSpeed jt_set_speed;

    jt_set_speed.request.speed = velocity;
//    std::cout << "Jt 5: " << jt_set_speed.request.speed << std::endl;
    jt_client[5].call(jt_set_speed);

    return;
}

//void setSpeedJoint6(ros::NodeHandle &node, double velocity)
void setSpeedJoint6(std::vector<ros::ServiceClient> &jt_client, double velocity)
{
//    ros::ServiceClient jt_client;// Service to set speed motor
//    jt_client = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint6_controller/set_speed");

    dynamixel_controllers::SetSpeed jt_set_speed;

    jt_set_speed.request.speed = velocity;
//    std::cout << "Jt 6: " << jt_set_speed.request.speed << std::endl;
    jt_client[6].call(jt_set_speed);

    return;
}

//void setSpeedJoint7(ros::NodeHandle &node, double velocity)
void setSpeedJoint7(std::vector<ros::ServiceClient> &jt_client, double velocity)
{
//    ros::ServiceClient jt_client;// Service to set speed motor
//    jt_client = node.serviceClient<dynamixel_controllers::SetSpeed>("/joint7_controller/set_speed");

    dynamixel_controllers::SetSpeed jt_set_speed;

    jt_set_speed.request.speed = velocity;
//    std::cout << "Jt 7: " << jt_set_speed.request.speed << std::endl;
    jt_client[7].call(jt_set_speed);

    return;
}

//void setSpeedGripper(ros::NodeHandle &node, double velocity)
void setSpeedGripper(std::vector<ros::ServiceClient> &jt_client, double velocity)
{
//    ros::ServiceClient jt_client;// Service to set speed motor
//    jt_client = node.serviceClient<dynamixel_controllers::SetSpeed>("/gripper_open_controller_1/set_speed");

    dynamixel_controllers::SetSpeed jt_set_speed;

    jt_set_speed.request.speed = velocity;
//    std::cout << "Gripper: " << jt_set_speed.request.speed << std::endl;
    jt_client[8].call(jt_set_speed);

    return;
}
