#include "arm_interface/callbacks.h"

void jtStateCallback1(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback2(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback3(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback4(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback5(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback6(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback7(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback8(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
//void handleDataCallback(const std_msgs::Float64MultiArray::ConstPtr &array, std::vector<double> &point,
//                        std_msgs::Bool &flag_frr)
//{
//  for(int i = 0; i < array->data.size(); i++)
//    point[i] = array->data[i];

//  flag_frr.data = true;
//}
void handleDataCallback(const grasp_affordance::HandlePose::ConstPtr &pose, std::vector<geometry_msgs::Point> &point,
                        std_msgs::Bool &flag_frr, tf::StampedTransform transform)
{
    point.resize(0);
    geometry_msgs::Point pt;

    pt.x = pose->centroid.x;
    pt.y = pose->centroid.y;
    pt.z = pose->centroid.z;
    point.push_back(pt);


    pt.x = pose->axis.x;
    pt.y = pose->axis.y;
    pt.z = pose->axis.z;
    point.push_back(pt);

    pt.x = pose->normal.x;
    pt.y = pose->normal.y;
    pt.z = pose->normal.z;

    // commented while coding for including orientation
//    tf::Vector3 pt_v3(pt.x, pt.y, pt.z);
//    tf::Vector3 tf_ptv3 = transform * pt_v3;// tf_ptv3 = Rotation_matrix*pt_v3 + translation_point
//    tf::Vector3 z_axis(0,0,1);
//    float v = tf_ptv3.dot(z_axis);

//    // If the normal vector is along the direction of z-axis of depth_optical_frame then reverse its direction
//    // i.e. Normal vector is in the cone of 0-90 degrees from the z-axis of depth_optical_frame then reverse its direction
//    if(v > 0)
//    {
//        pt.x -= 3*(pt.x - pose->centroid.x);
//        pt.y -= 3*(pt.y - pose->centroid.y);
//        pt.z -= 3*(pt.z - pose->centroid.z);
//    }
    point.push_back(pt);

    flag_frr.data = true;
}

void handleGenFlagCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag)
{
  flag.data = ob->data;
}

void kinectDataCountCallback(const std_msgs::Int32::ConstPtr &ob, std_msgs::Int32 &data_count)
{
  data_count.data = ob->data;
}

void gripperResetCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag)
{
  flag.data = ob->data;
}
void forceSensorCallback(const std_msgs::Float64::ConstPtr &ob, std_msgs::Float64 &force)
{
  force.data = ob->data;
}

//void motorStateCallback(const dynamixel_msgs::JointState::ConstPtr &jointMsg, bool &is_moving, double velocity,
//                        double current_pos, double error)
//{
//  is_moving = jointMsg->is_moving;
//  velocity = jointMsg->velocity;
//  current_pos = jointMsg->current_pos;
//  error = jointMsg->error;
//  return;
//}

//========================================
// Signal Handling
// does not work as the controller passes on to the subscribers
void signal_callback_handler(int signum)
{
   printf("Caught signal %d\n",signum);
   std::cout << "Press 'y' to exit" << std::endl;

   char ans;
   if(scanf(" %c", &ans) == 'y')
       exit(signum);
}

