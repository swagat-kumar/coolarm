/* Solving Inverse Kinematics using Inv-Fwd Scheme

  * Interfaced with actual CoolARM hardware.

  *  Run the program 'main.cpp' that is within the folder fwdkin/ folder

  * Step 1: #define CLUSTER
  * Step 2: #define RBF_TRAIN
  * Step 3: INVERT_RBF

  * Execute Coolarm related launch files

  * ------------------------------------------------------------- */

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <nnet.h>
#include <ksom_vmc.h>
#include <mymatrix.h>
#include <gnuplot_ci.h>
#include <opencv2/opencv.hpp>
#include <coolarm_fk.h>
#include <dynamixel_msgs/JointState.h>
#include <csignal>
#include <cstdio>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>
#include <ros/publisher.h>
#include <ros/subscriber.h>
#include <std_msgs/Int32.h>


using namespace std;
using namespace cv;
using namespace nnet;
using namespace vmc;
using namespace gnuplot_ci;

#define NC 3   // Dimension of Cartesian Space

// Step - 3: Invert the Network to find IK solution
#define INVERT_RBF
//#define PLOT
#define ROBOT
//#define COMMENTS_FOR_DEBUGGING
#define ROBOT_GRASP_DEMO
#define REACH_POINT

//==========================================================
// callback function
void jtStateCallback1(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
    //cout << "angle = " << angle << endl;
}
void jtStateCallback2(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback3(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback4(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback5(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback6(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback7(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback8(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void handleDataCallback(const std_msgs::Float64MultiArray::ConstPtr &array, std::vector<double> &point,
                        std_msgs::Bool &flag_frr)
{
  for(int i = 0; i < array->data.size(); i++)
    point[i] = array->data[i];

  flag_frr.data = true;
}
void handleGenFlagCallback(const std_msgs::Bool::ConstPtr &ob, std_msgs::Bool &flag)
{
  flag.data = ob->data;
}

void kinectDataCountCallback(const std_msgs::Int32::ConstPtr &ob, std_msgs::Int32 &data_count)
{
  data_count.data = ob->data;
}

//========================================
// Signal Handling
// does not work as the controller passes on to the subscribers
void signal_callback_handler(int signum)
{
   printf("Caught signal %d\n",signum);
   cout << "Press 'y' to exit" << endl;

   char ans;
   if(scanf(" %c", &ans) == 'y')
       exit(signum);
}
//===================================================
int main(int argc, char **argv)
{
    signal(SIGINT, signal_callback_handler);

#ifdef ROBOT
    //==========================================================
    // Setup for interacting with robot

    ros::init(argc, argv, "arm_client");
    ros::NodeHandle Joint[8];
    ros::Publisher command_pub[8];

    command_pub[0] = Joint[0].advertise<std_msgs::Float64>("/joint1_controller/command", 1000);
    command_pub[1] = Joint[1].advertise<std_msgs::Float64>("/dual_servo_controller/command", 1000);
    command_pub[2] = Joint[2].advertise<std_msgs::Float64>("/joint3_controller/command", 1000);
    command_pub[3] = Joint[3].advertise<std_msgs::Float64>("/joint4_controller/command", 1000);
    command_pub[4] = Joint[4].advertise<std_msgs::Float64>("/joint5_controller/command", 1000);
    command_pub[5] = Joint[5].advertise<std_msgs::Float64>("/joint6_controller/command", 1000);
    command_pub[6] = Joint[6].advertise<std_msgs::Float64>("/joint7_controller/command", 1000);
    command_pub[7] = Joint[7].advertise<std_msgs::Float64>("/gripper_open_controller_1/command", 1000);

    ros::Rate loop_rate(10);


    double jtangle[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    ros::Subscriber state_sub[8];

    state_sub[0] = Joint[0].subscribe<dynamixel_msgs::JointState>("/joint1_controller/state",
                                                                  1, boost::bind(jtStateCallback1, _1, boost::ref(jtangle[0])));
    state_sub[1] = Joint[1].subscribe<dynamixel_msgs::JointState>("/dummy_joint2_controller/state",
                                                                  1, boost::bind(jtStateCallback2, _1, boost::ref(jtangle[1])));
    state_sub[2] = Joint[2].subscribe<dynamixel_msgs::JointState>("/joint3_controller/state",
                                                                  1, boost::bind(jtStateCallback3, _1, boost::ref(jtangle[2])));
    state_sub[3] = Joint[3].subscribe<dynamixel_msgs::JointState>("/joint4_controller/state",
                                                                  1, boost::bind(jtStateCallback4, _1, boost::ref(jtangle[3])));
    state_sub[4] = Joint[4].subscribe<dynamixel_msgs::JointState>("/joint5_controller/state",
                                                                  1, boost::bind(jtStateCallback5, _1, boost::ref(jtangle[4])));
    state_sub[5] = Joint[5].subscribe<dynamixel_msgs::JointState>("/joint6_controller/state",
                                                                  1, boost::bind(jtStateCallback6, _1, boost::ref(jtangle[5])));
    state_sub[6] = Joint[6].subscribe<dynamixel_msgs::JointState>("/joint6_controller/state",
                                                                  1, boost::bind(jtStateCallback7, _1, boost::ref(jtangle[6])));

    state_sub[7] = Joint[7].subscribe<dynamixel_msgs::JointState>("/gripper_open_controller_1/state",
                                                                  1, boost::bind(jtStateCallback8, _1, boost::ref(jtangle[7])));

#endif


#ifdef ROBOT_GRASP_DEMO
    ros::NodeHandle affordance;

    ros::Subscriber sub_handle_data;
    ros::Subscriber sub_fk;
    ros::Subscriber sub_k_cnt;

    ros::Publisher pub_fr;
    ros::Publisher pub_frr;
    ros::Publisher pub_r_cnt;

    std::vector<double> centroid(3);
    std_msgs::Bool f_kinect;
    f_kinect.data = false;

    std_msgs::Bool f_robot;
    std_msgs::Bool f_robot_rcvd;
    std_msgs::Int32 r_cnt;
    r_cnt.data = 0;
    std_msgs::Int32 k_cnt;
    k_cnt.data = -1;

    f_robot.data = true;
    f_robot_rcvd.data = false;

    sub_handle_data = affordance.subscribe<std_msgs::Float64MultiArray>("/handle/data", 1,
                                                    boost::bind(handleDataCallback, _1, boost::ref(centroid),
                                                    boost::ref(f_robot_rcvd)));

    sub_fk = affordance.subscribe<std_msgs::Bool>("/flag_kinect", 1,
                                        boost::bind(handleGenFlagCallback, _1, boost::ref(f_kinect)));

    sub_k_cnt = affordance.subscribe<std_msgs::Int32>("/data_count_kinect", 1,
                                        boost::bind(kinectDataCountCallback, _1, boost::ref(k_cnt)));

    pub_fr = affordance.advertise<std_msgs::Bool>("/flag_robot_reached", 1);

    pub_frr = affordance.advertise<std_msgs::Bool>("/flag_robot_received", 1);

    pub_r_cnt = affordance.advertise<std_msgs::Int32>("/data_count_robot", 1);

    int count = 1;
    bool flag_robot_pose = false;
#else
    int count = 2;
#endif

    //======================================================================

#ifdef INVERT_RBF
    const int nx = 7;
    const int ny = 7;
    const int nz = 7;
    //----------------------------------------
    //Create a KSOM Network
    int N[5] = {nx, ny, nz, NC, NL};

    int ncmax = 100;

    //Create a SOM lattice
    ksom3_sc A(N, ncmax);

    A.netinfo();


    //=====================================

    //create a RBF network

    int noc = 700;

    int N2[3] = {NL, noc, NC};
    double af[2] = {5, 0.5}; //gaussian activation
    double param[3] = {1, -1, 0}; //input range = -1 to 1

    rbfn B(N2, af, param);

    cout << "Network Information" << B << endl;

    //==============================================

    // Load Neural Network parameters
    // Run "CLUSTER" and "TRAIN_RBF" modules

    cout << "\nLoading ANN parameters .." << endl;

    if( (access("/home/meenu/tcs/robot_arm_project/catkin_ws/src/arm_interface/src/input/ksom_sc_weights.txt",F_OK) ||
         access("/home/meenu/tcs/robot_arm_project/catkin_ws/src/arm_interface/src/input/ksom_sc_angles.txt", F_OK) ||
            access("/home/meenu/tcs/robot_arm_project/catkin_ws/src/arm_interface/src/input/rbfn_par_bp.txt",F_OK) ) != -1)
    {
        A.load_weights("/home/meenu/tcs/robot_arm_project/catkin_ws/src/arm_interface/src/input/ksom_sc_weights.txt");
        A.load_angles("/home/meenu/tcs/robot_arm_project/catkin_ws/src/arm_interface/src/input/ksom_sc_angles.txt");
        B.load_parameters("/home/meenu/tcs/robot_arm_project/catkin_ws/src/arm_interface/src/input/rbfn_par_bp.txt");

        cout << ".... done!" << endl;
    }
    else
    {
        cout << "ANN parameter files are missing ..." << endl;
        cout << "Run CLUSTER and TRAIN_RBF modules first" << endl;
        exit(-1);
    }

    //========================================

    const int np = 6; // no. of rows in jp
    Mat jp(6,3,CV_64F,0.0); // stores joint positions


    double qt[NL], qtn[NL];       // corresponding target joint angle
    double xr[NC], xrn[NC];       // actual robot end-effector position
    double xt[NC], xtn[NC];       // Target end-effector position
    double xc[NC], xcn[NC];       // network (RBFN) output


    double *th_c = myvector(NL);
    double *th_cn = myvector(NL);
    double *qc = myvector(NL);
    double *qcn = myvector(NL);

    //----------------------------
    //initial robot configuration

    th_c[0] = 0.0;
    th_c[1] = DEG2RAD(50);
    th_c[2] = 0.0;
    th_c[3] = DEG2RAD(20);
    th_c[4] = 0.0;
    th_c[5] = 0.0;
    th_c[6] = 0.0;

    while(ros::ok())
    {
      for(int cnt = 0; cnt < count; cnt++) // Operating for Single handle centroid
      {

#ifndef ROBOT_GRASP_DEMO
        cout << "\nCount = " << cnt << endl;
#endif
        ofstream ft("angle.txt");
        ofstream fr("rconfig.txt");
        ofstream fp("efposn.txt");
        ofstream fe1("error.txt");
        ofstream ftp("targetpos.txt");


#ifdef ROBOT_GRASP_DEMO
        std::cout << "Robot wants count: " << r_cnt.data << std::endl;


        // Wait for the kinect to generate handle centroid
        while((!f_kinect.data && f_robot.data) || k_cnt.data != r_cnt.data)
        {
          pub_fr.publish(f_robot);
          pub_r_cnt.publish(r_cnt);

          ros::spinOnce();
#ifdef REACH_POINT
          if(flag_robot_pose)
            system("python /home/meenu/tcs/robot_arm_project/catkin_ws/src/cool1000_ros_experimental/cool1000_controller/scripts/pose_zero_dummy.py");
#endif

          flag_robot_pose = false;
        }
        f_robot.data = false;
        f_robot_rcvd.data = false;



#ifdef COMMENTS_FOR_DEBUGGING
        std::cout << "robot tries to collect data: " << r_cnt.data << std::endl;
#endif

        while(!f_robot_rcvd.data)
        {
          ros::spinOnce();

          pub_fr.publish(f_robot);
          pub_frr.publish(f_robot_rcvd);
        }

        std::cout << "Robot received count: " << r_cnt.data << std::endl;

        for(int i = 0; i < NC; i++)
          xt[i] = centroid[i];


#else
        //--------------------------
        //Generate a Target point
        generate_data(xt, qt);

#endif

        //normalize the target position
        cart_posn_normalize(xt, xtn);

#ifndef ROBOT_GRASP_DEMO
        angle_normalize(qt, qtn);
#endif

        //Block 1: target position
        //writing position to txt file
        for(int i = 0; i < NC; i++)
          ftp << xt[i] << "\t";

#ifndef ROBOT_GRASP_DEMO
        //writing joint angles to txt file
        for(int i = 0; i < NL; i++)
          ftp << qt[i] << "\t";
        ftp << endl ;
#endif

#ifdef COMMENTS_FOR_DEBUGGING
        //---------------------------------
        cout << "Target position =" << xt[0] << "\t" << xt[1] << "\t" << xt[2] << endl;

#endif

        //Initial robot configurationspl
        joint_position(th_c, jp);

        for(int i = 0; i < np; i++)
        {
          for(int j = 0; j < 3; j++)
            fr << jp.at<double>(i,j) << "\t";
          fr << endl;
        }
        fr << endl << endl;

        //-------

        //Current robot end-effector position
        coolarm_7dof_FK(th_c, xr);

        //current end-effector position obtained from network
        angle_normalize(th_c, th_cn);
        B.network_output(th_cn, xcn);
        cart_posn_denormalize(xcn, xc);

        // Block 2: Initial robot position
        for(int i = 0; i < NC; i++)
          fp << xc[i] << "\t";
        for(int i = 0; i < NC; i++)
          fp << xr[i] << "\t";
        fp << endl << endl << endl;

        //-----------------------------------------
        //Initial guess (Lazy arm joint angle computation)
//        qcn = A.LA_theta_output(xtn, th_cn, 0.5);  // last value is sigma (std. dev)
        A.LA_theta_output(xtn, th_cn, 0.5, qcn); // function is changed
        //qcn = A.LA_theta_output(xtn, th_cn);
        angle_denormalize(qcn, qc);


        //Initial position of robot end-effector position
        coolarm_7dof_FK(qc, xr);

        //Initial end-effector position obtained from NN
        B.network_output(qcn, xcn);
        cart_posn_denormalize(xcn, xc);
        //------------------------------------------

        double se1;
        int step = 0;
        do
        {
          // Block 3: Position obtained from Inv-Fwd Scheme
          for(int i = 0; i < NC; i++)
            fp << xc[i] << "\t";
          for(int i = 0; i < NC; i++)
            fp << xr[i] << "\t";
          fp << endl;

          joint_position(qc, jp);

          for(int i = 0; i < np; i++)
          {
            for(int j = 0; j < 3; j++)
              fr << jp.at<double>(i,j) << "\t";
            fr << endl;
          }
          fr << endl << endl;

          //----------

          //update input using gradient_descent
          // qcn is the updated input

          B.input_jacobian();
          B.update_input_gd(0.02, xtn, qcn);

          //new end-effector position
          B.network_output(qcn, xcn);
          cart_posn_denormalize(xcn, xc);

          for(int i = 0; i < NL; i++)
            ft << qcn[i] << "\t";
          ft << endl;

          angle_denormalize(qcn, qc);
          coolarm_7dof_FK(qc, xr);
          cart_posn_normalize(xr, xrn);

          //Forward training
          B.backprop(xrn, 0.05, 0.05);


          se1 = 0.0;
          for(int i = 0;i < NC; i++)
            se1 += pow((xr[i] - xt[i]), 2.0);
          se1 = sqrt(se1);

          fe1 << se1 << endl;

          step = step + 1;

          if(step > 1000)
            break;
        }while(se1 > 0.002);

        ft.close();
        fp.close();
        fr.close();
        fe1.close();
        ftp.close();

#ifdef COMMENTS_FOR_DEBUGGING
        cout << "\nNo. of steps = " << step << endl;

        cout << "\nFinal Joint angle values = ";
#endif
        cout << "Final positioning Error = " << se1 << endl;

        for(int i = 0; i < NL; ++i)
        {
          th_c[i] = qc[i];

#ifdef COMMENTS_FOR_DEBUGGING
          cout << qc[i] << '\t';
#endif
        }
#ifdef COMMENTS_FOR_DEBUGGING
        cout << endl << endl;
#endif




        //--------------------------------
        // Enable GNUPLOT Interface
#ifdef PLOT
        GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
        GP_handle G2("/usr/bin/", "Iteration Steps", "Error");
        G1.gnuplot_cmd("splot 'targetpos.txt' u 1:2:3 w p pt 6 lt 3 ps 2 t 'Target Position'");
        G2.gnuplot_cmd("plot 'error.txt' w l lw 2 t 'mse'");
        G1.gnuplot_cmd("replot 'rconfig.txt' index 1:400 u 1:2:3 w lp pt 2 lw 2 lt 1 notitle");
        G1.gnuplot_cmd("replot 'efposn.txt' index 1:400 u 4:5:6 w p pt 4 lt 2 notitle");
//          cout << "\nPress Enter to Drive the Robot ..." << endl;


//          getchar();
  //        G1.gnuplot_cmd("set terminal postscript eps enhanced color solid lw 2");
  //        G2.gnuplot_cmd("set terminal postscript eps enhanced color solid lw 2");
  //        G1.gnuplot_cmd("set output 'rconfig.eps'");
  //        G2.gnuplot_cmd("set output 'error.eps'");
  //        G1.gnuplot_cmd("replot");
  //        G2.gnuplot_cmd("replot");
  //        G1.gnuplot_cmd("set output");
  //        G2.gnuplot_cmd("set output");
  //        G1.gnuplot_cmd("set terminal x11");
  //        G2.gnuplot_cmd("set terminal x11");
#endif

#ifdef ROBOT

        int flag;

#ifdef COMMENTS_FOR_DEBUGGING
        cout << "\nl: robot moves to handle\nx: cancel and go to select another handle" << std::endl;
        char ch;
        std::cin >> ch;

        if(ch == 'l')
          flag = 1;
        else
          flag = 0;
#else
        flag = 1;
#endif

#ifdef REACH_POINT
        if(flag == 1)
        {
          // send the calculated angles to the robot
          std_msgs::Float64 theta[8];
          for(int i = 0; i < 7; ++i)
          {
            theta[i].data = qc[i];
            command_pub[i].publish(theta[i]);
          }
          double prev;
          while(ros::ok())
          {
            ros::spinOnce();
            loop_rate.sleep();

            double serr = 0.0;
            for(int i = 0; i < 7; ++i)
              serr += pow((qc[i]-jtangle[i]),2.0);
            serr = sqrt(serr/7);
//            if(serr < 0.1)   break;

            if(serr == prev) break;
            else
              prev = serr;
          }
          ros::Rate rate(1);
          for(int i = 0; i < 5-1; i++)
            rate.sleep();

//          For controlling the opening of the gripper
          theta[7].data = 0.97; //close
          command_pub[7].publish(theta[7]);
          ros::spinOnce();

        }
#endif

#endif

#ifndef ROBOT_GRASP_DEMO
        cout << "Trying a new target position" << endl;
//              getchar();
#else
        cout << "Robot has reached the target position.\n " << std::endl;
//        system("python /home/meenu/tcs/robot_arm_project/catkin_ws/src/cool1000_ros_experimental/cool1000_controller/scripts/pose_zero_dummy.py");

//          For controlling the opening of the gripper
        std_msgs::Float64 theta;
        theta.data = -0.97; //open
        command_pub[7].publish(theta);
        ros::spinOnce();

        f_robot.data = true;
        f_kinect.data = false;
        f_robot_rcvd.data = false;
        r_cnt.data = r_cnt.data + 1;
        flag_robot_pose = true;


#endif

      } // for multiple points
    }

    //-----------------------------------

#endif //INVERT module ends here

    //====================


    return 0;
}
