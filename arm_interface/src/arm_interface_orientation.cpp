/* Solving Inverse Kinematics using Inv-Fwd Scheme

  * Interfaced with actual CoolARM hardware.

  *  Run the program 'main.cpp' that is within the folder fwdkin/ folder
  * Step 1: #define CLUSTER
  * Step 2: #define RBF_TRAIN
  * Step 3: INVERT_RBF

  * Execute Coolarm related launch files

  * ------------------------------------------------------------- */

#include "arm_interface/includedefs.h"

int main(int argc, char **argv)
{
//    signal(SIGINT, signal_callback_handler);

    ros::init(argc, argv, "arm_client");
    ros::NodeHandle node;
    InterfaceControl interface;

//==========================================================
    // Setup for command controlling of the robot
    // Anyone of the following control is chosen( in includedefs.h) to control the arm
    // create publisher and subscribers for 8 motors(7 joints + gripper)

#ifdef DYNAMIXEL_CONTROL
    // ROS Dynamixel based control of arm, setting up publishers and subscribers
    std::vector<ros::Publisher> command_pub(8);
    std::vector<ros::Subscriber> state_sub(8);
    std::vector<ros::ServiceClient> jt_client(8); // Service to set speed motor for 9 motors

    interface.createDynamixelCntrlPubSub(command_pub, state_sub, node);
    interface.subscribeServiceCntrlSpeed(node, jt_client);
#endif

#ifdef POPPY_CONTROL
    // Poppy framework based control of arm, setting publisher for position and speed
    std::vector<ros::Publisher> poppy_pos(8);
    std::vector<ros::Publisher> poppy_speed(8);

    interface.createPoppyCntrlPubSub(poppy_pos, poppy_speed, node);
#endif

    // Subscribe to the gripper force sensors
    std::vector<ros::Subscriber> sub_force_sensor(2);
    sub_force_sensor[0] = node.subscribe<std_msgs::Float64>("/force_sensor_1", 1, boost::bind(forceSensorCallback, _1,
                                                            boost::ref(interface.force_sensor[0])));
    sub_force_sensor[1] = node.subscribe<std_msgs::Float64>("/force_sensor_2", 1, boost::bind(forceSensorCallback, _1,
                                                            boost::ref(interface.force_sensor[1])));

//**********************************************************

//==========================================================
    // Transformations

    tf::TransformListener transform_listener;

    transform_listener.waitForTransform("/camera_depth_optical_frame", "/robot_base", ros::Time(0), ros::Duration(3));
    transform_listener.lookupTransform("/camera_depth_optical_frame", "/robot_base", ros::Time(0), interface.g_transform);

//**********************************************************

//==========================================================
    // Setting of publisher and subscribers for communicating handle data

    // commn_sub[0]: subscriber for flag that the data is being transmitted by sender, sub_fk
    // commn_sub[1]: subscriber for the count number of the data that is being transmitted, sub_k_cnt
    // commn_sub[2]: subscriber for the data, sub_handle_data
    std::vector<ros::Subscriber> commn_sub(3);

    // commn_pub[0]: publisher for flag requesting the new data, pub_fr
    // commn_pub[1]: publisher for the count number of the data requested by the receiver, pub_r_cnt
    // commn_pub[2]: publisher for flag that the data is received by the receiver, pub_frr
    std::vector<ros::Publisher> commn_pub(3);

    interface.createCommnProtocolPubSub(commn_sub, commn_pub, node);

//**********************************************************

    double current_theta[NL] = {0,0,0,0,0,0,0};

    while(ros::ok())
    {
        // Communication protocol to receive new handle pose data
        interface.getDatafromSender(commn_pub);

//==========================================================
        // Path planning to reach the object
        // Step 1: Compute inverse kinematics with initial arm position at home.
        // Take the desired EEF position as a point on the normal vector before the centroid
        // Step 2: Compute inverse kinematics with arm position at the normal vector point,
        // (Initial position now will be the joint angles with arm at the normal point)
        // Take the desired EEF position as centroid point

#ifdef ORIENTATION
        int steps = 1;
        double initial_theta[NL];// Give the initial position of the arm in terms of the joint angles

        for(int i = 0; i < NL; i++)
            initial_theta[i] = current_theta[i];

        std::vector<geometry_msgs::Point> path_points = interface.getPathPointalongNormalVector(steps);

//        double theta_0[NL];// = {0.0168, 1.1520, 0.3589, 0.6642, -1.6045, 0.2776, 0.1933};
        cv::Mat theta_step(steps,NL,CV_64F,0.0);// Final joint angles along the path
        cv::Mat theta_dot_step(steps,NL,CV_64F,0.0);// Final joint speed along the path
        cv::Mat orient_mat(3,3,CV_64F,0.0);

        interface.getOrientationMatrix(orient_mat);

//        Eigen::Vector3d v1(0.229, -0.1, -0.8979), v2, v3(0.3921, -0.8979, 0.2);
//        v1.norm();
//        v2 = v3.cross(v1);
//        orient_mat.at<double>(0,0) = v1(0);
//        orient_mat.at<double>(1,0) = v1(1);
//        orient_mat.at<double>(2,0) = v1(2);

//        orient_mat.at<double>(0,1) = v2(0);
//        orient_mat.at<double>(1,1) = v2(1);
//        orient_mat.at<double>(2,1) = v2(2);

//        orient_mat.at<double>(0,2) = v3(0);
//        orient_mat.at<double>(1,2) = v3(1);
//        orient_mat.at<double>(2,2) = v3(2);

        double rotation_angles[3];
        interface.getRotationAngles(orient_mat, rotation_angles);

        double pose[6];
        // Orientation is fixed for all the points along the normal vector
        pose[3] = rotation_angles[0];
        pose[4] = rotation_angles[1];
        pose[5] = rotation_angles[2];

        double init[NL] = {0,0,0,0,0,0,0};
        double theta_jacob[NL];
        ofstream f1("error.txt");
        ofstream f2("rconfig.txt");
        ofstream f3("robot_joints.txt");
        ofstream f4("actual.txt");

        for(int i = 0; i < steps; i++)
        {
            pose[0] = path_points[i].x;
            pose[1] = path_points[i].y;
            pose[2] = path_points[i].z;

//            cout << "Target Point:[" << pose[0] << " " << pose[1] << " " << pose[2] << "]" << endl;


            interface.JacobianInvKinOrientation(pose, init, theta_jacob, orient_mat, f1, f2, f3, f4);

            for(int j = 0; j < NL; j++)
            {
//                init[j] = theta_jacob[j];
                theta_step.at<double>(i,j) = theta_jacob[j];
//                theta_step.at<double>(steps-1,i) = theta_0[i];
//                cout << " " << theta_step.at<double>(steps-1,i);
                if(i==0)
                    theta_dot_step.at<double>(i,j) = 0.2;
                else
                    theta_dot_step.at<double>(i,j) = 0.025;
            }
        }
        f1.close();
        f2.close();
        f3.close();
        f4.close();
#endif
#ifdef JLA
        int steps = 1;
        double initial_theta[NL];// Give the initial position of the arm in terms of the joint angles

        for(int i = 0; i < NL; i++)
            initial_theta[i] = current_theta[i];
//            initial_theta[i] = 0.0;

        std::vector<geometry_msgs::Point> path_points = interface.getPathPointalongNormalVector(steps);

        cv::Mat theta_step(steps,NL,CV_64F,0.0);// Final joint angles along the path
        cv::Mat theta_dot_step(steps,NL,CV_64F,0.0);// Final joint speed along the path
        cv::Mat orient_mat(3,3,CV_64F,0.0);

        interface.getOrientationMatrix(orient_mat);

        double rotation_angles[3];
        interface.getRotationAngles(orient_mat, rotation_angles);

        double pose[6];
        // Orientation is fixed for all the points along the normal vector
        pose[3] = rotation_angles[0];
        pose[4] = rotation_angles[1];
        pose[5] = rotation_angles[2];

        double init[NL] = {0,0,0,0,0,0,0};
        double theta_jla[NL];
        ofstream f1("actual.txt");
        ofstream f2("rconfig.txt");
        ofstream f3("rank.txt");
        ofstream f4("target.txt");
        ofstream f5("error.txt");
        for(int i = 0; i < steps; i++)
        {
            pose[0] = path_points[i].x;
            pose[1] = path_points[i].y;
            pose[2] = path_points[i].z;

//            cout << "Target Point:[" << pose[0] << " " << pose[1] << " " << pose[2] << "]" << endl;


            interface.jla_orient(pose, init, theta_jla, f1, f2, f3 , f4, f5);

            for(int j = 0; j < NL; j++)
            {
                init[j] = 0;
                theta_step.at<double>(i,j) = theta_jla[j];
//                cout << " " << theta_step.at<double>(steps-1,i);
                if(i==0)
                    theta_dot_step.at<double>(i,j) = 0.2;
                else
                    theta_dot_step.at<double>(i,j) = 0.025;
            }
        }
        f1.close();
        f2.close();
        f3.close();
        f4.close();
        f5.close();

#endif

#ifdef JLA_NSO
        int steps;
        int d = 5;
        double initial_theta[NL];// Give the initial position of the arm in terms of the joint angles

        for(int i = 0; i < NL; i++)
            initial_theta[i] = current_theta[i];

#ifdef CONST_VELOCITY
        steps = 100;
#else
        steps = 20;// Specify the number of steps in the path to reach the object
#endif

        std::vector<int> num_iterations(steps);// Num of iterations for computation of inverse kinematics in each step
        num_iterations[0] = 200;
        int v = 1;// count for the number of the joint positions to be taken to reach the object
        for(int i = 1; i < steps; i++)
        {
            num_iterations[i] = 50;
#ifdef CONST_VELOCITY
            v++;
#else
            v = v+num_iterations[i]/d; // for sampling theta at every 'd'th sample
#endif
        }

        std::vector<geometry_msgs::Point> path_points = interface.getPathPointalongNormalVector(steps);
        // Specify the desired pose that you want at the EEF position in terms of joint angles(radians)
//        double theta_0[NL] = {0.0168, 1.1520, 0.3589, 0.6642, -1.6045, 0.2776, 0.0};
        double theta_0[NL] = {0.0168, 1.1520, 0.3589, 0.6642, -1.6045, 0.2776, 0.1933};
        cv::Mat theta_step(v,NL,CV_64F,0.0);// Final joint angles along the path
        cv::Mat theta_dot_step(v,NL,CV_64F,0.0);// Final joint speed along the path

        ofstream f1("actual.txt");
        ofstream f2("rconfig.txt");
        ofstream f3("target.txt");

        // Call the function to compute the path
        interface.obtainPathPlanning(initial_theta, steps, num_iterations, path_points, theta_0, theta_step,
                                     theta_dot_step, f1, f2, f3, d);

        f1.close();
        f2.close();
        f3.close();

#endif
//**********************************************************

//==========================================================
// GNUPLOT Interface
#ifdef PLOT

#ifdef ORIENTATION
        // GNUPLOT
        gnuplot_ci::GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
        G1.gnuplot_cmd("set terminal wxt");
        G1.gnuplot_cmd("set border");
        G1.gnuplot_cmd("set yrange [-0.5:0.5]");
        G1.gnuplot_cmd("set xrange [-0.5:0.5]");
        G1.gnuplot_cmd("set zrange [-0.0:0.5]");

        // plot the joints position final theta
        G1.gnuplot_cmd("splot 'rconfig.txt' u 1:2:3 w lp pt 4 lt -1 lw 2 t 'actual'");//, '' u 4:5:6 w lp pt 4 lt 9 lw 2 t 'desired'");

        // plot the error curve
        gnuplot_ci::GP_handle G3("/usr/bin/", "t", "Error");
        G3.gnuplot_cmd("plot 'error.txt' u 1:2 w d t 'Error'");

        // plot the axis at the origin
        double xorg[3] = {0,0,0};
        double xb[3][3] = {{0.1,0,0}, {0,0.1,0}, {0,0,0.1}};
        G1.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);

        // plot the axis at the target point
        double xtar[3] = {path_points[steps-1].x, path_points[steps-1].y, path_points[steps-1].z};
        double xt[3];
        double yt[3];
        double zt[3];

        cout << orient_mat << endl;
        xt[0] = 0.1*orient_mat.at<double>(0,0) + path_points[steps-1].x;
        xt[1] = 0.1*orient_mat.at<double>(1,0) + path_points[steps-1].y;
        xt[2] = 0.1*orient_mat.at<double>(2,0) + path_points[steps-1].z;
        cout << "axis: " << xt[0] << " " << xt[1] << " " << xt[2] << " " << endl;
        yt[0] = 0.1*orient_mat.at<double>(0,1) + path_points[steps-1].x;
        yt[1] = 0.1*orient_mat.at<double>(1,1) + path_points[steps-1].y;
        yt[2] = 0.1*orient_mat.at<double>(2,1) + path_points[steps-1].z;
        cout << "3vec: " << yt[0] << " " << yt[1] << " " << yt[2] << " " << endl;
        zt[0] = 0.1*orient_mat.at<double>(0,2) + path_points[steps-1].x;
        zt[1] = 0.1*orient_mat.at<double>(1,2) + path_points[steps-1].y;
        zt[2] = 0.1*orient_mat.at<double>(2,2) + path_points[steps-1].z;
        cout << "normal: " << zt[0] << " " << zt[1] << " " << zt[2] << " " << endl;
        G1.draw3dcoordAxis(xtar, xt, yt, zt,true);

        double pose_a[3];
        double xeb[3][3];
        bool VALID_FLAG;
        coolarm_pose_fk(theta_jacob, pose_a, VALID_FLAG);
        double xend[3][3] = {{pose_a[0], pose_a[1], pose_a[2]},
                             {pose_a[0], pose_a[1], pose_a[2]},
                             {pose_a[0], pose_a[1], pose_a[2]}};

        cv::Mat R(3,3,CV_64F,0.0);
        cv::Mat T(3,3,CV_64F, &xend);
        cv::Mat baseC(3,3,CV_64F, &xb);
        cv::Mat endC(3,3,CV_64F, 0.0);

        rotation_matrix(theta_jacob, R);

        // output is a column matrix
        endC = R * baseC.t() + T.t(); // check the transpose

        // make it a row matrix
        for(int i = 0; i < 3; ++i)
            for(int j = 0; j < 3; ++j)
                xeb[i][j] = endC.at<double>(j,i); // make it a row matrix

        G1.draw3dcoordAxis(xend[0], xeb[0], xeb[1], xeb[2], true, 2);
#endif
#ifdef JLA
        GP_handle G3("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
        G3.gnuplot_cmd("set terminal wxt");
        G3.gnuplot_cmd("set border");
        G3.gnuplot_cmd("splot 'actual.txt' u 1:2:3 w p t 'actual', 'target.txt' u 1:2:3 w d  t 'target'");
        GP_handle G2("/usr/bin/", "Error", "Time (s)");
        G2.gnuplot_cmd("set terminal wxt");
        G2.gnuplot_cmd("set border");
        G2.gnuplot_cmd("plot 'error.txt' u 1:2 w l");

        gnuplot_ci::GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
        G1.gnuplot_cmd("set terminal wxt");
        G1.gnuplot_cmd("set border");
        G1.gnuplot_cmd("set yrange [-0.5:0.5]");
        G1.gnuplot_cmd("set xrange [-0.5:0.5]");
        G1.gnuplot_cmd("set zrange [-0.0:0.5]");

        // plot the joints position final theta
        G1.gnuplot_cmd("splot 'rconfig.txt' u 1:2:3 w lp pt 4 lt -1 lw 2 t 'actual'");

        // plot the error curve
//        gnuplot_ci::GP_handle G3("/usr/bin/", "t", "Error/joint");
//        G3.gnuplot_cmd("plot 'error.txt' u 1:2 w d t 'Error'");

        // plot the axis at the origin
        double xorg[3] = {0,0,0};
        double xb[3][3] = {{0.1,0,0}, {0,0.1,0}, {0,0,0.1}};
        G1.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);

        // plot the axis at the target point
        double xtar[3] = {path_points[steps-1].x, path_points[steps-1].y, path_points[steps-1].z};
        double xt[3];
        double yt[3];
        double zt[3];

        cout << orient_mat << endl;
        xt[0] = 0.1*orient_mat.at<double>(0,0) + path_points[steps-1].x;
        xt[1] = 0.1*orient_mat.at<double>(1,0) + path_points[steps-1].y;
        xt[2] = 0.1*orient_mat.at<double>(2,0) + path_points[steps-1].z;
        cout << "axis: " << xt[0] << " " << xt[1] << " " << xt[2] << " " << endl;
        yt[0] = 0.1*orient_mat.at<double>(0,1) + path_points[steps-1].x;
        yt[1] = 0.1*orient_mat.at<double>(1,1) + path_points[steps-1].y;
        yt[2] = 0.1*orient_mat.at<double>(2,1) + path_points[steps-1].z;
        cout << "3vec: " << yt[0] << " " << yt[1] << " " << yt[2] << " " << endl;
        zt[0] = 0.1*orient_mat.at<double>(0,2) + path_points[steps-1].x;
        zt[1] = 0.1*orient_mat.at<double>(1,2) + path_points[steps-1].y;
        zt[2] = 0.1*orient_mat.at<double>(2,2) + path_points[steps-1].z;
        cout << "normal: " << zt[0] << " " << zt[1] << " " << zt[2] << " " << endl;
        G1.draw3dcoordAxis(xtar, xt, yt, zt,true);

        double pose_a[3];
        double xeb[3][3];
        bool VALID_FLAG;
        coolarm_pose_fk(theta_jla, pose_a, VALID_FLAG);
        double xend[3][3] = {{pose_a[0], pose_a[1], pose_a[2]},
                             {pose_a[0], pose_a[1], pose_a[2]},
                             {pose_a[0], pose_a[1], pose_a[2]}};

        cv::Mat R(3,3,CV_64F,0.0);
        cv::Mat T(3,3,CV_64F, &xend);
        cv::Mat baseC(3,3,CV_64F, &xb);
        cv::Mat endC(3,3,CV_64F, 0.0);

        rotation_matrix(theta_jla, R);

        // output is a column matrix
        endC = R * baseC.t() + T.t(); // check the transpose

        // make it a row matrix
        for(int i = 0; i < 3; ++i)
            for(int j = 0; j < 3; ++j)
                xeb[i][j] = endC.at<double>(j,i); // make it a row matrix

        G1.draw3dcoordAxis(xend[0], xeb[0], xeb[1], xeb[2], true, 2);
#endif
#ifdef JLA_NSO
        GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
        G1.gnuplot_cmd("set terminal wxt");
        G1.gnuplot_cmd("set border");
        G1.gnuplot_cmd("set ticslevel 0");
        G1.gnuplot_cmd("splot 'actual.txt' u 1:2:3 w p t 'actual', 'target.txt' u 1:2:3 w d  t 'target'");

        GP_handle G2("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
        G2.gnuplot_cmd("set terminal wxt");
        G2.gnuplot_cmd("set border 4095");
        G2.gnuplot_cmd("set ticslevel 0");
        G2.gnuplot_cmd("splot 'rconfig.txt' u 1:2:3 w lp lw 2 t 'Robot Configuration', 'actual.txt' u 1:2:3 w lp lt 3 t 'End-effector trajectory'");
#endif
//        cout << "Press enter" << endl;
//        getchar();
#endif
//**********************************************************

//==========================================================
        // Sending out the control commands to the arm using one of the controllers
        // Sends the commands to reach, pick and drop the object
        char ch;
//        std::cout << "Press 'p' to pick the object" << std::endl;
//        ch = getchar();
        ch = 'p';
        if(ch == 'p')
        {
            bool flag = true;
#ifdef DYNAMIXEL_CONTROL

#ifdef ORIENTATION
            int s = steps;
#endif
#ifdef JLA
            int s = steps;
#endif
#ifdef JLA_NSO
            int s = v;
#endif

        float time[s];
        interface.getTime(theta_step, theta_dot_step, current_theta, time);
#ifdef JLA_NSO
#ifndef CONST_VELOCITY
        for(int i = 1; i < s; i++)
            time[i] = 0.01*d;
#endif
#endif
        std::cout << "Time: [";
        for(int i = 0; i < s; i++)
            std::cout << time[i] << " ";
        std::cout << "]\n";

        // Open the gripper
        interface.openGripperDynamixel(command_pub[7], 2);
        // Reach the centroid point
        interface.reachPointDynamixel(command_pub, jt_client, theta_step, theta_dot_step, time, flag);
        // Close the gripper and hold the object
        interface.closeGripperDynamixel(command_pub[7]);

        // Go to drop location
        ros::Duration(1).sleep();
        steps = 2;
        cv::Mat drop_location(steps,NL,CV_64F, 0.0);
        cv::Mat moving_speed(steps,NL,CV_64F, 0.2);// moving speed is set to 0.2 radians per second
        double theta_drop[NL] = {1.4956, 0.4249, 0.0628, 0.3283, 0.0476, -0.9004, -0.0414};
        double theta_drop2[NL] = {1.51, 0.82, -0.07, 0.87, 0.13, -0.63, -0.02};
        float t[steps];
        t[0] = 9;
        t[1] = 3;
        for(int i = 0; i < NL; i++)
        {
            drop_location.at<double>(0,i) = theta_drop[i];
            drop_location.at<double>(1,i) = theta_drop2[i];
        }

        interface.reachPointDynamixel(command_pub, jt_client, drop_location, moving_speed, t, flag);



        // Open the gripper to drop the object
        interface.openGripperDynamixel(command_pub[7], 4);

        cv::Mat drop_location2(1,NL,CV_64F, 0.0);
        cv::Mat moving_speed2(1,NL,CV_64F, 0.2);
        for(int i = 0; i < NL; i++)
        {
            drop_location2.at<double>(0,i) = theta_drop[i];
        }

        float t1[1] = {3};

        interface.reachPointDynamixel(command_pub, jt_client, drop_location2, moving_speed2, t1, flag);
        for(int i = 0; i < NL; i++)
            current_theta[i] = theta_drop[i];
#endif

#ifdef POPPY_CONTROL

        float time[v];
        interface.getTime(theta_step, theta_dot_step, current_theta, time);
        std::cout << "Time: [";
        for(int i = 0; i < v; i++)
            std::cout << time[i] << " ";
        std::cout << "]\n";

        // Open the gripper
        interface.openGripperPoppy(poppy_pos[7], poppy_speed[7], 2);
        // Reach the centroid point
        interface.reachPointPoppy(poppy_pos, poppy_speed, theta_step, theta_dot_step, time);
        // Close the gripper and hold the object
        interface.closeGripperPoppy(poppy_pos[7], poppy_speed[7]);

        // Go to drop location
        double theta_drop[NL] = {1.4956, 0.4249, 0.0628, 0.3283, 0.0476, -0.9004, -0.0414};
        ros::Duration(1).sleep();

        cv::Mat drop_location(1,NL,CV_64F, 0.0);
        cv::Mat moving_speed(1,NL,CV_64F, DEG2RAD(30.0));// moving speed is set to 20 degrees per second
        for(int i = 0; i < NL; i++)
            drop_location.at<double>(0,i) = theta_drop[i];
        float t[1] = {6};
        interface.reachPointPoppy(poppy_pos, poppy_speed, drop_location, moving_speed,t);

        // Open the gripper to drop the object
        interface.openGripperPoppy(poppy_pos[7], poppy_speed[7], 4);

        for(int i = 0; i < NL; i++)
            current_theta[i] = theta_drop[i];
#endif
        }

//**********************************************************

    }
    return 0;
}

