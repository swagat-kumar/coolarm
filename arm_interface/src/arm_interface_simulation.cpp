/* Solving Inverse Kinematics using Inv-Fwd Scheme

  * Interfaced with actual CoolARM hardware.

  *  Run the program 'main.cpp' that is within the folder fwdkin/ folder

  * Step 1: #define CLUSTER
  * Step 2: #define RBF_TRAIN
  * Step 3: INVERT_RBF

  * Execute Coolarm related launch files

  * ------------------------------------------------------------- */

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <nnet.h>
#include <ksom_vmc.h>
#include <mymatrix.h>
#include <gnuplot_ci.h>
#include <opencv2/opencv.hpp>
#include <coolarm_fk.h>
#include <dynamixel_msgs/JointState.h>
#include <csignal>
#include <cstdio>


using namespace std;
using namespace cv;
using namespace nnet;
using namespace vmc;
using namespace gnuplot_ci;

#define NC 3   // Dimension of Cartesian Space

// Step - 3: Invert the Network to find IK solution
#define INVERT_RBF
#define PLOT
#define ROBOT

//==========================================================
// callback function
void jtStateCallback1(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
    //cout << "angle = " << angle << endl;
}
void jtStateCallback2(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback3(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback4(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback5(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
void jtStateCallback6(const dynamixel_msgs::JointState::ConstPtr& msg, double& angle)
{
    angle = msg->current_pos;
}
//========================================
// Signal Handling
// does not work as the controller passes on to the subscribers
void signal_callback_handler(int signum)
{
   printf("Caught signal %d\n",signum);
   cout << "Press 'y' to exit" << endl;

   char ans;
   if(scanf(" %c", &ans) == 'y')
       exit(signum);
}
//===================================================
int main(int argc, char **argv)
{
    signal(SIGINT, signal_callback_handler);

#ifdef ROBOT
    //==========================================================
    // Setup for interacting with robot

    ros::init(argc, argv, "arm_client");

    ros::NodeHandle Joint[6];
    ros::Publisher command_pub[6];

    command_pub[0] = Joint[0].advertise<std_msgs::Float64>("/joint1_controller/command", 1000);
    command_pub[1] = Joint[1].advertise<std_msgs::Float64>("/dual_servo_controller/command", 1000);
    command_pub[2] = Joint[2].advertise<std_msgs::Float64>("/joint3_controller/command", 1000);
    command_pub[3] = Joint[3].advertise<std_msgs::Float64>("/joint4_controller/command", 1000);
    command_pub[4] = Joint[4].advertise<std_msgs::Float64>("/joint5_controller/command", 1000);
    command_pub[5] = Joint[5].advertise<std_msgs::Float64>("/joint6_controller/command", 1000);

    ros::Rate loop_rate(10);


    double jtangle[6] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    ros::Subscriber state_sub[6];

    state_sub[0] = Joint[0].subscribe<dynamixel_msgs::JointState>("/joint1_controller/state",
                                                                  1, boost::bind(jtStateCallback1, _1, boost::ref(jtangle[0])));
    state_sub[1] = Joint[1].subscribe<dynamixel_msgs::JointState>("/dummy_joint2_controller/state",
                                                                  1, boost::bind(jtStateCallback2, _1, boost::ref(jtangle[1])));
    state_sub[2] = Joint[2].subscribe<dynamixel_msgs::JointState>("/joint3_controller/state",
                                                                  1, boost::bind(jtStateCallback3, _1, boost::ref(jtangle[2])));
    state_sub[3] = Joint[3].subscribe<dynamixel_msgs::JointState>("/joint4_controller/state",
                                                                  1, boost::bind(jtStateCallback4, _1, boost::ref(jtangle[3])));
    state_sub[4] = Joint[4].subscribe<dynamixel_msgs::JointState>("/joint5_controller/state",
                                                                  1, boost::bind(jtStateCallback5, _1, boost::ref(jtangle[4])));
    state_sub[5] = Joint[5].subscribe<dynamixel_msgs::JointState>("/joint6_controller/state",
                                                                  1, boost::bind(jtStateCallback6, _1, boost::ref(jtangle[5])));

#endif

    //======================================================================

#ifdef INVERT_RBF
    const int nx = 7;
    const int ny = 7;
    const int nz = 7;
    //----------------------------------------
    //Create a KSOM Network
    int N[5] = {nx, ny, nz, NC, NL};

    int ncmax = 100;

    //Create a SOM lattice
    ksom3_sc A(N, ncmax);

    A.netinfo();


    //=====================================

    //create a RBF network

    int noc = 700;

    int N2[3] = {NL, noc, NC};
    double af[2] = {5, 0.5}; //gaussian activation
    double param[3] = {1, -1, 0}; //input range = -1 to 1

    rbfn B(N2, af, param);

    cout << "Network Information" << B << endl;

    //==============================================

    // Load Neural Network parameters
    // Run "CLUSTER" and "TRAIN_RBF" modules

    cout << "\nLoading ANN parameters .." << endl;

    if( (access("ksom_sc_weights.txt",F_OK) ||
         access("ksom_sc_angles.txt", F_OK) ||
            access("rbfn_par_bp.txt",F_OK) ) != -1)
    {
        A.load_weights("ksom_sc_weights.txt");
        A.load_angles("ksom_sc_angles.txt");
        B.load_parameters("rbfn_par_bp.txt");

        cout << ".... done!" << endl;
    }
    else
    {
        cout << "ANN parameter files are missing ..." << endl;
        cout << "Run CLUSTER and TRAIN_RBF modules first" << endl;
        exit(-1);
    }

    //========================================

    const int np = 6; // no. of rows in jp
    Mat jp(6,3,CV_64F,0.0); // stores joint positions


    double qt[NL], qtn[NL];       // corresponding target joint angle
    double xr[NC], xrn[NC];       // actual robot end-effector position
    double xt[NC], xtn[NC];       // Target end-effector position
    double xc[NC], xcn[NC];       // network (RBFN) output


    double *th_c = myvector(NL);
    double *th_cn = myvector(NL);
    double *qc = myvector(NL);
    double *qcn = myvector(NL);

    //----------------------------
    //initial robot configuration

    th_c[0] = 0.0;
    th_c[1] = DEG2RAD(50);
    th_c[2] = 0.0;
    th_c[3] = DEG2RAD(20);
    th_c[4] = 0.0;
    th_c[5] = 0.0;
    th_c[6] = 0.0;


    for(int cnt = 0; cnt < 2; cnt++) // Multiple points
    {
        cout << "\nCount = " << cnt << endl;

        ofstream ft("angle.txt");
        ofstream fr("rconfig.txt");
        ofstream fp("efposn.txt");
        ofstream fe1("error.txt");
        ofstream ftp("targetpos.txt");

        //--------------------------
        //Generate a Target point
        generate_data(xt, qt);

        //normalize the target position
        cart_posn_normalize(xt, xtn);
        angle_normalize(qt, qtn);

        // Block 1: target position
        for(int i = 0; i < NC; i++)
            ftp << xt[i] << "\t";
        for(int i = 0; i < NL; i++)
            ftp << qt[i] << "\t";
        ftp << endl ;
        //---------------------------------
        cout << "Target position =" << xt[0] << "\t" << xt[1] << "\t" << xt[2] << endl;

        //Initial robot configurationspl
        joint_position(th_c, jp);

        for(int i = 0; i < np; i++)
        {
            for(int j = 0; j < 3; j++)
                fr << jp.at<double>(i,j) << "\t";
            fr << endl;
        }
        fr << endl << endl;

        //-------

        //Current robot end-effector position
        coolarm_7dof_FK(th_c, xr);

        //current end-effector position obtained from network
        angle_normalize(th_c, th_cn);
        B.network_output(th_cn, xcn);
        cart_posn_denormalize(xcn, xc);

        // Block 2: Initial robot position
        for(int i = 0; i < NC; i++)
            fp << xc[i] << "\t";
        for(int i = 0; i < NC; i++)
            fp << xr[i] << "\t";
        fp << endl << endl << endl;

        //-----------------------------------------
        //Initial guess (Lazy arm joint angle computation)
//        qcn = A.LA_theta_output(xtn, th_cn, 0.5);  // last value is sigma (std. dev)
        A.LA_theta_output(xtn, th_cn, 0.5, qcn); // function got changed
        //qcn = A.LA_theta_output(xtn, th_cn);
        angle_denormalize(qcn, qc);


        //Initial position of robot end-effector position
        coolarm_7dof_FK(qc, xr);

        //Initial end-effector position obtained from NN
        B.network_output(qcn, xcn);
        cart_posn_denormalize(xcn, xc);
        //------------------------------------------

        double se1;
        int step = 0;
        do
        {
            // Block 3: Position obtained from Inv-Fwd Scheme
            for(int i = 0; i < NC; i++)
                fp << xc[i] << "\t";
            for(int i = 0; i < NC; i++)
                fp << xr[i] << "\t";
            fp << endl;

            joint_position(qc, jp);

            for(int i = 0; i < np; i++)
            {
                for(int j = 0; j < 3; j++)
                    fr << jp.at<double>(i,j) << "\t";
                fr << endl;
            }
            fr << endl << endl;

            //----------

            //update input using gradient_descent
            // qcn is the updated input

            B.input_jacobian();
            B.update_input_gd(0.02, xtn, qcn);

            //new end-effector position
            B.network_output(qcn, xcn);
            cart_posn_denormalize(xcn, xc);

            for(int i = 0; i < NL; i++)
                ft << qcn[i] << "\t";
            ft << endl;

            angle_denormalize(qcn, qc);
            coolarm_7dof_FK(qc, xr);
            cart_posn_normalize(xr, xrn);

            //Forward training
            B.backprop(xrn, 0.05, 0.05);


            se1 = 0.0;
            for(int i = 0;i < NC; i++)
                se1 += pow((xr[i] - xt[i]), 2.0);
            se1 = sqrt(se1);

            fe1 << se1 << endl;

            step = step + 1;

            if(step > 1000)
                break;

        }while(se1 > 0.002);

        ft.close();
        fp.close();
        fr.close();
        fe1.close();
        ftp.close();

        cout << "\nNo. of steps = " << step << endl;
        cout << "Final positioning Error = " << se1 << endl;


        cout << "\nFinal Joint angle values = ";
        for(int i = 0; i < NL; ++i)
        {
            th_c[i] = qc[i];
            cout << qc[i] << '\t';
        }
        cout << endl << endl;




        //--------------------------------
        // Enable GNUPLOT Interface
#ifdef PLOT
        GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
        GP_handle G2("/usr/bin/", "Iteration Steps", "Error");
        G1.gnuplot_cmd("splot 'targetpos.txt' u 1:2:3 w p pt 6 lt 3 ps 2 t 'Target Position'");
        G2.gnuplot_cmd("plot 'error.txt' w l lw 2 t 'mse'");
        G1.gnuplot_cmd("replot 'rconfig.txt' index 1:400 u 1:2:3 w lp pt 2 lw 2 lt 1 notitle");
        G1.gnuplot_cmd("replot 'efposn.txt' index 1:400 u 4:5:6 w p pt 4 lt 2 notitle");
        cout << "\nPress Enter to Drive the Robot ..." << endl;
        getchar();
//        G1.gnuplot_cmd("set terminal postscript eps enhanced color solid lw 2");
//        G2.gnuplot_cmd("set terminal postscript eps enhanced color solid lw 2");
//        G1.gnuplot_cmd("set output 'rconfig.eps'");
//        G2.gnuplot_cmd("set output 'error.eps'");
//        G1.gnuplot_cmd("replot");
//        G2.gnuplot_cmd("replot");
//        G1.gnuplot_cmd("set output");
//        G2.gnuplot_cmd("set output");
//        G1.gnuplot_cmd("set terminal x11");
//        G2.gnuplot_cmd("set terminal x11");
#endif

#ifdef ROBOT

        // send the calculated angles to the robot
        std_msgs::Float64 theta[6];
        for(int i = 0; i < 6; ++i)
        {
            theta[i].data = qc[i];
            command_pub[i].publish(theta[i]);
        }

        while(ros::ok())
        {
            ros::spinOnce();
            loop_rate.sleep();

            double serr = 0.0;
            for(int i = 0; i < 6; ++i)
                serr += pow((qc[i]-jtangle[i]),2.0);
            serr = sqrt(serr/6);
            if(serr < 0.1)   break;
        }
        cout << "\nRobot has reached the target position." << endl;
        cout << "Press Enter to try a new target position" << endl;

        getchar();
#endif

    } // for multiple points

    //-----------------------------------

#endif //INVERT module ends here

    //====================


    return 0;
}
