#!/usr/bin/env python
import itertools
import numpy
import time
import rospy
import roslib

from std_msgs.msg import Float64
from std_msgs.msg import Float64MultiArray

import pypot.dynamixel

def jt_pos_callback(d, pos, flag_pos):
	pos[0] = d.data[0]
	pos[1] = d.data[1]
	pos[2] = d.data[1]
	pos[3] = d.data[2]
	pos[4] = d.data[3]
	pos[5] = d.data[4]
	pos[6] = d.data[5]
	pos[7] = d.data[6]
	flag_pos = True

def jt1_pos_callback(data, pos):
    pos[0] = data.data        

def jt2_pos_callback(data, pos):
    pos[1] = data.data
    pos[2] = data.data        

def jt3_pos_callback(data, pos):
    pos[3] = data.data        

def jt4_pos_callback(data, pos):
    pos[4] = data.data        

def jt5_pos_callback(data, pos):
    pos[5] = data.data 

def jt6_pos_callback(data, pos):
    pos[6] = data.data        

def jt7_pos_callback(data, pos):
    pos[7] = data.data        

def grp_pos_callback(data, pos):
    pos[8] = data.data        

def jt_speed_callback(d, speed, flag_speed):
	speed[0] = d.data[0]
	speed[1] = d.data[1]
	speed[2] = d.data[1]
	speed[3] = d.data[2]
	speed[4] = d.data[3]
	speed[5] = d.data[4]
	speed[6] = d.data[5]
	speed[7] = d.data[6]
	flag_speed = True
	
def jt1_speed_callback(data, speed):
    speed[0] = data.data

def jt2_speed_callback(data, speed):
    speed[1] = data.data
    speed[2] = data.data
    
def jt3_speed_callback(data, speed):
    speed[3] = data.data

def jt4_speed_callback(data, speed):
    speed[4] = data.data

def jt5_speed_callback(data, speed):
    speed[5] = data.data

def jt6_speed_callback(data, speed):
    speed[6] = data.data

def jt7_speed_callback(data, speed):
    speed[7] = data.data

def grp_speed_callback(data, speed):
    speed[8] = data.data

    

if __name__ == '__main__':
    ports = pypot.dynamixel.get_available_ports()
    print 'available ports:', ports

    if not ports:
        raise IOError('No port available.')

    arm_port = ['/dev/ttyUSB0', '/dev/ttyUSB1']
    for port in ports:
        if(port == arm_port[0] or port == arm_port[1]):
            break;
    # port = ports[0]
    print 'Using port: ', port

    dxl_io = pypot.dynamixel.DxlIO(port)
    print 'Connected! to: ', port
    rospy.init_node('arm_control', anonymous=True)
    ids = [0,1,2,3,4,5,6,7,8]

    dxl_io.enable_torque(ids)
    time.sleep(1)
    goal_pos = [0,0,0,0,0,0,0,0,-50]
    speed = [30,30,30,30,30,30,30,30,30]
    load = [50,50,50,50,50,50,50,50,50]
    # load = [100,100,100,100,100,100,100,100,100]
    flag_pos = True
    flag_speed = True
    

    z = zip(goal_pos, speed, load)
    print dict(zip(ids,z))
    dxl_io.set_goal_position_speed_load(dict(zip(ids,z)))
    time.sleep(2)

    # rospy.Subscriber("jt_pos", Float64MultiArray, jt_pos_callback, queue_size=1, goal_pos, flag_pos)
    rospy.Subscriber("jt1_pos", Float64, jt1_pos_callback, goal_pos)
    rospy.Subscriber("jt2_pos", Float64, jt2_pos_callback, goal_pos)
    rospy.Subscriber("jt3_pos", Float64, jt3_pos_callback, goal_pos)
    rospy.Subscriber("jt4_pos", Float64, jt4_pos_callback, goal_pos)
    rospy.Subscriber("jt5_pos", Float64, jt5_pos_callback, goal_pos)
    rospy.Subscriber("jt6_pos", Float64, jt6_pos_callback, goal_pos)
    rospy.Subscriber("jt7_pos", Float64, jt7_pos_callback, goal_pos)
    rospy.Subscriber("grp_pos", Float64, grp_pos_callback, goal_pos)
    
    # rospy.Subscriber("jt_speed", Float64MultiArray, jt_speed_callback, queue_size=1, speed, flag_speed)
    rospy.Subscriber("jt1_speed", Float64, jt1_speed_callback, speed)
    rospy.Subscriber("jt2_speed", Float64, jt2_speed_callback, speed)
    rospy.Subscriber("jt3_speed", Float64, jt3_speed_callback, speed)
    rospy.Subscriber("jt4_speed", Float64, jt4_speed_callback, speed)
    rospy.Subscriber("jt5_speed", Float64, jt5_speed_callback, speed)
    rospy.Subscriber("jt6_speed", Float64, jt6_speed_callback, speed)
    rospy.Subscriber("jt7_speed", Float64, jt7_speed_callback, speed)
    rospy.Subscriber("grp_speed", Float64, grp_speed_callback, speed)

    while not rospy.is_shutdown():
    	if(flag_speed and flag_pos):
            z = zip(goal_pos, speed, load)
            print dict(zip(ids,z))
            # dxl_io.set_goal_position_speed_load(dict(zip(ids,z)))
            flag_pos = False
            flag_speed = False
        time.sleep(1)

       # print dict(zip(ids,z))
       #  t = time.time()
       #  if (t - t0) > 100:
       #      break
            
       #  pos = AMP * numpy.sin(2 * numpy.pi * FREQ * t)
       #  ps = dxl_io.get_goal_position_speed_load((6,))
       # print 'ps: ', ps
       #  print 'pos new:', pos
       #  dxl_io.set_goal_position_speed_load(dict(zip(ids, itertools.repeat(pos), itertools.repeat(ss), itertools.repeat(100))))
       #  print 'ss:', ss
       #  value=(pos,ss,100)
       #  dxl_io.set_goal_position_speed_load({6: value})
       #  ss -= 1
       #  if (ss==0):
       #      ss=100
       #  print 'value:', value
       # ps = dxl_io.get_goal_position_speed_load((6,))
       # print 'ps: ', ps
        # time.sleep(3)
    