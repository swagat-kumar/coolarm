#!/usr/bin/env python
import rospy
import time
import numpy

from std_msgs.msg import Float64

def send_messages():
    
    jt1_pos = rospy.Publisher('jt1_pos', Float64, queue_size=10)
    jt2_pos = rospy.Publisher('jt2_pos', Float64, queue_size=10)
    jt3_pos = rospy.Publisher('jt3_pos', Float64, queue_size=10)
    jt4_pos = rospy.Publisher('jt4_pos', Float64, queue_size=10)
    jt5_pos = rospy.Publisher('jt5_pos', Float64, queue_size=10)
    jt6_pos = rospy.Publisher('jt6_pos', Float64, queue_size=10)
    jt7_pos = rospy.Publisher('jt7_pos', Float64, queue_size=10)

    jt1_speed = rospy.Publisher('jt1_speed', Float64, queue_size=10)
    jt2_speed = rospy.Publisher('jt2_speed', Float64, queue_size=10)
    jt3_speed = rospy.Publisher('jt3_speed', Float64, queue_size=10)
    jt4_speed = rospy.Publisher('jt4_speed', Float64, queue_size=10)
    jt5_speed = rospy.Publisher('jt5_speed', Float64, queue_size=10)
    jt6_speed = rospy.Publisher('jt6_speed', Float64, queue_size=10)
    jt7_speed = rospy.Publisher('jt7_speed', Float64, queue_size=10)
    
    rate = rospy.Rate(1)
    
    pos = [0,0,0,0,0,0,0]
    speed = [0,0,0,0,0,0,0]
    s = 30
    
    print 'Trying to publish messages'
    while not rospy.is_shutdown():
        # jt1_pos.publish(pos[0])
        # jt2_pos.publish(pos[1])
        jt3_pos.publish(pos[2])
        jt4_pos.publish(pos[3])
        jt5_pos.publish(pos[4])
        jt6_pos.publish(pos[5])
        # jt7_pos.publish(pos[6])

        # jt1_speed.publish(speed[0])		
        # jt2_speed.publish(speed[1])
        jt3_speed.publish(speed[2])
        jt4_speed.publish(speed[3])
        jt5_speed.publish(speed[4])
        jt6_speed.publish(speed[5])
        # jt7_speed.publish(speed[6])

        # for i in range(len(pos)):
        #     pos[i] += 0.01
        #     speed[i] += 0.01
        t = time.time()
        FREQ = 0.5

        pos[2] = 30 * numpy.sin(2 * numpy.pi * FREQ * t)
        pos[3] = 30 * numpy.sin(2 * numpy.pi * FREQ * t + numpy.pi*45/180)
        pos[4] = 30 * numpy.sin(2 * numpy.pi * FREQ * t + numpy.pi*90/180)
        pos[5] = 30 * numpy.sin(2 * numpy.pi * FREQ * t + numpy.pi*135/180)

        speed[2] = s
        speed[3] = s
        speed[4] = s
        speed[5] = s

        s -= 1
        if (s == 0):
            s = 30

        rate.sleep()
        rate.sleep()
        rate.sleep()

if __name__ == '__main__':
    rospy.init_node('arm_control_messenger', anonymous=True)
	# try:
    send_messages()
    # except rospy.ROSInterruptException:
  	    # pass