#ifndef _MATRIX_
#define _MATRIX_

#define myMAX(m, n) (m) < (n) ? (n) : (m)
#define myMIN(m, n) (m) < (n) ? (m) : (n)


/* --------------------------------------------
 * Note: These function calls are not efficient when called inside a
 * loop. 
 * ------------------------------------------------- */


//Date: 06 May 2008 Tuesday
double** mult(double **A, double **B, int m, int n, int p); //checked on 07/08/08
double** mult(double **A, double b, int m, int n);
double** mult(double **A, double b, int m);
double** mult(double b, double **A, int m);
double** mult(double b, double **A, int m, int n);
double* mult(double *x, double b, int m);
double* mult(double b, double *x, int m);
double* mult(double **A, double *x, int m, int n);
double* mult(double *x, double **A, int m, int n); //checked on 07/08/08
double** mult(double **A, double **B, int m);
double* mult(double *x, double **A, int m);  //checked on 07/08/08
double* mult(double **A,  double *x, int m);



// Date: 16 Feb 2007
double** outp( double *x,  double *y, int m, int n); 
double* add( double *x,  double *y, int m);  
double** add(double **x, double **y, int m, int n);  
double* sub( double *x,  double *y, int m);  
double** sub( double **x,  double **y, int m, int n);  
double dot(double *x, double *y, int m);
// Date: 17 Feb 2007
double** row_concat( double **A,  double **B, int m, int n, int p);   //checked on 07/08/08
double** col_concat( double **A,  double **B, int m, int n, int p);   //checked on 07/08/08 
double* concat( double *x,  double *y, int m, int n);
double* extract( double *x, int begin, int end);
double** extract( double **x, int begin[4]);
double* extract( double **x, int row, int col, int m, char choice);


//Date: 19 Feb 2007
double** assign( double **A, int m, int n);
double* assign( double *x, int m);
double** trans( double **A, int m, int n);
double trace( double **A, int m);

//double fnorm(double **A, int m, int n);
double norm2( double *x, int m);
double* random(int m);
double** random(int m, int n);

double* mat2vec( double **A, int m, int n);
double** vec2mat( double *a, int m, int n);

void display(double **q,int row,int col);
void display(double *q, int row, int col);
void display(double *q, int col);
void display(int **q, int row, int col);
void display(int *q, int col);

double** mymatrix(int, int);
double** mymatrix(int);
double** mymatrix(double **a, int m, int n);
double** mymatrix(double *a, int m, int n);
double** mymatrix(double *a, int m);
double** mymatrix(double a, int m);

double* myvector(int m);
double* myvector( double *a, int m);
double* eye(int m);
double** eye(int m, int n);

//deallocate memory
void free(double *x);
void free(double **x, int m);
void free(double ***x, int m, int n);

//----------------------------------
// Date: 06 May 2008
// 4th order fixed-step Runge-kutta routine
// -------------------------------------
void runge_kutta(double *xdot, double *x,void *param, double t,double h,int n,
    void (*func_dxdt)(double*, double*, double, void*));


//---------------------------------------------------
// Lapack routines
// Date: 05 October 2008, Sunday
// -------------------------------------------------
 
// Least Square routines
//void dgels(double **A, double *b, int m, int n, double *x);
//void dgelsy(double **A, double *b, int m, int n, double *x);
//void dgelss(double **A, double *b, int m, int n, double *x);
//void dgelss_pi(double **A, double *b, int m, int n, double *x);

// Pseudo -inverse routine
//double ** pinv(double **A, int m, int n);

// Converting C datatype to Fortan datatypes
//double* ctof_cm(double **in, int rows, int cols);

//normalize or denormalize a myvector
// Date: 30 January 2009
//void normalize(double *y, double *yn, int m, double *max, double *min, int bpf=1);
//void denormalize(double *yn, double *y, int m, double *max, double *min, int bpf=1);




#endif 

