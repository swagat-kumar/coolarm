#include<iostream>
#include <matrix.h>

//#define DDOT
//#define MAT
#define MULT
//#define MXMT
//#define TEST
//#define DGEMV
//#define FNORM
//#define EXTRACT
//#define CONCAT

using namespace std;

int main()
{

#ifdef DDOT
  double *x, *y, s;
  double a[5] = {1.0, 2.0, 5, 7, 8};
  double b[5] = {4, 8, 10, 7, 2};

  int n = 5;

  x = vector(a,n);
  y = vector(b,n);

  s = dot(x,y,n);

  cout << "x.y = " << s << endl;

  free(x);
  free(y);
#endif

#ifdef MAT
  double **A;
  double a[6] = {1, 2, 3, 4, 5, 6};

  int m = 2, n = 3;

  A = matrix(a, 2, 3);

  display(A,2,3);

#endif


#ifdef MULT

  // Checked on 07 August 2008
  // Found to be correct

  double **A, **B, **C, *x, *d, *e;
  double b[6] = {1, 2, 3, 4, 5, 6};
  double a[6] = {7, 8, 9, 10, 11, 12};
  double c[2] = {1, 2};

  A = matrix(a, 2, 3);
  B = matrix(b, 3, 2);
  C = matrix(2,2);
  x = vector(c, 2);
  d = vector(2);
  e = vector(3);

  C = mult(A, B, 2, 3, 2);
  d = mult(C,x,2,2);
  e = mult(x, A, 2, 3);

  cout << "A = ";
  display(A, 2, 3);
  cout << "B = ";
  display(B, 3, 2);
  cout << "C = A *B = ";
  display(C,2,2);
  cout << "d = ";
  display(d,2);
  cout << "e = ";
  display(e,3);

  free(A, 2);
  free(B, 3);
  free(C,2);
  free(d);
  free(x);
  free(e);
#endif

#ifdef MXMT
  double **A, **B, **C;
  double b[6] = {1, 2, 3, 4, 5, 6};
  double a[6] = {7, 8, 9, 10, 11, 12};
  A = matrix(a, 2, 3);
  B = matrix(b, 3, 2);
  C = matrix(3,3);  

  C = mxmt(A, B, 3, 3, 2);
  display(A, 2, 3);
  display(B, 3, 2);
  display(C,3,3);
  free(A);
  free(B);
  free(C);
#endif

#ifdef TEST
  double ***L, *B, **C;
  double b[3] = {1, 2, 3};
  int i, j, k;

  B = vector(b, 3);
  C = matrix(5, 4);

  L = new double **[5];
  for(i = 0; i < 5; i++)
  {
    L[i] = new double *[4];
    for(j = 0; j < 4; j++)
    {
      L[i][j] = new double [3];
      for(k = 0; k < 3; k++)
        L[i][j][k] = i*j*k;
    }
  }  

  for(i = 0; i < 5; i++)
  {
    C[i] = mxv(L[i], B, 4, 3);

    display(L[i], 4, 3);
  }


  display(B, 3);
  display(C, 5, 4);

  free(B);
  free(C);
  delete [] L;
#endif
#ifdef DGEMV
  double **A, *x, *y;

  double a[6] = {1, 2, 3, 4, 5, 6};
  double b[3] = {7, 8, 9};
  double c[2] = {10, 10};

  A = matrix(a, 2, 3);
  x = vector(b, 3);
  y = vector(c, 2);

  y = dgemv(A, x, y, 1, 1, 2, 3); 

  display(A, 2, 3);
  display(x,3);
  display(y,2);

  free(A);
  free(x);
  free(y);
#endif
  

  //Frobenius norm
  //Date: 03 February 2007
  //Status: OK
#ifdef FNORM
  double **A, f;
  double a[6] = {1, 2, 3, 4, 5, 6};

  A = matrix(a, 2, 3);
  f = fnorm(A, 2, 3);

  cout << "Matrix A:\n";
  display(A, 2, 3);

  cout << "Frobenius norm = " << f << endl;
#endif

#ifdef EXTRACT
  double *x = random(7);
  double *y = vector(4);
  y = extract(x, 0, 3);

  cout << "y = "; display(y, 4);
  cout << "x = "; display(x, 7);

  double **A = random(8, 5);
  double **B = matrix(3,2);
  double *d = vector(5);

  int range[4]={3,5,3,4};
  B = extract(A, range); 
  d = extract(A, 8, 5, 3, 'r');
  
  cout << "A = " ; display(A, 8,5);
  cout << "B = "; display(B,3,2);
  cout << "d = "; display(d, 5);
#endif

#ifdef CONCAT
  //checked on 07 August 2008
  //Found to be correct

  double a[6] = {1, 2, 3, 4, 5, 6};
  double b[6] = {7, 8, 9, 10, 11, 12};

  double **A = matrix(a, 2, 3);
  double **B = matrix(b, 2, 3);
  double **C = matrix(4, 3);
  double **D = matrix(2,6);

  C = row_concat(A, B, 2, 2, 3);
  D = col_concat(A, B, 2, 3, 3);

  cout << "A = "; display(A, 2, 3);
  cout << "B = "; display(B, 2, 3);
  cout << "C = "; display(C, 4, 3);
  cout << "D = "; display(D, 2, 6);
#endif


  return 0;
}
       
