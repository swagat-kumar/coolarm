#include<iostream>
#include<cmath>
#include<iomanip>
#include <mymatrix.h>
#include<cstdlib>

using namespace std;

//------------------
// Date: 30 January 2009, friday
void normalize(double *x, double *xn, int m, double *x_max, double *x_min, int bpf)
{
  if(bpf == 1)
  {
    for(int i = 0; i < m; i++)
      xn[i] = 2.0 *((x[i] - x_min[i])/(x_max[i] - x_min[i]) - 0.5);
  }
  else if(bpf == 0)
  {
    for(int i = 0; i < m; i++)
      xn[i] = (x[i] - x_min[i])/(x_max[i] - x_min[i]);
  }
  else
  {
    cout << "invalid value for last argument ... use 0 or 1" << endl;
    exit(-1);
  }
}//EOF
//---------------------------------------------------
void denormalize(double *xn, double *x, int m, double *x_max, double *x_min, int bpf)
{
  if(bpf == 1)
  {
    for(int i = 0; i < m; i++)
      x[i] = x_min[i] + (xn[i]/2.0 + 0.5) * (x_max[i] - x_min[i]);
  }
  else if(bpf == 0)
  {
    for(int i = 0; i < m; i++)
      x[i] = x_min[i] + xn[i] * (x_max[i] - x_min[i]);
  }
  else
  {
    cout << "invalid value for last argument .. use 0 or 1" << endl;
    exit(-1);
  }
}

//----------------------------------------------------
// Multiplication of square matrices
// Date 06 November 2007 Tuesday
// Status: 
//------------------------------------------------------------
// Outer Product: C = x * y' 
// C: m x n
// x: m x 1
// y: n x 1
// Date: 16 February 2007, Friday
// Status: Not tested ....
// --------------------------------------------
double** outp( double *x,  double *y, int m, int n)
{
  int i, j;
  double **C;
  C = mymatrix(m,n);

  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      C[i][j] = x[i] * y[j];

  return(C);
}
//-------------------------------------------
double dot(double *x, double *y, int m)
{
  double s = 0;
  for(int i = 0; i < m; i++)
    s += x[i] * y[i];
  return (s);
}
//---------------------------------------------------
//Add two myvectors z = x + y
//Date: 16 Feb 2007 Friday
//Status: tested  .... ok
//-----------------------------------
double* add( double *x,  double *y, int m)
{
  int i;
  double *z;
  z = myvector(m);
  for(i = 0; i < m; i++)
    z[i] = x[i] + y[i];
  return(z);
}
//-----------------------------------------------
// Add two matrices C = A + B
double** add( double **A,  double **B, int m, int n)
{
  int i, j;
  double **C;
  C = mymatrix(m,n);
  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      C[i][j] = A[i][j] + B[i][j];
  return(C);
}
//----------------------------------------------------
//Subtract two myvectors z = x - y
//Date: 16 Feb 2007 Friday
//Status: tested .... ok
//-----------------------------------
double* sub( double *x,  double *y, int m)
{
  int i;
  double *z;
  z = myvector(m);
  for(i = 0; i < m; i++)
    z[i] = x[i] - y[i];
  return(z);
}
//-----------------------------------------------
// Subtract two matrices C = A - B
// Date: 16 Feb 2007
// Status: tested ... ok
// --------------------------------------------
double** sub( double **A,  double **B, int m, int n)
{
  int i, j;
  double **C;
  C = mymatrix(m,n);
  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      C[i][j] = A[i][j] - B[i][j];
  return(C);
}
//------------------------------------------------------
// Matrix concatenation
//
// Column concatenation: 
// A : m x n
// B : m x p
// C : m x (n+p)
// 
// C = [A | B]
//
// Date: 17 Feb 2007 Saturday
// Status: checked on 07/08/08
// ----------------------------------------------
double** col_concat( double **A,  double **B, int m , int n, int p)
{
  int i, j;
  double **C;
  C = mymatrix(m, n+p);

  for(i = 0; i < m; i++)
  {
    for(j = 0; j < (n+p); j++)
    {
      if(j < n)
        C[i][j] = A[i][j];
      else
        C[i][j] = B[i][j-n];
    }
  }
  return(C);
}
//--------------------------------------------------------
// Matrix Concatenation
//
// Row Concatenation
// A : m x n
// B : p x n
// C : (m + p) x n
//
// C : [ A
//       B ] 
//
// Date: 17 Feb 2007 Saturday
// Status: Checked on 07/08/08
// ------------------------------------------------
double** row_concat( double **A,  double **B, int m , int p, int n)
{
  int i, j;
  double **C;
  C = mymatrix(m+p, n);

  for(i = 0; i < (m+p); i++)
    for(j = 0; j < n; j++)
    {
      if(i < m)
        C[i][j] = A[i][j];
      else
        C[i][j] = B[i-m][j];
    }
  return(C);
}
//---------------------------------------------------- 
// Vector concatenation
//
// z = [x | y]
// 
// Date: 17 February 2007
// Status: tested ... ok
// -------------------------
double* concat( double *x,  double *y, int m, int n)
{
  int i;
  double *z;
  z = myvector(m+n);

  for(i = 0; i < m+n; i++)
  {
    if(i < m)
      z[i] = x[i];
    else
      z[i] = y[i-m];
  }
  return(z);
}
//-------------------------------------------
// creating submyvector
double* extract( double *x, int beg, int end)
{
  double *y = myvector(end - beg + 1);

  for(int i = beg; i <= end; i++)
    y[i] = x[i];

  return y;
}
//-------------------------------------------------
double* extract( double **x, int row , int col, int m, char choice)
{
  double *y;

  if(choice == 'c')
  {
    y = myvector(row);
    for(int i = 0; i < row; i++)
      y[i] = x[i][m];
  }
  else if(choice == 'r')
  {
    y = myvector(col);
    for(int i = 0; i < col; i++)
      y[i] = x[m][i];
  }
  else
  {
    cout << "Invalid choice \n";
    exit(-1);
  }

  return y;
}
//----------------------------------------------------
double** extract( double**B, int range[4])
{
  int row_length = range[1] - range[0] + 1;
  int col_length = range[3] - range[2] + 1;

  double **A = mymatrix(row_length, col_length);

  int m = 0;;
  for(int i = range[0]; i <= range[1]; i++)
  {
    int n = 0;
    for(int j = range[2]; j <= range[3]; j++)
    {
      A[m][n] = B[i][j];
      n = n + 1;
    }
    m = m + 1;
  }

  return(A);
}

//------------------------------------------
double** assign( double **A, int m, int n)
{
  double **B;
  int i, j;
  B = mymatrix(m,n);
  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      B[i][j] = A[i][j];
  return(B);
}
//------------------------------------------
double* assign( double *x, int m)
{
  double *y = myvector(m);
  
  for(int i = 0; i < m; i++)
    y[i] = x[i];

  return(y);
}
//-------------------------------------------
double* mat2vec( double **A, int row, int col)
{
  double *b = myvector(row*col);

  for(int i = 0; i < row; i++)
    for(int j = 0; j < col; j++)
      b[i*col+j] = A[i][j];
  return b;
}
//---------------------------------------------
double** vec2mat( double *a, int row, int col)
{
  double **B = mymatrix(row, col);
  for(int i = 0; i < row; i++)
    for(int j = 0; j < col; j++)
      B[i][j] = a[i*col+j];

  return B;
}
//------------------------------------------------
// Generates a random myvector with elements between 0 and 1
double* random(int m)
{
  srand(time(NULL));

  double *y = myvector(m);

  for(int i = 0; i < m; i++)
    y[i] = rand()/(double)RAND_MAX;

  return(y);
}
//--------------------------------------------------
// Generates a random mymatrix with elements between 0 and 1
double** random(int m, int n)
{
  srand(time(NULL));
  double**A = mymatrix(m, n);

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      A[i][j] = rand()/(double)RAND_MAX;

  return(A);
}

//------------------------------------------------
// scalar multiplication
// y =  x * a
double* mult( double *x, double b, int m)
{
  double *y;
  y = myvector(m);
  int i;

  for(i = 0; i < m; i++)
    y[i] = b * x[i];

  return(y);
}
//------------------------------------
// scalar multiplication
// y = a * x
double* mult(double b, double *x, int m)
{
  double *y;
  y = myvector(m);
  int i;

  for(i = 0; i < m; i++)
    y[i] = b * x[i];

  return(y);
}
//---------------------------------
// Multiplication of a mymatrix with a scalar
// C = a * B
double** mult( double **B, double a, int m, int n)
{
  double **C;
  C = mymatrix(m,n);
  int i, j;

  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      C[i][j] = a * B[i][j];

  return(C);
}
//----------------------------------
double** mult( double a, double **B, int m, int n)
{
  double **C;
  C = mymatrix(m,n);
  int i, j;

  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      C[i][j] = a * B[i][j];

  return(C);
}
//--------------------------
double** mult( double **B, double a, int m)
{
  double **C = mymatrix(m,m);
  int i, j;

  for(i = 0; i < m; i++)
    for(j = 0; j < m; j++)
      C[i][j] = a * B[i][j];

  return(C);
}
//---------------------------
double** mult(double a, double **B, int m)
{
  double **C = mymatrix(m,m);
  int i, j;

  for(i = 0; i < m; i++)
    for(j = 0; j < m; j++)
      C[i][j] = a * B[i][j];

  return(C);
}
//---------------------------------------------
// Multiplying two square matrices
// C = A * B
double** mult( double **A,  double **B, int m)
{
  double **C;
  C = mymatrix(m,m);

  for(int i = 0; i < m; i++)
    for(int j = 0; j < m; j++)
    {
      double s = 0.0;
      for(int k = 0; k < m; k++)
        s += A[i][k] * B[k][j];
      C[i][j] = s;
    }

  return(C);
}
//------------------------------------------
// Multiplying two general matrices
// C : m x n
// A : m x p
// B : p x n
// C = A * B
// -----------------------------------------
double** mult( double **A,  double **B, int m, int p, int n)
{
  double **C = mymatrix(m,n);

  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
    {
      double s = 0.0;
      for(int k = 0; k < p; k++)
        s += A[i][k] * B[k][j];
      C[i][j] = s;
    }

  return(C);
}
//-------------------------------------------
// Matrix - myvector multiplication
// y = A * x
// A : m x n
// x : n x 1
// y : m x 1
//-------------------------------------------- 
double* mult( double **A,  double *x, int m, int n)
{
  double *y = myvector(m);

  for(int i = 0; i < m; i++)
  {
    double s = 0.0;
    for(int j = 0; j < n; j++)
      s += A[i][j] * x[j];
    y[i] = s;
  }

  return(y);
}
//-------------------------------------------
// y = A * x
// A : m x m
// x : m x m
// y : m x 1
//-------------------------------------------------- 
double* mult( double **A,  double *x, int m)
{
  double *y = myvector(m);

  for(int i = 0; i < m; i++)
  {
    double s = 0.0;
    for(int j = 0; j < m; j++)
      s += A[i][j] * x[j];
    y[i] = s;
  }

  return(y);
}
//-------------------------------------------------
// y' = x' * A
//
// y' : 1 x n
// x' : 1 x m
// A  : m x n
//----------------------------------------------- 
double* mult( double *x,  double **A, int m, int n)
{
  double *y = myvector(n);

  for(int i = 0; i < n; i++)
  {
    double s = 0.0;
    for(int j = 0; j < m; j++)
      s += x[j] * A[j][i];
    y[i] = s;
  }

  return(y);
}
//--------------------------------
// y' = x' * A
//
// A : m x m
// x' : 1 x m
// y' : 1 x m
double* mult( double *x,  double **A, int m)
{
  double *y = myvector(m);

  for(int i = 0; i < m; i++)
  {
    double s = 0.0;
    for(int j = 0; j < m; j++)
      s += x[j] * A[j][i];
    y[i] = s;
  }

  return(y);
}
//-------------------------------------------------
// Matrix transpose
double** trans( double **A, int m, int n)
{
  int i, j;
  double **B;
  B = mymatrix(n,m);
  
  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++)
      B[j][i] = A[i][j];

  return(B);
}
//------------------------------------
// Matrix Trace
inline double trace( double **A, int m)
{
  int i;
  double s;
  s = 0;
  for(i = 0; i < m; i++)
    s += A[i][i];
  return(s);
}
//--------------------------------------
// myvector norm
double norm2( double *x, int n)
{
  double s = 0.0;
  for(int i = 0; i < n; i++)
    s += x[i] * x[i];

  return (sqrt(s));
}

//--------------------------------------
// Compute Frobenius norm
// fnorm(A) = sqrt[ trace(A * A') ]
// Status: Tested ... OK
// Date: 03 February 2007
// ----------------------------------
/*double fnorm(double **A, int m, int n)
{
  double s;

  s = trace( mxm(A, trans(A, m, n), m, m, n), m);

  return(sqrt(s));
}*/
//------------------------------------------------------------------------
// BLAS routines uses Matrices in Column-Major mode and hence need to be
// transformed accordingly. 
// ---------------------------------------------------------------------
double* ctof_cm( double **in, int rows, int cols)
{
  double *out;
  int i, j;

  out = new double[rows*cols];
  for (i = 0; i < rows; i++)
    for (j = 0; j < cols; j++) 
      out[i + j * rows] = in[i][j]; // column-major mode
  return(out);
} 
//-------------------------------------------------------
//mymatrix display routines
void display( double *q, int row, int col)
{
  int i,j;
  cout << setprecision(4) << showpos << showpoint;
  cout << endl;
  for(i = 0; i < row; i++)
  {
    for(j = 0; j < col; j++)
      cout << setw(10) << scientific << q[i * col + j] << "\t";
    cout << endl;
  }
  cout << endl;
  cout << noshowpoint << noshowpos << dec << endl;
}
//--------------------------------------------
void display(double *q, int row)
{
  cout << setprecision(4) << showpos << showpoint;
  int i;
  cout << endl;
  for(i = 0; i < row; i++)
    cout << setw(10) << scientific << q[i] << "\t";
  cout << noshowpos << noshowpoint << dec << endl << endl;
}                      
//------------------------------------------
void display(double **q, int row, int col)
{
  int i,j;
  cout << setprecision(4) << showpos << showpoint;
  cout << endl;
  for(i = 0; i < row; i++)
  {
    for(j = 0; j < col; j++)
      cout << scientific << setw(10) << q[i][j] << "\t";
    cout << endl;
  }
  cout << noshowpos << noshowpoint << dec << endl << endl;
}  
//-----------------------------------------
void display(int **q, int row, int col)
{
  int i,j;
  cout << endl;
  for(i = 0; i < row; i++)
  {
    for(j = 0; j < col; j++)
      cout << setw(10) << q[i][j] << "\t";
    cout << endl;
  }
  cout << endl << endl;
}  
//-------------------------------------------
void display(int *q, int row)
{
  cout << setprecision(4) << setw(10) << showpos;
  int i;
  cout << endl;
  for(i = 0; i < row; ++i)
    cout << q[i] << "\t";
  cout << endl << endl;
}                      
//--------------------------------------
double* myvector(int m)
{
  int i;
  double *x = new double [m];
  for(i = 0; i < m; ++i)
    x[i] = 0.0;
  return(x);
}
//--------------------------------
double* myvector( double *a, int m)
{  
  int i;
  double *x;
  x = new double [m];
  for(i = 0; i < m; i++)
    x[i] = a[i];
  return(x);
}
//---------------------------------
double** mymatrix(int m, int n)
{
  int i,j;
  double **x;
  x = new double *[m];
  for(i = 0; i < m; i++)
  {
    x[i] = new double [n];
    for(j = 0; j < n; j++)
      x[i][j] = 0.0;
  }
  return(x);
}
//-------------------------------------
double** mymatrix(int m)
{
  double **x = new double *[m];
  for(int i = 0; i < m; i++)
  {
    x[i] = new double [m];
    for(int j = 0; j < m; j++)
      x[i][j] = 0.0;
  }
  return(x);
}
//---------------------------------
double** mymatrix(double *a, int m, int n)
{
  int i, j, k;
  double **x;

  k = 0;
  x = new double *[m];
  for(i = 0; i < m; i++)
  {
    x[i] = new double [n]; 
    for(j = 0; j< n; j++)
    {
      x[i][j] = a[k];
      k = k + 1;
    }
  }
  return(x);
}
//---------------------------------------------
double** mymatrix( double **a, int m, int n)
{  
  int i, j;
  double **x;
  x = new double *[m];
  for(i = 0; i < m; i++)
  {
    x[i] = new double [n];
    for(j = 0; j < n; j++)
      x[i][j] = a[i][j];
  }
  return(x);
}
//-------------------------------------------
// Identity mymatrix and myvector
double** eye(int m, int n)
{
  int i, j;
  double **x;
  x = new double *[m];
  for(i = 0; i < m; i++)
  {
    x[i] = new double [n];
    for(j = 0; j < n; j++)
    {
      if(i == j)
        x[i][j] = 1.0;
      else 
        x[i][j] = 0.0;
    }
  }
  return(x);
}
//---------------------------------------------
double* eye(int m)
{
  int i;
  double *x;
  x = new double [m];
  for(i = 0; i < m; i++)
    x[i] = 1.0;
  return(x);
}
//------------------------------
// Diagonal mymatrix with same diagonal elements
// Date: 17 Aug 2008 Sunday
// -------------------------------------------
double** mymatrix(double a, int m)
{
  double **x = new double *[m];
  for(int i = 0; i < m; i++)
  {
    x[i] = new double [m];
    for(int j = 0; j < m; j++)
    {
      if(i == j)
        x[i][j] = a;
      else
        x[i][j] = 0;
    }
  }
  return(x);
}
//------------------------------------------------------------
// Diagonal mymatrix with diagonal elements provided in a myvector
// Date: 17 August 2008 Sunday
// -----------------------------------------------------------
double** mymatrix(double *a, int m)
{
  double **x = new double *[m];
  for(int i = 0; i < m; i++)
  {
    x[i] = new double [m];
    for(int j = 0; j < m; j++)
    {
      if(i == j)
        x[i][j] = a[i];
      else
        x[i][j] = 0;
    }
  }
  return(x);
}
//-----------------------------------
// Memory deallocation routines
void free(double *x)
{
  delete [] x;
}
//deallocating an 2D array
// where m is the first dimension, i.e., x[m][n]
void free(double **x, int m)
{
  for(int i = 0; i < m; i++)
  {
    delete [] x[i];
  }
  delete [] x;
}
//deallocating 3D array x[m][n][p]
void free(double ***x, int m, int n)
{
  for(int i = 0; i < m; i++)
  {
    for(int j = 0; j < n; j++)
      delete [] x[i][j];
    delete [] x[i];
  }
  delete [] x;
}
