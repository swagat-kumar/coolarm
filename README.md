Description: Programs related to CoolARM robot manipulator
Author: Swagat Kumar
Email: swagat.kumar@gmail.com
License: GPL V 2.0 or higher

--------------------------------------
Downloads: 
+ Clone the CoolARM project into the src folder of your workspace.
   + $ git clone git@gitlab.com:swagat-kumar/coolarm.git -b sharath

+ Clone the gnuplot_ci project into the src folder of your workspace.
   + $ git clone git@gitlab.com:swagat-kumar/gnuplot_ci.git

+ Clone the NNLIB project into the src folder of your workspace.
   + $ git clone git@gitlab.com:swagat-kumar/nnlib.git

-----------------------------
Dependencies for cpp files:

- gnuplot_ci library 
- nnlib library 
- GNUPLOT V4.x
- OpenCV 2.4.x
- ROS Hydro Full installation
   + Link: http://wiki.ros.org/hydro/Installation/Ubuntu
- openni_launch
   + $ sudo apt-get install ros-hydro-openni-launch
   + Link: http://wiki.ros.org/openni_launch
- rviz
   + $ sudo apt-get install ros-hydro-rviz
   + Link: http://wiki.ros.org/rviz
- kinect_aux
   + $ sudo apt-get install ros-hydro-kinect-aux
   + Link: http://wiki.ros.org/kinect_aux
- rosserial_python 
   + $ sudo apt-get install ros-hydro-rosserial-python
   + Link: http://wiki.ros.org/rosserial_python?distro=hydro
- ROS hydro dynamixel
   + $ sudo apt-get install ros-hydro-dynamixel*
- LAPACK library 
   + $ sudo apt-get install liblapack-dev

-----------------------------
Build:

+ In terminal change the path to your catkin workspace
   + $ cd /path/to/catkin_workspace
   + $ catkin_make

+ The executables will be created in devel/lib

-----------------------------
Run: 

+ Get permissions to read and write on the USB connecting arm and arduino board
  + $ sudo chmod a+rwx /dev/ttyUSB0 // Incase USB shows ttyUSB1 reconnect the power cable and USB until it shows USB0.
  + $ sudo chmod a+rwx /dev/ttyACM0
  
Make use of one of the following 2 methods.

+ Method 1:
   + Open the terminal and launch the all.launch from the grasp_affordance
      + $ roslaunch grasp_affordance all.launch

   + Open new tab in the terminal and launch the localization_sensor.launch from the grasp_affordance 
      + $ roslaunch grasp_affordance localization_sensor.launch

+ Method 2: Run the following codes on new terminal
   + $ roslaunch openni_launch openni.launch
   + $ rosrun rosserial_python serial_node.py /dev/ttyACM0
   + $ roslaunch cool1000_controller cool1000_controller_dummy.launch
   + $ rosrun cool1000_controller set_all_torque_dummy.py 1
   + $ rosrun cool1000_controller pose_zero_dummy.py
   + $ roslaunch grasp_affordance localization_sensor.launch

+ In case the arm doesnot reach the home position properly or in case of failure of any motors then unplug the power cable for the arm and relaunch cool1000_controller_dummy.launch, rerun the following
   + $ roslaunch cool1000_controller cool1000_controller_dummy.launch
   + $ rosrun cool1000_controller set_all_torque_dummy.py 1
   + $ rosrun cool1000_controller pose_zero_dummy.py

+ Run the rviz for visualization of the grasp localizations
   + $ rosrun rviz rviz
   + Add display type "PointCloud2" and subscribe to /camera/depth_registered/points topic. Change the Fixed Frame in Global Options to camera_depth_optical_frame.
   + Add display type "marker_array" and subscribe to /demo_video/principal_axes_vectors.
   
+ Note: 
   + Parameters for the grasp_affordance localization algorithm can be changed in the localization_sensor.launch
   + You can specify the workspace limits with respect to the coolarm base frame(robot_base) in the localization_sensor.launch. The workspace limits specify the space within which grasp localizations has to obtained.
   + In case of repeated failure of the motors reconnect the cable connecting to the motor.
   
-----------------------------
Folders:
+ arm_interface
   - CMakeLists.txt
   - package.xml
   + include
      + arm_interface
         - callbacks.h
         - dynamixel_ros_speed_control.h
         - includedefs.h
         - interfacecontrol.h
   + scripts
      - arm_control_publisher.py
      - arm_control.py
   + src
      - arm_interface.cpp
      - arm_interface_orientation.cpp
      - arm_interface_simulation.cpp
      - callbacks.cpp
      - dynamixel_ros_speed_control.cpp
      - interfacecontrol.cpp

+ cool1000_ros_experimental
   + cool1000_controller
      + config
         - cool1000_dummy.yaml
         - cool1000_dummy.yaml
      + launch
         - cool1000_controller_dummy.launch
         - cool1000_hardware.launch
      + scripts
         - angle.txt
         - efposn.txt
         - error.txt
         - parallel_servo.py
         - parallel_servo_dummy.py
         - play.py
         - pose_zero.py
         - pose_zero_dummy.py
         - rconfig.txt
         - record.py
         - reduce_velocity.sh
         - set_all_torque.py
         - set_all_torque_dummy.py
         - targetpos.txt
         
      - CMakeLists.txt
      - package.xml
   + cool1000_description
      + meshes
         - base_link.dae
         - Cool1000.dae
         - Cool 1000.stl
         - gripper_left.dae
         - gripper_right.dae
         - link1.dae
         - link2.dae
         - link3.dae
         - link4.dae
         - link5.dae
         - link6.dae
         - link7.dae
         + backup
            - base.stl
            - gripper_left.stl
            - gripper_right.stl
            - link1.stl
            - link2.stl
            - link3.stl
            - link4.stl
            - link5.stl
            - link6.stl
            - link7.stl
      + urdf
         - cool1000.dae
         - cool1000.gv
         - cool1000.pdf
         - cool1000.urdf
         + backup
            - cool1000.urdf
      - CMakeLists.txt
      - package.xml
   + cool1000_gazebo
      + config
         - cool1000_gazebo_control.yaml
      + launch
         - cool1000_gazebo_control.launch
         - ool1000_world.launch
      - CMakeLists.txt
      - package.xml
   + src
   - Readme.md
   
+ cpp
   + fwdkin
   - coolarm_fk.h
   - coolarm_fk.cpp
   - main.cpp
   - fk_test.cpp
   - frame.cpp
   - pi_jla.cpp
   - test_jacob.cpp
   - test_rbfn.cpp
   - test_angle_jacob.cpp
   - CMakeLists.txt
   
+ Grasping
   + arduino_codes
      + gripper_force_sensor
         - gripper_force_sensor.ino
   + grasp_affordance
      + include
         + localize_handle
            - affordances.h
            - callbacks.h
            - curvature_estimation_taubin.h
            - curvature_estimation_taubin.hpp
            - cylindrical_shell.h
      + launch
         - all.launch
         - localization_sensor.launch
         - localization_pcd_file.h
         - robot_demo.h
      + msg
         - HandlePose.msg
      + src
         - affordances.cpp
         - callbacks.cpp
         - cylindrical_shell.cpp
         - localization.cpp
      - CMakeLists.txt
      - package.xml
   + kinect_aux
      + src
         - kinect_aux_node.cpp
         - CMakeLists.txt
         - package.xml
         - README.md
         - CHANGELOG.rst
   + kinect_setup_tf
      + src
         - kinect_tf_broadcaster.cpp
      + include
         + kinect_setup_tf
      + launch
         - kinect_base_tf.launch
      - CMakeLists.txt
      - package.xml
   - readme
   
+ images
   - dh_par.jpg

+ latex
   + fig
      - error_jt.tex
      - pose_config.tex
      - rconfig.tex
      - rconfig2_jla.tex
      - thnorm.tex
      - traj_error.tex
      - traj_error_jla.tex
   - invkin.aux
   - invkin.bbl
   - invkin.bib
   - invkin.blg
   - invkin.dvi
   - invkin.log
   - invkin.pdf
   - invkin.tex
      
+ maxima
   - fk.mac
   - fk.pdf
   - jacobian.mac
   - jacobian2.mac
   - orientation_pp.mac

+ octave 
   - DHCoolArm.m
   - ForwardKinematics.m
   
+ scilab
   - fk-puma.sce
   - fwdkin.sce
   - jacob_fk.sce
   - jacob_pp.sce
   - rotation.sce
   - velocity_ik.sce
   
+ util
   - matrix_test.cpp
   - mymatrix.cpp
   - mymatrix.h
 
+ vmc_src
   - ksom_rbf_inv.cpp
   - ksom_vmc.cpp
   - ksom_vmc.h
   - powercube_7dof_cam.h
   
- README.md
      
--------------------
+ arm_interface:
   + Package to interface the grasp localization algorithm and the CoolArm hardware. Receives end-effector position and orientation from the localization algorithm. Does the path planning to reach the object. Picks and place the object into a dropbox.

+ cool1000_ros_experimental
   + cool1000_controller: 
      + Contains the configuration file for the coolarm. Launch file to run the dynamixel motors of the arm. Scripts to control the arm.
   + cool1000_description:
      + Contains the urdf model for the coolarm
   + cool1000_gazebo: 
      + Contains files related to gazebo

+ cpp
   + fwdkin: 
      + Library for computation of forward and inverse kinematics for the coolarm.

+ Grasping
   + arduino_codes: 
      + Contains the program to be loaded into the arduino board. The program obtains reading from the force sensors and publishes them into ros topics. Read the "readme" file in the arduino codes to know how to upload the code to the arduino board.
   + grasp_affordance: 
      + Package to obtain the grasp localizations of the point cloud scene. This package is downloaded from http://wiki.ros.org/handle_detector. Modified to suit our project, with the algorithm for localization remaining the same. Publishes the end-effector position and orientation into a ros topic.
   + kinect_setup_tf: 
      + Package to obtain transformation from the camera_depth_optical_frame of kinect to the coolarm's base frame(robot_base). In case of rearrangement of kinect or robot's position make suitable change in the kinect_tf_broadcaster.cpp file.

+ Latex: 
   + Documentation on Inverse Kinematics methods

+ maxima: 
   + Generating forward kinematic equations using CAS

+ scilab:

-----------------------------











