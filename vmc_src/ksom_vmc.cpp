/* -----------------
 * 3D KSOM for VMC
 *
 * Date: 24 June 2008 Tuesday
 *
 * Last Update: 20 August 2008 Wednesday
 * --------------------------------------------*/
#include<iostream>
#include<cmath>
#include<fstream>
#include<iomanip>
#include<new>
#include "ksom_vmc.h"
#include <cstdlib>

using namespace std;
using namespace vmc;


//-------------------------------------
ksom3::ksom3(int Nip[5])
{
  // Number of nodes in each direction
  Nx = Nip[0];
  Ny = Nip[1];
  Nz = Nip[2];
  NIP = Nip[3];  // size of w vector
  NOP = Nip[4]; // size of output vector

  maxNodeCount = Nx * Ny * Nz;

  srand(time(NULL));

  try
  {
    W = new double *[maxNodeCount];
    TH = new double *[maxNodeCount];
    A = new double **[maxNodeCount];
    X = new double [NIP];
    Y = new double [NOP];
    A0 = new double *[NOP];

    for(int i = 0; i < NOP; i++)
    {
      A0[i] = new double [NIP];
      for(int j = 0; j < NIP; j++)
        A0[i][j] = 0.0;
    }

    idx = new int **[Nx];
    ridx = new int *[maxNodeCount];
    int cnt = 0;
    for(int i = 0; i < Nx; i++)
    {
      idx[i] = new int *[Ny];
      for(int j = 0; j < Ny; j++)
      {
        idx[i][j] = new int [Nz];
        for(int k = 0; k < Nz; k++)
        {
          idx[i][j][k] = cnt;

          ridx[cnt] = new int [3];
          ridx[cnt][0] = i;
          ridx[cnt][1] = j;
          ridx[cnt][2] = k;

          cnt = cnt + 1;
        }
      }
    }

    for(int i = 0; i < maxNodeCount; i++)
    {
      W[i] = new double[NIP];
      TH[i] = new double[NOP];
      A[i] = new double *[NOP];

      for(int j = 0; j < NOP; j++)
        A[i][j] = new double [NIP];
    }
  }catch(bad_alloc)
  {
    cout << "Memory allocation failure in ksom class" << endl;
    exit(-1);
  }

  int i = 0;
  for(int m = 0; m < Nx; m++)
    for(int n = 0; n < Ny; n++)
      for(int p = 0; p < Nz; p++)
      {
        for(int j = 0; j < NIP; j++)
          W[i][j] = -1.0 + 2.0 * rand()/(double)RAND_MAX;

        for(int j = 0; j < NOP; j++)
        {
          TH[i][j] = -1.0 + 2.0 * rand()/(double)RAND_MAX;
          for(int k = 0; k < NIP; k++)
            A[i][j][k] = -0.1 + 0.2 * rand()/(double)RAND_MAX;
        }
        i = i + 1;
      }
}//End of function

//==============================
//Copy Contructor

ksom3::ksom3(const ksom3 &a)
{
  // Number of nodes in each direction
  Nx = a.Nx;
  Ny = a.Ny;
  Nz = a.Nz;
  NIP = a.NIP;  // size of w vector
  NOP = a.NOP;  // size of output vector

  maxNodeCount = a.maxNodeCount;

  try
  {
    W = new double *[maxNodeCount];
    TH = new double *[maxNodeCount];
    A = new double **[maxNodeCount];
    X = new double [NIP];
    Y = new double [NOP];
    A0 = new double *[NOP];

    for(int i = 0; i < NOP; i++)
    {
      A0[i] = new double [NIP];
      for(int j = 0; j < NIP; j++)
        A0[i][j] = a.A0[i][j];
    }

    idx = new int **[Nx];
    ridx = new int *[maxNodeCount];
    int cnt = 0;
    for(int i = 0; i < Nx; i++)
    {
      idx[i] = new int *[Ny];
      for(int j = 0; j < Ny; j++)
      {
        idx[i][j] = new int [Nz];
        for(int k = 0; k < Nz; k++)
        {
          idx[i][j][k] = cnt;
          ridx[cnt] = new int [3];
          ridx[cnt][0] = i;
          ridx[cnt][1] = j;
          ridx[cnt][2] = k;
          cnt = cnt + 1;
        }
      }
    }

    for(int i = 0; i < maxNodeCount; i++)
    {
      W[i] = new double[NIP];
      TH[i] = new double[NOP];
      A[i] = new double *[NOP];

      for(int j = 0; j < NOP; j++)
        A[i][j] = new double [NIP];
    }
  }catch(bad_alloc)
  {
    cout << "Memory allocation in ksom3 copy constructor failed" << endl;
    exit(-1);
  }

  for(int i = 0; i < maxNodeCount; i++)
  {
    for(int j = 0; j < NIP; j++)
      W[i][j] = a.W[i][j];

    for(int j = 0; j < NOP; j++)
    {
      TH[i][j] = a.TH[i][j];
      for(int k = 0; k < NIP; k++)
        A[i][j][k] = a.A[i][j][k];
    }
  }

}//EOF

//================================================

ksom3::~ksom3()
{
  cout << "Destructor for ksom3 class is called ..." << endl;

  delete [] A;
  delete [] W;
  delete [] TH;
  delete [] X;
  delete [] Y;
  delete [] idx;
  delete [] ridx;
  delete [] A0;
}

//============================================

void ksom3::netinfo()
{
  cout << "-------- *** -----------" << endl;
  cout << "KSOM Lattice size = " << Nx << " x " << Ny << " x " << Nz << endl;
  cout << "No. of Inputs = " << NIP << endl;
  cout << "No. of Outputs = " << NOP << endl;
  cout << "Total number of nodes = " << maxNodeCount << endl;
  cout << "Dimension of W = " << maxNodeCount << " x " << NIP << endl;
  cout << "Dimension of TH = " << maxNodeCount << " x " << NOP << endl;
  cout << "Dimension of A = " << maxNodeCount << " x " << NOP << " x " << NIP << endl;
  cout << "-------- *** -----------" << endl;
}//EOF 

//===================================================

void ksom3::save_parameters()
{
  ofstream fw("ksom_weight.txt");
  ofstream ft("ksom_theta.txt");
  ofstream fa("ksom_jacob.txt");

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
      {
        for(int j = 0; j < NIP; j++)
          fw << W[idx[l][m][n]][j] << "\t";
        fw << endl;

        for(int j = 0; j < NOP; j++)
          ft << TH[idx[l][m][n]][j] << "\t";
        ft << endl;

        for(int j = 0; j < NOP; j++)
        {
          for(int k = 0; k < NIP; k++)
            fa << A[idx[l][m][n]][j][k] << "\t";
          fa << endl;
        }
        fa << endl << endl;
      }
  fw.close();
  ft.close();
  fa.close();
}//EOF

//==========================================================
void ksom3::save_weights(const char *filename)
{
  ofstream fw(filename);

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
      {
        for(int j = 0; j < NIP; j++)
          fw << W[idx[l][m][n]][j] << "\t";
        fw << endl;
      }


  fw.close();
}//EOF
//===================================================================
void ksom3::save_angles(const char *filename)
{
  ofstream ft(filename);

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
      {
        for(int j = 0; j < NOP; j++)
          ft << TH[idx[l][m][n]][j] << "\t";
        ft << endl;
      }
  ft.close();
}//EOF
//==========================================

void ksom3::read_parameters()
{
  ifstream fw("ksom_weight.txt");
  ifstream ft("ksom_theta.txt");
  ifstream fa("ksom_jacob.txt");

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
      {
        for(int j = 0; j < NIP; j++)
          fw >> W[idx[l][m][n]][j] ;

        for(int j = 0; j < NOP; j++)
          ft >> TH[idx[l][m][n]][j];

        for(int j = 0; j < NOP; j++)
          for(int k = 0; k < NIP; k++)
            fa >> A[idx[l][m][n]][j][k];
      }
  fw.close();
  ft.close();
  fa.close();
}
//================================================
// Date: 20 August 2008 Wednesday
void ksom3::save_weights(double **wt)
{
  
  int k = 0;
  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
      {
        for(int j = 0; j < NIP; j++)
          wt[k][j] = W[idx[l][m][n]][j] ;
        k = k + 1;
      }
}
//===============================================
void ksom3::load_angles(const char *filename)
{
  ifstream f1(filename);

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
        for(int j = 0; j < NOP; j++)
          f1 >> TH[idx[l][m][n]][j] ;
  f1.close();
}
//==============================================
void ksom3::load_weights(const char *filename)
{
  ifstream f1(filename);

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
        for(int j = 0; j < NIP; j++)
          f1 >> W[idx[l][m][n]][j] ;
  f1.close();
}
//==========================================

// Winner is obtained in the input space. Clusters in input and output
// spaces are created based on this winner index.

void ksom3::create_clusters(double eta, double sigma, const double *xd, 
    const double *thd, int tflag )
{
  if(sigma == 0.0)
    sigma += 0.0001;

  // Select a winner x, theta and phi spaces
  double Min_w = 50000;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double sw = 0.0;
        for(int l = 0; l < NIP; l++)  
          sw += pow((W[idx[i][j][k]][l] - xd[l]), 2.0);
        double zw = sqrt(sw);

        if(zw < Min_w)
        {
          Min_w = zw;
          windex[0] = i;
          windex[1] = j;
          windex[2] = k;
        }
      }//for each index

  //Update parameters
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        //compute lattice distance of each neuron
        //from winner neuron
        double dw = pow((windex[0]-i), 2.0) + 
          pow((windex[1]-j), 2.0) + pow((windex[2]-k), 2.0);

        //compute neighborhood function
        double hw = exp( -dw / (2 * sigma * sigma) );

        //update neuron weights
        for (int l = 0; l < NIP; l++) 
          W[idx[i][j][k]][l] += eta * hw * (xd[l] - W[idx[i][j][k]][l]); 

        if(tflag == 1)//update theta vectors
        {
          for (int l = 0; l < NOP; l++) 
            TH[idx[i][j][k]][l] += eta * hw * (thd[l] - TH[idx[i][j][k]][l]); 
        }

      }// Neuron index ... for each neuron    
}//EOF

//============================================

void ksom3::get_jacob(double ***a = NULL)
{
  if(a == NULL)
    return;
  else
  {
    for(int i = 0; i < Nx; i++)
      for(int j = 0; j < Ny; j++)
        for(int k = 0; k < Nz; k++)
        {
          for(int l = 0; l < NOP; l++)
            for(int m = 0; m < NIP; m++)
              a[idx[i][j][k]][l][m] = A[idx[i][j][k]][l][m];
        }
  }

}//EOF

//=====================================

void ksom3::parameter_init(double max, double min)
{
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        for(int l = 0; l < NIP; l++)
          W[idx[i][j][k]][l] = min + (max - min) * rand()/(double)RAND_MAX;

        for(int l = 0; l < NOP; l++)
        {
          TH[idx[i][j][k]][l] = min + (max - min) * rand()/(double)RAND_MAX;
          for(int m = 0; m < NIP; m++)
            A[idx[i][j][k]][l][m] =  min + (max - min) * rand()/(double)RAND_MAX; 
        }
      }

}//EOF

//=================================
//Call 'create_clusters' before calling  this function.

double* ksom3::coarse_movement(double sigma, double *x_t)
{
  double *sth = new double[NOP];
  double **sA = new double *[NOP];
  for(int i = 0; i < NOP; i++)
  {
    sA[i] = new double [NIP];
    sth[i] = 0.0;
    for(int j = 0; j < NIP; j++)
      sA[i][j] = 0.0;
  }

  shw = 0.0;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        int gamma = idx[i][j][k];
        double dw = pow((windex[0] - i), 2.0) + 
          pow((windex[1] - j), 2.0) + pow((windex[2] - k), 2.0);
        double hw = exp(-dw / (2 * sigma * sigma));

        for(int l = 0; l < NOP; l++)
        {
          double s1 = 0.0;
          for(int m = 0; m < NIP; m++)
          {
            s1 += A[gamma][l][m] * (x_t[m] - W[gamma][m]);
            sA[l][m] += hw * A[gamma][l][m];
          }
          sth[l] += hw * (TH[gamma][l] + s1);
        }
        shw += hw;
      }//for each node gamma

  double *th0 = new double [NOP];
  for(int i = 0; i < NOP; i++)
  {
    th0[i] = sth[i] / shw;
    for(int j = 0; j < NIP; j++)
      A0[i][j] = sA[i][j] / shw;
  }

  delete [] sth;
  delete [] sA;

  return th0;
}//EOF

//=============================================

double* ksom3::fine_movement(double *th0, double *x_t, double *v0)
{
  double *th1 = new double [NOP];
  for(int i = 0; i < NOP; i++)
  {
    double s1 = 0.0;
    for(int j = 0; j < NIP; j++)
      s1 += A0[i][j] * (x_t[j] - v0[j]);
    th1[i] = th0[i] + s1;
  }

  return(th1);
}//EOF

//==============================================

void ksom3::update_parameters_train(double *v1, double *v0, 
    double *th1, double *th0, double eta_a, double sigma)
{
  double *dv = new double[NIP];
  double dvnormsq = 0.0;
  for(int i = 0; i < NIP; i++)
  {
    dv[i] = v1[i] - v0[i];
    dvnormsq += dv[i] * dv[i];
  }

  if(dvnormsq < 1e-4)
    dvnormsq += 1e-4;
  
  double *dth = new double [NOP];
  for(int i = 0; i < NOP; i++)
    dth[i] = th1[i] - th0[i];

  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double dw = pow((windex[0] - i), 2.0) + 
          pow((windex[1] - j), 2.0) + pow((windex[2] - k), 2.0);
        double hw = exp(-dw /(2 * sigma * sigma));

        for(int l = 0; l < NOP; l++)
        {
          double s1 = 0.0;
          for(int m = 0; m < NIP; m++)
            s1 += A0[l][m] * dv[m];

          for(int m = 0; m < NIP; m++)
            A[idx[i][j][k]][l][m] += eta_a * hw * (dth[l] - s1) * dv[m] / (shw * dvnormsq);
        }
      }

  delete [] dv;
  delete [] dth;

}//EOF

//===============================

void ksom3::update_parameters_test(double *v1, double *v0, 
    double *th1, double *th0, double eta_a, double eta_t, double sigma)
{
  double *dv = new double[NIP];
  double dvnormsq = 0.0;
  for(int i = 0; i < NIP; i++)
  {
    dv[i] = v1[i] - v0[i];
    dvnormsq += dv[i] * dv[i];
  }

  if(dvnormsq < 1e-4)
    dvnormsq += 1e-4;

  double *dth = new double [NOP];
  for(int i = 0; i < NOP; i++)
    dth[i] = th1[i] - th0[i];

  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double dw = pow((windex[0] - i), 2.0) + 
          pow((windex[1] - j), 2.0) + pow((windex[2] - k), 2.0);
        double hw = exp(-dw * dw/(2 * sigma * sigma));

        for(int l = 0; l < NOP; l++)
        {
          double s1 = 0.0;
          double s2 = 0.0;
          for(int m = 0; m < NIP; m++)
          {
            s1 += A0[l][m] * dv[m];
            s2 += A[idx[i][j][k]][l][m] * (v0[m] - W[idx[i][j][k]][m]);
          }

          for(int m = 0; m < NIP; m++)
            A[idx[i][j][k]][l][m] += eta_a * hw * (dth[l] - s1) * dv[m] / (shw * dvnormsq);

          TH[idx[i][j][k]][l] += eta_t * hw * (th0[l] - TH[idx[i][j][k]][l] - s2) / shw;
        }
      }

  delete [] dv;
  delete [] dth;

}//EOF

//==========================================
// Date: 20 August 2008 Wednesday
// Status: Under Review
// ========================================
double* ksom3::theta_output(const double *xd, double sigma)
{
  // Select a winner in the input space
  double Min_w = 50000;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double sw = 0.0;
        for(int l = 0; l < NIP; l++)  
          sw += pow((W[idx[i][j][k]][l] - xd[l]), 2.0);
        double zw = sqrt(sw);

        if(zw < Min_w)
        {
          Min_w = zw;
          windex[0] = i;
          windex[1] = j;
          windex[2] = k;
        }
      }//for each index

  double *sth = new double [NOP];
  for(int i = 0; i < NOP; i++)
    sth[i] = 0.0;

  double shw = 0.0;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        int gamma = idx[i][j][k];
        double dw = pow((windex[0] - i), 2.0) + 
          pow((windex[1] - j), 2.0) + pow((windex[2] - k), 2.0);
        double hw = exp(-dw / (2 * sigma * sigma));

        for(int l = 0; l < NOP; l++)
          sth[l] += hw * TH[gamma][l];

        shw += hw;
      }

  for(int i = 0; i < NOP; i++)
    sth[i] = sth[i] / shw;

  return(sth);
}//EOF

//====================================================

//  VMC WITH SUB-CLUSTERS
//  CLASS DEFINITIONs FOR ksom3_sc
//
//
//  Date: 27 August 2008 Wednesday

//=============================================================
// theta: sub-cluster array
// nc : sub-cluster count : it is incremented when a new cluster is
// formed
//
// x : input theta vector of dimension DIM x 1
//
//
// ncmax: maximum sub-cluster count
//
// threshold: used for deciding whether to create new cluster
// ---------------------------------------------------------------
void create_subclusters2( double **theta, int *nc, const double *x, int ncmax, double threshold, double eta, int DIM)
{
  double sigma = 0.0001;
  // choose eta = 0.7;

  int count = *nc;
  int winner;

  if(count == 0)
  {
    count = count + 1;

    for(int i = 0; i < DIM; i++)
      theta[count-1][i] = x[i];

  }
  else
  {
    //Find a winner neuron
    double min = 5000;
    for(int i = 0; i < count; i++)
    {
      double sw = 0.0;
      for(int j = 0; j < DIM; j++)
        sw += pow((x[j] - theta[i][j]), 2.0);
      sw = sqrt(sw);

      if(sw < min)
      {
        min = sw;
        winner = i;
      }
    }

    // if minimum distance < threshold
    // this data point belongs to the 
    // winner node winner and hence update
    // the current nodes to represent this data
    // as well. Otherwise create a new node

    if(min < threshold)
    {
      //update centers
      for(int i = 0; i < count; i++)
      {
        //compute lattice distance of each neuron
        //from winner neuron

        double d = pow( (winner-i), 2.0);

        //compute neighborhood function
        double h = exp( -d / (2 * sigma * sigma) );


        //update neuron weights
        for (int l = 0; l < DIM; l++)     
          theta[i][l] += eta * h * (x[l] - theta[i][l]); 

      }
    }
    else // create new clusters 
    {
      if(count < ncmax) 
      {
        count = count + 1;

        for(int i = 0; i < DIM; i++)
          theta[count-1][i] = x[i];
      }
    }

  }// when count > 0

  //Return the number of clusters
  *nc = count;
}//EOF

//=====================================================
// constructor for class ksom3_sc

ksom3_sc::ksom3_sc(int N[], int ncmax):ksom3(N)
{
  NCMAX = ncmax;

  try
  {
    THETA = new double **[maxNodeCount];
    NC = new int [maxNodeCount];
    for(int i = 0; i < maxNodeCount; i++)
    {
      NC[i] = 0;
      THETA[i] = new double *[NCMAX];

      for(int j = 0; j < NCMAX; j++)
      {
        THETA[i][j] = new double [NOP];
        for(int k = 0; k < NOP; k++)
          THETA[i][j][k] = 0.0;
      }
    }
  }
  catch(bad_alloc){
    cout << "Memory allocation failure in ksom3_sc constructor" << endl;
    exit(-1);
  } 
}

//=========================================================

// copy constructor for class ksom3_sc

ksom3_sc::ksom3_sc(const ksom3_sc &a):ksom3(a)
{
  NCMAX = a.NCMAX;

  try
  {
    THETA = new double **[maxNodeCount];
    NC = new int [maxNodeCount];
    for(int i = 0; i < maxNodeCount; i++)
    {
      NC[i] = a.NC[i];
      THETA[i] = new double *[NCMAX];

      for(int j = 0; j < NCMAX; j++)
      {
        THETA[i][j] = new double [NOP];
        for(int k = 0; k < NOP; k++)
          THETA[i][j][k] = a.THETA[i][j][k];
      }
    }
  }
  catch(bad_alloc){
    cout << "Memory allocation failure in ksom3_sc copy constructor" << endl;
    exit(-1);
  } 
}

//==============================================================

ksom3_sc::~ksom3_sc()
{
  cout << "Destructor for class ksom3_sc is called ..." << endl;

  delete [] THETA;
  delete [] NC;
};

//============================================================

void ksom3_sc::netinfo()
{
  cout << "-------- *** -----------" << endl;
  cout << "KSOM Lattice size = " << Nx << " x " << Ny << " x " << Nz << endl;
  cout << "No. of Inputs = " << NIP << endl;
  cout << "No. of Outputs = " << NOP << endl;
  cout << "Total number of nodes = " << maxNodeCount << endl;
  cout << "Dimension of W = " << maxNodeCount << " x " << NIP << endl;
  cout << "Dimension of THETA = " << maxNodeCount << " x " << NCMAX << " x " << NOP << endl;
  cout << "-------- *** -----------" << endl;
}//EOF 
//==============================================
// Clusters in input (W) space is created as per KSOM algorithm
// More than one output cluster is created for each input cluster.
// --------------------------------------------------------------
void ksom3_sc::create_clusters(const double eta[], const double *xd, const double *thd, double threshold)
{
  int w_idx[3];

  double eta_w = eta[0];
  double eta_t = eta[1];
  double sigma = eta[2];

  // Select a winner in cartesian space
  double Min_w = 50000;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double sw = 0.0;
        for(int l = 0; l < NIP; l++)  
          sw += pow((W[idx[i][j][k]][l] - xd[l]), 2.0);
        double zw = sqrt(sw);

        if(zw < Min_w)
        {
          Min_w = zw;
          w_idx[0] = i;
          w_idx[1] = j;
          w_idx[2] = k;
        }

      }//for each index

  int windex = idx[w_idx[0]][w_idx[1]][w_idx[2]];

  //create subclusters in theta space for each winner
  create_subclusters2(THETA[windex], &NC[windex], thd, NCMAX, threshold, eta_t, NOP);

  //Update parameters
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        //compute lattice distance of each neuron
        //from winner neuron
        double dw = pow((w_idx[0]-i), 2.0) + 
          pow((w_idx[1]-j), 2.0) + pow((w_idx[2]-k), 2.0);

        //compute neighborhood function
        double hw = exp( -dw / (2 * sigma * sigma) );

        //update neuron weights
        for (int l = 0; l < NIP; l++) 
          W[idx[i][j][k]][l] += eta_w * hw * (xd[l] - W[idx[i][j][k]][l]); 

      }// Neuron index ... for each neuron    
}//EOF

//===================================================================

void ksom3_sc::save_angles(const char *filename)
{
  ofstream ft(filename);

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
        ft << NC[idx[l][m][n]] << endl;
  ft << endl << endl;

  for(int i = 0; i < maxNodeCount; i++)
  {
    for(int j = 0; j < NC[i]; j++)
    {
      for(int k = 0; k < NOP; k++)
        ft << THETA[i][j][k] << "\t";
      ft << endl;
    }

    ft << endl << endl;
  }
  ft.close();
}//EOF


//=====================================================
// Date: 27 August 2008, Wednesday
void ksom3_sc::load_angles(const char *filename)
{
  ifstream ft(filename);

  for(int l = 0; l < Nx; l++)
    for(int m = 0; m < Ny; m++)
      for(int n = 0; n < Nz; n++)
        ft >> NC[idx[l][m][n]];

  for(int i = 0; i < maxNodeCount; i++)
  {
    for(int j = 0; j < NC[i]; j++)
      for(int k = 0; k < NOP; k++)
        ft >> THETA[i][j][k];
  }

  ft.close();

}//EOF

//=======================================================
// This function returns the lazy arm joint angle output
// th_c: current theta configuration
// xd : target input vector
// sigma : spread of neighborhood function

// Date: 27 August 2008, Wednesday
// ======================================================

void ksom3_sc::LA_theta_output(const double *xd, const double *th_c, double sigma, double *sth)
{
  // Select a winner in the input space
  double Min_w = 50000;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double sw = 0.0;
        for(int l = 0; l < NIP; l++)  
          sw += pow((W[idx[i][j][k]][l] - xd[l]), 2.0);
        double zw = sqrt(sw);

        if(zw < Min_w)
        {
          Min_w = zw;
          windex[0] = i;
          windex[1] = j;
          windex[2] = k;
        }
      }//for each index

  int win_c; //index of winning subcluster

  double shw = 0.0;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        int gamma = idx[i][j][k];
        double dw = pow((windex[0] - i), 2.0) + 
          pow((windex[1] - j), 2.0) + pow((windex[2] - k), 2.0);
        double hw = exp(-dw / (2 * sigma * sigma));

        double min_th = 50000;
        for(int m = 0; m < NC[gamma]; m++)
        {
          double s1 = 0.0;
          for(int k = 0; k < NOP; k++)
            s1 += pow((THETA[gamma][m][k] - th_c[k]), 2.0);
          s1 = sqrt(s1);

          if(s1 < min_th)
          {
            min_th = s1;
            win_c = m;
          }
        }

        for(int l = 0; l < NOP; l++)
          sth[l] += hw * THETA[gamma][win_c][l];

        shw += hw;
      }

  for(int i = 0; i < NOP; i++)
    sth[i] = sth[i] / shw;

}//EOF

//----------------------------------------------------------
// This function returns the joint angle vector corresponding to the
// winning neuron in input space and nearest to the current angle
// vector th_c

void ksom3_sc::LA_theta_output(const double *xd, const double *th_c, double *sth)
{
  // Select a winner in the input space
  double Min_w = 50000;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double sw = 0.0;
        for(int l = 0; l < NIP; l++)  
          sw += pow((W[idx[i][j][k]][l] - xd[l]), 2.0);
        double zw = sqrt(sw);

        if(zw < Min_w)
        {
          Min_w = zw;
          windex[0] = i;
          windex[1] = j;
          windex[2] = k;
        }
      }//for each index

  int win_c; //index of winning subcluster

  int gamma = idx[windex[0]][windex[1]][windex[2]];

  double min_th = 50000;
  for(int m = 0; m < NC[gamma]; m++)
  {
    double s1 = 0.0;
    for(int k = 0; k < NOP; k++)
      s1 += pow((THETA[gamma][m][k] - th_c[k]), 2.0);
    s1 = sqrt(s1);

    if(s1 < min_th)
    {
      min_th = s1;
      win_c = m;
    }
  }


  for(int i = 0; i < NOP; i++)
    sth[i] = THETA[gamma][win_c][i];

}//EOF

//===================================================

double** ksom3_sc::all_config(const double *xd, int *nc) 
{
  // Select a winner in the input space
  double Min_w = 50000;
  for(int i = 0; i < Nx; i++)
    for(int j = 0; j < Ny; j++)
      for(int k = 0; k < Nz; k++)
      {
        double sw = 0.0;
        for(int l = 0; l < NIP; l++)  
          sw += pow((W[idx[i][j][k]][l] - xd[l]), 2.0);
        double zw = sqrt(sw);

        if(zw < Min_w)
        {
          Min_w = zw;
          windex[0] = i;
          windex[1] = j;
          windex[2] = k;
        }
      }//for each index

  int gamma = idx[windex[0]][windex[1]][windex[2]];

  double **th_c = new double *[NC[gamma]];
  for(int i = 0; i < NC[gamma]; i++)
  {
    th_c[i] = new double [NOP];

    for(int j = 0; j < NOP; j++)
      th_c[i][j] = THETA[gamma][i][j];
  }

  *nc = NC[gamma];


  return(th_c);
}//EOF

//======================================
// Provides sub-cluster count for each node (i,j,k)
// returns the total node count in the KSOM lattice
// Date: Feb 1, 2015
//=================================================
int ksom3_sc::get_subcluster_cnt(int *sc_cnt=NULL)
{
    int l = 0;
    if(sc_cnt != NULL)
    {

        for(int i = 0; i < Nx; ++i)
            for(int j = 0; j < Ny; ++j)
                for(int k = 0; k < Nz; ++k)
                {
                    sc_cnt[l] = NC[idx[i][j][k]];
                    l++;
                }
    }
    return(l);
}




