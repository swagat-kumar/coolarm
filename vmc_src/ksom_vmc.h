// KSOM for VMC
// Date: 24 June 2008 Tuesday
// Last Update: 20 August 2008 Wednesday
// ---------------------------------------------
#ifndef _KSOM_VMC
#define _KSOM_VMC

#include<iostream>
#include<new>

namespace vmc
{

  class ksom3
  {
    protected:
      int ***idx, NIP, NOP, maxNodeCount, **ridx;
      int Nx, Ny, Nz, windex[3];
      double **W, ***A, **TH, *X, *Y;
      double **A0, shw;

    public:
      ksom3(int N[]);
      ksom3(const ksom3 &a);
      ~ksom3();
      void netinfo();
      void save_parameters();
      void read_parameters();
      void parameter_init(double max, double min);
      void create_clusters(double eta, double sigma, 
          const double *xd, const double *thd, int tflag);
      void get_jacob(double ***a);
      double* coarse_movement(double sigma, double *x_t);
      double* fine_movement(double *th0, double *xt, double *v0);
      void update_parameters_train(double *v1, double *v0, 
          double *th1, double *th2, double eta_a, double sigma);
      void update_parameters_test(double *v1, double *v0, 
          double *th1, double *th2, double eta_a, double eta_t, double sigma);


      //20 Aug 2008 

      void save_weights(double **wt);
      void save_weights(const char *filename); //20/08/08
      void save_angles(const char *filename); // 20/08/08
      double* theta_output(const double *xd, double sigma);
      void load_angles(const char *filename);
      void load_weights(const char *filename);

  };


  // Date: 27 August 2008, Wednesday

  class ksom3_sc : public ksom3
  {
    protected:
      double ***THETA;
      int *NC, NCMAX;

    public:
      ksom3_sc(int N[], int ncmax);
      ksom3_sc(const ksom3_sc &a); //copy constructor 
      ~ksom3_sc();
      void create_clusters(const double eta[], const double *xd,const double *thd, double threshold);
      void netinfo();

      void save_angles(const char *filename); //overrides base class functions
      void load_angles(const char *filename); //overrides base class function

      void LA_theta_output(const double *xd, const double *th_c, double sigma, double *sth); //lazy_arm output
      void LA_theta_output(const double *xd, const double *th_c, double *sth); //lazy_arm output
      double** all_config(const double *xd, int *nc); //all possible configurations for a given target point
      int get_subcluster_cnt(int *nc); // get the subcluster count for each node
  };


}

//-------------------------------------

#endif
