/* --------------------------------------------------------------
 * Learning Inverse Kinematics using RBFN and KSOM
 *
 * 1. KSOM is used to create clusters in input as well as output
 * spaces
 *
 * 2. RBFN is used to learn Forward Kinematics : theta ---> x
 *
 * 3. During testing phase, Initial theta value is obtained from KSOM
 * corresponding to the target end-effector position. This theta
 * corresponds to the winner neuron.
 *
 * 4. Use sub-clustering to create multiple theta vectors for each xd.
 *
 * 
 * Date: 28 August 2008 Thursday
 *
 *
 * Results:
 *  - It is possible to attain an accuracy of 1 mm without exceeding
 *  joint angle limits
 *
 *  - 
 *  How to compile:
 *
 *  $ g++ -g ksom_rbf_inv.cpp ksom_vmc.cpp -I ~/routines/include/
 *  -I ~/vmc_prog_files/ -L ~/routines/lib/ -lneural -lroute
 *  $ ./a.out

 * ------------------------------------------------------------------- */

#include <iostream>
#include <cstdlib>
#include <fstream>
#include "ksom_vmc.h"
#include <powercube_7dof_cam.h>
#include <joint_position.h>
#include <nnet.h>
#include <matrix.h>
#include <gnuplot_ci.h>

#define NC 3

using namespace std;
using namespace nnet;
using namespace vmc;
using namespace gnuplot_ci;


//#define CLUSTER
//#define TRAIN_RBF
//#define RBF_TEST_RP
//#define INVERT_RBF
//#define INVERT_RP
//#define AVOID_OBS
//#define OA_ANIM
//#define INVERT_TRACK
//#define INVERT_TRACK2
#define MULTI_SOLN

int main()
{
  const int nx = 7;
  const int ny = 7;
  const int nz = 7;
  

  //--------------------------------
  //Create a KSOM Network 
  int N[5] = {nx, ny, nz, NC, NL};

  int ncmax = 100;

  //Create a SOM lattice
  ksom3_sc A(N, ncmax);

  A.netinfo();
  //----------------------
  int node_cnt = nx * ny * nz;

  double **Wt = matrix(node_cnt, NC);


#ifdef CLUSTER

  int NMAX = 50000;
  double eta1_init = 0.9, eta1_fin = 0.1;
  double sig_init = 0.9, sig_fin = 0.1; 

  ofstream fin("input.txt");

  cout << "Clustering starts here ... wait!" << endl;

  double eta[3];

  //Loop starts here ...
  for(int n = 1; n <= NMAX; n++)
  {
    double xt[NC], theta[NL];
    double xtn[NC], thetan[NL];

    //generate a random target  
    generate_data(xt, theta);

    cart_posn_normalize(xt, xtn);
    angle_normalize(theta, thetan);

    for(int i = 0; i < NC; i++)
      fin << xtn[i] << "\t";
    for(int i = 0; i < NL; i++)
      fin << thetan[i] << "\t";
    fin << endl;


    //Learning rate
    eta[0] = eta1_init * pow( (eta1_fin / eta1_init), 
        (n / (double)NMAX) );

    eta[1] = 0.7;

    // Sigma necessary for weight update
    eta[2] = sig_init * pow( (sig_fin / sig_init), 
        (n / (double)NMAX) );

    A.create_clusters(eta, xtn, thetan, 1.0);

  }//n-loop

  //save KSOM network parameters
  A.save_weights("ksom_sc_weights.txt");
  A.save_angles("ksom_sc_angles.txt");

  fin.close();
  cout << "Clustering ends ... parameters saved!" << endl;

#endif

  //=====================================

  //create a RBF network

  int noc = 700;

  int N2[3] = {NL, noc, NC};
  double af[2] = {5, 0.5}; //gaussian activation
  double param[3] = {1, -1, 0}; //input range = -1 to 1

  rbfn B(N2, af, param);

  B.netinfo();

  //======================================

#ifdef TRAIN_RBF

  double *cv = vector(noc*NL);
  double *wt = vector(NC*noc);

  cout << "\nTraining The RBF Network ...\n";
  cout << "Weights and centers are updated ...\n";

  double q[NL], qn[NL], x[NC], xn[NC];
  double xt[NC], xtn[NC];

  int NMAX = 700000;

  ofstream f1("error.txt");
  ofstream f2("tdata.txt");
  ofstream f3("evolve.txt");

  double savg = 0.0;
  for(int n = 1; n <= NMAX; n++)
  {
    generate_data(xt, q);

    angle_normalize(q, qn);
    cart_posn_normalize(xt, xtn);

    for(int i = 0; i < NL; i++)
      f2 << qn[i] << "\t";
    for(int i = 0; i < NC; i++)
      f2 << xtn[i] << "\t";
    f2 << endl;

    B.network_output(qn, xn);

    B.backprop(xtn, 0.1, 0.1);

    B.get_parameters(wt, cv);


    f3 << cv[4] << "\t" << cv[10] << "\t" << cv[200] << "\t" << wt[0] << "\t" << wt[100] << "\t" << wt[300] << endl;

    cart_posn_denormalize(xn, x);

    double s1 = 0.0;
    for(int i = 0; i < NC; i++)
      s1 += pow((xt[i] - x[i]), 2.0);
    s1 = sqrt(s1);
    savg += s1;

    f1 << savg/n << endl;
  }

  f1.close();
  f2.close();
  f3.close();

  B.save_parameters("rbfn_par_bp.txt");

  cout << " Training over ... parameters saved ...\n" << endl;

  delete [] cv;
  delete [] wt;

#endif

  //====================

#ifdef RBF_TEST_RP

  cout << "\n\n RBF Test: Random points ... " << endl;

  B.load_parameters("rbfn_par_bp.txt");

  ofstream ft("test.txt");

  double xt[NC], xtn[NC], q[NL], qn[NL], x[NC], xn[NC];

  int TMAX = 10000;

  double savg2 = 0.0;
  for(int n = 1; n <= TMAX; n++)
  {
    generate_data(xt, q);

    angle_normalize(q, qn);
    cart_posn_normalize(xt, xtn);

    for(int i = 0; i < NL; i++)
      ft << qn[i] << "\t";
    for(int i = 0; i < NC; i++)
      ft << xtn[i] << "\t";
    ft << endl;

    B.network_output(qn, xn);
    cart_posn_denormalize(xn, x);


    double s1 = 0.0;
    for(int i = 0; i < NC; i++)
      s1 += pow((xt[i] - x[i]), 2.0);
    s1 = sqrt(s1);
    savg2 += s1;
  }
  ft.close();

  cout << "Avg error = " << savg2/TMAX << " m" << endl;

#endif

  //=============================

#ifdef INVERT_RBF

  A.load_weights("ksom_sc_weights.txt");
  A.load_angles("ksom_sc_angles.txt");
  B.load_parameters("rbfn_par_bp.txt");

  cout << "\n\nNetwork inversion ... " << endl;

  ofstream ft("angle.txt");
  ofstream fr("rconfig.txt");
  ofstream fp("efposn.txt");
  ofstream fe1("error.txt");

  double **jp = matrix(5,3);

  double qt[NL], qtn[NL];
  double xr[NC], xrn[NC];
  double xt[NC], xtn[NC];
  double xc[NC], xcn[NC];

  //--------------------------
  //Generate a Target point 
  generate_data(xt, qt);

  //normalize the target position
  cart_posn_normalize(xt, xtn);
  angle_normalize(qt, qtn);

  for(int i = 0; i < NC; i++)
    fp << xt[i] << "\t";
  for(int i = 0; i < NC; i++)
    fp << xt[i] << "\t";
  fp << endl << endl << endl;
  //---------------------------------

  double *th_c = vector(NL);
  double *th_cn = vector(NL);
  double *qc = vector(NL);
  double *qcn = vector(NL);

  //----------------------------
  //initial robot configuration
  th_c[0] = -pi/2.0;
  th_c[1] = DEG2RAD(80);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;

  angle_normalize(th_c, th_cn);

  //current robot configuration
  jp = joint_posn(th_c);

  //Current robot end-effector position
  powercube_7dof_FK(th_c, xr);

  //current end-effector position obtained from network
  B.network_output(th_cn, xcn); 
  cart_posn_denormalize(xcn, xc);

  for(int i = 0; i < 5; i++)
  {
    for(int j = 0; j < 3; j++)
      fr << jp[i][j] << "\t";
    fr << endl;
  }
  fr << endl << endl; 

  for(int i = 0; i < NC; i++)
    fp << xc[i] << "\t";
  for(int i = 0; i < NC; i++)
    fp << xr[i] << "\t";
  fp << endl << endl << endl;
  //-------------------------------------------


  //-----------------------------------------
  //Initial guess 
  //qcn = A.LA_theta_output(xtn, th_cn, 0.01);
  qcn = A.LA_theta_output(xtn, th_cn);
  angle_denormalize(qcn, qc);

    
  //Initial position of robot end-effector position
  powercube_7dof_FK(qc, xr);

  //Initial end-effector position obtained from NN
  B.network_output(qcn, xcn); 
  cart_posn_denormalize(xcn, xc);
  //------------------------------------------


  double se1, se2;
  int step = 0;
  do
  {
    for(int i = 0; i < NC; i++)
      fp << xc[i] << "\t";
    for(int i = 0; i < NC; i++)
      fp << xr[i] << "\t";
    fp << endl;

    jp = joint_posn(qc);

    for(int i = 0; i < 5; i++)
    {
      for(int j = 0; j < 3; j++)
        fr << jp[i][j] << "\t";
      fr << endl;
    }
    fr << endl << endl; 

    //update input using gradient_descent
    // qcn is the updated input

    B.input_jacobian();
    B.update_input_gd(0.02, xtn, qcn);

    //new end-effector position
    B.network_output(qcn, xcn);
    cart_posn_denormalize(xcn, xc);

    for(int i = 0; i < NL; i++)
      ft << qcn[i] << "\t";
    ft << endl;

    angle_denormalize(qcn, qc);
    powercube_7dof_FK(qc, xr);
    cart_posn_normalize(xr, xrn);

    //Forward training
    B.backprop(xrn, 0.1, 0.2);


    se1 = 0.0;
    for(int i = 0;i < NC; i++)
      se1 += pow((xr[i] - xt[i]), 2.0);
    se1 = sqrt(se1);

    fe1 << se1 << endl;

    step = step + 1;

    if(step > 400)
      break;

  }while(se1 > 0.001);

  cout << "No. of steps = " << step << endl;


  ft.close();
  fp.close();
  fr.close();
  fe1.close();

  delete [] jp;

#endif //INVERT module ends here

  //----------------------------------------------------------

#ifdef INVERT_RP

  A.load_weights("ksom_sc_weights.txt");
  A.load_angles("ksom_sc_angles.txt");
  B.load_parameters("rbfn_par_bp.txt");

  cout << "Parameters loaded for KSOM and RBFN ..." << endl;

  //initial robot configuration
  double *th_c = vector(NL);
  double *th_cn = vector(NL);

  th_c[0] = -pi/2.0;
  th_c[1] = DEG2RAD(80);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;

  angle_normalize(th_c, th_cn);

  double *qc = vector(NL);
  double *qcn = vector(NL);

  int TMAX = 1000;

  srand(time(NULL));

  double qt[NL], qtn[NL];
  double xr[NC], xrn[NC];
  double xt[NC], xtn[NC];
  double xc[NC], xcn[NC];

  cout << "Network inversion : Random points\n";

  ofstream f1("steps.txt");
  ofstream f2("angles_rp.txt");

  double avg_step = 0.0;
  int rcnt = 0;
  double reject_error = 0.0;
  for(int n = 0; n < TMAX; n++)
  {
    //Generate a random point in the workspace
    generate_data(xt, qt);

    //normalize the target position
    cart_posn_normalize(xt, xtn);
    angle_normalize(qt, qtn);

    //Initial guess  is obtained from KSOM
    qcn = A.LA_theta_output(xtn, th_cn);

    angle_denormalize(qcn, qc);

    //current robot end-effector position
    powercube_7dof_FK(qc, xr);

    //current end-effector position obtained from NN
    B.network_output(qcn, xcn); 

    cart_posn_denormalize(xcn, xc);

    double se1;
    int step = 0;
    int reject = 0;
    do
    {
      //update input using gradient_descent
      // qcn is the updated input

      B.input_jacobian();
      B.update_input_gd(0.02, xtn, qcn);

      //new end-effector position
      B.network_output(qcn, xcn);

      cart_posn_denormalize(xcn, xc);
      angle_denormalize(qcn, qc);
      powercube_7dof_FK(qc, xr);
      cart_posn_normalize(xr, xrn);

      B.backprop(xrn, 0.1, 0.1);

      se1 = 0.0;
      for(int i = 0;i < NC; i++)
        se1 += pow((xr[i] - xt[i]), 2.0);
      se1 = sqrt(se1);

      step = step + 1;

      if(step > 400)
      {
        reject = 1;
        break;
      }

    }while(se1 > 0.001);

    if(reject != 1)
      avg_step += step;
    else
    {
      rcnt += 1;
      reject_error += se1;
    }

    f1 << n << "\t" << step << endl;
    cout << n << "\t" << step << endl;

    for(int i = 0; i < NL; i++)
    {
      th_cn[i] = qcn[i];
      f2 << qcn[i] << "\t";
    }
    f2 << endl;

  } //n-loop

  f1.close();
  f2.close();

  cout << "Avg no. of steps = " << avg_step/(TMAX-rcnt) << endl;
  cout << "no. of points reject = " << rcnt << endl;
  cout << "Avg error of rejected points = " << reject_error/rcnt << endl;

  delete [] qc;
  delete [] qcn;

#endif

  //-----------------------------

#ifdef AVOID_OBS

  cout << "All configurations for a given target point ..." << endl;

  A.load_weights("ksom_sc_weights.txt");
  A.load_angles("ksom_sc_angles.txt");
  B.load_parameters("rbfn_par_bp.txt");

  ofstream f1("all_config.txt");
  ofstream f2("target.txt");
  ofstream f3("ao_config.txt");
  ofstream ft("angle.txt");
  ofstream fp("efposn.txt");

  //initial robot configuration
  //---------------------------
  double *th_c = vector(NL);
  double *th_cn = vector(NL);
  double **jp = matrix(5, 3);

  th_c[0] = -pi/2.0-DEG2RAD(10);
  th_c[1] = DEG2RAD(60);
  th_c[2] = -DEG2RAD(10);
  th_c[3] = DEG2RAD(20);
  th_c[4] = DEG2RAD(50);
  th_c[5] = DEG2RAD(70);

  angle_normalize(th_c, th_cn);

  jp = joint_posn(th_c);

  for(int j = 0; j < 5; j++)
  {
    for(int k = 0; k < 3; k++)
      f3 << jp[j][k] << "\t";
    f3 << endl;
  }
  f3 << endl << endl;
  //------------------------------

  //Define obstacle by a sphere 
  //---------------------------

  double ro =  0.15;
  double co[3] = {0.0, 0.35, 0.1};

  //----------------------------

  //Fix a target:
  double xt[3] = {0.1, 0.5, 0.0};
  double xtn[3];
  int nsc;

  for(int i = 0; i < NC; i++)
    f2 << xt[i] << "\t";
  f2 << endl;

  cart_posn_normalize(xt, xtn);

  double **qn = matrix(100,NL);
  double *q = vector(NL);

  double **c = matrix(4,NC);
  double *r = vector(4);
  double *d = vector(4);

  qn = A.all_config(xtn, &nsc);

  cout << "no. of solutions = " << nsc << endl;

  double tmin = 5000;
  int win_c;
  for(int n = 0; n < nsc; n++)
  {
    angle_denormalize(qn[n], q);

    jp = joint_posn(q);

    for(int i = 0; i < NC; i++)
    {
      c[0][i] = (jp[0][i] + jp[1][i])/2.0;
      c[1][i] = (jp[1][i] + jp[2][i])/2.0;
      c[2][i] = (jp[2][i] + jp[3][i])/2.0;
      c[3][i] = (jp[3][i] + jp[4][i])/2.0;
    }

    int rflag = 0;
    for(int i = 0; i < 4; i++)
    {
      double s1 = 0.0;
      double s2 = 0.0;
      for(int j = 0; j < NC; j++)
      {
        s1 += pow((c[i][j] - jp[i+1][j]), 2.0);
        s2 += pow((c[i][j] - co[j]), 2.0);
      }
      r[i] = sqrt(s1);
      d[i] = sqrt(s2);

      if( (r[i]+ro) > d[i])
        rflag = 1;
    }

    double s3 = 0.0;
    for(int i = 0; i < NL; i++)
      s3 += pow((th_c[i] - q[i]),2.0);
    s3 = sqrt(s3);

    if(s3 < tmin && rflag == 0)
    {
      tmin = s3;
      win_c = n;
    }

    for(int j = 0; j < 5; j++)
    {
      for(int k = 0; k < 3; k++)
        f1 << jp[j][k] << "\t";
      f1 << endl;
    }
    f1 << endl << endl;

  }//nsc-loop 

  //-----------------------------------------
  // Select the initial guess for joint angle
  // -----------------------------------------
  
  double xr[NC], xrn[NC];
  double xc[NC], xcn[NC];

  double *qc = vector(NL);
  double *qcn = vector(NL);

  for(int i = 0; i < NL; i++)
    qcn[i] = qn[win_c][i];

  angle_denormalize(qcn, qc);

    
  //current robot end-effector position
  powercube_7dof_FK(qc, xr);

  //current end-effector position obtained from NN
  B.network_output(qcn, xcn); 

  cart_posn_denormalize(xcn, xc);

  //-----------------------------------------
  // Use inverse-forward learning rule to find 
  // new joint angle configurations
  // --------------------------------------

  double se1, se2;
  int step = 0;
  do
  {
    for(int i = 0; i < NC; i++)
      fp << xc[i] << "\t";
    for(int i = 0; i < NC; i++)
      fp << xr[i] << "\t";
    fp << endl;

    jp = joint_posn(qc);

    for(int i = 0; i < 5; i++)
    {
      for(int j = 0; j < 3; j++)
        f3 << jp[i][j] << "\t";
      f3 << endl;
    }
    f3 << endl << endl; 

    //update input using gradient_descent
    // qcn is the updated input

    B.input_jacobian();
    B.update_input_gd(0.02, xtn, qcn);

    //new end-effector position
    B.network_output(qcn, xcn);
    cart_posn_denormalize(xcn, xc);

    for(int i = 0; i < NL; i++)
      ft << qcn[i] << "\t";
    ft << endl;

    angle_denormalize(qcn, qc);
    powercube_7dof_FK(qc, xr);
    cart_posn_normalize(xr, xrn);

    //Forward training
    B.backprop(xrn, 0.1, 0.2);

    se1 = 0.0;
    for(int i = 0;i < NC; i++)
      se1 += pow((xr[i] - xt[i]), 2.0);
    se1 = sqrt(se1);

    step = step + 1;

    if(step > 400)
      break;

  }while(se1 > 0.001);

  cout << "No. of steps = " << step << endl;

  f1.close();
  f2.close();
  f3.close();
  ft.close();
  fp.close();


  delete [] q;
  delete [] qn;
  delete [] qc;
  delete [] qcn;
  delete [] c;
  delete [] r;
  delete [] th_c;
  delete [] th_cn;

  //--------------------------------------
  // GNUPLOT commands
  // ------------------------------------
  
  GP_handle G1("/usr/bin/", "X", "Y", "Z");   
  G1.gnuplot_cmd("set border 1023-128");
  G1.gnuplot_cmd("splot 'ao_config.txt' index 0 u 1:2:3 w l lw 2 t 'Starting position'");
  G1.gnuplot_cmd("replot 'ao_config.txt' index 1 u 1:2:3 w l lw 2 t 'Initial guess'");
  G1.gnuplot_cmd("replot 'ao_config.txt' index 2:80 u 1:2:3 w l t 'Robot movement'");
  G1.draw_cuboid2(-0.1,0.3,0.0, 0.2, 0.1, 0.2,4);
  //G1.draw_sphere(0.0,0.35,0.1,0.15,10, 6);
  G1.gnuplot_cmd("replot 'target.txt' using 1:2:3 w p ps 3 pt 7 lt 7 t 'target'");
  G1.gnuplot_cmd("replot 'efposn.txt' u 4:5:6 w l notitle");
  //G1.gnuplot_cmd("replot 'all_config.txt' u 1:2:3 w l notitle lt 5");

  getchar();

  G1.gnuplot_cmd("set terminal postscript eps enhanced monochrome 'Helvetica' lw 2 dl 3");
  G1.gnuplot_cmd("set output 'oa_mvmt_bw.eps'");
  G1.gnuplot_cmd("replot");
  G1.gnuplot_cmd("set output");
  G1.gnuplot_cmd("set terminal x11");

#endif

  //-----------------------------------------
  
#ifdef OA_ANIM

  ifstream f1("all_config.txt");

  GP_handle G1("/usr/bin/", "X", "Y", "Z");   
  G1.gnuplot_cmd("set border 1023-128");

  //Draw obstacle

  double **x = matrix(5,NC);
  double **c = matrix(4,NC);
  double *r = vector(4);

  int cnt = 0;
  char *cmd = new char[100];
  while(!f1.eof())
  {
    for(int i = 0; i < 5; i++)
    {
      for(int j = 0; j < NC; j++)
        f1 >> x[i][j];
    }

    G1.gnuplot_cmd("set terminal x11");

    if(!f1.eof())
    {
      sprintf(cmd, "splot 'all_config.txt' index %d using 1:2:3 w l lw 5", cnt);

      G1.gnuplot_cmd(cmd);

      for(int i = 0; i < NC; i++)
      {
        c[0][i] = (x[0][i] + x[1][i])/2.0;
        c[1][i] = (x[1][i] + x[2][i])/2.0;
        c[2][i] = (x[2][i] + x[3][i])/2.0;
        c[3][i] = (x[3][i] + x[4][i])/2.0;
      }

      for(int i = 0; i < 4; i++)
      {
        double s1 = 0.0;
        for(int j = 0; j < NC; j++)
          s1 += pow((c[i][j] - x[i+1][j]), 2.0);
        r[i] = sqrt(s1);

        G1.replot_sphere(c[i][0], c[i][1], c[i][2], r[i], 10, 5);
      }

      cnt = cnt+1;
    }

    G1.draw_cuboid2(-0.1,0.3,0.0, 0.2, 0.1, 0.2,4);
    G1.replot_sphere(0.0,0.35,0.1,0.15,10, 3);
    G1.gnuplot_cmd("replot 'target.txt' using 1:2:3 w p ps 3 pt 7 lt 7");


    char ans;
    char *cmd2 = new char[100];

    cout << "Do you want to save this image?(y/n):";
    cin >> ans;

    getchar();

    if(ans == 'y')
    {
      G1.gnuplot_cmd("set terminal gif");
      sprintf(cmd2, "set output 'oa%d.gif'", cnt-1);
      G1.gnuplot_cmd(cmd2);
      G1.gnuplot_cmd("replot");
      G1.gnuplot_cmd("set output");
    }
  }

  f1.close();

#endif

  //============================================

#ifdef INVERT_TRACK

  cout << "Tracking a trajectory with multi-step movement." << endl;

  A.load_weights("ksom_sc_weights.txt");
  A.load_angles("ksom_sc_angles.txt");
  B.load_parameters("rbfn_par_bp.txt");

  ofstream f1("step.txt");
  ofstream f2("efpos.txt");
  ofstream f3("angle.txt");
  ofstream f4("rconfig.txt");

  double *th_c = vector(NL);
  double *th_cn = vector(NL);
  double *qcn = vector(NL);
  double *qc = vector(NL);
  double **jp = matrix(5,3);

  th_c[0] = -pi/2.0;
  th_c[1] = DEG2RAD(80);
  th_c[2] = 0.0;
  th_c[3] = 0.0;//DEG2RAD(10);
  th_c[4] = 0.0;
  th_c[5] = 0.0;

  angle_normalize(th_c, th_cn);

  double xt[NC], xtn[NC];
  double xr[NC], xrn[NC];
  double xc[NC], xcn[NC];

  int sflag = 0;

  double dt = 0.01;
  for(double t = 0; t <= 2*M_PI; t=t+dt)
  {
    xt[0] = 0.2 * sin(t);
    xt[1] = 0.5 + 0.2 * cos(t);
    xt[2] = 5 * (xt[0] + 0.3)/6;   

    cart_posn_normalize(xt, xtn);

    if(sflag == 0)
    {
      //Initial guess  is obtained from KSOM
      qcn = A.LA_theta_output(xtn, th_cn);
      sflag = 1;
    }
    /*else
    {
      qcn = A.LA_theta_output(xtn, qcn);
    }*/

    angle_denormalize(qcn, qc);

    //current robot end-effector position
    powercube_7dof_FK(qc, xr);

    //current end-effector position obtained from NN
    B.network_output(qcn, xcn);               

    double se1;
    int step = 0;
    do
    {
      //update input using gradient_descent
      // qcn is the updated input

      B.input_jacobian();
      B.update_input_gd(0.02, xtn, qcn);

      //new end-effector position
      B.network_output(qcn, xcn);

      cart_posn_denormalize(xcn, xc);
      angle_denormalize(qcn, qc);
      powercube_7dof_FK(qc, xr);
      cart_posn_normalize(xr, xrn);

      B.backprop(xrn, 0.1, 0.1);

      se1 = 0.0;
      for(int i = 0;i < NC; i++)
        se1 += pow((xr[i] - xt[i]), 2.0);
      se1 = sqrt(se1);

      step = step + 1;

      if(step > 400)
        break;

    }while(se1 > 0.001);

    f1 << t << "\t" << step << endl;
    cout << t << "\t" << step << endl;

    angle_denormalize(qcn, qc);
    jp = joint_posn(qc);

    for(int i = 0; i < 5; i++)
    {
      for(int j = 0; j < 3; j++)
        f4 << jp[i][j] << "\t";
      f4 << endl;
    }
    f4 << endl << endl;

    for(int i = 0; i < NL; i++)
      f3 << qcn[i] << "\t";
    f3 << endl;

    for(int i = 0; i < NC; i++)
      f2 << xt[i] << "\t" << xc[i] << "\t" << xr[i] << "\t";
    f2 << endl;

  }//t_loop

  f1.close();
  f2.close();
  f3.close();
  f4.close();

  delete [] th_c;
  delete [] th_cn;
  delete [] qc;
  delete [] qcn;

#endif

  //-----------------------------------------------------

#ifdef INVERT_TRACK2
  cout << "Tracking a trajectory with single step movement." << endl;

  A.load_weights("ksom_sc_weights.txt");
  A.load_angles("ksom_sc_angles.txt");
  B.load_parameters("rbfn_par_bp.txt");

  ofstream f2("efpos.txt");
  ofstream f3("angle.txt");
  ofstream f4("rconfig.txt");
  ofstream f5("error.txt");

  double *th_c = vector(NL);
  double *th_cn = vector(NL);
  double *qcn = vector(NL);
  double *qc = vector(NL);
  double **jp = matrix(5,3);

  th_c[0] = -pi/2.0;
  th_c[1] = DEG2RAD(80);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(0);
  th_c[4] = 0.0;
  th_c[5] = 0.0;

  angle_normalize(th_c, th_cn);

  double xt[NC], xtn[NC];
  double xr[NC], xrn[NC];
  double xc[NC], xcn[NC];

  int sflag = 0;
  double savg = 0.0;
  int cnt = 0;

  double dt = 0.01;
  for(double t=0; t <= 2*M_PI; t=t+dt)
  {
    xt[0] = 0.2 * sin(t);
    xt[1] = 0.5 + 0.2 * cos(t);
    //xt[2] = 4 * (xt[0] + 0.3)/6;   
    xt[2] = 0.2;   

    cart_posn_normalize(xt, xtn);

    if(sflag == 0)
    {
      //Initial guess  is obtained from KSOM
      qcn = A.LA_theta_output(xtn, th_cn);
      sflag = 1;
    }
    else
    /*{
      qcn = A.LA_theta_output(xtn, qcn);
    }*/

    angle_denormalize(qcn, qc);

    //current robot end-effector position
    powercube_7dof_FK(qc, xr);

    //current end-effector position obtained from NN
    B.network_output(qcn, xcn);               

    B.input_jacobian();
    B.update_input_gd(0.03, xtn, qcn);

    //new end-effector position
    B.network_output(qcn, xcn);

    cart_posn_denormalize(xcn, xc);
    angle_denormalize(qcn, qc);
    powercube_7dof_FK(qc, xr);
    cart_posn_normalize(xr, xrn);

    B.backprop(xrn, 0.1, 0.2);

    double se1 = 0.0;
    for(int i = 0;i < NC; i++)
      se1 += pow((xr[i] - xt[i]), 2.0);
    se1 = sqrt(se1);

    double se2 = 0.0;
    for(int i = 0;i < NC; i++)
      se2 += pow((xc[i] - xt[i]), 2.0);
    se2 = sqrt(se2);

    f5 << t << "\t" << se1 << "\t" << se2 << endl;

    angle_denormalize(qcn, qc);
    jp = joint_posn(qc);

    for(int i = 0; i < 5; i++)
    {
      for(int j = 0; j < 3; j++)
        f4 << jp[i][j] << "\t";
      f4 << endl;
    }
    f4 << endl << endl;

    for(int i = 0; i < NL; i++)
      f3 << qcn[i] << "\t";
    f3 << endl;

    for(int i = 0; i < NC; i++)
      f2 << xt[i] << "\t" << xc[i] << "\t" << xr[i] << "\t";
    f2 << endl;

    cnt = cnt + 1;
    savg += se1;

  }//t_loop

  cout << "Average error = " << savg/cnt << endl;

  f2.close();
  f3.close();
  f4.close();

  delete [] th_c;
  delete [] th_cn;
  delete [] qc;
  delete [] qcn;

#endif

  //-----------------------------------------------------

#ifdef MULTI_SOLN 
  cout << "All configurations for a given target point ..." << endl;

  A.load_weights("ksom_sc_weights.txt");
  A.load_angles("ksom_sc_angles.txt");
  B.load_parameters("rbfn_par_bp.txt");

  ofstream f1("target.txt");
  ofstream f2("final_config.txt");
  ofstream f3("steps.txt");

  //Fix a target:
  double xt[3], xtn[3], qt[3];
  int nsc;

  //Generate a random point in the workspace
  generate_data(xt, qt);

  for(int i = 0; i < NC; i++)
    f1 << xt[i] << "\t";
  f1 << endl;

  cart_posn_normalize(xt, xtn);

  double **qn = matrix(100,NL);
  double *q = vector(NL);

  double **c = matrix(4,NC);
  double *r = vector(4);
  double *d = vector(4);
  double **jp = matrix(5,3);

  qn = A.all_config(xtn, &nsc);

  cout << "no. of solutions = " << nsc << endl;

  double xr[NC], xrn[NC];
  double xc[NC], xcn[NC];

  double *qc = vector(NL);
  double *qcn = vector(NL);


  //Inverse kinematic solution is obtained for all initial conditions
  int reject = 0;
  double avg_step = 0;
  for(int n = 0; n < nsc; n++)
  {
    for(int i = 0; i < NL; i++)
      qcn[i] = qn[n][i];

    //current end-effector position obtained from NN
    B.network_output(qcn, xcn); 

    //-----------------------------------------
    // Use inverse-forward learning rule to find 
    // new joint angle configurations
    // --------------------------------------

    double se1;
    int step = 0;
    int rflag = 0;
    do
    {
      //update input using gradient_descent
      // qcn is the updated input

      B.input_jacobian();
      B.update_input_gd(0.02, xtn, qcn);

      //new end-effector position
      B.network_output(qcn, xcn);
      cart_posn_denormalize(xcn, xc);

      angle_denormalize(qcn, qc);
      powercube_7dof_FK(qc, xr);
      cart_posn_normalize(xr, xrn);

      //Forward training
      B.backprop(xrn, 0.1, 0.2);

      se1 = 0.0;
      for(int i = 0;i < NC; i++)
        se1 += pow((xr[i] - xt[i]), 2.0);
      se1 = sqrt(se1);

      step = step + 1;

      if(step > 400)
      {
        rflag = 1;
        break;
      }
    }while(se1 > 0.001);

    if(rflag != 0)
      reject = reject + 1;
    else
    {
      jp = joint_posn(qc);

      for(int j = 0; j < 5; j++)
      {
        for(int k = 0; k < 3; k++)
          f2 << jp[j][k] << "\t";
        f2 << endl;
      }
      f2 << endl << endl;

      avg_step += step;
    }

    f3 << n << "\t" << step << endl;
  }//n_loop

  f1.close();
  f2.close();
  f3.close();

  cout << "avg no. of steps = " << avg_step/(nsc-reject) << endl;
  cout << "no. of points rejected = " << reject << endl;

#endif

  return 0;
}


