\documentclass[a4paper,10pt,twocolumn]{article}
\usepackage{amsmath,mathrsfs}
\usepackage{graphicx, graphics}
\usepackage{xcolor}
\usepackage[left=2cm,right=1cm,top=1cm,bottom=2cm]{geometry}

\graphicspath{ {./fig/} }

\usepackage[skins,theorems]{tcolorbox}
\tcbset{highlight math style={enhanced,colframe=blue,colback=white,arc=0pt,boxrule=1pt}}


\title{Inverse Kinematic Solution for a Redundant Manipulator}
\author{Swagat Kumar}
\date{}

\begin{document}
\maketitle

\section{Few Basics related to Angular Velocity}

\begin{flushright}
  \underline{\textbf{Date: March 22, 2015}} 
\end{flushright}

Rotation matrix $R$ is a proper orthonormal matrix ($RR^T = I$). This
gives rise to the fact that $R^{-1} = R^T$. Taking derivative gives

\begin{equation}
  \dot{R}R^T + R\dot{R}^T =  0 
  \label{eq:rot_der}
\end{equation}

Defining $S=\dot{R}R^T = \dot{R}R^{-1}$, we get $S+S^T = 0$. This indicates that
$S$ is a skew-symmetric matrix.  We also have following expression for $\dot{R}$: 

\begin{eqnarray*}
  S &=&  \dot{R}R^{-1} \nonumber \\
  \text{or,}\; SR &=&  \dot{R} \nonumber \\
  \text{or,}\; \dot{R}(t) &=& S(t)R(t)  
  \label{eq:rdot}
\end{eqnarray*}

\begin{flushright}
  \underline{\textbf{Date: April 08, 2015}}
\end{flushright}

In terms of angular velocity $\mathbf{w}(t)$, the above equation may be written as
\begin{equation}
  \dot{R}(t) = S(\mathbf{w}(t))R(t)
  \label{eq:rdotw}
\end{equation}

Other important properties of skew-symmetric matrix $S$ are  given by the following equations:

\begin{equation}
  S(\mathbf{a})\mathbf{p} = \mathbf{a} \times \mathbf{p}
  \label{eq:ssp2}
\end{equation}

\begin{equation}
  RS(\mathbf{a})R^T = S(R\mathbf(a))
  \label{eq:ssp3}
\end{equation}
where $R$ is the orthogonal rotation matrix and $\mathbf{a} \in
\mathscr{R}^3$, $\mathbf{p} \in \mathscr{R}^3$ are any two vectors in
3-dimensional space. The angular velocity about an axis $\mathbf{k}$ is defined as follows:
\begin{equation}
  \mathbf{w}(t) = \dot{\mathbf{\theta}}\mathbf{k}
  \label{eq:angvel}
\end{equation}
where $\mathbf{k}$ is the unit vector along the axis the rotation. 

\begin{flushright}
  \underline{\textbf{Date: April 10, 2015}}
\end{flushright}

\noindent \textbf{Addition of Angular Velocities:} \\
If we are given,
\begin{equation}
  R_n^0 = R_1^0 R_2^1 \dots R_n^{n-1}
  \label{eq:rotn}
\end{equation}

We can write
\begin{equation}
  \dot{R}_n^0 = S(\mathbf{w}_n^0) R_n^0 
  \label{dot_rotn}
\end{equation}
where 
\begin{equation}
  \mathbf{w}_n^0 = \mathbf{w}_1^0 + R_1^0\mathbf{w}_2^1 + R_2^0\mathbf{w}_3^2 + \dots + R_{n-1}^0 \mathbf{w}_n^{n-1}
  \label{eq:angveladd}
\end{equation}

\noindent\textbf{Angular velocity of a point attached to a moving frame}

The homogeneous transformation relates two frames $o_0x_0y_0z_0$ and $o_1x_1y_1z_1$ is given by

\begin{equation}
  H_1^0 = \begin{bmatrix} R_1^0 & O_1^0 \\ \mathbf{0} & 1 \end{bmatrix}
  \label{eq:homtrans}
\end{equation}

So a point vector may be represented as 

\begin{equation}
  \mathbf{p}^0 = R \mathbf{p}^1 + O
  \label{eq:ptvec}
\end{equation}

Taking time derivative we get,
\begin{eqnarray}
  \dot{\mathbf{p}}^0 &=&  \dot{R}\mathbf{p}^1  + \dot{O} \nonumber \\
  &=& S(\mathbf{w})R\mathbf{p}^1 + \dot{O} \nonumber \\
  &=&  \mathbf{w} \times \mathbf{r} + \mathbf{v} 
  \label{eq:pdot}
\end{eqnarray}
where $\mathbf{r}=Rp^1$ is the vector from $O_1$ to $\mathbf{p}$
expressed in the orientation of the frame $o_0x_0y_0z_0$.

\section{Derivation of the Manipulator Jacobian}

For an $n$-link manipulator with joint variables $q_1,q_2\dots q_n$,
the homogeneous transformation matrix from base frame to the
end-effector frame may be written as:

\begin{equation}
  T_n^0  = \begin{bmatrix} R_n^0 (\mathbf{q}) & O_n^0(\mathbf{q}) \\ 
                           \mathbf{0} & 1 \end{bmatrix}
  \label{eq:T0n}
\end{equation}

Let 
\begin{equation}
  S(\mathbf{w}_n^0) = \dot{R}_n^0 (R_n^0)^T
  \label{eq:sw1}
\end{equation}
where $\mathbf{w}_n^0$ is the angular velocity and $\mathbf{v}_n^0 =
\dot{O}_n^0$ is the linear velocity of the end-effector.  We seek the
following relationship between end-effector velocity and the joint
angle velocities:
\begin{equation}
  \begin{bmatrix}
    \mathbf{v}_n^0 \\ \mathbf{w}_n^0
  \end{bmatrix}  = \begin{bmatrix}
    J_{\boldsymbol{v}} \\ \boldsymbol{w}
  \end{bmatrix} \dot{\mathbf{q}} = J_n^0 \dot{\mathbf{q}}
  \label{eq:jacob1}
\end{equation}
The $6\times n$ matrix $J_n^0$ is called the manipulator Jacobian. 

If the $i$-th joint is revolute, then the $i$-th joint variable $q_i$
equals $\theta_i$ and the axis of rotation is $z_{i-1}$. With this
convention, let $w_i^{i-1}$ represent the angular velocity of link $i$
expressed relative to the frame $o_{i-1}x_{i-1}y_{i-1}z_{i-1}$. This angular velocity may be expressed as
\begin{equation}
  \boldsymbol{w}_i^{i-1} = \dot{q}_iz_{i-1}^{i-1} = \dot{q}_i \boldsymbol{k} 
  \label{eq:angvi}
\end{equation}
where $\boldsymbol{k}$ is the unit vector $(0,0,1)^T$. If the $i$-th
joint is prismatic, then the motion of frame $i$ relative to frame
$i-1$ is a translation and thus, $\boldsymbol{w}_i^{i-1}=0$.

Hence the overall angular velocity of the end-effector can be computed using equation \eqref{eq:angveladd} and this is given by 

\begin{eqnarray}
  \boldsymbol{w}_n^0 &=&  \rho_1\dot{q}_1\boldsymbol{k} +
  \rho_2\dot{q}_2R_1^0\boldsymbol{k} + \dots +
  \rho_n\dot{q}_nR_{n-1}^0\boldsymbol{k} \nonumber \\
  &=& \sum_{i=1}^n \rho_i \dot{q}_iz_{i-1}^0
  \label{eq:angvel_ef} 
\end{eqnarray}
where $\rho_i$ is equal to 1 if joint $i$ is revolute and 0 if the
joint $i$ is prismatic and $z_{i-1}^0 = R_{i-1}^0\boldsymbol{k}$. Hence the lower half of the Jacobian $J_{\boldsymbol{w}}$ is thus given as 

\begin{equation}
  \tcbhighmath[drop fuzzy shadow]{J_w = [\rho_1 z_0 \rho_2 z_1 \dots \rho_n z_{n-1}]}
  \label{eq:ang_jacob}
\end{equation}
where $\rho_i$ is 0 for a prismatic joint and 1 for a revolute joint.
Hence, for a serial manipulator with only revolute joints, the angular
Jacobian is given by
\begin{equation}
  J_w = [z_0\; R_1^0 \boldsymbol{k} \;R_1^0 R_2^1 \boldsymbol{k} \dots R_1^0 R_2^1R_n^{n-1} \boldsymbol{k}]
  \label{eq:angjac2}
\end{equation}
where $\boldsymbol{k} = (0, 0, 1)^T = \mathbf{z}_0^0$.

In short, the Jacobian is given by 

\begin{equation}
  \begin{bmatrix}
    v_n^0 \\ w_n^0
  \end{bmatrix} = 
  \begin{bmatrix}
    J_v \\ J_w
  \end{bmatrix} \dot{\mathbf{q}}
  \label{eq:jacobm}
\end{equation}
where 
\[
  J_v = [J_{v_1} J_{v_2} \dots J_{v_n}]
\]
and
\[
  J_w = [J_{w_1} J_{w_2} \dots J_{w_n}]
\]
with 
\begin{equation}
  J_{v_i} = \left\{\begin{array}{ll}
    z_{i-1}^0 \times (O_n^0 - O_{i-1}^0) & \textrm{for revolute joint}\ i \\
    z_{i-1}^0 & \textrm{for prismatic joint}\ i 
  \end{array} \right.
  \label{eq:jvi}
\end{equation}
and 
\begin{equation}
  J_{w_i} = \left\{ \begin{array}{ll}
    z_{i-1}^0 & \textrm{for revolute joint}\ i \\
    0 & \textrm{for prismatic joint}\ i
  \end{array}\right.
  \label{eq:jwi}
\end{equation}

In the above equations, the vector $z_i^0$ consists of the first three
elements of the third column of the transformation matrix $T_i^0$ and,
$O_i$ is the first three elements of the fourth column of the same
matrix. 

\textcolor{blue}{The Jacobian thus derived is known as Geometric
  Jacobian which is different from the analytical Jacobian which is
  derived by differentiating the forward kinematic equations with
  respect to the joint variables $\mathbf{q}$.}

\textcolor{red}{Somehow the Jacobian computed using this method does
  not work properly compared to the the Jacobian computed using the
  analytical expression $\frac{d\mathbf{f}}{d\mathbf{q}}$.}

  Some concerns are as follows:
  \begin{itemize}
    \item Last 3 axes of Coolarm manipulator do not intersect at a common point. Does it affect the above method?
    \item Secondly, the forward kinematics and D-H parameters are
      derived by using JJ Craig's method \cite{craig2005introduction}.
      Does it affect the derivation? Should I follow Vidyasagar's notation? 
    \item In this case, $z_0^0 = z_1^0$.  Should it matter?
    \item I have taken the order of orientation as rotation about $x$,
      $y$ and $z$ axes respectively. Prem says, the angular velocity
      vector $\boldsymbol{w}$ has no relationship with these
      orientation angles. 
  \end{itemize}


\section{Jacobian Transpose Method for Inverse Kinematics}  \label{sec:jacobt}

\begin{flushright}
  \underline{\textbf{Date: April 01, 2015}} 
\end{flushright}

Consider a Lyapunov function candidate as follows:

\begin{equation}
  V = \frac{1}{2} (\mathbf{e}^T \mathbf{e}) 
  \label{eq:lyap}
\end{equation} 
where $\mathbf{e} = \mathbf{x}_d - \mathbf{x}$ is the 6-dimensional
end-effector pose error. This is positive definite function having
minimum at $\mathbf{e} = 0$. We are considering a case desired pose
does not vary with time. In other words, the end-effector pose is
stationary (set point) with $\dot{\mathbf{x}}_d = 0$. 

Taking time derivative on both sides, we get 

\begin{eqnarray}
  \dot{V} &=&  (\mathbf{x}_d - \mathbf{x})^T \dot{\mathbf{x}}  \nonumber \\
  &=& \mathbf{e}^T J \dot{\boldsymbol{\theta}}
  \label{eq:vdot}
\end{eqnarray}

Select joint velocity vector as follows:

\begin{equation}
  \dot{\boldsymbol{\theta}} = \alpha J^T \mathbf{e}
  \label{eq:control1}
\end{equation}
where $\alpha = \frac{\langle \mathbf{e},
JJ^T\mathbf{e} \rangle}{\langle JJ^T\mathbf{e}, JJ^T\mathbf{e} \rangle}$ is a scale factor
which minimizes the error vector $\mathbf{e}$ after every update.
Substituting \eqref{eq:control1} into \eqref{eq:vdot}, we get 
\begin{equation}
  \dot{V} = -\alpha \|J^T\mathbf{e}\|^2 \le 0 
  \label{eq:vdot2}
\end{equation}

Hence the control input as provided in equation \eqref{eq:control1}
keeps the end-effector pose error bounded. The corresponding
simulation results are shown in Figure \ref{fig:error} and Figure
\ref{fig:pose_config} respectively.

\begin{figure}[!h]
  \centering
  \scalebox{0.7}{\input{./fig/error_jt.tex}}
  \caption{Evolution of pose error over time with Jacobian transpose method.}
  \label{fig:error}
\end{figure}

\begin{figure}[!h]
  \centering
  \scalebox{0.7}{\input{./fig/pose_config.tex}}
  \caption{Jacobian Transpose Method: Actual and Desired Pose Configuration}
  \label{fig:pose_config}
\end{figure}

\section{Pseudo-inverse Method for Inverse Kinematics} \label{sec:pinv}

The forward-kinematic equation is given by the following equation:

\begin{equation}
  \mathbf{x} = \boldsymbol{f}(\mathbf{q})
  \label{eq:fk}
\end{equation}
Let us assume that $\mathbf{q} \in \mathscr{R}^n$ and $\mathbf{x} \in
\mathscr{R}^m$. For a redundant manipulator, $n > m$. By taking
time-derivative on both sides of the above equation, we get
\begin{equation}
  \dot{\mathbf{x}} = J(\mathbf{q}) \dot{\mathbf{q}}
  \label{eq:fkvel}
\end{equation}
where $J$ is the $m \times n$ dimensional Jacobian of the manipulator.
The joint angles for a given end-effector pose $\mathbf{x}_d$ can be
obtained using matrix pseudo-inverse as shown below:

\begin{equation}
  \dot{\mathbf{q}} = J^{\dagger}(\mathbf{q})\dot{\mathbf{x}_d}
  \label{eq:ikvel}
\end{equation}

where $J^{\dagger}(\mathbf{q})$ represents the inverse of the Jacobian
matrix $J$. If $(J^TJ)$ is invertible, the pseudo-inverse is 
given by the Moore-Penrose inverse equation:
\begin{equation}
  J^{\dagger}(\mathbf{q}) = (J^TJ)^{-1}J^T
  \label{eq:mpi}
\end{equation}
This is otherwise known as the least square solution which minimizes
the cost function $\|\dot{\mathbf{x}}-J\dot{\mathbf{q}}\|^2$.  The
equation \eqref{eq:mpi} is considered as a solution for an
over-constrained problem where the number of equations ($m$) is less
than the number of variables $n$ and $\textrm{rank}(J) \le n$.  

If $(JJ^T)$ is invertible, then pseudo-inverse is the \textbf{minimum
norm} solution of the least square problem given by the following
equation:

\begin{equation}
  J^{\dagger} = J^T(JJ^T)^{-1}
  \label{eq:mns}
\end{equation}

The equation \eqref{eq:mns} is considered to be a solution for an
under-constrained problem where the number of equations $m$ is less
than the number of unknown variables $n$. Note that the equation
\eqref{eq:mns} is also said to provide the \emph{right pseudoinverse}
of $J$ as $JJ^{\dagger} = I$. Note that, $J^{\dagger}J \in
\mathscr{R}^{n\times n}$ and in general, $J^{\dagger}J \ne I$.

Some of the properties of pseudo-inverse:
\begin{eqnarray*}
  J^{\dagger}J J^{\dagger} &=&  J \\
  JJ^{\dagger} J &=& J^{\dagger} \\
  (JJ^{\dagger})^T &=&  JJ^{\dagger}\\
  (J^{\dagger}J)^T &=& J^{\dagger}J \\
  (J^{\dagger})^{\dagger} &=&  J \\
  (J^{\dagger})^T &=&  (J^T)^{\dagger}
  \label{eq:pi_prop}
\end{eqnarray*}

Singular configurations are those for which 
\begin{eqnarray*}
  det(J) &=& 0 \\
  det(J^TJ) &=&  0, \; m > n \\
  det(JJ^T) &=& 0 ,\; n > m
\end{eqnarray*}

An other property of the pseudoinverse is that the matrix
$I-J^{\dagger}J$ is a projection of $J$ onto nullspace. Such that for
any vector $\psi$ that satisfies $J(I-J^{\dagger}J)\psi = 0$, the
joint angle velocities could be written as

\begin{equation}
  \dot{\mathbf{q}} = J^{\dagger}\dot{\mathbf{x}} + (I-J^{\dagger}J)\psi
  \label{eq:nullsp}
\end{equation}

This can be easily proved by multiplying both sides of equation \eqref{eq:nullsp} by $J$ as shown below:

\begin{eqnarray*}
  J\dot{\mathbf{q}} &=& J[J^{\dagger}\dot{\mathbf{x}} + (I-J^{\dagger}J)\psi] \\
  &=& JJ^{\dagger}\dot{\mathbf{x}}+J(I-J^{\dagger}J)\psi \\
  &=& JJ^{\dagger}\dot{\mathbf{x}}+(J-JJ^{\dagger}J)\psi \\
  &=& \dot{\mathbf{x}}+ (J-J)\psi \\
  &=&  \dot{\mathbf{x}}
\end{eqnarray*}

In general, for $m < n$, $(I-J^{\dagger}J)\ne 0$, and all vectors of
the form $(I-J^{\dagger}J)\psi$ lie in the null space of $J$, i.e.,
$J(I-J^{\dagger}J)\psi = 0$.

\subsection{Closed Loop Inverse Kinematics Solution}

\begin{flushright}
  \underline{\textbf{Date: April 03, 2015}}
\end{flushright}

Consider the end-effector pose error and its time derivative be give as follows:

\begin{equation}
  \mathbf{e} = \mathbf{x_d} - \mathbf{x}; \; \dot{\mathbf{e}} = \dot{\mathbf{x_d}} - \dot{\mathbf{x}} 
  = \dot{\mathbf{x}}_{\mathbf{d}} - J\dot{\mathbf{q}}
  \label{eq:efferror}
\end{equation}

By selecting the joint velocities as 
\begin{equation}
  \dot{\mathbf{q}} = J^{\dagger} (\dot{\mathbf{x}}_{\mathbf{d}} + K_p (\mathbf{x}_d - \mathbf{x}))
  \label{eq:control2}
\end{equation}
the closed loop error dynamics becomes

\[
  \dot{\mathbf{e}} + K_p \mathbf{e} = 0
\]

Hence the control law \eqref{eq:control2} stabilizes the closed loop
error dynamics and the error will converge to zero if $K_p$ is
positive definite. The homogeneous part of the inverse kinematic
solution in \eqref{eq:nullsp} could be combined with
\eqref{eq:control2} in order to obtained a generalized closed loop
inverse kinematic solution. This part is taken from the work by Wang
\emph{et al.} \cite{wang2010inverse}.


\subsection{Null-Space Optimization}

The general inverse kinematic solution may be written as 
\begin{equation}
  \dot{\mathbf{q}} = J^{\dagger} \dot{\mathbf{x}} + (I-J^{\dagger}J)\dot{\mathbf{q}}_0
  \label{eq:gik}
\end{equation}
where $(I-J^{\dagger}J)$ is a projector of the joint velocity vector
$\dot{\mathbf{q}}_0$ onto $\mathscr{N}(J)$. The typical choice of the null space joint velocity vector is 

\begin{equation}
  \dot{\mathbf{q}}_0 = k_0 \left(\frac{\partial w(\mathbf{q})}{\partial \mathbf{q}} \right)^T
  \label{eq:nspq}
\end{equation}
with $k_0 > 0$ and $w(\mathbf{q})$ is a scalar objective function of
the joint variables and $\left(\frac{\partial w(\mathbf{q})}{\partial
  \mathbf{q}} \right)^T $ represents the gradient of $w$. A number of
  constraints could be imposed by using this objective function. For
  instance, the joint limit avoidance can be achieved by selecting the
  objective function as 

  \begin{equation}
    w(q) = \frac{1}{n}\sum_i^n \left(\frac{q_i - \bar{q}_i}{q_{iM} - q_{im}}\right)^2
    \label{eq:jla_cost}
  \end{equation}

  where $\bar{q}_i$ is the middle value of joint angles while $q_{iM}$
  ($q_{im}$) represent maximum (minimum) value of joint angles. 

  \begin{flushright}
    \underline{\textbf{Date: April 08, 2015}}
  \end{flushright}

  The following figures show the simulation results of these control
  algorithms. The figure \ref{fig:rcong-traj} shows some of the robot
  configurations for tracking a circular end-effector trajectory. The
  full Jacobian of dimension $6\times 7$ is used for computing joint
  velocities using \eqref{eq:ikvel}. The effect of null space
  optimization is shown in Figures \ref{fig:trajerr_jla} and
  \ref{fig:rcong-jla} respectively, where the null space is optimized
  for joint limit avoidance. As one can see from Figure
  \ref{fig:thnorm}, the joint angle norm is less with null space
  optimization compared to the case when self motion is not used. 


\begin{figure}[!h]
  \centering
  \scalebox{0.7}{\input{./fig/rconfig.tex}}
  \caption{Robot Configurations for tracking a trajectory \textbf{without} null space optimization}
  \label{fig:rcong-traj}
\end{figure}

%\begin{figure}[!h]
%  \centering
%  \scalebox{0.7}{\input{./fig/traj_error.tex}}
%  \caption{Trajectory tracking using Pseudo-inverse method}
%  \label{fig:trajerr}
%\end{figure}

\begin{figure}[h]
  \centering
  \scalebox{0.7}{\input{./fig/traj_error_jla.tex}}
  \caption{Trajectory Tracking with null space optimization}
  \label{fig:trajerr_jla}
\end{figure}

\begin{figure}[!h]
  \centering
  \scalebox{0.7}{\input{./fig/rconfig2_jla.tex}}
  \caption{Robot Configurations for tracking the trajectory with null space optimization.}
  \label{fig:rcong-jla}
\end{figure}

\begin{figure}[!h]
  \centering
  \scalebox{0.7}{\input{./fig/thnorm.tex}}
  \caption{Joint Angle norm with and without null space optimization}
  \label{fig:thnorm}
\end{figure}

\subsection{Damped Least Square Method} \label{sec:dls}

The pseudo-inverse method for inverse kinematics is given by

\begin{equation}
  \Delta \mathbf{q} = J^{\dagger}\mathbf{e}
  \label{eq:pinv2}
\end{equation}

In damped least square method, the $\Delta \mathbf{q}$ is selected so
as to minimize the following cost function

\begin{equation}
  V = \|J\Delta \mathbf{q} - \mathbf{e}\|^2 + \lambda^2 \|\Delta \mathbf{q}\|^2
  \label{eq:dls-cost}
\end{equation}
This gives us the following expression for joint angle velocities:

\begin{equation}
  \Delta \mathbf{q} = J^T(JJ^T+\lambda^2I)^{-1}\mathbf{e}
  \label{eq:dls}
\end{equation}
%---------------
\bibliographystyle{unsrt}
\bibliography{invkin}



\end{document}
