/* Testing Angular Jacobian
 * Date: April 22, 2015
 * ---------------------- */

#include <iostream>
#include <coolarm_fk.h>
#include <fstream>
#include <gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;

int main()
{
    //double theta_t[NL] = {0, DEG2RAD(50), DEG2RAD(20), DEG2RAD(10), DEG2RAD(30), DEG2RAD(0), DEG2RAD(0) }; // does not work
    //double theta_t[NL] = {0, DEG2RAD(50), DEG2RAD(20), DEG2RAD(10), DEG2RAD(0), DEG2RAD(0), DEG2RAD(0) }; // somewhat works
    double theta_t[NL] = {0, DEG2RAD(50), DEG2RAD(20), DEG2RAD(10), DEG2RAD(20), DEG2RAD(30), DEG2RAD(40) };
    double theta[NL] = {0,0,0,0,0,0,0};
    double theta_0[NL] = {0,0,0,0,0,0};
    double pose_t[NW], pose[NW];
    bool VALID_FLAG;

    cv::Mat Jpos(6,3,CV_64F,0.0);

    coolarm_pose_fk(theta_t, pose_t, VALID_FLAG);
    coolarm_pose_fk(theta, pose, VALID_FLAG);


    ofstream f6("desiredConfig.txt");

    joint_position(theta_t, Jpos);
    for(int i = 0; i < 6; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f6 << Jpos.at<double>(i,j) << "\t";
        f6 << endl;
    }
    f6 << endl << endl;
    f6.close();


    cv::Mat Pose_T(6,1, CV_64F, pose_t);
    cv::Mat Pose_T_dot(6,1,CV_64F, 0.0);
    cv::Mat Pose_C(6,1, CV_64F, pose);
    cv::Mat Jp(3,7,CV_64F, 0.0);
    cv::Mat Jw(3,7,CV_64F, 0.0);
    cv::Mat J(6,7,CV_64F, 0.0);
    cv::Mat Theta_dot(7,1,CV_64F, 0.0);
    cv::Mat Jinv(7,6, CV_64F, 0.0);

    cv::Mat singval(6,1,CV_64F,0.0); // singular values
    cv::Mat Error(6,1,CV_64F, 0.0);
    cv::Mat q0_dot(NL,1,CV_64F,0.0); // Self-motion velocity

    // Gains
    cv::Mat Kp(6,6,CV_64F,0.0);
    Kp = 1.5 * cv::Mat::eye(Kp.rows, Kp.cols, CV_64F);


    //control loop
    ofstream f1("actual.txt");
    ofstream f2("rconfig.txt");
    ofstream f3("rank.txt");
    ofstream f4("target.txt");
    ofstream f5("error.txt");
    ofstream f7("finalconfig.txt");

    int Tmax = 10.0;
    double dt = 0.01;
    for(double t = 0; t < Tmax; t = t + dt)
    {

        // Derivative of Target pose

        Pose_T_dot.at<double>(0,0) = 0.0;
        Pose_T_dot.at<double>(1,0) = 0.0;
        Pose_T_dot.at<double>(2,0) = 0.0;
        Pose_T_dot.at<double>(3,0) = 0.0;
        Pose_T_dot.at<double>(4,0) = 0.0;
        Pose_T_dot.at<double>(5,0) = 0.0;

        Error = Pose_T - Pose_C;

        position_jacobian(theta, Jp);
        orientation_jacobian(theta, Jw); // This works
        //angular_jacobian(theta, Jw);  //Does not work

        cv::vconcat(Jp, Jw, J);
        cv::SVD::compute(J, singval, cv::SVD::NO_UV);

        double rank = 0;
        for(int i = 0; i < 7; ++i)
        {
            if(singval.at<double>(i,1) > 0.0001)
                rank = rank + 1;
        }
        f3 << rank << endl;

        //cv::invert(J, Jinv, cv::DECOMP_SVD);


        // null space optimization for obtained a desired joint configuration
        for(int i = 0; i < NL; ++i)
        {
            q0_dot.at<double>(i,0) = 2*(theta[i] -theta_0[i]) / NL * pow((theta_max[i] - theta_min[i]),2.0);
        }



        //Control Input: Joint Angle velocities
        //Theta_dot = Jinv * (Pose_T_dot + Kp*Error);
        //Theta_dot = Jinv * (Pose_T_dot + Kp*Error) - 0.5 * (cv::Mat::eye(NL,NL,CV_64F)-Jinv*J) * q0_dot;

        // Pseudo-inverse method
        // Theta_dot = Jinv * Error;

        // Damped Least Square method
        double lambda = 0.2;
        Theta_dot = J.t() * (J*J.t()+lambda*lambda*cv::Mat::eye(J.rows, J.rows, CV_64F)).inv() * Error;

        for(int i = 0; i < NL; ++i)
            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);

        coolarm_pose_fk(theta, pose, VALID_FLAG);
        joint_position(theta, Jpos);

        for(int i = 0; i < 6; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        double se;
        if(VALID_FLAG)
        {
            se = 0.0;
            for(int i = 0; i < 6; ++i)
            {
                Pose_C.at<double>(i) = pose[i];
                se += Error.at<double>(i)*Error.at<double>(i);
                f1 << Pose_C.at<double>(i) << "\t";
            }
            f1 << endl;
            se = sqrt(se/6);
        }
        //f5 << t << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0)<< endl;
        f5 << t << "\t" << se << endl;


    }// time-loop

    cout << "Pose_T = " << Pose_T << endl;
    cout << "Pose_C = " << Pose_C << endl;

    for(int i = 0; i < 6; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f7 << Jpos.at<double>(i,j) << "\t";
        f7 << endl;
    }
    f7.close();

    f1.close();
    f2.close();
    f3.close();
    f4.close();
    f5.close();

    //----------------------------------------
    // Plotting Coordinate Axes
    //-----------------------------------------

    GP_handle G2("/usr/bin/", "Time (sec)", "Error");
    G2.gnuplot_cmd("set terminal wxt");
    G2.gnuplot_cmd("plot 'error.txt' w l lw 2 notitle");

    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border");
    G1.gnuplot_cmd("set yrange [-0.3:0.3]");
    G1.gnuplot_cmd("set xrange [0:0.5]");
    G1.gnuplot_cmd("set ticslevel 0");

    G1.gnuplot_cmd("splot 'finalconfig.txt' u 1:2:3 w lp pt 4 lt -1 lw 2 t 'actual pose'");
    G1.gnuplot_cmd("replot 'actual.txt' u 1:2:3 w l t 'actual trajectory'");
    G1.gnuplot_cmd("replot 'desiredConfig.txt' u 1:2:3 w lp pt 4 lt 9 lw 2 t 'desired pose'");

    double xorg[3] = {0,0,0}; // origin of base frame
    double xb[3][3] = {{0.1,0,0},{0,0.1,0},{0,0,0.1}};

    G1.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);

    double xend[3][3] = {{pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]}};

    double xend2[3][3] = {{pose_t[0], pose_t[1], pose_t[2]},
                         {pose_t[0], pose_t[1], pose_t[2]},
                         {pose_t[0], pose_t[1], pose_t[2]}};


    double xeb[3][3], xeb2[3][3];



    cv::Mat R(3,3,CV_64F,0.0);
    cv::Mat R2(3,3,CV_64F,0.0);
    cv::Mat T(3,3,CV_64F, &xend);
    cv::Mat T2(3,3,CV_64F, &xend2); //target
    cv::Mat baseC(3,3,CV_64F, &xb);
    cv::Mat endC(3,3,CV_64F, 0.0);
    cv::Mat endC2(3,3,CV_64F, 0.0); //target

    rotation_matrix(theta, R);
    rotation_matrix(theta_t, R2); //target

    //output is a column matrix
    endC = R * baseC.t() + T.t(); // Actual Orientation
    endC2 = R2 * baseC.t() + T2.t(); // target Orientation

    // make it a row matrix
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
        {
            xeb[i][j] = endC.at<double>(j,i); // make it a row matrix
            xeb2[i][j] = endC2.at<double>(j,i); // make it a row matrix
            cout << xeb[i][j] << ":" << xeb2[i][j] << "\t";
        }
        cout << endl;
    }


    G1.draw3dcoordAxis(xend[0], xeb[0], xeb[1], xeb[2], true, 2); // actual
    G1.draw3dcoordAxis(xend2[0], xeb2[0], xeb2[1], xeb2[2], true, 1); //desired


    getchar();

    return 0;

}


