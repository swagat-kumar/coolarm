#include <coolarm_fk.h>

//----------------------------
inline bool exist(const std::string& name)
{
    std::ifstream file(name.c_str());
    if(!file)            // If the file was not found, then file is 0, i.e. !file=1 or true.
        return false;    // The file was not found.
    else                 // If the file was found, then file is non-0.
        return true;     // The file was found.
}
//-------------------------------

//--------------------------------------------
void cart_posn_normalize(double x[], double xn[])
{
  int i;
  for(i = 0; i < NW; i++)
    xn[i] = 2.0 *((x[i] - c_min[i])/(c_max[i] - c_min[i]) - 0.5);
}
//-------------------------------------------
void cart_posn_denormalize(double xn[], double x[])
{
  int i;
  for(i = 0; i < NW; i++)
    x[i] = c_min[i] + (xn[i]/2.0 + 0.5) * (c_max[i] - c_min[i]);
}
//----------------------------------------
void angle_normalize(double theta[], double th_n[])
{
  int i;
  for(i = 0; i < NL; i++)
    th_n[i] = 2.0 * ((theta[i] - theta_min[i])/
        (theta_max[i] - theta_min[i]) - 0.5);
}
/* ---------------------------------------------- */
void angle_denormalize( double th_n[], double th[])
{
  int i;
  for(i = 0; i < NL; i++)
    th[i] = theta_min[i] + (th_n[i]/2.0 + 0.5) * (theta_max[i] - theta_min[i]);
}
//--------------------------------------------------------
// Forward kinematics
// Th - Joint angle values in radians
// x - Cartesian position of end-effector in meters
void coolarm_7dof_FK(const double Th[], double x[])
{
  double th_1, th_2, th_3, th_4, th_5, th_6, th_7;

  th_1 = Th[0];
  th_2 = Th[1];
  th_3 = Th[2];
  th_4 = Th[3] - M_PI/2.0;
  th_5 = Th[4];
  th_6 = Th[5] - M_PI/2.0;
  th_7 = Th[6];

  x[0] = cos (th_1)*(cos (th_2)*(cos (th_3)*(cos (th_4)*(cos
            (th_5)*(a5-sin (th_6)*d7)+a4)+sin (th_4)*cos
          (th_6)*d7)-sin (th_3)*sin (th_5)*(a5-sin (th_6)*d7))-sin
      (th_2)*(sin (th_4)*(cos (th_5)*(a5-sin (th_6)*d7)+a4)-cos
        (th_4)*cos (th_6)*d7-d3))-sin (th_1)*(sin (th_3)*(cos
        (th_4)*(cos (th_5)*(a5-sin (th_6)*d7)+a4)+sin (th_4)*cos
        (th_6)*d7)+cos (th_3)*sin (th_5)*(a5-sin (th_6)*d7));


  x[1] =  sin (th_1)*(cos (th_2)*(cos (th_3)*(cos (th_4)*(cos
            (th_5)*(a5-sin (th_6)*d7)+a4)+sin (th_4)*cos (th_6)*d7)-sin
        (th_3)*sin (th_5)*(a5-sin (th_6)*d7))-sin (th_2)*(sin
        (th_4)*(cos (th_5)*(a5-sin (th_6)*d7)+a4)-cos (th_4)*cos
        (th_6)*d7-d3))+cos (th_1)*(sin (th_3)*(cos (th_4)*(cos
            (th_5)*(a5-sin (th_6)*d7)+a4)+sin (th_4)*cos (th_6)*d7)+cos
        (th_3)*sin (th_5)*(a5-sin (th_6)*d7));

  x[2] = -sin (th_2)*(cos (th_3)*(cos (th_4)*(cos (th_5)*(a5-sin
            (th_6)*d7)+a4)+sin (th_4)*cos (th_6)*d7)-sin (th_3)*sin
      (th_5)*(a5-sin (th_6)*d7))-cos (th_2)*(sin (th_4)*(cos
        (th_5)*(a5-sin (th_6)*d7)+a4)-cos (th_4)*cos (th_6)*d7-d3)+d1;

}
//====================================================
// Computes the 3x3 rotation matrix for the end-effector coordinate system wrt the base frame
// Input: joint angle vector: Th[7]
// Output: Roatation matrix: R[3][3]
//----------------------------------------------
void rotation_matrix(const double Th[], Mat &R)
{
    double theta[8];

    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3] - M_PI/2.0;
    theta[5] = Th[4];
    theta[6] = Th[5] - M_PI/2.0;
    theta[7] = Th[6];

    R.at<double>(0,0) = cos(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*(sin(theta[5])*sin(theta[7])+cos(theta[5])*cos(theta[6])
                            *cos(theta[7]))+sin(theta[4])*sin(theta[6])*cos(theta[7]))-sin(theta[3])*(sin(theta[5])*cos(theta[6])
                            *cos(theta[7])-cos(theta[5])*sin(theta[7])))-sin(theta[2])*(sin(theta[4])*(sin(theta[5])*sin(theta[7])
             +cos(theta[5])*cos(theta[6])*cos(theta[7]))-cos(theta[4])*sin(theta[6])*cos(theta[7])))-sin(theta[1])*(sin(theta[3])*(cos(theta[4])
            *(sin(theta[5])*sin(theta[7])+cos(theta[5])*cos(theta[6])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*cos(theta[7]))
            +cos(theta[3])*(sin(theta[5])*cos(theta[6])*cos(theta[7])-cos(theta[5])*sin(theta[7])));


    R.at<double>(0,1) = cos(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))
            -sin(theta[4])*sin(theta[6])*sin(theta[7]))-sin(theta[3])*(cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7])))-sin(theta[2])*(sin(theta[4])
            *(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+cos(theta[4])*sin(theta[6])*sin(theta[7])))-sin(theta[1])*(sin(theta[3])*(cos(theta[4])
       *(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))-sin(theta[4])*sin(theta[6])*sin(theta[7]))+cos(theta[3])*(cos(theta[5])*cos(theta[7])
       -sin(theta[5])*cos(theta[6])*sin(theta[7])));

    R.at<double>(0,2) = cos(theta[1])*(cos(theta[2])*(cos(theta[3])*(sin(theta[4])*cos(theta[6])-cos(theta[4])*cos(theta[5])*sin(theta[6]))
            +sin(theta[3])*sin(theta[5])*sin(theta[6]))-sin(theta[2])*(-sin(theta[4])*cos(theta[5])*sin(theta[6])
            -cos(theta[4])*cos(theta[6])))-sin(theta[1])*(sin(theta[3])*(sin(theta[4])*cos(theta[6])-cos(theta[4])*cos(theta[5])*sin(theta[6]))-cos(theta[3])*sin(theta[5])*sin(theta[6]));

    R.at<double>(1,0) = sin(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*(sin(theta[5])*sin(theta[7])+cos(theta[5])*cos(theta[6])*cos(theta[7]))+sin(theta[4])*sin(theta[6])
                           *cos(theta[7]))-sin(theta[3])*(sin(theta[5])*cos(theta[6])*cos(theta[7])-cos(theta[5])*sin(theta[7])))-sin(theta[2])*(sin(theta[4])
            *(sin(theta[5])*sin(theta[7])+cos(theta[5])*cos(theta[6])*cos(theta[7]))-cos(theta[4])*sin(theta[6])*cos(theta[7])))+cos(theta[1])*(sin(theta[3])*(cos(theta[4])
       *(sin(theta[5])*sin(theta[7])+cos(theta[5])*cos(theta[6])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*cos(theta[7]))+cos(theta[3])*(sin(theta[5])*cos(theta[6])*cos(theta[7])
       -cos(theta[5])*sin(theta[7])));

    R.at<double>(1,1) = sin(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))-sin(theta[4])*sin(theta[6])
                           *sin(theta[7]))-sin(theta[3])*(cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7])))-sin(theta[2])*(sin(theta[4])
            *(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+cos(theta[4])*sin(theta[6])*sin(theta[7])))+cos(theta[1])*(sin(theta[3])*(cos(theta[4])
       *(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))-sin(theta[4])*sin(theta[6])*sin(theta[7]))+cos(theta[3])*(cos(theta[5])*cos(theta[7])
       -sin(theta[5])*cos(theta[6])*sin(theta[7])));

    R.at<double>(1,2) = sin(theta[1])*(cos(theta[2])*(cos(theta[3])*(sin(theta[4])*cos(theta[6])-cos(theta[4])*cos(theta[5])*sin(theta[6]))+sin(theta[3])*sin(theta[5])
                          *sin(theta[6]))-sin(theta[2])*(-sin(theta[4])*cos(theta[5])*sin(theta[6])-cos(theta[4])*cos(theta[6])))+cos(theta[1])*(sin(theta[3])*(sin(theta[4])*cos(theta[6])
       -cos(theta[4])*cos(theta[5])*sin(theta[6]))-cos(theta[3])*sin(theta[5])*sin(theta[6]));


    R.at<double>(2,0) = -sin(theta[2])*(cos(theta[3])*(cos(theta[4])*(sin(theta[5])*sin(theta[7])+cos(theta[5])*cos(theta[6])*cos(theta[7]))+sin(theta[4])*sin(theta[6])*cos(theta[7]))
            -sin(theta[3])*(sin(theta[5])*cos(theta[6])*cos(theta[7])-cos(theta[5])*sin(theta[7])))-cos(theta[2])*(sin(theta[4])*(sin(theta[5])*sin(theta[7])
      +cos(theta[5])*cos(theta[6])*cos(theta[7]))-cos(theta[4])*sin(theta[6])*cos(theta[7]));


    R.at<double>(2,1) = -sin(theta[2])*(cos(theta[3])*(cos(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))-sin(theta[4])*sin(theta[6])
                          *sin(theta[7]))-sin(theta[3])*(cos(theta[5])*cos(theta[7])-sin(theta[5])*cos(theta[6])*sin(theta[7])))
            -cos(theta[2])*(sin(theta[4])*(-cos(theta[5])*cos(theta[6])*sin(theta[7])-sin(theta[5])*cos(theta[7]))+cos(theta[4])*sin(theta[6])*sin(theta[7]));


    R.at<double>(2,2) = -sin(theta[2])*(cos(theta[3])*(sin(theta[4])*cos(theta[6])-cos(theta[4])*cos(theta[5])*sin(theta[6]))+sin(theta[3])*sin(theta[5])*sin(theta[6]))
            -cos(theta[2])*(-sin(theta[4])*cos(theta[5])*sin(theta[6])-cos(theta[4])*cos(theta[6]));

}
//==========================================================
// Coolarm forward kinematics that returns the position and
// orientation of the end-effector
// Input: Joint angle vector: theta[7]
// Output: end-effector pose: xpose[6];
// Orientation is in terms of X-Y-Z fixed angles (gamma -->roll, beta --> pitch, alpha --> Yaw)
//---------------------------------------------
void coolarm_pose_fk(double Th[], double xp[], bool & VALID)
{
    double theta[8];

    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3] - M_PI/2.0;
    theta[5] = Th[4];
    theta[6] = Th[5] - M_PI/2.0;
    theta[7] = Th[6];

    xp[0] = cos(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)
             +sin(theta[4])*cos(theta[6])*d7)-sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))-sin(theta[2])*(sin(theta[4])
            *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)-cos(theta[4])*cos(theta[6])*d7-d3))-sin(theta[1])*(sin(theta[3])*(cos(theta[4])
              *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)+cos(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7));

    xp[1] = sin(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)
             +sin(theta[4])*cos(theta[6])*d7)-sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))-sin(theta[2])*(sin(theta[4])
            *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)-cos(theta[4])*cos(theta[6])*d7-d3))+cos(theta[1])*(sin(theta[3])*(cos(theta[4])
              *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)+cos(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7));

    xp[2] = -sin(theta[2])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)
            -sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))-cos(theta[2])*(sin(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)
            -cos(theta[4])*cos(theta[6])*d7-d3)+d1;

    Mat R(3,3, CV_64F);
    rotation_matrix(Th, R); // Pass the original Th[] variable.

    // Beta - Pitch - Rotation about Y-axis. consider the positive value of the sqrt
    xp[3] = atan2(-R.at<double>(2,0), fabs(sqrt(pow(R.at<double>(0,0), 2) + pow(R.at<double>(1,0), 2.0) )) );

    if(xp[3] > -M_PI/2.0 && xp[3] < M_PI/2.0)
    {
        // Alpha - Roll- Rotation about z-axis
        xp[4] = atan2(R.at<double>(1,0)/cos(xp[3]), R.at<double>(0,0)/cos(xp[3]));

        // Gamma - Yaw - Rotation about X-axis
        xp[5] = atan2(R.at<double>(2,1)/cos(xp[3]), R.at<double>(2,2)/cos(xp[3]));

        VALID = true;
    }
    else if (xp[3] == M_PI/2.0)
    {
        xp[4] = 0.0;
        xp[5] = atan2(R.at<double>(0,1), R.at<double>(1,1));

        VALID = true;
    }
    else if(xp[3] == - M_PI/2.0)
    {
        xp[4] = 0.0;
        xp[5] = -atan2(R.at<double>(0,1), R.at<double>(1,1));

        VALID = true;
    }
    else
    {
        VALID = false;
    }
}
//===============================================================

//---------------------------------------------------
// Generates input-output data for the manipulator
void generate_data(double Uc[], double Th[])
{
    do
    {
        for(int i = 0; i < NL; i++)
            Th[i] = theta_min[i] + (theta_max[i] - theta_min[i]) *
                    (rand()/(double)RAND_MAX);

        //So that Ut is in the workspace of robot
        coolarm_7dof_FK(Th,Uc);

        if( (Uc[0] >= c_min[0]) && (Uc[0] < c_max[0]) &&
                (Uc[1] >= c_min[1]) && (Uc[1] < c_max[1]) &&
                (Uc[2] >= c_min[2]) && (Uc[2] < c_max[2]) )
            break;
    }while(1);
}
//----------------------------------------------------------
// Generates input-output data for the manipulator
// Input: Joint angles
// Output: End-effector pose in 6D
void generate_pose_data(double Uc[], double Th[])
{
    bool repeat_flag = true;
    do
    {
        bool VALID = true;
        for(int i = 0; i < NL; i++)
            Th[i] = theta_min[i] + (theta_max[i] - theta_min[i]) *
                    (rand()/(double)RAND_MAX);

        // find the end-effector pose for a given theta vector
        // forward kinematics
        coolarm_pose_fk(Th, Uc, VALID);

        if(VALID == true)
        {
            unsigned int valid_cnt = 0;
            for(int i = 0; i < NW; ++i)
            {
                if( (Uc[i] > c_min[i]) && (Uc[i] < c_max[i]) )  // if values are valid, no need to repeat
                    valid_cnt += 1;
            }
            if(valid_cnt < NW)  // one or more data points exceed the bounds, repeat
                repeat_flag = true;
            else
                repeat_flag = false;
        }
        else
            repeat_flag = true;

    }while(repeat_flag);
}


//-----------------------------------------------
// Gives the position of various joints
// th[7] is in radians
void joint_position(const double theta[7], Mat &X)
{
    //These are system offsets to be incorporated into joint angles
    double th[7];
    th[0] = theta[0];
    th[1] = theta[1];
    th[2] = theta[2];
    th[3] = theta[3] - M_PI/2.0;
    th[4] = theta[4];
    th[5] = theta[5] - M_PI/2.0;
    th[6] = theta[6];


    //transformation matrices
    double t1[4][4] = { {cos(th[0]), -sin(th[0]), 0, 0}, {sin(th[0]), cos(th[0]), 0, 0}, {0, 0, 1, d1}, {0,0,0,1} };
    double t2[4][4] = { {cos(th[1]), -sin(th[1]), 0, 0}, {0, 0, 1, 0}, {-sin(th[1]), -cos(th[1]), 0, 0}, {0,0,0,1} };
    double t3[4][4] = { {cos(th[2]), -sin(th[2]), 0, 0}, {0, 0, -1, -d3}, {sin(th[2]), cos(th[2]), 0, 0}, {0,0,0,1} };
    double t4[4][4] = { {cos(th[3]), -sin(th[3]), 0, 0}, {0, 0, 1, 0}, {-sin(th[3]), -cos(th[3]), 0, 0}, {0,0,0,1} };
    double t5[4][4] = { {cos(th[4]), -sin(th[4]), 0, a4}, {0, 0, -1, 0}, {sin(th[4]), cos(th[4]), 0, 0}, {0,0,0,1} };
    double t6[4][4] = { {cos(th[5]), -sin(th[5]), 0, a5}, {0, 0, -1, 0}, {sin(th[5]), cos(th[5]), 0, 0}, {0,0,0,1} };
    double t7[4][4] = { {cos(th[6]), -sin(th[6]), 0, 0}, {0, 0, 1, d7}, {-sin(th[6]), -cos(th[6]), 0, 0}, {0,0,0,1} };

    Mat T1 = Mat(4, 4, CV_64F, t1);
    Mat T2 = Mat(4, 4, CV_64F, t2);
    Mat T3 = Mat(4, 4, CV_64F, t3);
    Mat T4 = Mat(4, 4, CV_64F, t4);
    Mat T5 = Mat(4, 4, CV_64F, t5);
    Mat T6 = Mat(4, 4, CV_64F, t6);
    Mat T7 = Mat(4, 4, CV_64F, t7);



    //Mat X = Mat(6, 3,CV_64F, 0.0);

    Mat TF1 = T1 * T2;
    X.row(1) = TF1.col(3).rowRange(0,3).t();


    Mat TF2 = TF1*T3*T4;
    X.row(2) = TF2.col(3).rowRange(0,3).t();


    Mat TF3 = TF2*T5;
    X.row(3) = TF3.col(3).rowRange(0,3).t();


    Mat TF4 = TF3*T6;
    X.row(4) = TF4.col(3).rowRange(0,3).t();


    Mat TF5 = TF4 * T7;
    X.row(5) = TF5.col(3).rowRange(0,3).t();

    //cout << X << endl;

}
//=============================================================
// Jp : dX /d theta : 3x7
void position_jacobian(double Th[], cv::Mat &Jp)
{
    double theta[8];

    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3] - M_PI/2.0;
    theta[5] = Th[4];
    theta[6] = Th[5] - M_PI/2.0;
    theta[7] = Th[6];



    //Elements of Jacobian matrix,

    Jp.at<double>(0,0) = -sin(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)
             +sin(theta[4])*cos(theta[6])*d7)-sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))-sin(theta[2])*(sin(theta[4])
            *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)-cos(theta[4])*cos(theta[6])*d7-d3))-cos(theta[1])*(sin(theta[3])*(cos(theta[4])
             *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)+cos(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7));

    Jp.at<double>(0,1) = cos(theta[1])*(-sin(theta[2])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)
             +sin(theta[4])*cos(theta[6])*d7)-sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))-cos(theta[2])*(sin(theta[4])
           *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)-cos(theta[4])*cos(theta[6])*d7-d3));


    Jp.at<double>(0,2) = cos(theta[1])*cos(theta[2])*(-sin(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)
                             +sin(theta[4])*cos(theta[6])*d7)-cos(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))-sin(theta[1])*(cos(theta[3])*(cos(theta[4])
                              *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)-sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7));

    Jp.at<double>(0,3) = cos(theta[1])*(cos(theta[2])*cos(theta[3])*(cos(theta[4])*cos(theta[6])*d7-sin(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4))
            -sin(theta[2])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7))-sin(theta[1])*sin(theta[3])*(cos(theta[4])*cos(theta[6])*d7
            -sin(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4));

    Jp.at<double>(0,4) = cos(theta[1])*(cos(theta[2])*(-cos(theta[3])*cos(theta[4])*sin(theta[5])*(a5-sin(theta[6])*d7)-sin(theta[3])*cos(theta[5])
           *(a5-sin(theta[6])*d7))+sin(theta[2])*sin(theta[4])*sin(theta[5])*(a5-sin(theta[6])*d7))-sin(theta[1])*(cos(theta[3])*cos(theta[5])*(a5-sin(theta[6])*d7)
            -sin(theta[3])*cos(theta[4])*sin(theta[5])*(a5-sin(theta[6])*d7));

    Jp.at<double>(0,5) = cos(theta[1])*(cos(theta[2])*(cos(theta[3])*(-sin(theta[4])*sin(theta[6])*d7-cos(theta[4])*cos(theta[5])*cos(theta[6])*d7)
            +sin(theta[3])*sin(theta[5])*cos(theta[6])*d7)-sin(theta[2])*(cos(theta[4])*sin(theta[6])*d7-sin(theta[4])*cos(theta[5])*cos(theta[6])*d7))
            -sin(theta[1])*(sin(theta[3])*(-sin(theta[4])*sin(theta[6])*d7-cos(theta[4])*cos(theta[5])*cos(theta[6])*d7)-cos(theta[3])*sin(theta[5])*cos(theta[6])*d7);

    Jp.at<double>(0,6) = 0.0;

    Jp.at<double>(1,0) = cos(theta[1])*(cos(theta[2])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)
             +sin(theta[4])*cos(theta[6])*d7)-sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))-sin(theta[2])*(sin(theta[4])
            *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)-cos(theta[4])*cos(theta[6])*d7-d3))-sin(theta[1])*(sin(theta[3])*(cos(theta[4])
              *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)+cos(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7));


    Jp.at<double>(1,1) = sin(theta[1])*(-sin(theta[2])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)
            -sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))-cos(theta[2])*(sin(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)-cos(theta[4])*cos(theta[6])*d7-d3));


    Jp.at<double>(1,2) = sin(theta[1])*cos(theta[2])*(-sin(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)
             -cos(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7))+cos(theta[1])*(cos(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)
                              +sin(theta[4])*cos(theta[6])*d7)-sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7));


    Jp.at<double>(1,3) = sin(theta[1])*(cos(theta[2])*cos(theta[3])*(cos(theta[4])*cos(theta[6])*d7-sin(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4))-sin(theta[2])*(cos(theta[4])
             *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7))+cos(theta[1])*sin(theta[3])*(cos(theta[4])*cos(theta[6])*d7
            -sin(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4));

    Jp.at<double>(1,4) = sin(theta[1])*(cos(theta[2])*(-cos(theta[3])*cos(theta[4])*sin(theta[5])*(a5-sin(theta[6])*d7)-sin(theta[3])*cos(theta[5])*(a5-sin(theta[6])*d7))
            +sin(theta[2])*sin(theta[4])*sin(theta[5])*(a5-sin(theta[6])*d7))+cos(theta[1])*(cos(theta[3])*cos(theta[5])*(a5-sin(theta[6])*d7)
            -sin(theta[3])*cos(theta[4])*sin(theta[5])*(a5-sin(theta[6])*d7));

    Jp.at<double>(1,5) = sin(theta[1])*(cos(theta[2])*(cos(theta[3])*(-sin(theta[4])*sin(theta[6])*d7-cos(theta[4])*cos(theta[5])*cos(theta[6])*d7)
            +sin(theta[3])*sin(theta[5])*cos(theta[6])*d7)-sin(theta[2])*(cos(theta[4])*sin(theta[6])*d7-sin(theta[4])*cos(theta[5])
                          *cos(theta[6])*d7))+cos(theta[1])*(sin(theta[3])*(-sin(theta[4])*sin(theta[6])*d7-cos(theta[4])*cos(theta[5])
                            *cos(theta[6])*d7)-cos(theta[3])*sin(theta[5])*cos(theta[6])*d7);

    Jp.at<double>(1,6) = 0.0;


    Jp.at<double>(2,0) = 0.0;

    Jp.at<double>(2,1) = sin(theta[2])*(sin(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)-cos(theta[4])*cos(theta[6])*d7-d3)-cos(theta[2])*(cos(theta[3])*(cos(theta[4])
                             *(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)-sin(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7));

    Jp.at<double>(2,2) = -sin(theta[2])*(-sin(theta[3])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7)-cos(theta[3])*sin(theta[5])*(a5-sin(theta[6])*d7));

    Jp.at<double>(2,3) = -sin(theta[2])*cos(theta[3])*(cos(theta[4])*cos(theta[6])*d7-sin(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4))
            -cos(theta[2])*(cos(theta[4])*(cos(theta[5])*(a5-sin(theta[6])*d7)+a4)+sin(theta[4])*cos(theta[6])*d7);

    Jp.at<double>(2,4) = cos(theta[2])*sin(theta[4])*sin(theta[5])*(a5-sin(theta[6])*d7)-sin(theta[2])*(-cos(theta[3])*cos(theta[4])*sin(theta[5])*(a5-sin(theta[6])*d7)
                           -sin(theta[3])*cos(theta[5])*(a5-sin(theta[6])*d7));

    Jp.at<double>(2,5) = -sin(theta[2])*(cos(theta[3])*(-sin(theta[4])*sin(theta[6])*d7 -cos(theta[4])*cos(theta[5])*cos(theta[6])*d7)+sin(theta[3])*sin(theta[5])*cos(theta[6])*d7)
            -cos(theta[2])*(cos(theta[4])*sin(theta[6])*d7-sin(theta[4])*cos(theta[5])*cos(theta[6])*d7);

    Jp.at<double>(2,6) = 0.0;

}

//==============================================
void orientation_jacobian(double Th[], cv::Mat &Jw)
{
    double theta[8];

    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3] - M_PI/2.0;
    theta[5] = Th[4];
    theta[6] = Th[5] - M_PI/2.0;
    theta[7] = Th[6];

    // Rotation Matrix
    Mat R = Mat(3,3, CV_64F);
    rotation_matrix(Th, R);  //pass the original Th[] vector


    double r21 = R.at<double>(1,0);
    double r11 = R.at<double>(0,0);
    double r31 = R.at<double>(2,0);
    double r32 = R.at<double>(2,1);
    double r33 = R.at<double>(2,2);
    double r22 = R.at<double>(1,1);
    double r12 = R.at<double>(0,1);

    double beta = atan2(-r31, abs(sqrt(r11*r11 + r21*r21)));

    if(beta > -M_PI/2.0 && beta < M_PI/2.0) // Case 1
    {
        // d (beta) / dtheta

        Jw.at<double>(0,0) = ((r11*r21+(
                                   ((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                                   ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                                   ((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                                   r11)*r31)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        Jw.at<double>(0,1) = -(((((sin(theta[1])*cos(theta[2])*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[1])*sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+(
                    (sin(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[1])*cos(theta[2])*cos(theta[4]))*sin(theta[6])+
                    ((sin(theta[1])*cos(theta[2])*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-sin(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*cos(theta[7]))*r21+(
                    ((cos(theta[1])*cos(theta[2])*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+cos(theta[1])*sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[1])*cos(theta[2])*cos(theta[4]))*sin(theta[6])+
                    ((cos(theta[1])*cos(theta[2])*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-cos(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*cos(theta[7]))*r11)*r31+(
                    ((sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-cos(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+
                    ((-cos(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[2])*cos(theta[4]))*sin(theta[6])+(cos(theta[2])*sin(theta[3])*sin(theta[5])+(sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r21*r21+(
                    ((sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-cos(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+
                    ((-cos(theta[2])*cos(theta[3])*sin(theta[4])-sin(theta[2])*cos(theta[4]))*sin(theta[6])+(cos(theta[2])*sin(theta[3])*sin(theta[5])+(sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r11*r11)/(
                    sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        Jw.at<double>(0,2) = -(((((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+
                    ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r21+(
                    ((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+
                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r11)*r31+(
                    (sin(theta[2])*sin(theta[3])*cos(theta[4])*sin(theta[5])-sin(theta[2])*cos(theta[3])*cos(theta[5]))*sin(theta[7])+
                    (sin(theta[2])*sin(theta[3])*sin(theta[4])*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r21*r21+(
                    (sin(theta[2])*sin(theta[3])*cos(theta[4])*sin(theta[5])-sin(theta[2])*cos(theta[3])*cos(theta[5]))*sin(theta[7])+
                    (sin(theta[2])*sin(theta[3])*sin(theta[4])*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r11*r11)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        Jw.at<double>(0,3) = -(((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])+(
                    (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[6])+
                    ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*cos(theta[7]))*r21+(
                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])+(
                    (cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[6])+
                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*cos(theta[7]))*r11)*r31+((sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))
                    *sin(theta[5])*sin(theta[7])+((-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*cos(theta[7]))*r21*r21+(
                    (sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])+
                    ((-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*cos(theta[7]))*r11*r11)/(sqrt(r21*r21+r11*r11)*
                    (r31*r31+r21*r21+r11*r11));


        Jw.at<double>(0,4) = -(((((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[7])+(((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[6])*
                cos(theta[7]))*r21+(((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[7])+(((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[6])*
                cos(theta[7]))*r11)*r31+((sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[7])+
                ((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*r21*r21+(
                (sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[7])+
                ((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*r11*r11)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));


        Jw.at<double>(0,5) = (((((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[6])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*cos(theta[7])*r21+(
                ((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*cos(theta[7])*r11)*r31+
                ((sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*cos(theta[6]))*cos(theta[7])*r21*r21+
                ((sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*cos(theta[6]))*cos(theta[7])*r11*r11)/(
                sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        Jw.at<double>(0,6) = -((((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[7]))*r21+((
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*r11)*r31
                +(((sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*sin(theta[6])+((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-sin(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[7]))*r21*r21+(
                ((sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*sin(theta[6])+((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-sin(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[7]))*r11*r11)/(sqrt(r21*r21+r11*r11)*(r31*r31+r21*r21+r11*r11));

        //------------------------------

        // d (alpha) / d theta: 1 x 7

        Jw.at<double>(1,0) = -((((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*
                sin(theta[7])+(((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                r21-r11*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,1) = ((((cos(theta[1])*cos(theta[2])*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+cos(theta[1])*sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[1])*cos(theta[2])*cos(theta[4]))*sin(theta[6])+
                    ((cos(theta[1])*cos(theta[2])*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5])-cos(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*cos(theta[7]))*r21+(
                    ((-sin(theta[1])*cos(theta[2])*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-sin(theta[1])*sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+(
                    (sin(theta[1])*cos(theta[2])*cos(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[6])+
                    (sin(theta[1])*sin(theta[2])*sin(theta[3])*sin(theta[5])+(-sin(theta[1])*cos(theta[2])*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,2) = ((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+
                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r21+(
                    ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*sin(theta[5])+(cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                    (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[4])*sin(theta[6])+
                    ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,3) = -((((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])+(
                    ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[6])+
                    ((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*cos(theta[7]))*r21+(
                    ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*sin(theta[7])+(
                    (sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[6])+
                    ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*cos(theta[7]))*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,4) = ((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[7])
                +(((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*
                r21+(((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*sin(theta[7])+
                ((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[6])*cos(theta[7]))*
                r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,5) = -((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[6])+((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*cos(theta[7])*r21+(
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*cos(theta[7])*r11)/(r21*r21+r11*r11);

        Jw.at<double>(1,6) = -(((((sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                (((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*r21+((
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]))*cos(theta[7]))*r11)/(
                r21*r21+r11*r11);

        //----------------------------

        // d gamma / d theta : 1 x 7

        Jw.at<double>(2,0) = 0.0;

        Jw.at<double>(2,1) = ((((cos(theta[2])*cos(theta[3])*sin(theta[4])+sin(theta[2])*cos(theta[4]))*sin(theta[6])+((cos(theta[2])*cos(theta[3])*cos(theta[4])-sin(theta[2])*sin(theta[4]))*cos(theta[5])-cos(theta[2])*sin(theta[3])*sin(theta[5]))*cos(theta[6]))*sin(theta[7])
                +((cos(theta[2])*cos(theta[3])*cos(theta[4])-sin(theta[2])*sin(theta[4]))*sin(theta[5])+cos(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[7]))*r33+
                ((cos(theta[2])*sin(theta[3])*sin(theta[5])+(sin(theta[2])*sin(theta[4])-cos(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+(cos(theta[2])*cos(theta[3])*sin(theta[4])+sin(theta[2])*cos(theta[4]))*cos(theta[6]))*r32)/(r33*r33+r32*r32);

        Jw.at<double>(2,2) = -(((sin(theta[2])*sin(theta[3])*sin(theta[4])*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                (sin(theta[2])*sin(theta[3])*cos(theta[4])*sin(theta[5])-sin(theta[2])*cos(theta[3])*cos(theta[5]))*cos(theta[7]))*r33+
                ((-sin(theta[2])*cos(theta[3])*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[4])*cos(theta[5]))*sin(theta[6])+sin(theta[2])*sin(theta[3])*sin(theta[4])*cos(theta[6]))*r32)/(r33*r33+r32*r32);

        Jw.at<double>(2,3) = ((((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[6])+(cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*cos(theta[5])*cos(theta[6]))*sin(theta[7])+
                (cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[5])*cos(theta[7]))*r33+
                ((sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*cos(theta[5])*sin(theta[6])+(cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[6]))*r32)/(r33*r33+r32*r32);

        Jw.at<double>(2,4) = -((((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*cos(theta[6])*sin(theta[7])+
                (sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[7]))*r33+
                ((-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])-sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[6])*r32)/(r33*r33+r32*r32);

        Jw.at<double>(2,5) = (((sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*sin(theta[6])+(sin(theta[2])*cos(theta[3])*sin(theta[4])-cos(theta[2])*cos(theta[4]))*cos(theta[6]))*sin(theta[7])*
                r33+((cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[6])+(sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*r32)/(r33*r33+r32*r32);

        Jw.at<double>(2,6) = -((((cos(theta[2])*sin(theta[4])+sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])+sin(theta[2])*sin(theta[3])*cos(theta[5]))*sin(theta[7])+
                ((cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]))*sin(theta[6])+(sin(theta[2])*sin(theta[3])*sin(theta[5])+(-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*r33)/(
                r33*r33+r32*r32);

    }
    else if (beta == M_PI/2.0)
    {
        // d beta / d theta = 0.0
        // d alpha / d theta = 0.0;

        for(int i = 0; i < 7; ++i)
        {
            Jw.at<double>(0,i) = 0.0;
            Jw.at<double>(1,i) = 0.0;
        }

        // d gamma / d theta

        Jw.at<double>(2,0) = (((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                (((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*r22-r12*r12
                )/(r22*r22+r12*r12);

        Jw.at<double>(2,1) = ((cos(theta[1])*r22-sin(theta[1])*r12)*r32)/(r22*r22+r12*r12);

        Jw.at<double>(2,2) = ((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*r22+((
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[4])*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*sin(theta[5])+(cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*r12)/(r22*r22+r12*r12);

        Jw.at<double>(2,3) = ((((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*sin(theta[7])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*cos(theta[7]))*r22+((
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*sin(theta[7])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*cos(theta[7]))*r12)/(r22*r22+r12*r12);

        Jw.at<double>(2,4) = -((((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*
                cos(theta[6])*sin(theta[7])+((-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*
                cos(theta[7]))*r22+((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*
                cos(theta[6])*sin(theta[7])+((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                cos(theta[7]))*r12)/(r22*r22+r12*r12);

        Jw.at<double>(2,5) = -((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                sin(theta[6])+((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*sin(theta[7])*r22+(
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*sin(theta[7])*r12)/(r22*r22+r12*r12);

        Jw.at<double>(2,6) = -((((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*
                sin(theta[7])+(((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                r22+((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                r12)/(r22*r22+r12*r12);

    }
    else if (beta == -M_PI/2.0)
    {
        // d beta / d theta = 0.0
        // d alpha / d theta = 0.0;

        for(int i = 0; i < 7; ++i)
        {
            Jw.at<double>(0,i) = 0.0;
            Jw.at<double>(1,i) = 0.0;
        }

        // d gamma / d theta

        Jw.at<double>(2,0) = -(((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])+sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                (((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*r22-r12*r12
                )/(r22*r22+r12*r12);

        Jw.at<double>(2,1) = -((cos(theta[1])*r22-sin(theta[1])*r12)*r32)/(r22*r22+r12*r12);

        Jw.at<double>(2,2) = -((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[4])*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[5])+(cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*cos(theta[4])*sin(theta[5])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*r22+((
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[4])*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*cos(theta[5]))*cos(theta[6]))*sin(theta[7])+
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[4])*sin(theta[5])+(cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[5]))*cos(theta[7]))*r12)/(r22*r22+r12*r12);

        Jw.at<double>(2,3) = -((((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[6])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*sin(theta[7])+
                ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*cos(theta[7]))*r22+((
                ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[5])*cos(theta[6]))*sin(theta[7])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[5])*cos(theta[7]))*r12)/(r22*r22+r12*r12);


        Jw.at<double>(2,4) = ((((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*
                cos(theta[6])*sin(theta[7])+((-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*
                cos(theta[7]))*r22+((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*
                cos(theta[6])*sin(theta[7])+((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*
                cos(theta[7]))*r12)/(r22*r22+r12*r12);

        Jw.at<double>(2,5) = ((((cos(theta[1])*cos(theta[2])*sin(theta[3])+sin(theta[1])*cos(theta[3]))*sin(theta[5])+(cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*sin(theta[6])
                +((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*sin(theta[7])*r22+(
                ((cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*sin(theta[5])+((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*sin(theta[6])+
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*cos(theta[6]))*sin(theta[7])*r12)/(r22*r22+r12*r12);


        Jw.at<double>(2,6) = ((((cos(theta[1])*sin(theta[2])*sin(theta[4])+(sin(theta[1])*sin(theta[3])-cos(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*sin(theta[5])+(-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]))*
                sin(theta[7])+(((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])+cos(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*sin(theta[5])+((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])-cos(theta[1])*sin(theta[2])*sin(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                r22+((((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])-sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])+(sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*cos(theta[5]))*sin(theta[7])+(
                ((-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])-sin(theta[1])*sin(theta[2])*cos(theta[4]))*sin(theta[6])+
                ((sin(theta[1])*cos(theta[2])*sin(theta[3])-cos(theta[1])*cos(theta[3]))*sin(theta[5])+(sin(theta[1])*sin(theta[2])*sin(theta[4])+(-cos(theta[1])*sin(theta[3])-sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4]))*cos(theta[5]))*cos(theta[6]))*cos(theta[7]))*
                r12)/(r22*r22+r12*r12);

    }
    else
    {
        cout << "Beta should lie between -pi/2 to +pi/2" << endl;
        exit(-1);
    }


}//end-of-jacobian-function

//=================================================
// Computes the coordinates after rotation given in roll-pitch-yaw
// xp[3][3] - original coordinate axes
// xp2[3][3] - coordinate axes after rotation
// rpy[3] - roll-pitch-yaw angles in RADIAN
// t[3][3] - translation matrix
// xp2 = Rz * Ry * Rx * xp + t
//----------------------------------------------------------------
void rotated_axis(const cv::Mat &XP, const double rpy[], const cv::Mat &T, cv::Mat &XP2)
{

    double Alpha = rpy[0]; // roll - rotation about z axis
    double Beta = rpy[1]; // Pitch - rotation about y-axis
    double Gamma = rpy[2]; // Yaw - rotation about x-axis

    //Rotation about z axis
    double rz[3][3] = {
        {cos(Alpha), -sin(Alpha), 0},
        {sin(Alpha), cos(Alpha), 0},
        {0, 0, 1}
    };
    Mat RZ = Mat(3,3,CV_64F, &rz);

    // Rotation about y axis
    double ry[3][3] = {
        {cos(Beta), 0, sin(Beta)},
        {0, 1, 0},
        {-sin(Beta), 0, cos(Beta)}
    };
    Mat RY = Mat(3,3,CV_64F, &ry);


    // Rotation about X axis
    double rx[3][3] = {
        {1, 0, 0},
        {0, cos(Gamma), -sin(Gamma)},
        {0, sin(Gamma), cos(Gamma)}
    };
    Mat RX = Mat(3,3,CV_64F, &rx);


    // Transformed axes

    XP2 = RZ * RY * RX * XP.t() + T;

}

//=======================================================
// This is computed using Rotation matrices
// See the file "jacobian2.mac" file
// Not tested yet:
// Date: April 15, 2015
//========================================
void angular_jacobian(double Th[], cv::Mat &Jw)
{

    double theta[8];

    theta[1] = Th[0];
    theta[2] = Th[1];
    theta[3] = Th[2];
    theta[4] = Th[3] - M_PI/2.0;
    theta[5] = Th[4];
    theta[6] = Th[5] - M_PI/2.0;
    theta[7] = Th[6];

    Jw.at<double>(0,0) = 0;
    Jw.at<double>(0,1) = 0;
    Jw.at<double>(0,2) = -sin(theta[1]);
    Jw.at<double>(0,3) = cos(theta[1])*sin(theta[2]);
    Jw.at<double>(0,4) = -cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]);
    Jw.at<double>(0,5) = (cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*sin(theta[4])
            +cos(theta[1])*sin(theta[2])*cos(theta[4]);
    Jw.at<double>(0,6) = ((cos(theta[1])*cos(theta[2])*cos(theta[3])-sin(theta[1])*sin(theta[3]))*cos(theta[4])
            -cos(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])-
                (-cos(theta[1])*cos(theta[2])*sin(theta[3])-sin(theta[1])*cos(theta[3]))*cos(theta[5]);

    Jw.at<double>(1,0) = 0;
    Jw.at<double>(1,1) = 0;
    Jw.at<double>(1,2) = cos(theta[1]);
    Jw.at<double>(1,3) = sin(theta[1])*sin(theta[2]);
    Jw.at<double>(1,4) = cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]);
    Jw.at<double>(1,5) = (cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*sin(theta[4])
            +sin(theta[1])*sin(theta[2])*cos(theta[4]);
    Jw.at<double>(1,6) = ((cos(theta[1])*sin(theta[3])+sin(theta[1])*cos(theta[2])*cos(theta[3]))*cos(theta[4])
            -sin(theta[1])*sin(theta[2])*sin(theta[4]))*sin(theta[5])-
                (cos(theta[1])*cos(theta[3])-sin(theta[1])*cos(theta[2])*sin(theta[3]))*cos(theta[5]);


    Jw.at<double>(2,0) = 1;
    Jw.at<double>(2,1) = 1;
    Jw.at<double>(2,2) = 0;
    Jw.at<double>(2,3) = cos(theta[2]);
    Jw.at<double>(2,4) = sin(theta[2])*sin(theta[3]);
    Jw.at<double>(2,5) = cos(theta[2])*cos(theta[4])-sin(theta[2])*cos(theta[3])*sin(theta[4]);
    Jw.at<double>(2,6) = (-cos(theta[2])*sin(theta[4])-sin(theta[2])*cos(theta[3])*cos(theta[4]))*sin(theta[5])
            -sin(theta[2])*sin(theta[3])*cos(theta[5]);
}

void get_euler_angles(cv::Mat &R, double xp[])
{
    bool VALID;

    // Beta - Pitch - Rotation about Y-axis. consider the positive value of the sqrt
    xp[3] = atan2(-R.at<double>(2,0), fabs(sqrt(pow(R.at<double>(0,0), 2) + pow(R.at<double>(1,0), 2.0) )) );

    if(xp[3] > -M_PI/2.0 && xp[3] < M_PI/2.0)
    {
        // Alpha - Roll- Rotation about z-axis
        xp[4] = atan2(R.at<double>(1,0)/cos(xp[3]), R.at<double>(0,0)/cos(xp[3]));

        // Gamma - Yaw - Rotation about X-axis
        xp[5] = atan2(R.at<double>(2,1)/cos(xp[3]), R.at<double>(2,2)/cos(xp[3]));

        VALID = true;
    }
    else if (xp[3] == M_PI/2.0)
    {
        xp[4] = 0.0;
        xp[5] = atan2(R.at<double>(0,1), R.at<double>(1,1));

        VALID = true;
    }
    else if(xp[3] == - M_PI/2.0)
    {
        xp[4] = 0.0;
        xp[5] = -atan2(R.at<double>(0,1), R.at<double>(1,1));

        VALID = true;
    }
    else
    {
        VALID = false;
    }
}
