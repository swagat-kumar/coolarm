// Test the forward kinematics of CoolARM
#include <iostream>
#include <cmath>
#include <fstream>
#include <coolarm_fk.h>
#include <gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;

#define NC 3

//#define ROBOT_CONFIG
//#define DATAGEN
#define ROBOT_ORIENT

int main()
{

#ifdef ROBOT_CONFIG
    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border");

    ofstream f1("efposn.txt");
    ofstream f2("rconfig.txt");

    double theta[NL] = {0, DEG2RAD(50), 0, DEG2RAD(20), 0, 0, 0}; // in degrees
    double x[NC] = {0,0,0};

    Mat xr(6,3,CV_64F,0.0);

    coolarm_7dof_FK(theta,x);

    joint_position(theta, xr);

    for(int i = 0; i < NC; ++i)
        f1 << x[i] << "\t";
    f1 << endl;
    for(int i = 0; i < 6; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f2 << xr.at<double>(i,j) << "\t";
        f2 << endl;
    }
    f2 << endl << endl;

    f1.close();
    f2.close();

    cout << "program ends ... check output." << endl;

    G1.gnuplot_cmd("splot 'rconfig.txt' u 1:2:3 w lp pt 3 lw 2");
    G1.gnuplot_cmd("replot 'efposn.txt' u 1:2:3 w p pt 4 lt 3");

    cout << "Press enter key to exit ..." << endl;
    getchar();

#endif

#ifdef DATAGEN

    ofstream f3("test.txt");
    for(int i = 0; i < 100000; ++i)
    {
        double xt[3], theta[7];
        generate_data(xt, theta);

        for(int j = 0; j < 3; ++j)
            f3 << xt[j] << "\t";
        for(int k = 0; k < 7; ++k)
            f3 << theta[k] << "\t";
        f3 << endl;

    }
    f3.close();
#endif

    //======================================

#ifdef ROBOT_ORIENT

    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border 4095");

    ofstream f1("efposn.txt");
    ofstream f2("rconfig.txt");

    //double theta[NL] = {0, DEG2RAD(50), 0, DEG2RAD(20), 0, 0, 0}; // in degrees
    double theta[NL] = {0, DEG2RAD(50), DEG2RAD(-20), DEG2RAD(20), 0, DEG2RAD(30), 0}; // in degrees
    double x[6] = {0,0,0,0,0,0}; //end-effector position

    Mat xr(6,3,CV_64F,0.0);
    Mat R(3,3,CV_64F, 0.0); // Rotation matrix


    Mat endC(3,3,CV_64F,0.0); // coordinate axes at end-effector
    Mat endC2(3,3,CV_64F,0.0); // coordinate axes at end-effector
    Mat T(3,3, CV_64F,0.0); // Translation


    // Obtain end-effector position and orientation for a given set of joint angles
    bool VALID;
    coolarm_pose_fk(theta,x, VALID);

    // all angles in radians
    //Roll-Alpha
    //Pitch-Beta
    //Yaw - Gamma
    double rpy[3] = {x[4], x[3], x[5]};

    if(!VALID)
    {
        cout << "VALID = " << VALID << endl;

        cout << "roll-pitch-yaw: " << RAD2DEG(rpy[0]) << "\t" << RAD2DEG(rpy[1]) << "\t" << RAD2DEG(rpy[2]) << endl;
    }


    double xorg[3] = {0,0,0}; //origin of base frame
    double dx[3] = {0.05,0.1,0.1}; // size of base frame

    double xee[3][3]={{x[0],x[0],x[0]},{x[1],x[1],x[1]}, {x[2],x[2],x[2]}}; // origin of end-effector

    double xb[3][3] ={{xorg[0]+dx[0],0,0},{0,xorg[1]+dx[1],0},{0,0,xorg[2]+dx[2]}}; // ??

    float scale = 2.0;
    double xb2[3][3] ={{xorg[0]+scale*dx[0],0,0},{0,xorg[1]+scale*dx[1],0},{0,0,xorg[2]+scale*dx[2]}}; //


    T = Mat(3,3, CV_64F, &xee);

    cv::Mat baseC(3,3,CV_64F, &xb);
    cv::Mat baseC2(3,3,CV_64F, &xb2);
    cv::Mat endCb(3,3,CV_64F,0.0);


    joint_position(theta, xr);

    rotation_matrix(theta, R);

    endC = R * baseC.t() + T; //column vectors
    endCb = R * baseC2.t() + T; //column vectors



    rotated_axis(baseC, rpy, T, endC2); // checking rpy


    //cout << "T = " << T << endl;
    //cout << "Base = " << base_coord << endl;
    //cout << "end_coord = " << end_coord << endl;

    for(int i = 0; i < NC; ++i)
        f1 << x[i] << "\t";
    f1 << endl;

    for(int i = 0; i < 6; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f2 << xr.at<double>(i,j) << "\t";
        f2 << endl;
    }
    f2 << endl << endl;

    f1.close();
    f2.close();



    G1.gnuplot_cmd("set yrange [-0.5:0.5]");

    G1.gnuplot_cmd("splot 'rconfig.txt' u 1:2:3 w lp pt 3 lt 5 lw 2");
    G1.gnuplot_cmd("replot 'efposn.txt' u 1:2:3 w p pt 4 lt 3");


    G1.draw3dcoordAxis(xorg,dx,true);

    double xpos[3] = {x[0],x[1],x[2]};
    double xp[3][3], xp2[3][3];

    for(int i = 0; i < 3; ++i)
        for(int j = 0; j < 3; ++j)
        {
            xp[i][j] = endCb.at<double>(j,i); // transpose
            xp2[i][j] = endC2.at<double>(j,i); // transpose
        }

    double xp2b[3][3] = {{xpos[0]+dx[0], xpos[1], xpos[2]},
                         {xpos[0], xpos[1]+dx[1], xpos[2]},
                         {xpos[0], xpos[1], xpos[2]+dx[2]}};

    G1.draw3dcoordAxis(xpos, xp[0], xp[1], xp[2],true,1);
    G1.draw3dcoordAxis(xpos, xp2[0],xp2[1],xp2[2],true,2);
    //G1.draw3dcoordAxis(xpos,xp2b[0], xp2b[1], xp2b[2],true,1);


    cout << "Press enter key to exit ..." << endl;
    getchar();

#endif

    //=====================================================

    return 0;

}
