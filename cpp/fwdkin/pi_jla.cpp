/* Pseudo-inverse based Inverse Kinematic Solution
 * Use Null-space joint velocity for JOINT Limit Avoidance (JLA)
 *
 * \dot{q} = pinv(J) * (\dot{x}+ Kp * error) + (I-pinv(J)*J)\dot{q}_0
 *
 * \dot{q}_0 = alpha * dW/dq
 * where W(q) avoid joint limits
 *
 * Date: April 03, 2015
 * Last Update: April 09, 2015
 * -----------------------------------------
*/

#include<iostream>
#include<coolarm_fk.h>
#include<fstream>
#include<gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;

const int NC = 3;

//-----------------
// choose one of the following two options

//#define POSE
#define EE_POSN

//------------------
// Choose one of the following two options

#define TARGET_POINT
//#define TARGET_TRAJ

int main()
{

#ifdef POSE


    // Initial values
    double pose[6] = {0.0,0,0,0,0};
    double theta[NL] = {0,0,0,0,0,0,0};


    bool VALID_FLAG;

    cv::Mat Pose_T(6,1, CV_64F, 0.0);
    cv::Mat Pose_T_dot(6,1, CV_64F, 0.0); //time-derivative

    cv::Mat Pose_C(6,1, CV_64F, pose);
    cv::Mat Jp(3,7,CV_64F, 0.0);
    cv::Mat Jw(3,7,CV_64F, 0.0);
    cv::Mat J(6,7,CV_64F, 0.0);
    cv::Mat Theta_dot(7,1,CV_64F, 0.0);
    cv::Mat Error(6,1,CV_64F, 0.0);
    cv::Mat Jpinv(7,6, CV_64F, 0.0);
    cv::Mat Jpos(6,3,CV_64F,0.0);
    cv::Mat singval(6,1,CV_64F,0.0); // singular values
    cv::Mat q0_dot(NL,1,CV_64F,0.0); // Self-motion velocity

    // Gains
    cv::Mat Kp(6,6,CV_64F,0.0);

    Kp = 1.5 * cv::Mat::eye(Kp.rows, Kp.cols, CV_64F);



    // Control Loop

    ofstream f1("actual.txt");
    ofstream f2("rconfig.txt");
    ofstream f3("rank.txt");
    ofstream f4("target.txt");
    ofstream f5("error.txt");
    ofstream f6("jointvel.txt");
    int Tmax = 100;
    double dt = 0.01;
    double w = 1.0; //2*M_PI/Tmax;
    for(double t = 0; t < Tmax; t = t + dt)
    {
        //Target Pose (trajectory)
        Pose_T.at<double>(0,0) = 0.1;//0.15;
        Pose_T.at<double>(1,0) = 0.1;//0.2*sin(w*t);
        Pose_T.at<double>(2,0) = 0.3;//0.3+0.2*cos(w*t);
        Pose_T.at<double>(3,0) = 0.1;
        Pose_T.at<double>(4,0) = 0.1;
        Pose_T.at<double>(5,0) = 0.1;

        // Derivative of Target pose

        Pose_T_dot.at<double>(0,0) = 0.0;
        Pose_T_dot.at<double>(1,0) = 0.0;//0.2*w*cos(w*t);
        Pose_T_dot.at<double>(2,0) = 0.0;//-0.2*w*sin(w*t);
        Pose_T_dot.at<double>(3,0) = 0.0;
        Pose_T_dot.at<double>(4,0) = 0.0;
        Pose_T_dot.at<double>(5,0) = 0.0;


        for(int i = 0; i < 6; i++)
        {
            //cout << Pose_T.at<double>(i,0) << "\t";
            f4 << Pose_T.at<double>(i,0) << "\t";
        }
        f4 << endl;


        Error = Pose_T - Pose_C;

        position_jacobian(theta, Jp);
        //orientation_jacobian(theta, Jw);
        angular_jacobian(theta, Jw);  // Does not work ....



        cv::vconcat(Jp, Jw, J);

        cv::SVD::compute(J, singval, cv::SVD::NO_UV);

        double rank = 0;
        for(int i = 0; i < 7; ++i)
        {
            if(singval.at<double>(i,1) > 0.0001)
                rank = rank + 1;
        }

        //cout << "rank = " << rank << endl;

        f3 << rank << endl;


        cv::invert(J, Jpinv, cv::DECOMP_SVD);


        //Control Input: Joint Angle velocities
//        Theta_dot = Jpinv * (Pose_T_dot + Kp*Error);

        // null space optimization for obtained a desired joint configuration
        for(int i = 0; i < NL; ++i)
        {
            q0_dot.at<double>(i,0) = 2*(theta[i]) / NL * pow((theta_max[i] - theta_min[i]),2.0);
        }


        // Joint angle velocity using Self-motion component
        Theta_dot = Jpinv * (Pose_T_dot + Kp*Error) - 5*(cv::Mat::eye(NL,NL,CV_64F)-Jpinv*J) * q0_dot;

        for(int i = 0; i < NL; ++i)
        {
            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);

            if(theta[i] > theta_max[i]) theta[i] = theta_max[i];
            else if(theta[i] < theta_min[i]) theta[i] = theta_min[i];
        }



        coolarm_pose_fk(theta, pose, VALID_FLAG);
        joint_position(theta, Jpos);

        for(int i = 0; i < 6; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        if(VALID_FLAG)
        {
            for(int i = 0; i < 6; ++i)
            {
                Pose_C.at<double>(i) = pose[i];
                f1 << Pose_C.at<double>(i) << "\t";
            }
            f1 << endl;
        }

         f5 << t << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0)<< endl;
    } // time-loop
    f1.close();
    f2.close();
    f3.close();
    f4.close();
    f5.close();

    // ---------------------
    // Plotting
    // -----------------


    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal qt");
    G1.gnuplot_cmd("set border");

    G1.gnuplot_cmd("splot 'actual.txt' u 1:2:3 w p t 'actual', 'target.txt' u 1:2:3 w d  t 'target'");
    GP_handle G2("/usr/bin/", "Time (s)", "Error");
    G2.gnuplot_cmd("set terminal wxt");
    G2.gnuplot_cmd("set border");
    G2.gnuplot_cmd("plot 'error.txt' u 1:2 w l");
    cout << "Press Enter to exit .." << endl;

    GP_handle G3("/usr/bin/", "Rank", "Time (s)");
    G3.gnuplot_cmd("set terminal wxt");
    G3.gnuplot_cmd("set border");
    G3.gnuplot_cmd("plot 'rank.txt' w l");

    getchar();
    G1.gnuplot_cmd("set terminal pslatex color solid lw 2");
    G1.gnuplot_cmd("set output 'traj_error.tex'");
    G1.gnuplot_cmd("replot");
    G1.gnuplot_cmd("set output");

#endif

    //------------------------
    // Target End-effector position is to be reached with the constraint that the robot
    // configuration should be close to the theta_0.
    // This is achieved using null space optimization
    //-----------------------
#ifdef EE_POSN


    double dt = 0.01;

    double theta[NL] = {0,0,0,0,0,0,0};

//    double theta_0[NL] = {DEG2RAD(40), DEG2RAD(80), DEG2RAD(-20), DEG2RAD(65), DEG2RAD(-90), DEG2RAD(-30), 0};
    //double theta_0[NL] = {0.0168, 1.1520, 0.3589, 0.6642, -1.6045, 0.2776, 0.0};
     //double theta_0[NL] = {0.0, 0, 0, 0, 0, 0, 0.0};
    double theta_0[NL] = {0.0168, 1.15, 0.358, 0.664, -1.6, 0.277, 0};
    double jtvel_max = DEG2RAD(30);
    double jtvel_min = -DEG2RAD(30);
    double posn[NC];


    // INitial robot position

    for(int i = 0; i < NL; ++i)
        theta[i] = 0.0;


    coolarm_7dof_FK(theta, posn);

    cout << "Initial End-effector Position = ";
    for(int i = 0; i < NC; ++i)
        cout << posn[i] << "\t";
    cout << endl;
    cout << "Press Enter to Continue ..." << endl;
    getchar();

    cv::Mat Posn_T(NC,1, CV_64F, 0.0);
    cv::Mat Posn_T_dot(NC,1, CV_64F, 0.0); //time-derivative
    cv::Mat Posn_C(NC,1, CV_64F, posn);
    cv::Mat Jp(3,NL,CV_64F, 0.0);
    cv::Mat Theta_dot(NL,1,CV_64F, 0.0);
    cv::Mat Error(NC,1,CV_64F, 0.0);
    cv::Mat Jpinv(NL,NC, CV_64F, 0.0);
    cv::Mat Jpos(6,3,CV_64F,0.0);
    cv::Mat singval(3,1,CV_64F,0.0); // singular values
    cv::Mat q0_dot(NL,1,CV_64F,0.0); // Self-motion velocity

    int N = 1000;
    int idx=0;
    cv::Mat theta_dot_all(N,NL,CV_64F,0.0);
    cv::Mat theta_all(N,NL,CV_64F,0.0);
    // Gains
    cv::Mat Kp(NC,NC,CV_64F,0.0);
    Kp = 5 * cv::Mat::eye(Kp.rows, Kp.cols, CV_64F);

    // Control Loop

    ofstream f1("actual.txt");
    ofstream f2("rconfig.txt");
    ofstream f3("rank.txt");
    ofstream f4("target.txt");
    ofstream f5("error.txt");
    ofstream f6("thnorm2.txt");
    ofstream f7("jointvel.txt");

    int Tmax = 20;
    double w = 1.0; //2*M_PI/Tmax;
    for(double t = 0; t < Tmax; t = t + dt)
    {

        //Target Pose (trajectory)
#ifdef TARGET_TRAJ
        Posn_T.at<double>(0,0) = 0.15;
        Posn_T.at<double>(1,0) = 0.2*sin(w*t);
        Posn_T.at<double>(2,0) = 0.3+0.2*cos(w*t);

        // Derivative of Target pose

        Posn_T_dot.at<double>(0,0) = 0.0;
        Posn_T_dot.at<double>(1,0) = 0.2*w*cos(w*t);
        Posn_T_dot.at<double>(2,0) = -0.2*w*sin(w*t);
#endif

#ifdef TARGET_POINT

        Posn_T.at<double>(0,0) = 0.19;
        Posn_T.at<double>(1,0) = -0.30;
        Posn_T.at<double>(2,0) = 0.07;

        // Derivative of Target pose

        Posn_T_dot.at<double>(0,0) = 0.0;
        Posn_T_dot.at<double>(1,0) = 0.0;
        Posn_T_dot.at<double>(2,0) = 0.0;

#endif

        for(int i = 0; i < NC; i++)
        {
            //cout << Posn_C.at<double>(i,0) << "\t";
            f4 << Posn_T.at<double>(i,0) << "\t";
        }
        f4 << endl;


        Error = Posn_T - Posn_C;

        position_jacobian(theta, Jp);

        cv::SVD::compute(Jp, singval, cv::SVD::NO_UV);

        double rank = 0;
        for(int i = 0; i < NC; ++i)
        {
            if(singval.at<double>(i,1) > 0.0001)
                rank = rank + 1;
        }
        f3 << rank << endl;

        // null space optimization for obtained a desired joint configuration
        for(int i = 0; i < NL; ++i)
        {
            q0_dot.at<double>(i,0) = 2*(theta[i] -theta_0[i]) / NL * pow((theta_max[i] - theta_min[i]),2.0);
        }


        cv::invert(Jp, Jpinv, cv::DECOMP_SVD);


        // Joint angle velocity using Self-motion component
        Theta_dot = Jpinv * (Posn_T_dot + Kp*Error) - 20*(cv::Mat::eye(NL,NL,CV_64F)-Jpinv*Jp) * q0_dot;


        double th_norm = 0.0;
        for(int i = 0; i < NL; ++i)
        {

//            if(Theta_dot.at<double>(i) > jtvel_max)
//                Theta_dot.at<double>(i) = jtvel_max;
//            else if(Theta_dot.at<double>(i) < jtvel_min)
//                    Theta_dot.at<double>(i) = jtvel_min;

            //if(Theta_dot.at<double>(i) > DEG2RAD(30))
              //  Theta_dot.at<double>(i) = DEG2RAD(30);
            //else if(Theta_dot.at<double>(i) < DEG2RAD(-30))
              //  Theta_dot.at<double>(i) = DEG2RAD(-30);


            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);
            th_norm += theta[i]*theta[i];

            theta_all.at<double>(idx,i) = theta[i];
            theta_dot_all.at<double>(idx,i) = Theta_dot.at<double>(i);

            f7<<Theta_dot.at<double>(i)<<"\t";
        }
        f7 << endl;
        th_norm = sqrt(th_norm);

        f6 << th_norm << endl;
        f7 << endl;

        coolarm_7dof_FK(theta, posn);
        joint_position(theta, Jpos);

        for(int i = 0; i < 6; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        for(int i = 0; i < NC; ++i)
        {
            Posn_C.at<double>(i) = posn[i];
            f1 << Posn_C.at<double>(i) << "\t";
        }
        f1 << endl;

        f5 << t << "\t" << sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/3)<< endl;
    } // time-loop

    cout << "Final Joint Angles in Degrees = " << endl;
    for(int i = 0; i < NL; ++i)
        cout << RAD2DEG(theta[i]) << "\t";
    cout << endl;

    f1.close();
    f2.close();
    f3.close();
    f4.close();
    f5.close();
    f6.close();
    f7.close();

    // ---------------------
    // Plotting
    // -----------------


    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal qt");
    G1.gnuplot_cmd("set border");
    G1.gnuplot_cmd("set ticslevel 0");
    G1.gnuplot_cmd("splot 'actual.txt' u 1:2:3 w p t 'actual', 'target.txt' u 1:2:3 w p ps 3 t 'target'");

    GP_handle G2("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G2.gnuplot_cmd("set terminal qt");
    G2.gnuplot_cmd("set border 4095");
    G2.gnuplot_cmd("set ticslevel 0");
    G2.gnuplot_cmd("splot 'rconfig.txt' u 1:2:3 w lp lw 2 t 'Robot Configuration', 'actual.txt' u 1:2:3 w lp lt 3 t 'End-effector trajectory'");

    GP_handle G3("/usr/bin/", "Time", "Error");
    G3.gnuplot_cmd("set terminal wxt");
    G3.gnuplot_cmd("plot 'error.txt' w l lw 2");

    cout << "Press Enter to exit .." << endl;


    getchar();
    //------------------------------------
    // generate plots
    G1.gnuplot_cmd("set terminal pslatex color solid lw 2");
    G1.gnuplot_cmd("set output 'traj_error.tex'");
    G1.gnuplot_cmd("replot");
    G1.gnuplot_cmd("set output");

    G2.gnuplot_cmd("set terminal pslatex color solid lw 2");
    G2.gnuplot_cmd("set output 'rconfig2.tex'");
    G2.gnuplot_cmd("replot");
    G2.gnuplot_cmd("set output");

#endif


    return 0;
}
