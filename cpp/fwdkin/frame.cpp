// Understanding Orientation

#include<iostream>
#include<gnuplot_ci.h>
#include<cmath>
#include<opencv2/opencv.hpp>

#define DEG2RAD(x) M_PI*x/180

using namespace std;
using namespace gnuplot_ci;

#define ROTATION

int main()
{

#ifdef ROTATION
    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border");

    double x[3] = {0,0,0};


    double xp[3][3] = {{1,0,0}, {0,1,0}, {0,0,1}};

    // points are in rows
    G1.draw3dcoordAxis(x, xp[0], xp[1], xp[2]);

    cv::Mat Rx(3,3,CV_64F,0.0);
    cv::Mat Ry(3,3,CV_64F,0.0);
    cv::Mat Rz(3,3,CV_64F,0.0);

    double alpha = 30, beta = 0, gamma = 0; // in degrees


    Rz.at<double>(0,0) = cos(DEG2RAD(alpha));
    Rz.at<double>(0,1) = -sin(DEG2RAD(alpha));
    Rz.at<double>(1,0) = sin(DEG2RAD(alpha));
    Rz.at<double>(1,1) = cos(DEG2RAD(alpha));
    Rz.at<double>(2,2) = 1.0;

    Ry.at<double>(0,0) = cos(DEG2RAD(beta));
    Ry.at<double>(0,2) = sin(DEG2RAD(beta));
    Ry.at<double>(2,0) = -sin(DEG2RAD(beta));
    Ry.at<double>(2,2) = cos(DEG2RAD(beta));
    Ry.at<double>(1,1) = 1.0;


    Rx.at<double>(0,0) = 1.0;
    Rx.at<double>(1,1) = cos(DEG2RAD(gamma));
    Rx.at<double>(1,2) = -sin(DEG2RAD(gamma));
    Rx.at<double>(2,1) = sin(DEG2RAD(gamma));
    Rx.at<double>(2,2) = cos(DEG2RAD(gamma));

    cv::Mat xp1(3,3,CV_64F, &xp);

    cv::Mat xp2(3,3,CV_64F,0.0);

    // the points are in columns in xp2
    xp2 = Rz * Ry * Rx * xp1.t();

    double xp2_array[3][3];

    // Note that the (i,j)the elements are interchanged to
    // put it in row format (its like taking transpose)

    for(int i = 0; i < 3; ++i)
        for(int j = 0; j < 3; ++j)
            xp2_array[i][j] = xp2.at<double>(j,i);

    cout << "Rz = " << Rz << endl;
    cout << "xp1 = " << xp1 << endl;
    cout << "xp1.t = " << xp1.t() << endl;
    cout << "xp2 = " << xp2 << endl;

    // xp2 is converted into row format before passing to this function
    G1.draw3dcoordAxis(x, xp2_array[0], xp2_array[1], xp2_array[2],true, 2);



    getchar();

#endif

    return 0;
}
