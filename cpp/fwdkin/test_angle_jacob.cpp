/* Testing the Angular Jacobian
 * Date: April 16, 2015
 * Date: April 30, 2015: Still no improvement.
 *
 * ------------------------- */
#include<iostream>
#include<coolarm_fk.h>
#include<fstream>
#include<gnuplot_ci.h>

using namespace std;
using namespace gnuplot_ci;

int main()
{

    // Initial values
    double pose[6] = {0.0,0,0,0,0};
    double pose_t[6] = {0,0,0,0,0,0};
    double theta[NL] = {0,DEG2RAD(90),DEG2RAD(0),DEG2RAD(0),DEG2RAD(0),DEG2RAD(0),DEG2RAD(90)};

    cv::Mat Pose_T(6,1, CV_64F, pose_t);
    cv::Mat Pose_T_dot(6,1,CV_64F, 0.0);
    cv::Mat Pose_C(6,1, CV_64F, pose);
    cv::Mat Jp(3,7,CV_64F, 0.0);
    cv::Mat Jw(3,7,CV_64F, 0.0);
    cv::Mat J(6,7,CV_64F, 0.0);
    cv::Mat Theta_dot(7,1,CV_64F, 0.0);
    cv::Mat Jinv(7,6, CV_64F, 0.0);
    cv::Mat Jpos(6,3,CV_64F,0.0);
    cv::Mat singval(6,1,CV_64F,0.0); // singular values

    // Initial Robot Pose
    bool VALID_FLAG;
    coolarm_pose_fk(theta, pose_t, VALID_FLAG);

    joint_position(theta, Jpos);


    ofstream f6("init_config.txt");
    for(int i = 0; i < 6; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f6 << Jpos.at<double>(i,j) << "\t";
        f6 << endl;
    }
    f6 << endl << endl;
    f6.close();



    cv::Mat R(3,3,CV_64F,0.0);
    rotation_matrix(theta, R);

    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border");
    G1.gnuplot_cmd("set yrange [-0.5:0.5]");
    G1.gnuplot_cmd("set xrange [-0.5:0.5]");
    G1.gnuplot_cmd("splot 'init_config.txt' index 0 u 1:2:3 w lp pt 4 lt -1 lw 2 t 'Initial'");


    double xorg[3] = {0,0,0};
    double xb[3][3] = {{0.1,0,0}, {0,0.1,0}, {0,0,0.1}};

    G1.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);

    double xend[3][3] = {{pose_t[0], pose_t[1], pose_t[2]},
                             {pose_t[0], pose_t[1], pose_t[2]},
                             {pose_t[0], pose_t[1], pose_t[2]}};

    double xeb[3][3];
    cv::Mat endC(3,3,CV_64F, 0.0);
    cv::Mat baseC(3,3,CV_64F, &xb);
    cv::Mat T(3,3,CV_64F, &xend);

    endC = R * baseC.t() + T.t(); // check the transpose

    // make it a row matrix
    for(int i = 0; i < 3; ++i)
        for(int j = 0; j < 3; ++j)
            xeb[i][j] = endC.at<double>(j,i);

    G1.draw3dcoordAxis(xend[0], xeb[0], xeb[1], xeb[2], true, 1);


    ofstream f1("actual.txt");
    ofstream f2("rconfig.txt");
    ofstream f3("rank.txt");
    //ofstream f4("target.txt");
    //ofstream f5("error.txt");

    // Control Loop

    int Tmax = 3.0;
    double dt = 0.01;
    for(double t = 0; t < Tmax; t = t + dt)
    {

        Pose_T_dot.at<double>(0,0) = 0.0;
        Pose_T_dot.at<double>(1,0) = 0.0;
        Pose_T_dot.at<double>(2,0) = 0.0;
        Pose_T_dot.at<double>(3,0) = 0.0;
        Pose_T_dot.at<double>(4,0) = 0.2;
        Pose_T_dot.at<double>(5,0) = 0.0;


        position_jacobian(theta, Jp);
        //orientation_jacobian(theta, Jw);
        angular_jacobian2(theta, Jw);

        cv::vconcat(Jp, Jw, J);

        cv::SVD::compute(J, singval, cv::SVD::NO_UV);

        double rank = 0;
        for(int i = 0; i < 7; ++i)
        {
            if(singval.at<double>(i,1) > 0.0001)
                rank = rank + 1;
        }

        f3 << rank << endl;


        cv::invert(J, Jinv, cv::DECOMP_SVD);

        //Control Input: Joint Angle Velocities
        Theta_dot = Jinv * Pose_T_dot;

        for(int i = 0; i < NL; ++i)
            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);

        coolarm_pose_fk(theta, pose, VALID_FLAG);
        joint_position(theta, Jpos);

        for(int i = 0; i < 6; ++i)
        {
            for(int j = 0; j < 3; ++j)
                f2 << Jpos.at<double>(i,j) << "\t";
            f2 << endl;
        }
        f2 << endl << endl;

        if(VALID_FLAG)
        {
            for(int i = 0; i < 6; ++i)
            {
                Pose_C.at<double>(i) = pose[i];
                f1 << Pose_C.at<double>(i) << "\t";
            }
            f1 << endl;
        }
    } // time-loop

    f1.close();
    f2.close();
    f3.close();
   // f4.close();
    //f5.close();

    //===============================
    // GNU PLOT

    coolarm_pose_fk(theta, pose, VALID_FLAG);

    double xend2[3][3] = {{pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]}};

    double xeb2[3][3];
    G1.gnuplot_cmd("replot 'rconfig.txt' index 99 u 1:2:3 w l lt 9 lw 2 t 'Final'");

    cv::Mat endC2(3,3,CV_64F, 0.0);
    cv::Mat R2(3,3,CV_64F,0.0);
    cv::Mat T2(3,3,CV_64F, &xend2);

    rotation_matrix(theta, R2);

    endC2 = R2 * baseC.t() + T2.t();

    for(int i = 0; i < 3; ++i)
        for(int j = 0; j < 3; ++j)
            xeb2[i][j] = endC2.at<double>(j,i);

    G1.draw3dcoordAxis(xend2[0], xeb2[0], xeb2[1], xeb2[2], true, 2);

    getchar();




    return 0;
}
