// Computing Inverse Kinematic Solution for End-Effector Position Using Inverse-Forward Scheme
// Date: October 29, 2014
// ------------------------------------------------------------------------------
#include <iostream>
#include <opencv2/opencv.hpp>
#include <coolarm_fk.h>
#include <nnet.h>
#include <ksom_vmc.h>
#include <mymatrix.h>
#include <gnuplot_ci.h>


const int NC = 3;   // Dimension for Position
const int NO = 3;   // Dimension for Orientation

using namespace std;
using namespace cv;
using namespace nnet;
using namespace vmc;
using namespace gnuplot_ci;


// Step - 1: Create clusters in Input-Output Space
//#define CLUSTER

// Step - 2: Train a RBF network to learn the forward kinematics map, f : th --> [x, phi]
//#define RBFN1
#define RBFN2
//#define RBFN3
//#define RBFN4
//#define TRAIN_RBF

// Not a step ... Just check if the training is proper
//#define RBF_TEST_RP

// Step - 3: Invert the Network to find IK solution
//#define INVERT_RBF

// Test IK for multiple random points
#define INVERT_RP


// IF you want to see the plots as well ...
// Makesure GNUPlot is installed
//#define PLOT



int main()
{
    //Create clusters in input (End-effector position) and output (joint angles) space
    const int nx = 7;
    const int ny = 7;
    const int nz = 7;
    //----------------------------------------
    //Create a KSOM Network
    int N[5] = {nx, ny, nz, NW, NL};

    int node_cnt = nx * ny * nz;

    int ncmax = 150;

    //Create a SOM lattice
    ksom3_sc A(N, ncmax);

    A.netinfo();
    //----------------------


#ifdef CLUSTER
    // create clusters in input and output space

  int NMAX = 500000;
  double eta1_init = 0.9, eta1_fin = 0.1;
  double sig_init = 0.9, sig_fin = 0.1;

  ofstream fin1("position.txt"); // normalized position
  ofstream fin2("angle.txt"); // normalized angle
  ofstream fin3("dposition.txt"); // actual data
  ofstream fin4("dangle.txt"); // actual angle
  ofstream fin5("subclust.txt"); // Number of subclusters

  cout << "Clustering starts here ... wait!" << endl;

  double eta[3];

  int interval = 100000;
  //Loop starts here ...
  for(int n = 1; n <= NMAX; n++)
  {
      if (n % interval == 0)
          cout << "\n Iteration Step = " << n << "/" << NMAX << endl;

    double xt[NW], theta[NL];
    double xtn[NW], thetan[NL];

    //generate a random target
    if (NW == 3)
        generate_data(xt, theta);
    else if(NW == 6)
        generate_pose_data(xt, theta);


    cart_posn_normalize(xt, xtn);
    angle_normalize(theta, thetan);

    for(int i = 0; i < NW; i++)
    {
      fin1 << xtn[i] << "\t";
      fin3 << xt[i] << "\t";
    }
    for(int i = 0; i < NL; i++)
    {
      fin2 << thetan[i] << "\t";
      fin4 << theta[i] << "\t";
    }
    fin1 << endl;
    fin2 << endl;
    fin3 << endl;
    fin4 << endl;


    //Learning rate
    eta[0] = eta1_init * pow( (eta1_fin / eta1_init),
        (n / (double)NMAX) );

    eta[1] = 0.7;

    // Sigma necessary for weight update
    eta[2] = sig_init * pow( (sig_fin / sig_init),
        (n / (double)NMAX) );

    A.create_clusters(eta, xtn, thetan, 1.0); // threshold for sub-clusters could be changed

  }//n-loop

  //save KSOM network parameters
  A.save_weights("ksom_sc_weights.txt");
  A.save_angles("ksom_sc_angles.txt");

  int *sc_cnt = new int [node_cnt];
  int ret_cnt = A.get_subcluster_cnt(sc_cnt);

  // Save the number of sub-clusters
  for(int i = 0; i < ret_cnt; ++i)
      fin5 << i << "\t" << sc_cnt[i] << endl;


  fin1.close();
  fin2.close();
  fin3.close();
  fin4.close();
  fin5.close();

  delete [] sc_cnt;

  cout << "Clustering ends ... parameters saved!" << endl;
  //-------------------------------------
#ifdef PLOT
  cout << "GNUPLot ....." << endl;

  GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
  GP_handle G2("/usr/bin/", "th1 (rad)", "th2 (rad)", "th3 (rad)");
  GP_handle G3("/usr/bin/", "th4 (rad)", "th5 (rad)", "th6 (rad)");
  GP_handle G5("/usr/bin/","Node Index", "No. of Sub-clusters formed");
  G1.gnuplot_cmd("set view 49,325");
  G1.gnuplot_cmd("splot 'position.txt' u 1:2:3 w d t 'Robot workspace'");
  G1.gnuplot_cmd("replot 'ksom_sc_weights.txt' u 1:2:3 w p t 'clusters'");
  G2.gnuplot_cmd("set view 19,46");
  G2.gnuplot_cmd("splot 'angle.txt' u 1:2:3 w d t 'First 3 joint angles'");
  G2.gnuplot_cmd("replot 'ksom_sc_angles.txt' u 1:2:3 w p t 'clusters'");
  G3.gnuplot_cmd("set view 58,28");
  G3.gnuplot_cmd("splot 'angle.txt' u 4:5:6 w d t 'Last 3 joint angles'");
  G3.gnuplot_cmd("replot 'ksom_sc_angles.txt' u 4:5:6 w p t 'clusters'");
  G5.gnuplot_cmd("plot 'subclust.txt' u 1:2 w p");

  GP_handle G4("/usr/bin/", "Pitch (rad)", "Yaw (rad)", "Roll (rad)");
  if(NW ==6)
  {
      G4.gnuplot_cmd("splot 'position.txt' u 4:5:6 w d t 'Orienation'");
      G4.gnuplot_cmd("replot 'ksom_sc_weights.txt' u 4:5:6 w p t 'clusters'");
  }

  cout << "Press enter to exit ...." << endl;
  getchar();

  //----------
  // save the plot files
  G1.gnuplot_cmd("set terminal postscript eps enhanced color");
  G1.gnuplot_cmd("set output 'workspace.eps'");
  G1.gnuplot_cmd("replot");
  G1.gnuplot_cmd("set output");
  G2.gnuplot_cmd("set terminal postscript eps enhanced color");
  G2.gnuplot_cmd("set output 'angle1.eps'");
  G2.gnuplot_cmd("replot");
  G2.gnuplot_cmd("set output");
  G3.gnuplot_cmd("set terminal postscript eps enhanced color");
  G3.gnuplot_cmd("set output 'angle2.eps'");
  G3.gnuplot_cmd("replot");
  G3.gnuplot_cmd("set output");

  if(NW == 6)
  {
      G4.gnuplot_cmd("set terminal postscript eps enhanced color");
      G4.gnuplot_cmd("set output 'orientation.eps'");
      G4.gnuplot_cmd("replot");
      G4.gnuplot_cmd("set output");
  }



#endif
  //------------------------------------------------

#endif


  //=====================================
  // Learn Forward Kinematics using RBFN

#ifdef RBFN1
  //create a RBF network

  int noc = 1200;

  int N2[3] = {NL, noc, NW};
  double af[2] = {5, 0.5}; //gaussian activation
  double param[3] = {1, -1, 0}; //input range = -1 to 1

  rbfn B(N2, af, param);

  cout << "Network Information" << B << endl;

#elif defined RBFN2

  // number of centres in both the networks
  int noc1 = 700;
  int noc2 = 1000;

  int N2[3] = {NL, noc1, NC}; // input - angle, output - position
  int N3[3] = {NL+NC, noc2, NO}; // input - position+angle, output - orientation
  double af1[2] = {5, 1.0}; // gaussian activation for hidden neurons and linear for output layer neurons
  double af2[2] = {5, 0.7};
  double param[3] = {1, -1, 0}; //input range = -1 to 1

  // one network to learn angle --> position map
  rbfn B(N2, af1, param);

  // one network to learn angle+position --> orientation map
  rbfn B2(N3, af2, param);

  cout << "Network Information " << B << endl;
  cout << "Network Information " << B2 << endl;

#elif defined RBFN3

  // Learn a map between joint angles ---> X (3 dimensional end-effector position)
  // input: theta - NL x 1, Output - NC x 1 (NC=3)

  int noc = 700; // number of hidden neurons

  int N2[3] = {NL, noc, NC};
  double af[2] = {5, 0.5}; //gaussian activation
  double param[3] = {1, -1, 0}; //input range = -1 to 1

  rbfn B(N2, af, param);

  cout << "Network Information" << B << endl;

#elif defined RBFN4

  // number of centres in both the networks
  int noc1 = 700;
  int noc2 = 700;

  int N2[3] = {NL, noc1, NC}; // input - angle, output - position
  int N3[3] = {NL, noc2, NO}; // input - position+angle, output - orientation
  double af1[2] = {5, 1.0}; // gaussian activation for hidden neurons and linear for output layer neurons
  double af2[2] = {5, 1.0};
  double param[3] = {1, -1, 0}; //input range = -1 to 1

  // one network to learn angle --> position map
  rbfn B(N2, af1, param);

  // one network to learn angle+position --> orientation map
  rbfn B2(N3, af2, param);

  cout << "Network Information " << B << endl;
  cout << "Network Information " << B2 << endl;
#else

  cout << "Select the appropriate RBFN network to proceed. Exiting ..." << endl;
  exit(-1);

#endif

  //======================================

#ifdef TRAIN_RBF

#ifdef RBFN1

  double *cv = myvector(noc*NL);
  double *wt = myvector(NW*noc);

  cout << "\nTraining The RBF Network ...\n";
  cout << "Wait ... Weights and centers are being updated ...\n";

  double q[NL], qn[NL], x[NW], xn[NW];
  double xt[NW], xtn[NW];

  int NMAX = 500000;

  ofstream f1("error1.txt");
  ofstream f2("tdata.txt");
  ofstream f3("evolve.txt");

  int interval = 100000;
  double savg = 0.0;
  for(int n = 0; n < NMAX; n++)
  {
      if (n % interval == 0)
          cout << "\nIteration Step = " << n << "/" << NMAX << endl;

      if(NW == 3)
          generate_data(xt, q);
      else if(NW == 6)
          generate_pose_data(xt, q);
      else
      {
          cerr << "Invalid dimension for input space" << endl;
          exit(-1);
      }

    angle_normalize(q, qn);
    cart_posn_normalize(xt, xtn);

    for(int i = 0; i < NL; i++)
      f2 << qn[i] << "\t";
    for(int i = 0; i < NW; i++)
      f2 << xtn[i] << "\t";
    f2 << endl;

    B.network_output(qn, xn);

    B.backprop(xtn, 0.1, 0.1);

    B.get_parameters(wt, cv);

    f3 << cv[4] << "\t" << cv[10] << "\t" << cv[200] << "\t" << wt[0] << "\t" << wt[100] << "\t" << wt[300] << endl;

    cart_posn_denormalize(xn, x);

    double s1 = 0.0;
    for(int i = 0; i < NW; i++)
      s1 += pow((xt[i] - x[i]), 2.0);
    s1 = sqrt(s1);
    savg += s1;

    f1 << savg/n << endl;
  }

  f1.close();
  f2.close();
  f3.close();

  B.save_parameters("rbfn_par_bp1.txt");

  cout << "\nTraining over ... parameters saved ...\n" << endl;

  delete [] cv;
  delete [] wt;

#ifdef PLOT
  //--------------------------
  cout << "GNUPlot ...." << endl;
  GP_handle G1("/usr/bin/", "Iteration", "Error");
  G1.gnuplot_cmd("plot 'error.txt' u 1 w l lw 2 ");
  cout << "Press Enter to exit ..." << endl;
  getchar();
  G1.gnuplot_cmd("set terminal postscript eps enhanced colour solid lw 2");
  G1.gnuplot_cmd("set output 'training_error.eps");
  G1.gnuplot_cmd("replot");
  G1.gnuplot_cmd("set output");
#endif
  //--------------------------------------
  // TRAIN_RBF: RBFN2
  // If two RBFN networks are used then training will be different
  //---------------------------------------
#elif defined RBFN2


  cout << "\nTraining TWO RBF Networks ...\n";
  cout << "Wait ... Weights and centers are being updated ...\n";

  double q[NL], qn[NL], x[NW], xn[NW];
  double xt[NW], xtn[NW];

  double qpn[10];  // input vector = angle + position
  double pos_tn[NC], phi_tn[3]; // normalized target values
  double pos_n[NC], phi_n[3];  // normalized network output

  int NMAX = 200000;

  // It is important to check that NW is at least 3
  if(NW < NC)
  {
      cerr << "NW must be greater than or equal to 3" << endl;
      exit(-1);
  }

  ofstream f1("error2.txt");

  int interval = 50000;
  double savg = 0.0;
  double savg1 = 0.0, savg2 = 0.0; // error in network 1 and 2.
  for(int n = 0; n < NMAX; n++)
  {
      if (n % interval == 0)
          cout << "\nIteration Step = " << n << "/" << NMAX << endl;

      //Generate a data point
      if(NW == 3)
          generate_data(xt, q);
      else if(NW == 6)
          generate_pose_data(xt, q);
      else
      {
          cerr << "Invalid dimension for input space" << endl;
          exit(-1);
      }

      // normalize the data
      angle_normalize(q, qn);
      cart_posn_normalize(xt, xtn);

      // qpn = angle + position
      for(int i = 0; i < NL; i++)
          qpn[i] = qn[i];

      for(int i = 0; i < NW; i++)
      {
          if(i < NC)
          {
              qpn[i+NL] = xtn[i];
              pos_tn[i] = xtn[i];
          }
          else
              phi_tn[i-NC] = xtn[i];
      }


      //compute network output
      B.network_output(qn, pos_n);
      B2.network_output(qpn, phi_n);

      // Train the network using Back-Propagation algorithm
      B.backprop(pos_tn, 0.1, 0.1);  // train with desired position

      B2.backprop(phi_tn, 0.1, 0.1); // train with desired orientation


      //combine the output of two networks to form a single output: Pose vector
      for(int i = 0; i < NW; ++i)
      {
          if(i < NC)
              xn[i] = pos_n[i];  //position
          else
              xn[i] = phi_n[i-NC];  //orientation
      }

      cart_posn_denormalize(xn, x);


      double s1 = 0.0, s2 = 0.0, s3 = 0.0;
      for(int i = 0; i < NW; i++)
      {
          s1 += pow((xt[i] - x[i]), 2.0);

          if(i < NC)
              s2 += pow((xt[i]-x[i]),2.0); //error in position
          else
              s3 += pow((xt[i]-x[i]),2.0);  // error in orientation
      }
      s1 = sqrt(s1);
      s2 = sqrt(s2);
      s3 = sqrt(s3);
      savg += s1;
      savg1 += s2;
      savg2 += s3;

      f1 << savg/n << "\t" << savg1/n << "\t" << savg2/n << endl;
  }

  f1.close();



  B.save_parameters("rbfn_par_bp.txt");
  B2.save_parameters("rbfn_par_bp2.txt");

  cout << "\nTraining over ... parameters saved ...\n" << endl;

  //--------------------------------------------------------
  // TRAIN_RBF:RBFN3: NL = 7, NW = 6, NC = 3
  // Learns a forward kinematics from joint angle vector to end-effector position
  //--------------------------------------------------------
#elif defined RBFN3

  cout << "\nTraining TWO RBF Networks ...\n";
  cout << "Wait ... Weights and centers are being updated ...\n";

  double q[NL], qn[NL], x[NW], xn[NW];
  double xt[NW], xtn[NW];
  double pos_n[NC];
  double pos_tn[NC];


  int NMAX = 200000;

  ofstream f1("error3.txt");

  int interval = 50000;
  double savg = 0.0;

  for(int n = 0; n < NMAX; n++)
  {
      if (n % interval == 0)
          cout << "\nIteration Step = " << n << "/" << NMAX << endl;

      //Generate a data point
     if(NW == 6)
          generate_pose_data(xt, q);
      else
      {
          cerr << "This module is valid for NW=6. Exiting ..." << endl;
          exit(-1);
      }

      // normalize the data
      angle_normalize(q, qn);
      cart_posn_normalize(xt, xtn);

      //compute network output
      B.network_output(qn, pos_n);

      for(int i = 0; i < NC; ++i) // Note that NC = 3 but NW = 6
          pos_tn[i] = xtn[i];

      // Train the network using Back-Propagation algorithm
      B.backprop(pos_tn, 0.1, 0.1);  // train with desired position


      //combine the output of two networks to form a single output: Pose vector
      for(int i = 0; i < NW; ++i)
      {
          if(i < NC)
              xn[i] = pos_n[i];  //position
          else
              xn[i] = xtn[i]; // orientation (this information is not obtained from RBFN)
      }

      cart_posn_denormalize(xn, x);


      double s1 = 0.0;
      for(int i = 0; i < NC; i++)
          s1 += pow((xt[i] - x[i]), 2.0);
      s1 = sqrt(s1);
      savg += s1;

      f1 << savg/n <<  endl;
  }

  f1.close();

  B.save_parameters("rbfn_par_bp3.txt");

  cout << "\nTraining over ... parameters saved ...\n" << endl;

#elif defined RBFN4

  cout << "\nTraining TWO RBF Networks ...\n";
  cout << "Wait ... Weights and centers are being updated ...\n";

  double q[NL], qn[NL], x[NW], xn[NW];
  double xt[NW], xtn[NW];


  double pos_tn[NC], phi_tn[3]; // normalized target values
  double pos_n[NC], phi_n[3];  // normalized network output

  int NMAX = 200000;

  // It is important to check that NW is at least 3
  if(NW < NC)
  {
      cerr << "NW must be greater than or equal to NC" << endl;
      exit(-1);
  }

  ofstream f1("error2.txt");

  int interval = 100000;
  double savg = 0.0;
  double savg1 = 0.0, savg2 = 0.0; // error in network 1 and 2.
  for(int n = 0; n < NMAX; n++)
  {
      if (n % interval == 0)
          cout << "\nIteration Step = " << n << "/" << NMAX << endl;

      //Generate a data point
      if(NW == 3)
          generate_data(xt, q);
      else if(NW == 6)
          generate_pose_data(xt, q);
      else
      {
          cerr << "Invalid dimension for input space" << endl;
          exit(-1);
      }

      // normalize the data
      angle_normalize(q, qn);
      cart_posn_normalize(xt, xtn);


      for(int i = 0; i < NW; i++)
      {
          if(i < NC)
              pos_tn[i] = xtn[i];
          else
              phi_tn[i-NC] = xtn[i];
      }


      //compute network output
      B.network_output(qn, pos_n);
      B2.network_output(qn, phi_n);

      // Train the network using Back-Propagation algorithm
      B.backprop(pos_tn, 0.1, 0.1);  // train with desired position

      B2.backprop(phi_tn, 0.1, 0.1); // train with desired orientation


      //combine the output of two networks to form a single output: Pose vector
      for(int i = 0; i < NW; ++i)
      {
          if(i < NC)
              xn[i] = pos_n[i];  //position
          else
              xn[i] = phi_n[i-NC];  //orientation
      }

      cart_posn_denormalize(xn, x);


      double s1 = 0.0, s2 = 0.0, s3 = 0.0;
      for(int i = 0; i < NW; i++)
      {
          s1 += pow((xt[i] - x[i]), 2.0);

          if(i < NC)
              s2 += pow((xt[i]-x[i]),2.0); //error in position
          else
              s3 += pow((xt[i]-x[i]),2.0);  // error in orientation
      }
      s1 = sqrt(s1);
      s2 = sqrt(s2);
      s3 = sqrt(s3);
      savg += s1;
      savg1 += s2;
      savg2 += s3;

      f1 << savg/n << "\t" << savg1/n << "\t" << savg2/n << endl;
  }

  f1.close();

  B.save_parameters("rbfn_par_bp4.txt");
  B2.save_parameters("rbfn_par_bp41.txt");

  cout << "\nTraining over ... parameters saved ...\n" << endl;

#else

  cout << "Choose the appropriate RBFN network. Exiting ... " << endl;
  exit(-1);

#endif // RBFN123

#endif // TRAIN_RBF
// RBFN Training ends here
  //====================


#ifdef RBF_TEST_RP

#ifdef RBFN1

  cout << "\n\n RBF Test: Random points ... " << endl;

  B.load_parameters("rbfn_par_bp.txt");

  ofstream ft("test.txt");

  double xt[NW], xtn[NW], q[NL], qn[NL], x[NW], xn[NW];

  int TMAX = 10000;

  double savg2 = 0.0;
  for(int n = 1; n <= TMAX; n++)
  {
      if(NW == 3)
          generate_data(xt, q);
      else if (NW == 6)
          generate_pose_data(xt, q);
      else
      {
          cerr << "Invalid dimension for robot workspace" << endl;
          exit(-1);
      }

    angle_normalize(q, qn);
    cart_posn_normalize(xt, xtn);

    for(int i = 0; i < NL; i++)
      ft << qn[i] << "\t";
    for(int i = 0; i < NW; i++)
      ft << xtn[i] << "\t";
    ft << endl;

    B.network_output(qn, xn);
    cart_posn_denormalize(xn, x);


    double s1 = 0.0;
    for(int i = 0; i < NW; i++)
      s1 += pow((xt[i] - x[i]), 2.0);
    s1 = sqrt(s1);
    savg2 += s1;
  }
  ft.close();

  cout << "Avg error = " << savg2/TMAX << " m" << endl;

#endif
#endif

  //=============================


#ifdef INVERT_RBF

#ifdef RBFN1
    // Load the Network parameters

  if(exist("ksom_sc_weights.txt"))
      A.load_weights("ksom_sc_weights.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  if(exist("ksom_sc_angles.txt"))
      A.load_angles("ksom_sc_angles.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }


  if(exist("rbfn_par_bp1.txt"))
      B.load_parameters("rbfn_par_bp1.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }


  cout << "\n\nNetwork inversion ... " << endl;

  ofstream ft("angle.txt");
  ofstream fr("rconfig.txt");
  ofstream fp1("efposn_network.txt");
  ofstream fp2("efposn_robot.txt");
  ofstream fe1("error.txt");
  ofstream ftp("targetpos.txt");


  //double **jp = mymatrix(5,3);
  const int np = 6; // no. of rows in jp
  Mat jp(6,3,CV_64F,0.0); // stores joint positions


  double qt[NL], qtn[NL];       // corresponding target joint angle
  double xr[NW], xrn[NW];       // actual robot end-effector position
  double xt[NW], xtn[NW];       // Target end-effector position
  double xc[NW], xcn[NW];       // network (RBFN) output

  //--------------------------
  //Generate a Target point
  if(NW == 3)
      generate_data(xt, qt);
  else if (NW == 6)
      generate_pose_data(xt, qt);
  else
  {
      cerr << "Invalid dimension for robot workspace. Exiting ..." << endl;
      cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
      exit(-1);
  }

  //normalize the target position
  cart_posn_normalize(xt, xtn);
  angle_normalize(qt, qtn);

  // Block 1: target position
  for(int i = 0; i < NW; i++)
      ftp << xt[i] << "\t";
  for(int i = 0; i < NL; i++)
      ftp << qt[i] << "\t";
  ftp << endl ;
  //---------------------------------

  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);
  double *qc = myvector(NL);
  double *qcn = myvector(NL);

  //----------------------------
  //initial robot configuration
  th_c[0] = 0.0;
  th_c[1] = DEG2RAD(50);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;
  th_c[6] = 0.0;


  //Initial robot configurationspl
  joint_position(th_c, jp);

  for(int i = 0; i < np; i++)
  {
    for(int j = 0; j < 3; j++)
      fr << jp.at<double>(i,j) << "\t";
    fr << endl;
  }
  fr << endl << endl;

  //-------

  //Current robot end-effector position
  if(NW == 3)
      coolarm_7dof_FK(th_c, xr);
  else if (NW == 6)
      coolarm_pose_fk(th_c, xr);
  else
  {
      cerr << "\n Invalid dimension for robot workspace. Exiting ..." << endl;
      cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
      exit(-1);
  }

  //current end-effector position obtained from network
  angle_normalize(th_c, th_cn);
  B.network_output(th_cn, xcn);
  cart_posn_denormalize(xcn, xc);

  // Block 2: Initial robot position
  for(int i = 0; i < NW; i++)
    fp1 << xc[i] << "\t";
  for(int i = 0; i < NW; i++)
    fp2 << xr[i] << "\t";
  fp1 << endl;
  fp2 << endl;
  //-------------------------------------------


  //-----------------------------------------
  // Make an initial guess for joint angle vector using KSOM network
  //Initial guess (Lazy arm joint angle computation)
  A.LA_theta_output(xtn, th_cn, 0.5,qcn);  // last value is sigma (std. dev)

  angle_denormalize(qcn, qc);


  //Initial position of robot end-effector position
  if(NW == 3)
      coolarm_7dof_FK(qc, xr);
  else if (NW == 6)
      coolarm_pose_fk(qc, xr);
  else
  {
      cerr << "\n Invalid dimension for robot workspace. Exiting ..." << endl;
      cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
      exit(-1);
  }

  //Initial end-effector position obtained from NN
  B.network_output(qcn, xcn);
  cart_posn_denormalize(xcn, xc);
  //------------------------------------------


  // inverse-forward update scheme to finer IK solution

  double se1,se2;
  int step = 0;
  do
  {
      // Block 3: Position obtained from Inv-Fwd Scheme
    for(int i = 0; i < NW; i++)
      fp1 << xc[i] << "\t";
    for(int i = 0; i < NW; i++)
      fp2 << xr[i] << "\t";
    fp1 << endl;
    fp2 << endl;

    joint_position(qc, jp);

    for(int i = 0; i < np; i++)
    {
      for(int j = 0; j < 3; j++)
        fr << jp.at<double>(i,j) << "\t";
      fr << endl;
    }
    fr << endl << endl;

    //----------

    //update input using gradient_descent
    // qcn is the updated input

    B.input_jacobian();
    B.update_input_gd(0.01, xtn, qcn); // works for NW=3
    //B.update_input_gd(0.2, xtn, qcn);

    //new end-effector position
    B.network_output(qcn, xcn);
    cart_posn_denormalize(xcn, xc);

    for(int i = 0; i < NL; i++)
      ft << qcn[i] << "\t";
    ft << endl;

    angle_denormalize(qcn, qc);

    if(NW == 3)
        coolarm_7dof_FK(qc, xr);
    else if (NW == 6)
        coolarm_pose_fk(qc, xr);
    else
    {
        cerr << "\n Invalid dimension for robot workspace. Exiting ..." << endl;
        cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
        exit(-1);
    }

     cart_posn_normalize(xr, xrn);

    //Forward training
    //B.backprop(xrn, 0.05, 0.05); // works for NW=3
    B.backprop(xrn, 0.02, 0.02);


    // Check for only positioning error. The orientation obtain is Good enough
    se1 = 0.0, se2 = 0.0;
    for(int i = 0;i < NW; i++)
    {
        if(i < NC)
            se1 += pow((xr[i] - xt[i]), 2.0);
        else
            se2 += pow((xr[i] - xt[i]), 2.0);
    }

    se1 = sqrt(se1);
    se2 = sqrt(se2);

    fe1 << se1 << "\t" << se2 << endl;

    step = step + 1;

    if(step > 2000)
      break;

  }while(se1 > 0.02);

  ft.close();
  fp1.close();
  fp2.close();
  fr.close();
  fe1.close();
  ftp.close();

  cout << "No. of steps = " << step << endl;
  cout << "Final positioning Error = " << se1 << endl;

  cout << "\nTarget pose =";
  for(int i = 0; i < NW; ++i)
      cout << xt[i] << "\t";
  cout << endl;
  cout << "Actual End-effector pose achieved = ";
  for(int i = 0; i < NW; ++i)
      cout << xr[i] << "\t";
  cout << endl;

  cout << "Final Joint angle values = ";
  for(int i = 0; i < NL; ++i)
      cout << qc[i] << '\t';
  cout << endl;




  //--------------------------------
  // Enable GNUPLOT Interface
#ifdef PLOT
  GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
  GP_handle G2("/usr/bin/", "Iteration Steps", "Error");
  G1.gnuplot_cmd("splot 'targetpos.txt' u 1:2:3 w p pt 6 lt 3 ps 2 t 'Target Position'");
  G2.gnuplot_cmd("plot 'error.txt' w l lw 2 t 'mse'");
  G1.gnuplot_cmd("replot 'rconfig.txt' index 1:400 u 1:2:3 w lp pt 2 lw 2 lt 1 notitle");
  G1.gnuplot_cmd("replot 'efposn_robot.txt' u 1:2:3 w p pt 4 lt 2 notitle");
  cout << "Press Enter to Exit ..." << endl;
  getchar();
  G1.gnuplot_cmd("set terminal postscript eps enhanced color solid lw 2");
  G2.gnuplot_cmd("set terminal postscript eps enhanced color solid lw 2");
  G1.gnuplot_cmd("set output 'rconfig.eps'");
  G2.gnuplot_cmd("set output 'error.eps'");
  G1.gnuplot_cmd("replot");
  G2.gnuplot_cmd("replot");
  G1.gnuplot_cmd("set output");
  G2.gnuplot_cmd("set output");
#endif
  //---------------------------------------------------------------
  //-----------------------------------------------------------
  // INVERT_RBF: RBFN2
  //-----------------------------------------------------------
  //------------------------------------------------------------
#elif defined RBFN2

  // Read the parameter files into the networks
  if(exist("ksom_sc_weights.txt"))
      A.load_weights("ksom_sc_weights.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  if(exist("ksom_sc_angles.txt"))
      A.load_angles("ksom_sc_angles.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }


  if(exist("rbfn_par_bp.txt"))
      B.load_parameters("rbfn_par_bp.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }
  if(exist("rbfn_par_bp2.txt"))
      B2.load_parameters("rbfn_par_bp2.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }
  //----------------------------------------------
  cout << "\nParameters loaded into the networks" << endl;
  cout << "\n\nNetwork inversion ... " << endl;

  ofstream ft("angle.txt");
  ofstream fp1("efposn_network.txt");
  ofstream fp2("efposn_robot.txt");
  ofstream fe1("error.txt");

  double qt[NL], qtn[NL];       // corresponding target joint angle
  double xr[NW], xrn[NW];       // actual robot end-effector position
  double xt[NW], xtn[NW];       // Target end-effector position
  double xc[NW], xcn[NW];       // network (RBFN) output
  double pos_tn[NC], phi_tn[NC]; // normalized TARGET position and orientation
  double pos_n[NC], phi_n[NC];  // normalized NETWORK output - position and orientation
  double pos_rn[NC], phi_rn[NC]; // normalized robot output (orientation)
  double qpn[10];
  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);
  double *qc = myvector(NL);
  double *qcn = myvector(NL);
  //--------------------------
  //Generate a Target point
 if (NW == 6)
      generate_pose_data(xt, qt);
  else
  {
      cerr << "This module is valid for NW=6. Exiting ..." << endl;
      cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
      exit(-1);
  }

  //normalize the target position
  cart_posn_normalize(xt, xtn);
  angle_normalize(qt, qtn);

  //target values
  for(int i = 0; i < NW; ++i)
  {
      if(i < NC)
          pos_tn[i] = xtn[i];
      else
          phi_tn[i-NC] = xtn[i];
  }


  // Block 1: target position
  ofstream ftp("targetpos.txt");
  for(int i = 0; i < NW; i++)
      ftp << xt[i] << "\t";
  for(int i = 0; i < NL; i++)
      ftp << qt[i] << "\t";
  ftp << endl ;
  ftp.close();
  //---------------------------------


  //initial robot configuration
  th_c[0] = 0.0;
  th_c[1] = DEG2RAD(50);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;
  th_c[6] = 0.0;



  //current end-effector position obtained from network
  angle_normalize(th_c, th_cn);

  //-------------------------------------------
  // COARSE Inverse Kinematic Solution obtained from KSOM
  //-----------------------------------------

  // Make an initial guess for joint angle vector using KSOM network
  //Initial guess (Lazy arm joint angle computation)

  A.LA_theta_output(xtn, th_cn, 0.5, qcn);  // last value is sigma (std. dev)

  angle_denormalize(qcn, qc);

  //------------------------------------------

  // FINE Solution obtained using inverse-forward update scheme on RBFN

  //-----------------------------------------------------------

  //Initial end-effector position obtained from NN
  B.network_output(qcn, pos_n);

  double se1, se2, se3;
  int step = 0;
  do
  {
      // update input angle (qcn) only for position target
      B.input_jacobian();
      B.update_input_gd(0.02, pos_tn, qcn);

      // The updated qcn is now updated by RBFN2 to minimize orientation error
      for(int i = 0; i < NL+NC; ++i)
      {
          if(i < NL)
              qpn[i] = qcn[i];
          else
              qpn[i] = xtn[i-NL];
      }

      // New orientation obtained with new qpn
      B2.network_output(qpn, phi_n);

      // update input (qpn) so as to reduce orientation error
      B2.input_jacobian();
      B2.update_input_gd2(0.04, phi_tn, qpn, NL);

      // Updated joint angles
      for(int i = 0; i < NL; ++i)
          qcn[i] = qpn[i];

      // New end-effector position obtained after orientation update
      B.network_output(qcn, pos_n);
      B2.network_output(qpn, phi_n);


      angle_denormalize(qcn, qc);

      // Actual Robot Position attained with these angles
     if (NW == 6)
          coolarm_pose_fk(qc, xr);
      else
      {
          cerr << "\n This module is valid for NW=6. Exiting ..." << endl;
          cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
          exit(-1);
      }

      cart_posn_normalize(xr, xrn);

      for(int i = 0; i < NW; ++i)
      {
          if(i < NC)
              pos_rn[i] = xrn[i];
          else
              phi_rn[i-NC] = xrn[i];
      }


      //Forward training
      B.backprop(pos_rn, 0.02, 0.02);
      B2.backprop(phi_rn, 0.04, 0.04);


      // Check for only positioning error. The orientation obtain is Good enough
      se1 = 0.0, se2 = 0.0, se3 = 0.0;
      for(int i = 0;i < NW; i++)
      {
          se1 += pow((xr[i] - xt[i]), 2.0);
          if(i < NC)
              se2 += pow((xr[i]-xt[i]),2.0);
          else
              se3 += pow((xr[i]-xt[i]),2.0);
      }
      se1 = sqrt(se1);
      se2 = sqrt(se2);
      se3 = sqrt(se3);

      fe1 << se1 << "\t" << se2 << "\t" << se3 << endl;

      step = step + 1;

      if(step > 100)
          break;

  }while(se2 > 0.02);


  ft.close();
  fp1.close();
  fp2.close();
  fe1.close();

  cout << "No. of steps = " << step << endl;
  cout << "Final positioning Error = " << se2 << "  m" << endl;
  cout << "Final Orientation Error = " << se3 << "  rad" << endl;
  cout << "Total Error = " << se1 << endl;

  cout << "\nTarget pose =";
  for(int i = 0; i < NW; ++i)
      cout << xt[i] << "\t";
  cout << endl;
  cout << "Actual pose  = ";
  for(int i = 0; i < NW; ++i)
      cout << xr[i] << "\t";
  cout << endl;

  cout << "Final Joint angle values = ";
  for(int i = 0; i < NL; ++i)
      cout << qc[i] << '\t';
  cout << endl;

  delete [] th_c;
  delete [] th_cn;
  delete [] qc;
  delete [] qcn;

  //------------------------------------------------------
  // INVERT_RBF: RBFN3: NL=7, NW = 6, NC = 3
  //--------------------------------------------------------
#elif defined RBFN3

  // Read the parameter files into the networks
  if(exist("ksom_sc_weights.txt") &&
          exist("ksom_sc_angles.txt") &&
          exist("rbfn_par_bp3.txt"))
  {
      A.load_weights("ksom_sc_weights.txt");
      A.load_angles("ksom_sc_angles.txt");
      B.load_parameters("rbfn_par_bp3.txt");
  }
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  //----------------------------------------------
  cout << "\nParameters loaded into the networks" << endl;

  double qt[NL], qtn[NL];       // corresponding target joint angle
  double xr[NW], xrn[NW];       // actual robot end-effector position
  double xt[NW], xtn[NW];       // Target end-effector position
  double pos_n[NC], pos_tn[NC], pos_rn[3];  // position only


  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);
  double *qc = myvector(NL);
  double *qcn = myvector(NL);

  //--------------------------
  //Generate a Target point
  if (NW == 6)
      generate_pose_data(xt, qt);
  else
  {
      cerr << "This module is valid only for NW=6. Exiting ..." << endl;
      cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
      exit(-1);
  }

  //normalize the target position
  cart_posn_normalize(xt, xtn);
  angle_normalize(qt, qtn);

  for(int i = 0; i < NC; ++i) // NC=3
      pos_tn[i] = xtn[i];

  //initial robot configuration
  th_c[0] = 0.0;
  th_c[1] = DEG2RAD(50);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;
  th_c[6] = 0.0;


  //current end-effector position obtained from network
  angle_normalize(th_c, th_cn);

  //Initial guess (Lazy arm joint angle computation)
  A.LA_theta_output(xtn, th_cn, 0.1, qcn);
  angle_denormalize(qcn, qc);


  //Initial position of robot end-effector position
  if (NW == 6)
      coolarm_pose_fk(qc, xr);
  else
  {
      cerr << "\n This module is valid only for NW = 6. Exiting ..." << endl;
      cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
      exit(-1);
  }

  //Initial end-effector position obtained from NN
  B.network_output(qcn, pos_n);

  ofstream fe1("error.txt");
  double se1, se2;
  int step = 0;
  do
  {
      // update input angle only for position target
      B.input_jacobian();
      B.update_input_gd(0.01, pos_tn, qcn);

      //new end-effector position
      B.network_output(qcn, pos_n);

      angle_denormalize(qcn, qc);

      if (NW == 6)
          coolarm_pose_fk(qc, xr);
      else
      {
          cerr << "\n Invalid dimension for robot workspace. Exiting ..." << endl;
          cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
          exit(-1);
      }

      cart_posn_normalize(xr, xrn);

      for(int i = 0; i < NC; ++i) // NC=3
          pos_rn[i] = xrn[i];

      //Forward training
      B.backprop(pos_rn, 0.01, 0.01);

      // Check for only positioning error. The orientation obtain is Good enough
      se1 = 0.0, se2 = 0.0;
      for(int i = 0;i < NW; i++)
      {
          if(i < NC)
              se1 += pow((xr[i] - xt[i]), 2.0);
          else
              se2 += pow((xr[i] - xt[i]),2.0);
      }
      se1 = sqrt(se1);
      se2 = sqrt(se2);
      fe1 << se1 << "\t" << se2 << endl;

      step = step + 1;

      if(step > 2000)
          break;

  }while(se1 > 0.02);
  fe1.close();


delete [] th_c;
delete [] th_cn;
delete [] qc;
delete [] qcn;

  cout << "No. of steps = " << step << endl;
  cout << "Final positioning Error = " << se1 << endl;
  cout << "FInal Orientation Error = " << se2 << endl;

  cout << "\nTarget pose =";
  for(int i = 0; i < NW; ++i)
      cout << xt[i] << "\t";
  cout << endl;
  cout << "Actual pose = ";
  for(int i = 0; i < NW; ++i)
      cout << xr[i] << "\t";
  cout << endl;

  cout << "Final Joint angle values = ";
  for(int i = 0; i < NL; ++i)
      cout << qc[i] << '\t';
  cout << endl;

  //----------------------------------------
  // INVERT_RBF:RBFN4
  //-----------------------------------------

#elif defined RBFN4

  // Read the parameter files into the networks
  if(exist("ksom_sc_weights.txt"))
      A.load_weights("ksom_sc_weights.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  if(exist("ksom_sc_angles.txt"))
      A.load_angles("ksom_sc_angles.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }


  if(exist("rbfn_par_bp4.txt"))
      B.load_parameters("rbfn_par_bp4.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }
  if(exist("rbfn_par_bp41.txt"))
      B2.load_parameters("rbfn_par_bp41.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }
  //----------------------------------------------
  cout << "\nParameters loaded into the networks" << endl;
  cout << "\n\nNetwork inversion ... " << endl;

  ofstream ft("angle.txt");
  ofstream fp1("efposn_network.txt");
  ofstream fp2("efposn_robot.txt");
  ofstream fe1("error.txt");




  double qt[NL], qtn[NL];       // corresponding target joint angle
  double xr[NW], xrn[NW];       // actual robot end-effector position
  double xt[NW], xtn[NW];       // Target end-effector position
  double xc[NW], xcn[NW];       // network (RBFN) output
  double pos_tn[NC], phi_tn[NC]; // normalized TARGET position and orientation
  double pos_n[NC], phi_n[NC];  // normalized NETWORK output - position and orientation
  double pos_rn[NC], phi_rn[NC]; // normalized robot output (orientation)
  double qpn[10];
  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);
  double *qc = myvector(NL);
  double *qcn = myvector(NL);

  //--------------------------
  //Generate a Target point
  if (NW == 6)
      generate_pose_data(xt, qt);
  else
  {
      cerr << "This module is valid only for NW = 6. Exiting ..." << endl;
      cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
      exit(-1);
  }

  //normalize the target position
  cart_posn_normalize(xt, xtn);
  angle_normalize(qt, qtn);

  //target values
  for(int i = 0; i < NW; ++i)
  {
      if(i < NC)
          pos_tn[i] = xtn[i];
      else
          phi_tn[i-NC] = xtn[i];
  }

  //-------------------------------
  ofstream ftp("targetpos.txt");
  // Block 1: target position
  for(int i = 0; i < NW; i++)
      ftp << xt[i] << "\t";
  for(int i = 0; i < NL; i++)
      ftp << qt[i] << "\t";
  ftp << endl ;
  ftp.close();
  //---------------------------------



  //initial robot configuration
  th_c[0] = 0.0;
  th_c[1] = DEG2RAD(50);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;
  th_c[6] = 0.0;

  //current end-effector position obtained from network
  angle_normalize(th_c, th_cn);


  //-------------------------------------------
  // COARSE Inverse Kinematic Solution obtained from KSOM
  //-----------------------------------------

  // Make an initial guess for joint angle vector using KSOM network
  //Initial guess (Lazy arm joint angle computation)
  A.LA_theta_output(xtn, th_cn, 0.5, qcn);

  angle_denormalize(qcn, qc);


  // Fine movement

  double se1, se2, se3;
  int step = 0;
  do
  {

      //Initial end-effector position obtained from NN
      B.network_output(qcn, pos_n);


      // update input angle only for position target
      B.input_jacobian();
      B.update_input_gd(0.01, pos_tn, qcn);

      B2.network_output(qcn, phi_n);


      // update input for orientation target
      B2.input_jacobian();
      B2.update_input_gd(0.02, phi_tn, qcn);


      //new end-effector position
      B.network_output(qcn, pos_n);
      B2.network_output(qcn, phi_n);


      angle_denormalize(qcn, qc);


      // Actual Robot pose
      if(NW == 3)
          coolarm_7dof_FK(qc, xr);
      else if (NW == 6)
          coolarm_pose_fk(qc, xr);
      else
      {
          cerr << "\n Invalid dimension for robot workspace. Exiting ..." << endl;
          cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
          exit(-1);
      }

      cart_posn_normalize(xr, xrn);

      for(int i = 0; i < NW; ++i)
      {
          if(i < NC)
              pos_rn[i] = xrn[i];
          else
              phi_rn[i-NC] = xrn[i];
      }

      //Forward training

      B.backprop(pos_rn, 0.02, 0.02);
      B2.backprop(phi_rn, 0.03, 0.03);


      // Check for only positioning error. The orientation obtain is Good enough
      se1 = 0.0, se2 = 0.0, se3 = 0.0;
      for(int i = 0;i < NW; i++)
      {
          se1 += pow((xr[i] - xt[i]), 2.0);
          if(i < NC)
              se2 += pow((xr[i]-xt[i]),2.0);
          else
              se3 += pow((xr[i]-xt[i]),2.0);
      }
      se1 = sqrt(se1);
      se2 = sqrt(se2);
      se3 = sqrt(se3);

      fe1 << se1 << "\t" << se2 << "\t" << se3 << endl;

    step = step + 1;

    if(step > 1000)
      break;

  }while(se2 > 0.02);


  ft.close();
  fp1.close();
  fp2.close();
  fe1.close();


  cout << "No. of steps = " << step << endl;
  cout << "Positioning Error = " << se2 << " m." <<  endl;
  cout << "Orientation Error = " << se3 << " rad" << endl;

  cout << "\nTarget pose =";
  for(int i = 0; i < NW; ++i)
      cout << xt[i] << "\t";
  cout << endl;
  cout << "Actual pose  = ";
  for(int i = 0; i < NW; ++i)
      cout << xr[i] << "\t";
  cout << endl;

  cout << "Joint Angles = ";
  for(int i = 0; i < NL; ++i)
      cout << qc[i] << "\t";
  cout << endl;

  delete [] th_c;
  delete [] th_cn;
  delete [] qc;
  delete [] qcn;


#else
  cout << "Choose Appropriate RBFN network. Exiting ..." << endl;
  exit(-1);

#endif // RBFN123

  //-----------------------------------

#endif //INVERT module ends here

  //===============================================
  // Inverting multiple points
  //=================================================

#ifdef INVERT_RP

#ifdef RBFN1

  // Load Network Parameters
  if(exist("ksom_sc_weights.txt"))
      A.load_weights("ksom_sc_weights.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  if(exist("ksom_sc_angles.txt"))
      A.load_angles("ksom_sc_angles.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  if(exist("rbfn_par_bp.txt"))
      B.load_parameters("rbfn_par_bp.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  cout << "Parameters loaded for KSOM and RBFN ..." << endl;

  //initial robot configuration
  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);

  th_c[0] = 0.0;
  th_c[1] = DEG2RAD(80);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;

  angle_normalize(th_c, th_cn);

  double *qc = myvector(NL);
  double *qcn = myvector(NL);

  int TMAX = 1000;

  srand(time(NULL));

  double qt[NL], qtn[NL];
  double xr[NW], xrn[NW];
  double xt[NW], xtn[NW];
  double xc[NW], xcn[NW];

  cout << "Network inversion : Random points\n";

  ofstream f1("steps.txt");
  ofstream f2("angles_rp.txt");

  double avg_step = 0.0;
  int rcnt = 0;
  double reject_error = 0.0;
  for(int n = 0; n < TMAX; n++)
  {
      //Generate a random point in the workspace
      if(NW == 3)
          generate_data(xt, qt);
      else if (NW == 6)
          generate_pose_data(xt, qt);
      else
      {
          cerr << "__FILE__: " << "__LINE__:" << "Invalid value for Workspace dimension (use 3 or 6)" << endl;
          exit(-1);
      }

    //normalize the target position
    cart_posn_normalize(xt, xtn);
    angle_normalize(qt, qtn);

    //Initial guess  is obtained from KSOM
    A.LA_theta_output(xtn, th_cn,0.5,qcn);

    angle_denormalize(qcn, qc);

    //current robot end-effector position
    coolarm_7dof_FK(qc, xr);

    //current end-effector position obtained from NN
    B.network_output(qcn, xcn);

    cart_posn_denormalize(xcn, xc);

    double se1;
    int step = 0;
    bool reject = false;
    do
    {
      //update input using gradient_descent
      // qcn is the updated input

      B.input_jacobian();
      B.update_input_gd(0.02, xtn, qcn);

      //new end-effector position
      B.network_output(qcn, xcn);

      cart_posn_denormalize(xcn, xc);
      angle_denormalize(qcn, qc);

      if(NW == 3)
          coolarm_7dof_FK(qc, xr);
      else if (NW == 6)
          coolarm_pose_fk(qc, xr);
      else
      {
          cerr <<"__FILE__:__LINE__:" << "Invalid workspace dimension. Choose 3 or 6" << endl;
          exit(-1);
      }

      cart_posn_normalize(xr, xrn);

      B.backprop(xrn, 0.1, 0.1);

      se1 = 0.0;
      for(int i = 0;i < 3; i++)
        se1 += pow((xr[i] - xt[i]), 2.0);
      se1 = sqrt(se1);

      step = step + 1;

      if(step > 2000)
      {
        reject = true;
        break;
      }

    }while(se1 > 0.02);

    if(reject == false)
      avg_step += step;
    else
    {
      rcnt += 1;
      reject_error += se1;
    }

    f1 << n << "\t" << step << endl;
    cout << n << "\t" << step << endl;

    for(int i = 0; i < NL; i++)
    {
      th_cn[i] = qcn[i];
      f2 << qcn[i] << "\t";
    }
    f2 << endl;

  } //n-loop

  f1.close();
  f2.close();

  cout << "Avg no. of steps = " << avg_step/(TMAX-rcnt) << endl;
  cout << "no. of points reject = " << rcnt << endl;
  cout << "Avg error of rejected points = " << reject_error/rcnt << endl;

  delete [] qc;
  delete [] qcn;

  // -------------------------------------
  // INVERT_RP: RBFN2: TWO RBFN Networks are used
  // ----------------------------------------

#elif defined RBFN2

  // Read the parameter files into the networks
  cout << "\nLoading Network Parameters" << endl;

  if(!exist("ksom_sc_weights.txt") ||
          !exist("ksom_sc_angles.txt") ||
          !exist("rbfn_par_bp.txt") ||
          !exist("rbfn_par_bp2.txt"))
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  //----------------------------------------------
  cout << "Parameters loaded into the networks" << endl;

  cout << "\n\nSolving IK for multiple points  ... " << endl;

  double qt[NL], qtn[NL];       // corresponding target joint angle
  double xr[NW], xrn[NW];       // actual robot end-effector position
  double xt[NW], xtn[NW];       // Target end-effector position
  double pos_tn[NC], phi_tn[NC]; // normalized TARGET position and orientation
  double pos_n[NC], phi_n[NC];  // normalized NETWORK output - position and orientation
  double pos_rn[NC], phi_rn[NC]; // normalized robot output (orientation)
  double qpn[10];
  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);
  double *qc = myvector(NL);
  double *qcn = myvector(NL);
  int TMAX = 100;
  int RUN_MAX = 10;
  int STEP_MAX = 100;

  srand(time(NULL));

  //initial robot configuration
  th_c[0] = 0.0;
  th_c[1] = DEG2RAD(50);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;
  th_c[6] = 0.0;


  angle_normalize(th_c, th_cn);

  cout << "Network inversion : Random points\n";


  double run_avg_pe = 0.0, run_avg_oe = 0.0;
  for(int run = 0; run < RUN_MAX; run++)
  {
      // load parameters
      A.load_angles("ksom_sc_weights.txt");
      A.load_angles("ksom_sc_angles.txt");
      B.load_parameters("rbfn_par_bp.txt");
      B2.load_parameters("rbfn_par_bp2.txt");

      cout << "run = " << run << endl;
      double pos_error = 0.0, ort_error = 0.0;
      for(int n = 0; n < TMAX; n++)
      {

          //--------------------------
          //Generate a Target point
          if (NW == 6)
              generate_pose_data(xt, qt);
          else
          {
              cerr << "Valid only for NW=6. Exiting ..." << endl;
              cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
              exit(-1);
          }

          //normalize the target position
          cart_posn_normalize(xt, xtn);
          angle_normalize(qt, qtn);

          //target values
          for(int i = 0; i < NW; ++i)
          {
              if(i < NC)
                  pos_tn[i] = xtn[i];
              else
                  phi_tn[i-NC] = xtn[i];
          }

          // Coarse movement using KSOM
          // th_cn is same for all iterations

          A.LA_theta_output(xtn, th_cn, 1.0, qcn);  // last value is sigma (std. dev)

          angle_denormalize(qcn, qc);


          //Initial end-effector position obtained from NN
          B.network_output(qcn, pos_n);


          // Fine movement

          double se1, se2, se3;
          for(int step = 0; step < STEP_MAX; ++step)
          {
              // update input angle (qcn) only for position target
              B.input_jacobian();
              B.update_input_gd(0.01, pos_tn, qcn);

              // The updated qcn is now updated by RBFN2 to minimize orientation error
              for(int i = 0; i < NL+NC; ++i)
              {
                  if(i < NL)
                      qpn[i] = qcn[i];
                  else
                      qpn[i] = xtn[i-NL];
              }

              // New orientation obtained with new qpn
              B2.network_output(qpn, phi_n);

              // update input (qpn) so as to reduce orientation error
              B2.input_jacobian();
              B2.update_input_gd2(0.01, phi_tn, qpn, NL);

              // Updated joint angles
              for(int i = 0; i < NL; ++i)
                  qcn[i] = qpn[i];

              // New end-effector pose
              B.network_output(qcn, pos_n);
              B2.network_output(qpn, phi_n);

              angle_denormalize(qcn, qc);

              bool VALID = true;
              // Actual Robot Position attained with these angles
              if (NW == 6)
              {
                  coolarm_pose_fk(qc, xr, VALID);
                  if(VALID == false)
                  {
                      cout << "Invalid values" << endl;
                      exit(-1);
                  }
              }
              else
              {
                  cerr << "\n Valid only for NW = 6. Exiting ..." << endl;
                  cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
                  exit(-1);
              }

              cart_posn_normalize(xr, xrn);

              for(int i = 0; i < NW; ++i)
              {
                  if(i < NC)
                      pos_rn[i] = xrn[i];
                  else
                      phi_rn[i-NC] = xrn[i];
              }


              //Forward training
              B.backprop(pos_rn, 0.01, 0.01);
              B2.backprop(phi_rn, 0.01, 0.01);


              // Check for only positioning error. The orientation obtain is Good enough
              se1 = 0.0, se2 = 0.0, se3 = 0.0;
              for(int i = 0;i < NW; i++)
              {
                  se1 += pow((xr[i] - xt[i]), 2.0);
                  if(i < NC)
                      se2 += pow((xr[i]-xt[i]),2.0);
                  else
                      se3 += pow((xr[i]-xt[i]),2.0);
              }
              se1 = sqrt(se1/NW);
              se2 = sqrt(se2/NC);
              se3 = sqrt(se3/NO);

          } //step-loop

          pos_error += se2;
          ort_error += se3;

      } // n-loop (number of points)

      run_avg_pe += pos_error / TMAX;
      run_avg_oe += ort_error / TMAX;

  }//run-loop

  cout << "Average Positioning Error = " << run_avg_pe / RUN_MAX << endl;
  cout << "Average Orientation Error = " << run_avg_oe / RUN_MAX << endl;



  delete [] th_c;
  delete [] th_cn;
  delete [] qc;
  delete [] qcn;

  //---------------------------
  // INVERT_RP:RBFN3
  //---------------------------
#elif defined RBFN3

  // Read the parameter files into the networks
  cout << "\nLoading Network Parameters" << endl;

  if(!exist("ksom_sc_weights.txt") ||
          !exist("ksom_sc_angles.txt") ||
          !exist("rbfn_par_bp3.txt"))
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }
  //----------------------------------------------
  cout << "Parameters loaded into the networks" << endl;

  cout << "\n\nSolving IK for multiple points  ... " << endl;

  double qt[NL], qtn[NL];       // corresponding target joint angle
  double xr[NW], xrn[NW];       // actual robot end-effector position
  double xt[NW], xtn[NW];       // Target end-effector position
  double pos_tn[NC], phi_tn[NC]; // normalized TARGET position and orientation
  double pos_n[NC], phi_n[NC];  // normalized NETWORK output - position and orientation
  double pos_rn[NC], phi_rn[NC]; // normalized robot output (orientation)
  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);
  double *qc = myvector(NL);
  double *qcn = myvector(NL);
  int TMAX = 100;
  int RUN_MAX = 10;
  int STEP_MAX = 100;

  //initial robot configuration
  th_c[0] = 0.0;
  th_c[1] = DEG2RAD(50);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;
  th_c[6] = 0.0;

  angle_normalize(th_c, th_cn);

  double run_avg_pe = 0.0, run_avg_oe = 0.0;
  for(int run = 0; run < RUN_MAX; run++)
  {
      // load parameters
      A.load_angles("ksom_sc_weights.txt");
      A.load_angles("ksom_sc_angles.txt");
      B.load_parameters("rbfn_par_bp3.txt");


      cout << "run = " << run << endl;
      double pos_error = 0.0, ort_error = 0.0;
      for(int n = 0; n < TMAX; n++)
      {

          //--------------------------
          //Generate a Target point
          if (NW == 6)
              generate_pose_data(xt, qt);
          else
          {
              cerr << "Valid only for NW=6. Exiting ..." << endl;
              cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
              exit(-1);
          }

          //normalize the target position
          cart_posn_normalize(xt, xtn);
          angle_normalize(qt, qtn);

          //target values
          for(int i = 0; i < NW; ++i)
          {
              if(i < NC)
                  pos_tn[i] = xtn[i];
              else
                  phi_tn[i-NC] = xtn[i];
          }

          // Coarse movement using KSOM
          // th_cn is same for all iterations

          A.LA_theta_output(xtn, th_cn, 1.0, qcn);  // last value is sigma (std. dev)

          angle_denormalize(qcn, qc);

          //Initial end-effector position obtained from NN
          B.network_output(qcn, pos_n);

          // Fine movement

          double se1, se2, se3;
          for(int step = 0; step < STEP_MAX; ++step)
          {
              // update input angle (qcn) only for position target
              B.input_jacobian();
              B.update_input_gd(0.01, pos_tn, qcn);

              // New end-effector pose
              B.network_output(qcn, pos_n);

              angle_denormalize(qcn, qc);

              // Actual Robot Position attained with these angles
              if (NW == 6)
                  coolarm_pose_fk(qc, xr);
              else
              {
                  cerr << "\n Valid only for NW = 6. Exiting ..." << endl;
                  cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
                  exit(-1);
              }

              cart_posn_normalize(xr, xrn);

              for(int i = 0; i < NW; ++i)
              {
                  if(i < NC)
                      pos_rn[i] = xrn[i];
                  else
                      phi_rn[i-NC] = xrn[i];
              }

              //Forward training
              B.backprop(pos_rn, 0.01, 0.01);

              // Check for only positioning error. The orientation obtain is Good enough
              se1 = 0.0, se2 = 0.0, se3 = 0.0;
              for(int i = 0;i < NW; i++)
              {
                  se1 += pow((xr[i] - xt[i]), 2.0);
                  if(i < NC)
                      se2 += pow((xr[i]-xt[i]),2.0);
                  else
                      se3 += pow((xr[i]-xt[i]),2.0);
              }
              se1 = sqrt(se1/NW);
              se2 = sqrt(se2/NC);
              se3 = sqrt(se3/NO);

          } //step-loop

          pos_error += se2;
          ort_error += se3;

      } // n-loop (number of points)

      run_avg_pe += pos_error / TMAX;
      run_avg_oe += ort_error / TMAX;

  }//run-loop

  cout << "Average Positioning Error = " << run_avg_pe / RUN_MAX << endl;
  cout << "Average Orientation Error = " << run_avg_oe / RUN_MAX << endl;


  delete [] th_c;
  delete [] th_cn;
  delete [] qc;
  delete [] qcn;


#endif // RBFN123

#endif //INVERT_RP



  return 0;
}

