#ifndef _COOLARM_
#define _COOLARM_

#include <cmath>
#include <fstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

#define DEG2RAD(x) M_PI*x/180.0
#define RAD2DEG(x) 180.0*x/M_PI

#define NL 7   // Dimension of joint space
#define NW 6   // Dimension of workspace ( 3 - for only position, 6 - for pose = position + orientation)


// Range of Configuration space
const double theta_max[7] = {2.6, 1.57, 2.6, 1.57, 1.57, 1.57, 2.9};
const double theta_min[7] = {-2.6, -1.57, -2.6, -1.57, -1.57, -1.57, -2.9};
//double theta_t[NL] =        {0, 1.0, 0, 0.8, -0.0, 0, 0};
// Range of Workspace in Cartesian Space
#if NW == 3
const double c_min[3] = {0, -0.4, 0};
const double c_max[3] = {0.45, 0.4, 0.6};
#elif NW == 6  // how to define the limit for orientation
const double c_min[6] = {0, -0.4, 0, -1.5, -3.14, -3.14};  //position and orientation (pitch-yaw-roll)
const double c_max[6] = {0.45, 0.4, 0.6, 1.5, 3.14, 3.14}; //position and orientation (pitch-yaw-roll)
#endif

// pitch (beta) range: -1.5 to 1.5
// yaw (alpha) range: -3.14 to 3.14
// roll (gamma) range: -3.14 to 3.14

//d-h parameters
const double d1 = 0.1271, d3 = 0.14818, d7 = 0.17721, a4 = 0.074, a5 = 0.07849;  // in meters

inline bool exist(const std::string& name);
void cart_posn_normalize(double x[], double xn[]);
void cart_posn_denormalize(double xn[], double x[]);
void angle_normalize(double theta[], double th_n[]);
void angle_denormalize( double th_n[], double th[]);
void coolarm_7dof_FK(const double Th[], double x[]);
void rotation_matrix(const double Th[], Mat &R);
void coolarm_pose_fk(double Th[], double xp[], bool & VALID);
void generate_data(double Uc[], double Th[]);
void generate_pose_data(double Uc[], double Th[]);
void joint_position(const double theta[7], Mat &X);
void position_jacobian(double Th[], cv::Mat &Jp);
void orientation_jacobian(double Th[], cv::Mat &Jw);
void rotated_axis(const cv::Mat &XP, const double rpy[], const cv::Mat &T, cv::Mat &XP2);
void angular_jacobian(double Th[], cv::Mat &Jw);

void get_euler_angles(cv::Mat &R, double xp[]);

#endif
