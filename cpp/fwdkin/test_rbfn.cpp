#include <iostream>
#include <opencv2/opencv.hpp>
#include <coolarm_fk.h>
#include <nnet.h>
#include <ksom_vmc.h>
#include <mymatrix.h>



// DOn't change this value.
const int NC = 3;   // Dimension of physical workspace - only position
const int NO = 3;   // Size of Orientation vector


using namespace std;
using namespace cv;
using namespace nnet;
using namespace vmc;



// Step - 1: Create clusters in Input-Output Space
#define CLUSTER

// Step - 2: Train a RBF network to learn the forward kinematics map, f : th --> [x, phi]

//#define RBFN4
//#define TRAIN_RBF

// Not a step ... Just check if the training is proper
//#define RBF_TEST_RP

// Step - 3: Invert the Network to find IK solution
//#define INVERT_RBF

//#define INVERT_RP



// Test IK for multiple random points
//#define INVERT_RP


//*******************
// MAIN Function
//*******************

int main()
{
    //Create clusters in input (End-effector position) and output (joint angles) space
    const int nx = 7;
    const int ny = 7;
    const int nz = 7;
    //----------------------------------------
    //Create a KSOM Network
    int N[5] = {nx, ny, nz, NW, NL};

    int ncmax = 100;

    //Create a SOM lattice
    ksom3_sc A(N, ncmax);

    A.netinfo();
    //----------------------


#ifdef CLUSTER

    int node_cnt = nx * ny * nz;
    // create clusters in input and output space using KSOM


  int NMAX = 500000;
  double eta1_init = 0.9, eta1_fin = 0.1;
  double sig_init = 0.9, sig_fin = 0.1;

  ofstream fin1("position.txt"); // normalized position
  ofstream fin2("angle.txt"); // normalized angle
  ofstream fin3("dposition.txt"); // actual data
  ofstream fin4("dangle.txt"); // actual angle
  ofstream fin5("subclust.txt"); // Number of subclusters

  cout << "Clustering starts here ... wait!" << endl;

  double eta[3];

  int interval = 100000;
  //Loop starts here ...
  for(int n = 1; n <= NMAX; n++)
  {
      if (n % interval == 0)
          cout << "\n Iteration Step = " << n << "/" << NMAX << endl;

    double xt[NW], theta[NL];
    double xtn[NW], thetan[NL];

    //generate a random target
    if (NW == 3)
        generate_data(xt, theta);
    else if(NW == 6)
        generate_pose_data(xt, theta);


    cart_posn_normalize(xt, xtn);
    angle_normalize(theta, thetan);

    for(int i = 0; i < NW; i++)
    {
      fin1 << xtn[i] << "\t";
      fin3 << xt[i] << "\t";
    }
    for(int i = 0; i < NL; i++)
    {
      fin2 << thetan[i] << "\t";
      fin4 << theta[i] << "\t";
    }
    fin1 << endl;
    fin2 << endl;
    fin3 << endl;
    fin4 << endl;


    //Learning rate
    eta[0] = eta1_init * pow( (eta1_fin / eta1_init),
        (n / (double)NMAX) );

    eta[1] = 0.7;

    // Sigma necessary for weight update
    eta[2] = sig_init * pow( (sig_fin / sig_init),
        (n / (double)NMAX) );

    A.create_clusters(eta, xtn, thetan, 1.0); // threshold for sub-clusters could be changed

  }//n-loop

  //save KSOM network parameters
  A.save_weights("ksom_sc_weights.txt");
  A.save_angles("ksom_sc_angles.txt");

  int *sc_cnt = new int [node_cnt];
  int ret_cnt = A.get_subcluster_cnt(sc_cnt);

  // Save the number of sub-clusters
  for(int i = 0; i < ret_cnt; ++i)
      fin5 << i << "\t" << sc_cnt[i] << endl;


  fin1.close();
  fin2.close();
  fin3.close();
  fin4.close();
  fin5.close();

  delete [] sc_cnt;

  cout << "Clustering ends ... parameters saved!" << endl;
  //-------------------------------------

#endif


  //========================================

#ifdef RBFN4

  // number of centres in both the networks
  int noc1 = 700;
  int noc2 = 700;

  int N2[3] = {NL, noc1, 3}; // input - angle, output - position
  int N3[3] = {NL, noc2, 3}; // input - position+angle, output - orientation
  double af1[2] = {5, 1.0}; // gaussian activation for hidden neurons and linear for output layer neurons
  double af2[2] = {5, 1.0};
  double param[3] = {1, -1, 0}; //input range = -1 to 1

  // one network to learn angle --> position map
  rbfn B(N2, af1, param);

  // one network to learn angle+position --> orientation map
  rbfn B2(N3, af2, param);

  cout << "Network Information " << B << endl;
  cout << "Network Information " << B2 << endl;

#endif

  //=========================================

#ifdef TRAIN_RBF

#ifdef RBFN4

  cout << "\nTraining TWO RBF Networks ...\n";
  cout << "Wait ... Weights and centers are being updated ...\n";

  double q[NL], qn[NL], x[NW], xn[NW];
  double xt[NW], xtn[NW];


  double pos_tn[NC], phi_tn[3]; // normalized target values
  double pos_n[NC], phi_n[3];  // normalized network output

  int NMAX = 200000;

  // It is important to check that NW is at least 3
  if(NW < NC)
  {
      cerr << "NW must be greater than or equal to NC" << endl;
      exit(-1);
  }

  ofstream f1("error2.txt");

  int interval = 100000;
  double savg = 0.0;
  double savg1 = 0.0, savg2 = 0.0; // error in network 1 and 2.
  for(int n = 0; n < NMAX; n++)
  {
      if (n % interval == 0)
          cout << "\nIteration Step = " << n << "/" << NMAX << endl;

      //Generate a data point
      if(NW == 3)
          generate_data(xt, q);
      else if(NW == 6)
          generate_pose_data(xt, q);
      else
      {
          cerr << "Invalid dimension for input space" << endl;
          exit(-1);
      }

      // normalize the data
      angle_normalize(q, qn);
      cart_posn_normalize(xt, xtn);


      for(int i = 0; i < NW; i++)
      {
          if(i < NC)
              pos_tn[i] = xtn[i];
          else
              phi_tn[i-NC] = xtn[i];
      }


      //compute network output
      B.network_output(qn, pos_n);
      B2.network_output(qn, phi_n);

      // Train the network using Back-Propagation algorithm
      B.backprop(pos_tn, 0.1, 0.1);  // train with desired position

      B2.backprop(phi_tn, 0.1, 0.1); // train with desired orientation


      //combine the output of two networks to form a single output: Pose vector
      for(int i = 0; i < NW; ++i)
      {
          if(i < NC)
              xn[i] = pos_n[i];  //position
          else
              xn[i] = phi_n[i-NC];  //orientation
      }

      cart_posn_denormalize(xn, x);


      double s1 = 0.0, s2 = 0.0, s3 = 0.0;
      for(int i = 0; i < NW; i++)
      {
          s1 += pow((xt[i] - x[i]), 2.0);

          if(i < NC)
              s2 += pow((xt[i]-x[i]),2.0); //error in position
          else
              s3 += pow((xt[i]-x[i]),2.0);  // error in orientation
      }
      s1 = sqrt(s1);
      s2 = sqrt(s2);
      s3 = sqrt(s3);
      savg += s1;
      savg1 += s2;
      savg2 += s3;

      f1 << savg/n << "\t" << savg1/n << "\t" << savg2/n << endl;
  }

  f1.close();

  B.save_parameters("rbfn_par_bp4.txt");
  B2.save_parameters("rbfn_par_bp41.txt");

  cout << "\nTraining over ... parameters saved ...\n" << endl;
#endif //RBFN2


#endif //TRAIN_RBF

  //=======================================================

#ifdef INVERT_RBF

#ifdef RBFN4

  // Read the parameter files into the networks
  if(exist("ksom_sc_weights.txt"))
      A.load_weights("ksom_sc_weights.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  if(exist("ksom_sc_angles.txt"))
      A.load_angles("ksom_sc_angles.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }


  if(exist("rbfn_par_bp4.txt"))
      B.load_parameters("rbfn_par_bp4.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }
  if(exist("rbfn_par_bp41.txt"))
      B2.load_parameters("rbfn_par_bp41.txt");
  else
  {
      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }
  //----------------------------------------------
  cout << "\nParameters loaded into the networks" << endl;
  cout << "\n\nNetwork inversion ... " << endl;

  ofstream ft("angle.txt");
  ofstream fp1("efposn_network.txt");
  ofstream fp2("efposn_robot.txt");
  ofstream fe1("error.txt");




  double qt[NL], qtn[NL];       // corresponding target joint angle
  double xr[NW], xrn[NW];       // actual robot end-effector position
  double xt[NW], xtn[NW];       // Target end-effector position
  double xc[NW], xcn[NW];       // network (RBFN) output
  double pos_tn[NC], phi_tn[NC]; // normalized TARGET position and orientation
  double pos_n[NC], phi_n[NC];  // normalized NETWORK output - position and orientation
  double pos_rn[NC], phi_rn[NC]; // normalized robot output (orientation)
  double qpn[10];
  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);
  double *qc = myvector(NL);
  double *qcn = myvector(NL);

  //--------------------------
  //Generate a Target point
  if (NW == 6)
      generate_pose_data(xt, qt);
  else
  {
      cerr << "This module is valid only for NW = 6. Exiting ..." << endl;
      cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
      exit(-1);
  }

  //normalize the target position
  cart_posn_normalize(xt, xtn);
  angle_normalize(qt, qtn);

  //target values
  for(int i = 0; i < NW; ++i)
  {
      if(i < NC)
          pos_tn[i] = xtn[i];
      else
          phi_tn[i-NC] = xtn[i];
  }

  //-------------------------------
  ofstream ftp("targetpos.txt");
  // Block 1: target position
  for(int i = 0; i < NW; i++)
      ftp << xt[i] << "\t";
  for(int i = 0; i < NL; i++)
      ftp << qt[i] << "\t";
  ftp << endl ;
  ftp.close();
  //---------------------------------



  //initial robot configuration
  th_c[0] = 0.0;
  th_c[1] = DEG2RAD(50);
  th_c[2] = 0.0;
  th_c[3] = DEG2RAD(20);
  th_c[4] = 0.0;
  th_c[5] = 0.0;
  th_c[6] = 0.0;

  //current end-effector position obtained from network
  angle_normalize(th_c, th_cn);


  //-------------------------------------------
  // COARSE Inverse Kinematic Solution obtained from KSOM
  //-----------------------------------------

  // Make an initial guess for joint angle vector using KSOM network
  //Initial guess (Lazy arm joint angle computation)
  A.LA_theta_output(xtn, th_cn, 0.5, qcn);

  angle_denormalize(qcn, qc);


  // Fine movement

  double se1, se2, se3;
  int step = 0;
  do
  {

      //Initial end-effector position obtained from NN
      B.network_output(qcn, pos_n);


      // update input angle only for position target
      B.input_jacobian();
      B.update_input_gd(0.01, pos_tn, qcn);

      B2.network_output(qcn, phi_n);


      // update input for orientation target
      B2.input_jacobian();
      B2.update_input_gd(0.02, phi_tn, qcn);


      //new end-effector position
      B.network_output(qcn, pos_n);
      B2.network_output(qcn, phi_n);


//      for(int i = 0; i < NW; ++i)
//      {
//          if(i < NC)
//              xcn[i] = pos_n[i];
//          else
//              xcn[i] = phi_n[i-NC];
//      }

//    cart_posn_denormalize(xcn, xc);

      angle_denormalize(qcn, qc);


      // Actual Robot pose
      if(NW == 3)
          coolarm_7dof_FK(qc, xr);
      else if (NW == 6)
          coolarm_pose_fk(qc, xr);
      else
      {
          cerr << "\n Invalid dimension for robot workspace. Exiting ..." << endl;
          cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
          exit(-1);
      }

      cart_posn_normalize(xr, xrn);

      for(int i = 0; i < NW; ++i)
      {
          if(i < NC)
              pos_rn[i] = xrn[i];
          else
              phi_rn[i-NC] = xrn[i];
      }


      //Forward training

      B.backprop(pos_rn, 0.02, 0.02);
      B2.backprop(phi_rn, 0.03, 0.03);


      // Check for only positioning error. The orientation obtain is Good enough
      se1 = 0.0, se2 = 0.0, se3 = 0.0;
      for(int i = 0;i < NW; i++)
      {
          se1 += pow((xr[i] - xt[i]), 2.0);
          if(i < NC)
              se2 += pow((xr[i]-xt[i]),2.0);
          else
              se3 += pow((xr[i]-xt[i]),2.0);
      }
      se1 = sqrt(se1);
      se2 = sqrt(se2);
      se3 = sqrt(se3);

      fe1 << se1 << "\t" << se2 << "\t" << se3 << endl;

    step = step + 1;

    if(step > 1000)
      break;

  }while(se2 > 0.02);



  ft.close();
  fp1.close();
  fp2.close();
  fe1.close();


  cout << "No. of steps = " << step << endl;
  cout << "Positioning Error = " << se2 << " m." <<  endl;
  cout << "Orientation Error = " << se3 << " rad" << endl;

  cout << "\nTarget pose =";
  for(int i = 0; i < NW; ++i)
      cout << xt[i] << "\t";
  cout << endl;
  cout << "Actual pose  = ";
  for(int i = 0; i < NW; ++i)
      cout << xr[i] << "\t";
  cout << endl;

  cout << "Joint Angles = ";
  for(int i = 0; i < NL; ++i)
      cout << qc[i] << "\t";
  cout << endl;

  delete [] th_c;
  delete [] th_cn;
  delete [] qc;
  delete [] qcn;

#endif


#endif //INVERT_RBF

//============================================

#ifdef INVERT_RP

#ifdef RBFN4

  // Read the parameter files into the networks
  if(!exist("ksom_sc_weights.txt") ||
          !exist("ksom_sc_angles.txt") ||
          !exist("rbfn_par_bp4.txt") ||
          !exist("rbfn_par_bp41.txt"))
  {

      cerr << __FILE__ << ": " << __LINE__<< " : Parameter file not found" << endl;
      exit(-1);
  }

  //----------------------------------------------


  double qt[NL], qtn[NL];       // corresponding target joint angle
  double xr[NW], xrn[NW];       // actual robot end-effector position
  double xt[NW], xtn[NW];       // Target end-effector position
  double pos_tn[NC], phi_tn[NC]; // normalized TARGET position and orientation
  double pos_n[NC], phi_n[NC];  // normalized NETWORK output - position and orientation
  double pos_rn[NC], phi_rn[NC]; // normalized robot output (orientation)
  double *th_c = myvector(NL);
  double *th_cn = myvector(NL);
  double *qc = myvector(NL);
  double *qcn = myvector(NL);
  int TMAX = 100;
  int STEP_MAX = 100;
  int RUN_MAX = 10;

  double run_avg_pe = 0.0, run_avg_oe = 0.0;
  for(int run = 0; run < RUN_MAX; ++run )
  {
      // Load Network Parameters
      A.load_weights("ksom_sc_weights.txt");
      A.load_angles("ksom_sc_angles.txt");
      B.load_parameters("rbfn_par_bp4.txt");
      B2.load_parameters("rbfn_par_bp41.txt");

      cout << "run = " << run << endl;
      double pos_error = 0.0, ort_error = 0.0;
      for(int n = 0; n < TMAX; ++n)
      {


          //--------------------------
          //Generate a Target point
          if (NW == 6)
              generate_pose_data(xt, qt);
          else
          {
              cerr << "This module is valid only for NW = 6. Exiting ..." << endl;
              cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
              exit(-1);
          }

          //normalize the target position
          cart_posn_normalize(xt, xtn);
          angle_normalize(qt, qtn);

          //target values
          for(int i = 0; i < NW; ++i)
          {
              if(i < NC)
                  pos_tn[i] = xtn[i];
              else
                  phi_tn[i-NC] = xtn[i];
          }


          //initial robot configuration
          // For all points, the initial configuration is to be
          // the same
          th_c[0] = 0.0;
          th_c[1] = DEG2RAD(50);
          th_c[2] = 0.0;
          th_c[3] = DEG2RAD(20);
          th_c[4] = 0.0;
          th_c[5] = 0.0;
          th_c[6] = 0.0;

          //current end-effector position obtained from network
          angle_normalize(th_c, th_cn);

          //coarse movement using KSOM
          A.LA_theta_output(xtn, th_cn, 1.0, qcn);

          angle_denormalize(qcn, qc);

          //Initial end-effector position obtained from NN
          B.network_output(qcn, pos_n);


          // Fine movement

          double se1, se2, se3;
          for(int step = 0; step < STEP_MAX; step++)
          {
              // update input angle only for position target
              B.input_jacobian();
              B.update_input_gd(0.01, pos_tn, qcn);

              // compute the current orientation for this configuration
              B2.network_output(qcn, phi_n);

              // update input for orientation target
              B2.input_jacobian();
              B2.update_input_gd(0.01, phi_tn, qcn);


              //new end-effector position
              B.network_output(qcn, pos_n);
              B2.network_output(qcn, phi_n);

              angle_denormalize(qcn, qc);

              //Actual robot end-effector pose
              if (NW == 6)
                  coolarm_pose_fk(qc, xr);
              else
              {
                  cerr << "\n This module is valid only for NW =6. Exiting ..." << endl;
                  cerr << "File:" << __FILE__ << "\t Line Number:" << __LINE__ << endl;
                  exit(-1);
              }

              cart_posn_normalize(xr, xrn);

              for(int i = 0; i < NW; ++i)
              {
                  if(i < NC)
                      pos_rn[i] = xrn[i];
                  else
                      phi_rn[i-NC] = xrn[i];
              }

              //Forward training

              B.backprop(pos_rn, 0.02, 0.02);
              B2.backprop(phi_rn, 0.02, 0.02);

              se1 = 0.0, se2 = 0.0, se3 = 0.0;
              for(int i = 0;i < NW; i++)
              {
                  se1 += pow((xr[i] - xt[i]), 2.0);
                  if(i < NC)
                      se2 += pow((xr[i]-xt[i]),2.0);
                  else
                      se3 += pow((xr[i]-xt[i]),2.0);
              }
              se1 = sqrt(se1/NW);
              se2 = sqrt(se2/NC);
              se3 = sqrt(se3/NO);

          } //step-loop

          pos_error += se2;
          ort_error += se3;

      }// n-loop (number of points)


      run_avg_pe += pos_error / TMAX;
      run_avg_oe += ort_error / TMAX;

  }// run_loop


  cout << "Average Positioning Error = " << run_avg_pe / RUN_MAX << endl;
  cout << "Average Orientation Error = " << run_avg_oe / RUN_MAX << endl;

  delete [] th_c;
  delete [] th_cn;
  delete [] qc;
  delete [] qcn;

#endif // RBFN4

#endif // INVERT_RP

  return 0;
}
// END of MAIN FUnction
