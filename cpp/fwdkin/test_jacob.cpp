/* Solving Inverse Kinematics by using Jacobian Transpose Method
 *
 * Start Date: March 26, 2015; Thursday
 * Last Update: April 1st, 2015: Wednesday
 *
 * V = 0.5 E^T * E;
 * \dot{V} = - E^T * J * \dot{\theta}
 *
 * Select \dot{\theta} = \alpha * J^T * E;  This makes
 *
 * \dot{V} = -alpha * E^T * J * J^T * E = - alpha * || J^T * E || <= 0 (N.S.D)
 *
 * \alpha = ||J^T * E || / || J * J^T * E ||
 *
 * Hence the system is stable and error remains bounded.
 *
 * --------------------------------------------------------- */

#include<iostream>
#include<coolarm_fk.h>
#include<cmath>
#include<fstream>
#include<gnuplot_ci.h>
#include<ros/ros.h>
#include<time.h>
#include<eigen3/Eigen/Core>
#include<eigen3/Eigen/Dense>

using namespace std;
using namespace gnuplot_ci;

int main()

{
    // Desired or Target Pose

    //double theta_t[NL] = {0, DEG2RAD(50), DEG2RAD(10), DEG2RAD(20), 0, DEG2RAD(20), DEG2RAD(40)}; // in degrees
    //double theta_t[NL] = {0, DEG2RAD(50), 0, DEG2RAD(20), 0, DEG2RAD(0), DEG2RAD(0)}; // in degrees
    //double theta_t[NL] = {0, 1.09, 0, 0.45, -1.3, 0, 0}; // in degrees
//    double theta_t[NL] = {0.6, 1.09, 0, 0.45, -1.2, 0, 0}; // in radians
    double theta_t[NL] = {-0.066, 1.2, 0.05, 0.836, -1.13, -0.075, 0.3};

    double pose_t[6];

    cout << "Initial angles: ";
    for(int i = 3; i < 6; i++)
        cout << pose_t[i] << "\t";
    cout << endl;

    bool VALID_FLAG;
    coolarm_pose_fk(theta_t, pose_t, VALID_FLAG);

    cv::Mat matrix(3,3,CV_64F,0.0);
    Eigen::Vector3d v1(0.229, -0.1, -0.8979), v2, v3(0.3921, -0.8979, 0.2);
    v1.norm();
    v2 = v3.cross(v1);
    matrix.at<double>(0,0) = v1(0);
    matrix.at<double>(1,0) = v1(1);
    matrix.at<double>(2,0) = v1(2);

    matrix.at<double>(0,1) = v2(0);
    matrix.at<double>(1,1) = v2(1);
    matrix.at<double>(2,1) = v2(2);

    matrix.at<double>(0,2) = v3(0);
    matrix.at<double>(1,2) = v3(1);
    matrix.at<double>(2,2) = v3(2);

    get_euler_angles(matrix, pose_t);

    cout << "Reassigned angles: ";
    for(int i = 3; i < 6; i++)
        cout << pose_t[i] << "\t";
    cout << endl;

    for(int i = 0; i < NL; i++)
    {
        if(theta_t[i] > theta_max[i])
        {
            std::cout << "Theta target is greater than limit for joint " << i << ": theta_t=" << theta_t[i]
                      << " theta_max=" << theta_max[i];
            return 0;
        }
        else if(theta_t[i] < theta_min[i])
        {
            std::cout << "Theta target is lesser than limit for joint " << i << ": theta_t=" << theta_t[i]
                      << " theta_min=" << theta_min[i];
            return 0;
        }
    }

    std::cout << "ALL THE TARGET JOINTS ARE WITHIN THE LIMITS!!!" << std::endl;

    // Initial values
    double pose[6] = {0.0,0,0,0,0};
    double theta[NL] = {0,DEG2RAD(50),0,0,0,0,0};
    double dt = 0.01;

    cv::Mat Pose_T(6,1, CV_64F, pose_t);
    cv::Mat Pose_C(6,1, CV_64F, pose);
    cv::Mat Jp(3,7,CV_64F, 0.0);
    cv::Mat Jw(3,7,CV_64F, 0.0);
    cv::Mat J(6,7,CV_64F, 0.0);
    cv::Mat Theta_dot(7,1,CV_64F, 0.0);
    cv::Mat Error(6,1,CV_64F, 0.0);
    cv::Mat Jpos(6,3,CV_64F, 0.0);

    // Control Loop
    ofstream f1("error.txt");
    ofstream f3("robot_joints.txt");
    ofstream f4("actual.txt");

    Error = Pose_T - Pose_C;
    double prev_error = sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0);
    int idx = 0;

//    double time = ros::Time::now().toSec();

    time_t now = time(0);
    double wow = time(0);
    for(double t = 0; t < 500; t = t + dt)
    {
        position_jacobian(theta, Jp);
        orientation_jacobian(theta, Jw);

        cv::vconcat(Jp, Jw, J);
        double alpha = cv::norm(J.t()*Error) / cv::norm(J*J.t()*Error);

        Theta_dot = alpha * J.t() * Error;

        double theta_itmd[NL];
        for(int i = 0; i < NL; ++i)
        {
            theta_itmd[i] = theta[i] + dt * Theta_dot.at<double>(i);
//            theta[i] = theta[i] + dt * Theta_dot.at<double>(i);
        }

        coolarm_pose_fk(theta_itmd, pose, VALID_FLAG);

        if(VALID_FLAG)
        {
            for(int i = 0; i < 6; ++i)
                Pose_C.at<double>(i) = pose[i];
        }

        Error = Pose_T - Pose_C;
        double cur_error = sqrt(cv::Mat(Error.t()*Error).at<double>(0,0)/6.0);

        if(cur_error == 0 || (cur_error > prev_error && idx!=0))
        {
            break;
        }
        else
        {
            for(int i = 0; i < NL; i++)
                theta[i] = theta_itmd[i];
            idx++;
            prev_error = cur_error;
        }

        f1 << t << "\t" << cur_error << endl;

        joint_position(theta, Jpos);
        for(int i = 0; i < 6; i++)
        {
            for(int j = 0; j < 3; j++)
                f3 << Jpos.at<double>(i,j) << "\t";
            f3 << endl;
        }
        f3 << endl << endl;

        for(int i = 0; i < 3; i++ )
            f4 << Pose_C.at<double>(i) << "\t";
        f4 << endl;
    }
    f1.close();
    f3.close();
    f4.close();

    now = time(0) - now;
    wow = time(0) - wow;
    cout << "Number of iteration required: " << idx << " Error: " << prev_error << " Time: " << wow <<  endl << endl;

    cout << "pose = ";
    for(int i = 0; i < 6; ++i)
        cout << pose[i] << "\t";
    cout << endl;
    cout << "pose_t = ";
    for(int i = 0; i < 6; ++i)
        cout << pose_t[i] << "\t";
    cout << endl;

    coolarm_pose_fk(theta, pose, VALID_FLAG);
    joint_position(theta, Jpos);

    cv::Mat Jpos2(6,3,CV_64F,0.0);

    joint_position(theta_t, Jpos2);

    ofstream f2("rconfig.txt");
    for(int i = 0; i < 6; ++i)
    {
        for(int j = 0; j < 3; ++j)
            f2 << Jpos.at<double>(i,j) << "\t";
        for(int j = 0; j < 3; ++j)
            f2 << Jpos2.at<double>(i,j) << "\t";
        f2 << endl;
    }
    f2.close();


    // GNUPLOT

    GP_handle G1("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
    G1.gnuplot_cmd("set terminal wxt");
    G1.gnuplot_cmd("set border");
    G1.gnuplot_cmd("set yrange [-0.5:0.5]");
    G1.gnuplot_cmd("set xrange [-0.5:0.5]");
    G1.gnuplot_cmd("set zrange [-0.0:0.5]");

    G1.gnuplot_cmd("splot 'rconfig.txt' u 1:2:3 w lp pt 4 lt -1 lw 2 t 'actual'");//, '' u 4:5:6 w lp pt 4 lt 9 lw 2 t 'desired'");

//    GP_handle G2("/usr/bin/", "X (m)", "Y (m)", "Z (m)");
//    G2.gnuplot_cmd("set terminal wxt");
//    G2.gnuplot_cmd("set border 4095");
//    G2.gnuplot_cmd("set ticslevel 0");
//    G2.gnuplot_cmd("splot 'robot_joints.txt' u 1:2:3 w lp lw 2 t 'Robot Configuration', 'actual.txt' u 1:2:3 w lp lt 3 t 'End-effector trajectory'");

    GP_handle G3("/usr/bin/", "X (m)", "Y (m)");
    G3.gnuplot_cmd("plot 'error.txt' u 1:2 w d t 'Error'");

    double xorg[3] = {0,0,0};
    double xb[3][3] = {{0.1,0,0}, {0,0.1,0}, {0,0,0.1}};
    //double xb2[3][3] = {{0.2,0,0}, {0,0.4,0}, {0,0,0.2}};

    G1.draw3dcoordAxis(xorg, xb[0], xb[1], xb[2],true);

    double dx[3] = {0.1, 0.1, 0.1};
    double xend[3][3] = {{pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]},
                         {pose[0], pose[1], pose[2]}};

    double xend2[3][3] = {{pose_t[0], pose_t[1], pose_t[2]},
                         {pose_t[0], pose_t[1], pose_t[2]},
                         {pose_t[0], pose_t[1], pose_t[2]}};


    double xeb[3][3], xeb2[3][3];

    //G1.draw3dcoordAxis(xend[0], dx,true,1);

    cv::Mat R(3,3,CV_64F,0.0);
    cv::Mat R2(3,3,CV_64F,0.0);
    cv::Mat T(3,3,CV_64F, &xend);
    cv::Mat T2(3,3,CV_64F, &xend2);
    cv::Mat baseC(3,3,CV_64F, &xb);
    cv::Mat baseC2(3,3,CV_64F, &xb);
    cv::Mat endC(3,3,CV_64F, 0.0);
    cv::Mat endC2(3,3,CV_64F, 0.0);

    rotation_matrix(theta, R);
    rotation_matrix(theta_t, R2);

    // Rotation matrix
    cout << R2 << std::endl;

    // output is a column matrix
    endC = R * baseC.t() + T.t(); // check the transpose
    endC2 = R2 * baseC2.t() + T2.t(); // check the transpose

    cv::Mat vectors(3,3,CV_64F,0.0);
    vectors = endC2-T2.t();
//    cout << "Vectors:\n" << vectors << endl;

    // make it a row matrix
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
        {
            xeb[i][j] = endC.at<double>(j,i); // make it a row matrix
            xeb2[i][j] = endC2.at<double>(j,i); // make it a row matrix
            cout << xeb[i][j] << ":" << xeb2[i][j] << "\t";
        }
        cout << endl;
    }


    G1.draw3dcoordAxis(xend[0], xeb[0], xeb[1], xeb[2], true, 1);
    G1.draw3dcoordAxis(xend2[0], xeb2[0], xeb2[1], xeb2[2], true, 2);

    getchar();

    G1.gnuplot_cmd("set terminal pslatex color solid lw 2");
    G1.gnuplot_cmd("set output 'pose_config.tex' ");
    G1.gnuplot_cmd("replot");
    G1.gnuplot_cmd("set output");

//    cv::Mat V(3,3,CV_64F,0.0);
//    cv::Mat Rx(3,3,CV_64F,0.0);
//    cv::Mat Ry(3,3,CV_64F,0.0);
//    cv::Mat Rz(3,3,CV_64F,0.0);

//    Rx.at<double>(0,0) = 1;
//    Rx.at<double>(1,1) = cos(pose_t[5]);
//    Rx.at<double>(1,2) = sin(pose_t[5]);
//    Rx.at<double>(2,1) = -sin(pose_t[5]);
//    Rx.at<double>(2,2) = cos(pose_t[5]);

//    Ry.at<double>(0,0) = cos(pose_t[3]);
//    Ry.at<double>(0,2) = -sin(pose_t[3]);
//    Ry.at<double>(1,1) = 1;
//    Ry.at<double>(2,0) = sin(pose_t[3]);
//    Ry.at<double>(2,2) = cos(pose_t[3]);

//    Rz.at<double>(0,0) = cos(pose_t[4]);
//    Rz.at<double>(0,1) = sin(pose_t[4]);
//    Rz.at<double>(1,0) = -sin(pose_t[4]);
//    Rz.at<double>(1,1) = cos(pose_t[4]);
//    Rz.at<double>(2,2) = 1;

//    V = endC2-T2.t();

//    cout << "Vectors:" << endl;
//    cout << V << endl;
//    cv::Mat diag_mat(3,3,CV_64F,0.0);

//    diag_mat = Rz*Ry*Rx*V;
//    cout << "Diag_matrix:" <<endl;
//    cout << diag_mat << endl;

    return 0;

}
