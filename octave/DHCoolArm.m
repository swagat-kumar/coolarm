function [] = DHCoolArm()

% Asimov's derived DH parameters for Cool Arm 1000 standard
% L1 = Link('d', 127.1, 'a', 0, 'alpha', pi/2);
% L2 = Link('d', 0, 'a', 0, 'alpha', -pi/2);
% L3 = Link('d', 148.18, 'a', 0, 'alpha', pi/2);
% L4 = Link('d', 0, 'a', 0, 'alpha', -pi/2);
% L5 = Link('d', 0, 'a', 74, 'alpha', pi/2);
% L6 = Link('d', 0, 'a', 0, 'alpha', -pi/2);
% L7 = Link('d', 249.7, 'a', 0, 'alpha', pi/2);

 L1 = Link('d', 127.1, 'a', 0, 'alpha', 0,'modified');
 L2 = Link('d', 0, 'a', 0, 'alpha', -pi/2,'modified');
 L3 = Link('d', 148.18, 'a', 0, 'alpha', pi/2,'modified');
 L4 = Link('d', 0, 'a', 0, 'alpha', -pi/2,'modified');
 L5 = Link('d', 0, 'a', 74, 'alpha', pi/2,'modified');
 L6 = Link('d', 0, 'a', 78.49, 'alpha', -pi/2,'modified');
 L7 = Link('d', 177.21, 'a', 0, 'alpha', pi/2,'modified');

coolRobot = SerialLink([L1 L2 L3 L4 L5 L6 L7], 'name', 'cool robot');
coolRobot.fkine([0.0 0.0 0.0 -1.57 0.0 1.57 0.0])
coolRobot.plot([0.0 0.0 0.0 -1.57 0.0 1.57 0.0])
end
