function [T_70] = ForwardKinematics(t1,t2,t3,t4,t5,t6,t7)

%General Coefficients
syms        ai_1  di   ti    alpi_1;
%Special Coefficients
syms        d1    d2   d3    d4    d5    d6    d7;
syms        a0    a1   a2    a3    a4    a6    a7;
syms        alp0  alp1 alp2  alp3  alp4  alp6  alp7;

mypiDIV2 = deg2rad(90);

% Link Length values in the Arm
a0=0;
a1=0;
a2=0;
a3=0;
a4=74;
a5=78.49;
a6=0;

% Link Twist angle between the links of the Arm
alp0=0;
alp1= -mypiDIV2;
alp2= mypiDIV2;
alp3= -mypiDIV2;
alp4= mypiDIV2;
alp5= -mypiDIV2;
alp6= mypiDIV2;

%Link Offset values in the Arm
d1=127.1;
d2=0;
d3=148.18;
d4=0;
d5=0;
d6=0;
d7=177.21;

% Joint Angles of the links in the Arm
t1 = deg2rad(t1);
t2 = deg2rad(t2);
t3 = deg2rad(t3);
t4 = deg2rad(t4);
t5 = deg2rad(t5);
t6 = deg2rad(t6);
t7 = deg2rad(t7);

%General Homogeneous Transform from DH parameters
R_X = [1          0      0   0  ;   0     cos(alpi_1) -sin(alpi_1) 0; 0   sin(alpi_1) cos(alpi_1) 0 ; 0 0 0 1];
D_X = [1          0      0  ai_1;   0       1               0      0; 0        0          1       0 ; 0 0 0 1];
R_Z = [cos(ti) -sin(ti)  0   0  ; sin(ti) cos(ti)           0      0; 0        0          1       0 ; 0 0 0 1];
D_Z = [1          0      0   0  ;   0       1               0      0; 0        0          1       di; 0 0 0 1];

T_gen = R_X*D_X*R_Z*D_Z;

T_10f=subs(T_gen,{ti,alpi_1,ai_1,di},{t1,alp0,a0,d1});
T_21f=subs(T_gen,{ti,alpi_1,ai_1,di},{t2,alp1,a1,d2});
T_32f=subs(T_gen,{ti,alpi_1,ai_1,di},{t3,alp2,a2,d3});
T_43f=subs(T_gen,{ti,alpi_1,ai_1,di},{t4,alp3,a3,d4});
T_54f=subs(T_gen,{ti,alpi_1,ai_1,di},{t5,alp4,a4,d5});
T_65f=subs(T_gen,{ti,alpi_1,ai_1,di},{t6,alp5,a5,d6});
T_76f=subs(T_gen,{ti,alpi_1,ai_1,di},{t7,alp6,a6,d7});

T_70 = T_10f * T_21f * T_32f * T_43f * T_54f * T_65f * T_76f;
 
end
