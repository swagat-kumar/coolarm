
//Understanding Roll-Pitch-Yaw (RPY) Angles
clear;
clc;
//--------------------------
function y=DEG2RAD(x)
    y = %pi*x/180.0;
endfunction

function x = RAD2DEG(y)
    x = 180*y/%pi;
endfunction

//-------------------------
Alpha = 45; // Yaw in Degrees
 Beta = 45; // Pitch in Degrees
Gamma = 45; // Roll in Degrees
t = [1,1,1; ...
        1, 1, 1; ...
        1, 1, 1]; // translation in meters

// Rotation about z axis
RZ = [cos(DEG2RAD(Alpha)), -sin(DEG2RAD(Alpha)), 0; ...
                sin(DEG2RAD(Alpha)), cos(DEG2RAD(Alpha)), 0; ...
                0, 0, 1
                ];
                

// Rotation about y axis
RY = [cos(DEG2RAD(Beta)), 0, sin(DEG2RAD(Beta)); ...
            0, 1, 0;
            -sin(DEG2RAD(Beta)), 0, cos(DEG2RAD(Beta))
            ];
            
            
 
// Rotataion about X axis
RX = [1, 0, 0;  ...
                0, cos(DEG2RAD(Gamma)), -sin(DEG2RAD(Gamma));
                0, sin(DEG2RAD(Gamma)), cos(DEG2RAD(Gamma))
                ];

xp1 = [0.2, 0 , 0; ...
        0, 0.2, 0; ...
        0, 0, 0.2];

xp2 = RZ * RY * RX * xp1 + t;


