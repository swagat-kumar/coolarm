//Forward Kinectics of Cool1000 Robot ARM
clear;
clc;

d1 = [127.1, 0, 148.18, 0, 0, 0, 177.21]; // in mm
//d1 = [127.1, 0, 148.18, 74, 78.49, 0, 177.21]; // in mm
d = d1./1000; // in m


alpha1 = [0, -90, 90, -90, 90, 90, -90]; // in Degrees
alpha = alpha1.* %pi/180; // in radian

a1 = [0, 0, 0, 0, 74, 78.49, 0]; // in mm
//a1 = [0,0,0,0,0,0,0];
a = a1./1000; // in m

//joint angles
offset = %pi/2.0;
theta = [0, %pi/2, 0, %pi/2-offset, 0, 0-offset 0]; // in radian


// Transformation matrices
T = zeros(4,4,7);
P = zeros(6,3);

for i = 1:7
    T(:,:,i)  = [   cos(theta(i)), -sin(theta(i)), 0, a(i); ...
                    sin(theta(i))*cos(alpha(i)), cos(theta(i))*cos(alpha(i)), ...
                    -sin(alpha(i)), -sin(alpha(i))*d(i); ...
                    sin(theta(i))*sin(alpha(i)), cos(theta(i))*sin(alpha(i)), ...
                    cos(alpha(i)), cos(alpha(i))*d(i); ...
                    0, 0, 0, 1 ...
                    ]; 
end

//Transformation matrices
TF1 = zeros(4,4);
TF1 = T(:,:,1)*T(:,:,2);
P(2,:) = TF1(1:3,4)';


TF2 = zeros(4,4);
TF2 = TF1 * T(:,:,3) * T(:,:,4);
P(3,:) = TF2(1:3,4)';



TF3 = zeros(4,4);
TF3 = TF2 * T(:,:,5);
P(4,:) = TF3(1:3,4)';



TF4 = zeros(4,4);
TF4 = TF3 * T(:,:,6);
P(5,:) = TF4(1:3,4)';


TF5 = zeros(4,4);
TF5 = TF4 * T(:,:,7);
P(6,:) = TF5(1:3,4)';


fprintfMat('position.txt', P, '%10.2f');

//Final Rotation matrix
R = TF5(1:3,1:3);

// gamma - roll: rotatation about x-axis
// beta - pitch: rotation about y-axis
// alpha - yaw: rotation about z-axis



Beta = atan(-R(3,1), sqrt(R(1,1)^2 + R(2,1)^2));
Alpha = atan(R(2,1)/cos(Beta), R(1,1)/cos(Beta));
Gamma = atan(R(3,2)/cos(Beta), R(3,3)/cos(Beta));

