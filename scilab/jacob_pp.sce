// Testing Jacobian as suggested by Prem
// Date: March 23, 2015; Monday
clc;
clear;
clearglobal;



exec('jacob_fk.sci');

theta = zeros(7,1);


theta_d = [0; 50; 0; 20; 0 ; 0; 0 ];  // angles in degrees
theta_t = theta_d .* %pi/180; // angles in radian

pose_d = coolarm_pose_fk(theta_t);

//pose = pose_d + rand(pose_d, "normal");
pose = pose_d;
pose(4) = pose_d(4) + 0.1;


theta = theta_t + rand(theta_t, "normal");

//pose = zeros(6,1);
posn = zeros(3,1);

dt = 0.001;

Jv = zeros(3,7);
Jw = zeros(3,7);
J = zeros(6,7);

f1 = mopen('./error.txt', 'w');
f2 = mopen('./angle.txt', 'w');
for t = 0:dt:10;

    E = pose_d - pose; 
    
    
    Jv = position_jacobian(theta); 
    Jw = angle_jacobian(theta);
    J = [Jv; Jw]; 
    
    d_theta =  J'*E;
    
    theta = theta +  d_theta .* dt;
    
    for i = 1:7
        if (theta(i) > theta_max(i)) then
            theta(i) = theta_max(i);
        elseif (theta(i) < theta_min(i)) then
            theta(i) = theta_min(i);
        end
    end
    
    pose = coolarm_pose_fk(theta);
   
    se = E'*E;
    
    for i = 1:6
        mfprintf(f2, '%f\t', theta(i));
    end
    mfprintf(f1, '%f\n', se);
    mfprintf(f2, '%f\n',theta(7));
end
mclose(f1);
mclose(f2);
