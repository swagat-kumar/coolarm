unset arrow
unset label
unset key

set style line 1 lt 1 lw 3 # red
set style line 2 lt 2 lw 3 # green
set style line 3 lt 3 lw 3 # blue

set style arrow 1 head filled size screen 0.025,15,45 ls 1
set style arrow 2 head filled size screen 0.025,15,45 ls 2
set style arrow 3 head filled size screen 0.025,15,45 ls 3


# joint 1 frame
set arrow from 0,0,0 to 0.02,0,0 as 1
set arrow from 0,0,0 to 0,0.02,0 as 2
set arrow from 0,0,0 to 0,0,0.1 as 3

# joint 2 frame
#set arrow from 0,0,0.13 to -0.02,0,0.13 as 1
#set arrow from 0,0,0.13 to 0,0.0,0.16 as 2
#set arrow from 0,0,0.13 to 0,0.02,0.13 as 3

set ticslevel 0
splot 'position.txt' u 1:2:3 w l lw 2
set xlabel "X (m)"
set ylabel "Y (m)"
set zlabel "Z (m)"

replot "<(sed -n '2p' position.txt)" u 1:2:3 w p pt 1 ps 2 lt 3
replot "<(sed -n '3p' position.txt)" u 1:2:3 w p pt 2 ps 2 lt 3
replot "<(sed -n '4p' position.txt)" u 1:2:3 w p pt 3 ps 2 lt 3
replot "<(sed -n '5p' position.txt)" u 1:2:3 w p pt 4 ps 2 lt 3
replot "<(sed -n '6p' position.txt)" u 1:2:3 w p pt 5 ps 2 lt 3
                                                    

set label "0" at -0.01,-0.01,0
set label "1,2" at -0.015, -0.015, 0.13
set label "3,4" at -0.015, -0.015, 0.28
