// Forward Kinematics - Puma 560
clear;
clc;

// link offset
d1 = [100, 50, 0, 70, 0, 0]; // in mm
d = d1./1000; // in m

alpha1 = [0, -90, 0, -90, 90, -90]; // in degrees
alpha = alpha1 .* %pi / 180.0; // in radians

// link length
a1 = [0,0, 120, 0, 0, 0]; // in mm
a = a1./1000; // in m

// joint angles
theta = [0,0,0,0,0,0]; // in radian


// Transformation matrices
T = zeros(4,4,6);
P = zeros(5,3);


for i = 1:6
    T(:,:,i)  = [   cos(theta(i)), -sin(theta(i)), 0, a(i); ...
                    sin(theta(i))*cos(alpha(i)), cos(theta(i))*cos(alpha(i)), ...
                    -sin(alpha(i)), -sin(alpha(i))*d(i); ...
                    sin(theta(i))*sin(alpha(i)), cos(theta(i))*sin(alpha(i)), ...
                    cos(alpha(i)), cos(alpha(i))*d(i); ...
                    0, 0, 0, 1 ...
                    ]; 
end

//Transformation matrices
TF1 = zeros(4,4);
TF1 = T(:,:,1);
P(2,:) = TF1(1:3,4)';


TF2 = zeros(4,4);
TF2 = TF1 * T(:,:,2);
P(3,:) = TF2(1:3,4)';


TF3 = zeros(4,4);
TF3 = TF2 * T(:,:,3);
P(4,:) = TF3(1:3,4)';


TF4 = zeros(4,4);
TF4 = TF3 * T(:,:,4) * T(:,:,5)*T(:,:,6);
P(5,:) = TF4(1:3,4)';



fprintfMat('position.txt', P, '%10.2f');

